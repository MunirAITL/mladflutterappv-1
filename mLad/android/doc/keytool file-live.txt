Store Pass
M**&&%%ah@ma@n640&%%02**!!

Keystore MM
UKM@gic%%ah@MM@@n640&%%02**!!

Alias
Mortgage_Magic


mlad_v1.0_130222.apk
=================================================================================
create keystore file
================================================================================================================
keytool -genkey -v -keystore mm.keystore -alias Mortgage_Magic -keyalg RSA -keysize 2048 -validity 10000

create jks file
=================================================================================================================

keytool -genkey -v -keystore mm.jks -storetype JKS -keyalg RSA -keysize 2048 -validity 10000 -alias Mortgage_Magic

The JKS keystore uses a proprietary format. It is recommended to migrate to PKCS12 which is an industry standard format using:
=================================================================================================================
keytool -importkeystore -srckeystore mm.jks -destkeystore mm.jks -deststoretype pkcs12

generate SSH1 Key
=================================================================================================================
keytool -keystore mm.jks -list -v

MD5: 9A:F5:C2:70:0D:C4:A5:DE:5C:FC:AB:C3:99:18:08:4D
SHA1: A5:C8:2A:13:CC:5D:64:40:5E:90:EF:B8:5E:7A:A9:41:B6:E6:FD:CF


========================================
For getting keystore file info
keytool -v -list -keystore mm.keystore
keystore file jks file password = ?
