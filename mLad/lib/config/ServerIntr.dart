import 'Server.dart';

class ServerIntr {
  //  Case Lead Dashboard
  static const String CASELEAD_DB_INTR1_GET_URL = Server.BASE_URL +
      "/api/mortgagecasepaymentinfo/getuserpaymentsummaryreport?UserId=#userId#";

  static const String CASELEAD_DB_INT2_GET_URL = Server.BASE_URL +
      "/api/casereport/get/getleadtatusintroducerwisereportdatabyintroducer?UserCompanyId=#companyId#&Criteria=#criteria#&Status=#status#&IsSpecificDate=#isSpecificDate#&FromDateTime=#fromDateTime#&ToDateTime=#toDateTime#&AdviserId=#advisorId#&IntroducerId=#introducerId#&Title=#title#";

  //  Case Lead Navigator
  static const String CASELEAD_NAVIGATOR_GET_URL = Server.BASE_URL +
      "/api/users/get/negotiatoruserbyintroducerid?IntroducerId=#introducerId#";
}
