import 'package:aitl/Mixin.dart';
import 'package:flutter/material.dart';

class MyTheme {
  static final bool isBoxDeco = false;
  static final double txtSize = 2;
  static final String fontFamily = "Roboto";
  static final double appbarElevation = .5;
  static final double txtLineSpace = 1.3;

  //static final Color bgGrayColor = HexColor.fromHex("#F4F7FB");
  static final bgBlueColor = BoxDecoration(
      borderRadius: new BorderRadius.circular(10),
      gradient: LinearGradient(
          colors: [
            HexColor.fromHex("#2B4564"),
            HexColor.fromHex("#112B4A"),
          ],
          begin: const FractionalOffset(0.0, 0.0),
          end: const FractionalOffset(0.0, 0.0),
          stops: [0.0, 1.0],
          tileMode: TileMode.clamp));

  static final Color bgColor = Color(0xFF61748C);
  static final Color bgColor2 = Color(0xFFf5f9ff);

  static final bgGrayColor = HexColor.fromHex("#F7F6F8");
  static final dBlueAirColor = Color(0xFF252551);

  static final statusBarColor = HexColor.fromHex("#2D4665");

  static final titleColor = Color(0xFF2D4665);
  static final title2Color = Color(0xFF6E6E82);
  static final inputColor = Color(0xFF6E6E82);
  //static final txtColor = HexColor.fromHex("#0D0D0D");
  static final lightTxtColor = HexColor.fromHex("#E9F7FE");
  static final hintTxtColor = HexColor.fromHex("#E4E6EB");
  static final fbColor = HexColor.fromHex("#4286F5");

  static final Color brandColor = HexColor.fromHex("#C1272D");
  static final lRedColor = HexColor.fromHex("#FA5656");
  static final dRedColor = HexColor.fromHex("#C51733");
  static final Color pinkColor = HexColor.fromHex("#FFA9AC");
  static final Color greyColor = HexColor.fromHex("#F1F1F4");
  static final Color dgreyColor = HexColor.fromHex("#FFA9AC");
  static final lGrayColor = HexColor.fromHex("#DFE2E7");
  static final Color l3BlueColor = HexColor.fromHex("#cad7dc");
  static final Color appbarProgColor = brandColor;

  //statusbar color
  static final Color appbarColor = Colors.grey[300];

  //  widgets color theme
  static final Color switchOffColor = HexColor.fromHex("#F5787D");

  //  pages color theme

  // welcome theme
  static final Color welcomeTitleColor = HexColor.fromHex("#2D2D2D");

  // mycases
  static final Color mycasesNFBtnColor = HexColor.fromHex("#B4B4B4");

  //  timeline
  static final Color timelineTitleColor = HexColor.fromHex("#1F3548");
  static final Color timelinePostCallerNameColor = HexColor.fromHex("#2D2D2D");

  //  more
  static final Color moreTxtColor = HexColor.fromHex("#1F3548");

  //  lead
  static final Color leadSubTitle = HexColor.fromHex("#151522");

  static final radioThemeData = ThemeData(
    unselectedWidgetColor: MyTheme.bgColor,
    disabledColor: MyTheme.bgColor,
    selectedRowColor: MyTheme.bgColor,
    indicatorColor: MyTheme.bgColor,
    toggleableActiveColor: MyTheme.bgColor,
  );

  static final radioTheme = ThemeData(
    unselectedWidgetColor: MyTheme.statusBarColor,
    disabledColor: MyTheme.statusBarColor,
    selectedRowColor: MyTheme.statusBarColor,
    indicatorColor: MyTheme.statusBarColor,
    toggleableActiveColor: MyTheme.statusBarColor,
  );

  static final ThemeData themeData = ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.dark,
    primaryColor: Colors.white,
    accentColor: HexColor.fromHex("#ffffff"),

    //iconTheme: IconThemeData(color: Colors.red),
    //primarySwatch: Colors.grey,
    //primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),
    //visualDensity: VisualDensity.adaptivePlatformDensity,
    //scaffoldBackgroundColor: Colors.pink,

    cardTheme: CardTheme(
      color: Colors.white,
    ),

    //primarySwatch: Colors.white,
    //primaryTextTheme: TextTheme(headline6: TextStyle(color: Colors.white)),

    /*inputDecorationTheme: InputDecorationTheme(
      focusedBorder:
          UnderlineInputBorder(borderSide: BorderSide(color: Colors.red)),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
      border: UnderlineInputBorder(
        borderSide: BorderSide(color: Colors.red),
      ),
    ),*/

    // Define the default font family.
    fontFamily: fontFamily,

    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    /*textTheme: TextTheme(
      headline1: TextStyle(
          fontSize: 72.0, fontWeight: FontWeight.bold, color: Colors.black),
      headline6: TextStyle(
          fontSize: 36.0, fontStyle: FontStyle.italic, color: Colors.black),
      bodyText2: TextStyle(
          fontSize: 14.0, fontFamily: fontFamily, color: Colors.black),
    ),

    buttonTheme: ButtonThemeData(
      buttonColor: Colors.blueAccent,
      shape: RoundedRectangleBorder(),
      textTheme: ButtonTextTheme.accent,
      splashColor: Colors.lime,
    ),*/
  );

  static final boxDeco = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      border: Border(
          left: BorderSide(color: Colors.grey, width: 1),
          right: BorderSide(color: Colors.grey, width: 1),
          top: BorderSide(color: Colors.grey, width: 1),
          bottom: BorderSide(color: Colors.grey, width: 1)),
      color: Colors.transparent);

  static final picEmboseCircleDeco =
      BoxDecoration(color: Colors.grey, shape: BoxShape.circle, boxShadow: [
    BoxShadow(
      color: Colors.grey.shade500,
      blurRadius: 5.0,
      spreadRadius: 2.0,
    ),
  ]);
}
