import 'package:flutter/material.dart';

import '../Server.dart';

class NewCaseCfg {
  static const int ALL = 901;
  static const int IN_PROGRESS = 902;
  static const int SUBMITTED = 903;
  static const int FMA_SUBMITTED = 904;
  static const int COMPLETED = 905;

  static const List<String> listSliderImages = [
    Server.DOMAIN + "/assets/img/slider/1.jpg",
    Server.DOMAIN + "/assets/img/slider/2.jpg",
    Server.DOMAIN + "/assets/img/slider/3.jpg",
  ];

  static final List<Map<String, dynamic>> listCreateNewCase = [
    {
      "index": 0,
      "title": "Residential Mortgage",
      "url": "assets/images/screens/db_cus/new_case/ic_case_1.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": residentialMortgageSubList,
      "bgColor": Color(0xFFDBF9EC),
    },
    {
      "index": 1,
      "title": "Residential Remortgage",
      "url": "assets/images/screens/db_cus/new_case/ic_case_2.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": residentialRemortgageSubList,
      "bgColor": Color(0xFFFFD7E8),
    },
    {
      "index": 2,
      "title": "Second Charge - Residential",
      "url": "assets/images/screens/db_cus/new_case/ic_case_3.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": secondChargeResidentialSubList,
      "bgColor": Color(0xFFDEF7FF),
    },
    {
      "index": 3,
      "title": "Buy to Let Mortgage",
      "url": "assets/images/screens/db_cus/new_case/ic_case_4.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": buyToLetMortgageSubList,
      "bgColor": Color(0xFFE5E7FF),
    },
    {
      "index": 4,
      "title": "Buy to Let Remortgage",
      "url": "assets/images/screens/db_cus/new_case/ic_case_5.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": buyToLetRemortgageSubList,
      "bgColor": Color(0xFFEDEDED),
    },
    {
      "index": 5,
      "title": "Business Lending",
      "url": "assets/images/screens/db_cus/new_case/ic_case_6.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": businessLendingSubList,
      "bgColor": Color(0xFFF6F5C6),
    },
    {
      "index": 6,
      "title": "Second Charge Buy to Let & Commercial",
      "url": "assets/images/screens/db_cus/new_case/ic_case_7.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": secondChargeBuyToLetCommercialSubList,
      "bgColor": Color(0xFFFFEDD8),
    },
    {
      "index": 7,
      "title": "Commercial Mortgages or Loans",
      "url": "assets/images/screens/db_cus/new_case/ic_case_8.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": commercialMortgagesOrLoansSubList,
      "bgColor": Color(0xFFDBF9EC),
    },
    {
      "index": 8,
      "title": "Development Finance",
      "url": "assets/images/screens/db_cus/new_case/ic_case_9.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": developmentFinanceSubList,
      "bgColor": Color(0xFFFFD7E8),
    },
    {
      "index": 9,
      "title": "Let to Buy",
      "url": "assets/images/screens/db_cus/new_case/ic_case_10.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": letToBuySubList,
      "bgColor": Color(0xFFDEF7FF),
    },
    {
      "index": 10,
      "title": "Equity Release",
      "url": "assets/images/screens/db_cus/new_case/ic_case_11.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": equeityReleaseSubList,
      "bgColor": Color(0xFFE5E7FF),
    },
    {
      "index": 11,
      "title": "Bridging Loan",
      "url": "assets/images/screens/db_cus/new_case/ic_case_12.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": bridgingLoanSubList,
      "bgColor": Color(0xFFEDEDED),
    },
    {
      "index": 12,
      "title": "General Insurance",
      "url": "assets/images/screens/db_cus/new_case/ic_case_13.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": generalInsuranceSubList,
      "bgColor": Color(0xFFF6F5C6),
    },
  ];
  //
  static final List<Map<String, dynamic>> generalInsuranceSubList = [
    {
      "index": 0,
      "title": "Buildings & Contents",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Building Only",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Contents Only",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> bridgingLoanSubList = [
    {
      "index": 0,
      "title": "Auction Purchase",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Standard Bridging Loan",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Semi Commercial Bridging Loan",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 3,
      "title": "Commercial Bridging Loan",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 4,
      "title": "Regulated Bridging Loan",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 5,
      "title": "Structured Short Term Finance",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> equeityReleaseSubList = [
    {
      "index": 0,
      "title": "Equity Release",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Home Reversion",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> residentialMortgageSubList = [
    {
      "index": 0,
      "title": "Home Mover",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "First time buyer",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Help to Buy Mortgage",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 3,
      "title": "Right to Buy",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": true,
    },
    {
      "index": 4,
      "title": "Shared Ownership",
      "url": "assets/images/screens/db_cus/new_case/ic_case_5.png",
      "isOtherApplicant": true,
      "isSPV": true,
    },
  ];
  static final List<Map<String, dynamic>> residentialRemortgageSubList = [
    {
      "index": 0,
      "title": "Right to Buy",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Shared Ownership",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Standard Remortgage",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> secondChargeResidentialSubList = [];

  static final List<Map<String, dynamic>> buyToLetMortgageSubList = [
    {
      "index": 0,
      "title": "Buy to Let Mortgage",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "First time landlord",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Consumer buy to Let",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> buyToLetRemortgageSubList = [
    {
      "index": 0,
      "title": "Experienced Landlord",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Consumer buy to Let",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> businessLendingSubList = [];
  static final List<Map<String, dynamic>>
      secondChargeBuyToLetCommercialSubList = [];
  static final List<Map<String, dynamic>> commercialMortgagesOrLoansSubList =
      [];

  static final List<Map<String, dynamic>> developmentFinanceSubList = [
    {
      "index": 0,
      "title": "Full Development Project",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Conversion Project",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Heavy Refurbishment",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 3,
      "title": "Light Refurbishment",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> letToBuySubList = [];

  addOtherTitleInCase() {
    if (listCreateNewCase.length == 9) {
      listCreateNewCase.add({
        "index": 9,
        "title": "Others",
        "url": "assets/images/screens/db_cus/new_case/ic_case_10.png",
        "isOtherApplicant": false,
        "isSPV": false,
        "subItem": "",
      });
    } else {
      listCreateNewCase[9] = {
        "index": 9,
        "title": "Others",
        "url": "assets/images/screens/db_cus/new_case/ic_case_10.png",
        "isOtherApplicant": false,
        "isSPV": false,
        "subItem": "",
      };
    }
  }
}
