import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/MyTheme.dart';

InputBox({
  ctrl,
  context,
  lableTxt,
  kbType,
  inputAction,
  focusNode,
  focusNodeNext,
  len,
  isPwd = false,
  autofocus = false,
  isobscureText = false,
  callback,
}) =>
    TextField(
      controller: ctrl,
      focusNode: focusNode,
      autofocus: autofocus,
      keyboardType: kbType,
      textInputAction: inputAction,
      onEditingComplete: () {
        // Move the focus to the next node explicitly.
        if (focusNode != null) {
          focusNode.unfocus();
        } else {
          FocusScope.of(context).requestFocus(new FocusNode());
        }
        if (focusNodeNext != null) {
          FocusScope.of(context).requestFocus(focusNodeNext);
        } else {
          FocusScope.of(context).requestFocus(new FocusNode());
        }
      },
      inputFormatters: (kbType == TextInputType.phone)
          ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
          : (kbType == TextInputType.emailAddress)
              ? [
                  FilteringTextInputFormatter.allow(RegExp(
                      "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                ]
              : null,
      obscureText: (isobscureText) ? isPwd : false,
      maxLength: len,
      autocorrect: false,
      style: TextStyle(
        color: Colors.black,
        fontSize: 17,
        height: MyTheme.txtLineSpace,
      ),
      decoration: new InputDecoration(
        counterText: '',
        hintText: lableTxt,
        hintStyle: new TextStyle(
          color: Colors.grey,
          fontSize: 17,
          height: MyTheme.txtLineSpace,
        ),
        labelStyle: new TextStyle(
          color: Colors.black,
          fontSize: 17,
          height: MyTheme.txtLineSpace,
        ),
        suffixIcon: (isPwd)
            ? GestureDetector(
                onTap: () {
                  isobscureText = !isobscureText;
                  callback(isobscureText);
                },
                child: new Icon(
                  isobscureText ? Icons.visibility : Icons.visibility_off,
                  color: Colors.grey,
                ),
              )
            : SizedBox(),
        contentPadding: EdgeInsets.only(left: 20, right: 20),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: const BorderRadius.all(
            const Radius.circular(8),
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 1),
          borderRadius: const BorderRadius.all(
            const Radius.circular(8),
          ),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey, width: 1),
          borderRadius: const BorderRadius.all(
            const Radius.circular(8),
          ),
        ),
      ),
    );
