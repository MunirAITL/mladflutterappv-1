import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../txt/Txt.dart';
import 'InputBoxMM4.dart';

class InputTitleBoxMM4 extends StatefulWidget {
  final String title;
  final TextEditingController input;
  final TextInputType kbType;
  final int len;
  final int minLen;
  final int minLine;
  final int maxLine;
  final String ph;
  final bool isPwd;
  final bool autofocus;
  final Function onChange;
  Widget prefixIco;
  Widget suffixIco;
  final String tooltipTxt;
  final bool isReadonly;
  InputTitleBoxMM4({
    @required this.title,
    @required this.input,
    @required this.kbType,
    @required this.len,
    this.minLen = 0,
    this.minLine,
    this.maxLine,
    this.ph,
    this.isPwd = false,
    this.autofocus = false,
    this.prefixIco,
    this.suffixIco,
    this.onChange,
    this.tooltipTxt = '',
    this.isReadonly = false,
  });

  @override
  State createState() => _InputTitleBoHTState();
}

class _InputTitleBoHTState extends State<InputTitleBoxMM4> with Mixin {
  @override
  void initState() {
    super.initState();
    widget.input.addListener(() {
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Txt(
                  txt: widget.title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
              ),
              widget.tooltipTxt.length > 0
                  ? GestureDetector(
                      onTap: () {
                        showToolTips(context: context, txt: widget.tooltipTxt);
                      },
                      child: Icon(
                        Icons.info_outline_rounded,
                        color: Colors.black54,
                        size: 20,
                      ))
                  : SizedBox()
            ],
          ),
          SizedBox(height: 10),
          InputBoxMM4(
            context: context,
            ctrl: widget.input,
            lableTxt: (widget.ph == null) ? widget.title : widget.ph,
            kbType: widget.kbType,
            len: widget.len,
            isPwd: widget.isPwd,
            autofocus: widget.autofocus,
            maxLines: widget.maxLine,
            prefixIco: widget.prefixIco,
            suffixIco: widget.suffixIco,
            onChange: widget.onChange,
            isReadonly: widget.isReadonly,
          ),
        ],
      ),
    );
  }
}
