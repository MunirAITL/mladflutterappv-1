import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
import 'tabItem.dart';
import 'package:badges/badges.dart';

class BottomNavigation extends StatelessWidget with Mixin {
  final BuildContext context;
  final ValueChanged<int> onSelectTab;
  final List<TabItem> tabs;
  final bool isHelpTut;
  final int totalMsg;
  final int totalNoti;
  BottomNavigation({
    @required this.context,
    @required this.onSelectTab,
    @required this.tabs,
    @required this.isHelpTut,
    @required this.totalMsg,
    @required this.totalNoti,
  });

  int curTab = 0;

  @override
  Widget build(BuildContext context) {
    curTab = MainCustomerScreenState.currentTab;
    // if (userData.communityId == 2)
    //   curTab = MainIntroducerScreenState.currentTab;

    return BottomAppBar(
      color: MyTheme.statusBarColor,
      shape: CircularNotchedRectangle(),
      //notchMargin: 4,
      //clipBehavior: Clip.antiAlias,
      child: BottomNavigationBar(
        //selectedLabelStyle: TextStyle(fontSize: 14),
        selectedItemColor: Colors.red,
        currentIndex: curTab,
        //unselectedLabelStyle: TextStyle(fontSize: 14),
        unselectedItemColor: Colors.white,
        backgroundColor: MyTheme.statusBarColor,
        type: BottomNavigationBarType.fixed,
        items: tabs
            .map(
              (e) => _buildItem(
                index: e.getIndex(),
                icon: e.icon,
                tabName: e.tabName,
              ),
            )
            .toList(),
        onTap: (index) => onSelectTab(
          index,
        ),
      ),
    );
  }

  BottomNavigationBarItem _buildItem(
      {int index, AssetImage icon, String tabName}) {
    final topBadgePos = getHP(context, 4);
    final endBadgePos = getHP(context, 2);
    final int totalBadge = (index == 2 && totalMsg > 0)
        ? totalMsg
        : (index == 3 && totalNoti > 0)
            ? totalNoti
            : 0;

    return BottomNavigationBarItem(
      icon: (isHelpTut && curTab == index)
          ? Stack(clipBehavior: Clip.none, children: <Widget>[
              (index == 2 || index == 3)
                  ? Badge(
                      showBadge: (totalBadge > 0) ? true : false,
                      position: BadgePosition.topEnd(
                          top: -topBadgePos, end: -endBadgePos),
                      badgeContent: Text(
                        totalBadge.toString(),
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      child: ImageIcon(
                        icon,
                        color: _tabColor(index: index),
                      ),
                    )
                  : ImageIcon(
                      icon,
                      color: _tabColor(index: index),
                    ),
              new Positioned(
                top: -getHP(context, 9),
                right: -10,
                child: Image.asset(
                  "assets/images/icons/help_hand3.png",
                  width: 60,
                  height: 70,
                ),
              )
            ])
          : Badge(
              showBadge: (totalBadge > 0) ? true : false,
              position:
                  BadgePosition.topEnd(top: -topBadgePos, end: -endBadgePos),
              badgeContent: Text(
                totalBadge.toString(),
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              child: ImageIcon(
                icon,
                color: _tabColor(index: index),
              ),
            ),
      // ignore: deprecated_member_use
      title: Txt(
          txt: tabName,
          txtColor: _tabColor(index: index),
          txtSize: MyTheme.txtSize - .9,
          txtAlign: TextAlign.center,
          isBold: (curTab == index) ? true : false),
    );
  }

  Color _tabColor({int index}) {
    return curTab == index ? Colors.red : Colors.white;
  }
}
