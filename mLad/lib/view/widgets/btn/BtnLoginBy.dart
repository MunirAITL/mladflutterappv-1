import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'BtnIcon.dart';

class BtnLoginBy extends StatelessWidget with Mixin {
  final Function callbackLoginMobile;
  final Function callbackFBLogin;
  final Function callbackGLogin;

  const BtnLoginBy(
      {Key key,
      @required this.callbackLoginMobile,
      @required this.callbackFBLogin,
      @required this.callbackGLogin})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Card(
        margin: const EdgeInsets.only(bottom: 5),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(50.0),
            topRight: Radius.circular(50.0),
          ),
        ),
        child: BtnIcon(
          txt: "Sign in",
          txtColor: Colors.black,
          bgColor: Colors.white,
          width: getW(context),
          height: getHP(context, 6),
          icon: null,
          image: Image(
            image: AssetImage(
              "assets/images/icons/g_icon.png",
            ),
            color: null,
            width: 30,
            height: 30,
          ),
          isRightIco: false,
          isCurve: false,
          callback: () => callbackGLogin(),
        ),
      ),
    );
  }
}
