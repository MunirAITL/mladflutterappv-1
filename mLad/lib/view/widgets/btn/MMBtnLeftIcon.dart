import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class MMBtnLeftIcon extends StatelessWidget with Mixin {
  final String txt;
  final String imageFile;
  Color bgColor;
  final double width;
  final double height;
  final double radius;
  final Function callback;

  MMBtnLeftIcon({
    Key key,
    @required this.txt,
    @required this.imageFile,
    @required this.width,
    @required this.height,
    this.radius = 10,
    @required this.callback,
    this.bgColor,
  }) {
    if (bgColor == null) bgColor = MyTheme.brandColor;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        callback();
      },
      child: Container(
        width: width,
        height: (height != null) ? height : getHP(context, 6),
        alignment: Alignment.center,
        //color: MyTheme.brownColor,
        decoration: new BoxDecoration(
          color: bgColor,
          borderRadius: new BorderRadius.circular(radius),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              imageFile,
              fit: BoxFit.fitWidth,
            ),
            SizedBox(
              width: 10,
            ),
            Txt(
              txt: txt,
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
          ],
        ),
      ),
    );
  }
}
