import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

AlertDialogCustom(
        {BuildContext context,
        Function onConfirmClick,
        Function onCancelClick}) =>
    AwesomeDialog(
        dialogBackgroundColor: Colors.white,
        context: context,
        animType: AnimType.SCALE,
        dialogType: DialogType.NO_HEADER,
        //showCloseIcon: true,
        //buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
        //customHeader: Icon(Icons.info, size: 50),
        body: Center(
          child: Stack(
              overflow: Overflow.visible,
              alignment: Alignment.center,
              children: [
                SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Txt(
                            txt:
                                "Are you sure want to submit your application?",
                            txtColor: Colors.black,
                            txtSize: 2,
                            txtAlign: TextAlign.center,
                            isBold: true),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Txt(
                            txt:
                                """Once the application is submitted our advisers will begin to process the application. The information you have provided will be checked for verification. We may also conduct credit searches and other due-diligence as a part of processing your application and you are hereby giving us explicit permission to do so.""",
                            txtColor: Colors.black38,
                            txtAlign: TextAlign.justify,
                            txtSize: 1.5,
                            isBold: true),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Txt(
                            txt:
                                """By submitting this application you are confirming that you have read the Terms and Conditions and you declare that the information provided in this fact-find is true and accurate.""",
                            txtColor: Colors.black38,
                            txtAlign: TextAlign.justify,
                            txtSize: 1.5,
                            isBold: true),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Txt(
                            txt:
                                "Once submitted you can no longer edit this application.",
                            txtColor: Colors.red,
                            txtSize: 2,
                            txtAlign: TextAlign.left,
                            isBold: false),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: -50,
                  child: Container(
                      //width: 50.0,
                      //height: 50.0,
                      padding: const EdgeInsets.all(
                          10), //I used some padding without fixed width and height
                      decoration: new BoxDecoration(
                        shape: BoxShape
                            .circle, // You can use like this way or like the below line
                        //borderRadius: new BorderRadius.circular(30.0),
                        color: Colors.white,
                      ),
                      child: Icon(Icons.info,
                          color: MyTheme.dBlueAirColor, size: 40)),
                )
              ]),
        ),
        //title: 'This is Ignored',
        //desc: 'This is also Ignored',
        btnOkText: "Confirm",
        btnOkColor: MyTheme.titleColor,
        btnOkOnPress: () {
          //Navigator.pop(context);
          //callback(email.text.trim());
          onConfirmClick();
        },
        btnCancelText: "Cancel",
        btnCancelColor: Colors.grey,
        btnCancelOnPress: () {
          if (onCancelClick != null) onCancelClick();
        })
      ..show();
