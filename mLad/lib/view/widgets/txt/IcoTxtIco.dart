import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class IcoTxtIco extends StatelessWidget with Mixin {
  final IconData leftIcon;
  final IconData rightIcon;
  final txt;
  final double txtSize;
  TextAlign txtAlign;
  double leftIconSize;
  double rightIconSize;
  double height;
  Color iconColor;
  Color txtColor;

  IcoTxtIco(
      {Key key,
      @required this.txt,
      @required this.leftIcon,
      @required this.rightIcon,
      this.txtSize = 2,
      this.iconColor = Colors.grey,
      this.txtColor = Colors.black87,
      this.txtAlign = TextAlign.center,
      this.leftIconSize = 25,
      this.rightIconSize = 30,
      this.height = 10})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: getHP(context, 6),
      decoration: MyTheme.boxDeco,
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 0),
        child: ListTile(
          dense: true,
          contentPadding: EdgeInsets.only(left: 0.0, right: 0.0),
          leading: (leftIcon != null)
              ? Icon(
                  leftIcon,
                  color: iconColor,
                  size: leftIconSize,
                )
              : SizedBox(),
          minLeadingWidth: 0,
          title: Txt(
            txt: txt,
            txtColor: (txt.toString().toLowerCase().contains("date"))
                ? Colors.grey
                : Colors.black,
            txtSize: txtSize,
            txtAlign: TextAlign.start,
            isBold: false,
          ),
          trailing: (rightIcon != null)
              ? Icon(
                  rightIcon,
                  color: Colors.grey,
                  size: rightIconSize,
                )
              : SizedBox(),
        ),
      ),
    );
  }
}
