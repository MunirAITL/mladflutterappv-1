import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class BoldTxt extends StatelessWidget with Mixin {
  final String initiatorDisplayName;
  final String text;
  final String eventName;

  const BoldTxt({
    Key key,
    @required this.initiatorDisplayName,
    @required this.text,
    @required this.eventName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: RichText(
        text: new TextSpan(
          // Note: Styles for TextSpans must be explicitly defined.
          // Child text spans will inherit styles from parent

          style: new TextStyle(
            height: MyTheme.txtLineSpace,
            fontSize: getTxtSize(context: context, txtSize: MyTheme.txtSize),
            color: MyTheme.timelineTitleColor,
          ),
          children: <TextSpan>[
            new TextSpan(
                text: initiatorDisplayName,
                style: new TextStyle(fontWeight: FontWeight.bold)),
            new TextSpan(text: text),
            new TextSpan(
                text: eventName,
                style: new TextStyle(fontWeight: FontWeight.bold)),
          ],
        ),
      ),
    );
  }
}
