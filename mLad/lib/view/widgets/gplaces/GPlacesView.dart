// to get places detail (lat/lng)
import 'package:aitl/config/GoogleAPIKey.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
import 'package:google_api_headers/google_api_headers.dart';

//  https://github.com/fluttercommunity/flutter_google_places/issues/140
//  https://stackoverflow.com/questions/55879550/how-to-fix-httpexception-connection-closed-before-full-header-was-received

class GPlacesView extends StatefulWidget {
  final String address;
  final String title;
  final Function(String address, Location loc) callback;
  const GPlacesView(
      {Key key,
      @required this.title,
      this.address = "",
      @required this.callback})
      : super(key: key);
  @override
  _GPlacesViewState createState() => _GPlacesViewState();
}

class _GPlacesViewState extends State<GPlacesView> with Mixin {
  @override
  Widget build(BuildContext context) {
    final address = (widget.address == "") ? "Pick Address" : widget.address;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Txt(
            txt: widget.title,
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.start,
            isBold: true),
        SizedBox(height: 15),
        Container(
          //width: getW(context, 100),
          child: new GestureDetector(
            onTap: () => _handlePressButton(),
            child: new Container(
                width: getW(context),
                //height: getH(context) * .07,
                //margin: const EdgeInsets.only(left: 20, right: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: Colors.grey, //
                    width: 1,
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 10, bottom: 10, left: 20, right: 20),
                  child: Txt(
                      txt: address,
                      txtColor:
                          (widget.address == "") ? Colors.grey : Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                )),
          ),
        ),
      ],
    );
  }

  void onError(PlacesAutocompleteResponse response) {
    print(response.errorMessage.toString());
    //homeScaffoldKey.currentState.showSnackBar(
    //SnackBar(content: Text(response.errorMessage)),
    //);
  }

  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: GoogleServerAPIKey.ApiKey,
      onError: onError,
      mode: Mode.overlay,
      language: "en",
      types: [""],
      strictbounds: false,
      components: [Component(Component.country, "uk")],
    );
    if (p != null) {
      //displayPrediction(p, (String address, Location loc) {
      //widget.callback(address, loc);
      //});

      try {
        GoogleMapsPlaces _places = GoogleMapsPlaces(
          apiKey: GoogleServerAPIKey.ApiKey,
          apiHeaders: await GoogleApiHeaders().getHeaders(),
        );
        if (_places != null) {
          PlacesDetailsResponse detail =
              await _places.getDetailsByPlaceId(p.placeId);
          String address = detail.result.formattedAddress; //p.description;
          widget.callback(address, detail.result.geometry.location);
        }
      } catch (e) {
        myLog("Location data Error = " + e.toString());
      }
    }
  }
}
