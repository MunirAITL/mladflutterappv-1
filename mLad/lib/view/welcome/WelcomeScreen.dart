import 'dart:async';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/NoAccess.dart';
import 'package:aitl/view/auth/LoginLandingScreen.dart';
import 'package:aitl/view/auth/email/signin_page.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/db_cus/more/settings/CaseAlertScreen.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  State createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with Mixin, StateListener {
  bool isDoneCookieCheck = false;

  StateProvider _stateProvider;

  @override
  void onStateChanged(ObserverState state) async {
    try {} catch (e) {}
  }

  @override
  void onStateChangedWithData(ObserverState state, data) async {
    try {
      if (state == ObserverState.STATE_HREF_LOGIN) {
        //print(data);
        if (data == null) {
          appInit();
        } else {
          Get.to(() => SigninPage(hrefLoginData: data))
              .then((value) => appInit());
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    appInit();
    /*final controller = FlutterMethodChannel().stringStream;
    controller.listen((data) {
      if (data == null) {
        //appInit();
      } else {
        Get.to(() => SigninPage(hrefLoginData: data))
            .then((value) => appInit());
      }
    });*/
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    super.dispose();
  }

  appInit() async {
    try {
      SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
      SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: MyTheme.brandColor));

      //  apns
      //  https://console.firebase.google.com/project/_/notification
      //  https://medium.com/comerge/implementing-push-notifications-in-flutter-apps-aef98451e8f1
      /*  final pushNotificationService = PushNotificationService(_firebaseMessaging);
      await pushNotificationService.initialise(callback: (enumFCM which, Map<String, dynamic> message) {
        switch (which) {
          case enumFCM.onMessage:
            fcmClickNoti(message);
            break;
          case enumFCM.onLaunch:
            break;
          case enumFCM.onResume:
            break;
          default:
        }
      });*/

      try {
        Future.delayed(Duration.zero, () {
          appData.getUDID(context);
        });

        //  cookie
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          await userData.setUserModel();
          debugPrint("User Data mobile number = " +
              userData.userModel.isMobileNumberVerified.toString());

          if (!Server.isOtp) {
            /*   navTo(context: context, isRep: true, page: () => (userData.communityId == 2) ? MainIntroducerScreen() : MainCustomerScreenNew()).then((value) {
              */ /* setState(() {
              isDoneCookieCheck = true;
            });*/ /*
            });*/

            if (userData.userModel.communityID.toString() == "1") {
              Get.offAll(
                () => MainCustomerScreen(),
              ).then((value) {
                setState(() {
                  isDoneCookieCheck = true;
                });
              });

              /*     if (userData.communityId == 2) {
                                                        navTo(context: context, isRep: true, page: () => MainIntroducerScreen()).then((value) {
                                                          //callback(route);
                                                        });
                                                      } else {
                                                        navTo(
                                                          context: context,
                                                          isRep: true,
                                                          page: () => MainCustomerScreenNew(),
                                                        ).then((value) {
                                                          //callback(route);
                                                        });
                                                      }*/

            } else {
              Get.off(() => NoAccess());
            }
          } //else if (userData.userModel.isEmailVerified) {
          if (userData.userModel.communityID.toString() == "1") {
            Get.offAll(() => MainCustomerScreen());
          } else {
            Get.off(() => NoAccess());
          }
          // } else {
          // Get.off(() => LoginLandingScreen());
          // }
          /*} else {
            debugPrint("User Data mobile number = ");
            if (userData.userModel.mobileNumber != null &&
                userData.userModel.mobileNumber.isNotEmpty) {
              Sms2APIMgr().wsLoginMobileOtpPostAPI(
                context: context,
                countryCode: '',
                mobile: userData.userModel.mobileNumber,

                // mobile: getFullPhoneNumber(countryCodeTxt: countryCode,number: _phoneController.text.trim()),
                callback: (model) {
                  if (model != null && mounted) {
                    try {
                      if (model.success) {
                        try {
                          if (mounted) {
                            Sms2APIMgr().wsSendOtpNotiAPI(
                                context: context,
                                otpId: model.responseData.userOTP.id,
                                callback: (model) {
                                  if (model != null && mounted) {
                                    try {
                                      if (model.success) {
                                        try {
                                          Get.offAll(
                                            () => Sms3Screen(
                                              mobileUserOTPModel:
                                                  model.responseData.userOTP,
                                            ),
                                          );
                                          /* navTo(
                                            context: context,
                                            page: () => Sms3Screen(
                                              mobileUserOTPModel:
                                                  model.responseData.userOTP,
                                            ),
                                          ).then((value) {
                                            //callback(route);
                                          });*/
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      } else {
                                        try {
                                          if (mounted) {
                                            final err = model
                                                .messages.postUserotp[0]
                                                .toString();
                                            showToast(context: context,
                                                txtColor: Colors.white,
                                                bgColor: MyTheme.brandColor,
                                                msg: err);
                                          }
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      }
                                    } catch (e) {
                                      myLog(e.toString());
                                    }
                                  }
                                });
                          }
                        } catch (e) {
                          myLog(e.toString());
                        }
                      } else {
                        try {
                          if (mounted) {
                            final err =
                                model.messages.postUserotp[0].toString();
                            showToast(context: context,
                                txtColor: Colors.white,
                                bgColor: MyTheme.brandColor,
                                msg: err);
                          }
                        } catch (e) {
                          myLog(e.toString());
                        }
                      }
                    } catch (e) {
                      myLog(e.toString());
                    }
                  }
                },
              );
            } else {
              navTo(
                  context: context,
                  page: () => Sms2Screen(
                        mobile: '',
                      )).then((value) {
                //callback(route);
              });
            }
          }*/
        } else {
          setState(() {
            isDoneCookieCheck = true;
          });
        }
      } catch (e) {
        myLog(e.toString());
      }
    } catch (e) {}
  }

  fcmClickNoti(Map<String, dynamic> message) async {
    try {
      if (mounted) {
        if (userData.userModel.communityID.toString() == "1") {
          Get.to(() => CaseAlertScreen(
                message: message,
              )).then((value) async {
            await userData.setUserModel();
            Get.offAll(
              () => MainCustomerScreen(),
            ).then((value) {
              setState(() {
                isDoneCookieCheck = true;
              });
            });
          });
        } else {
          Get.off(() => NoAccess());
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: (!isDoneCookieCheck)
                ? Container(
                    color: MyTheme.bgColor,
                  )
                : callLoginPage(),
            /* : Container(
                    //decoration: MyTheme.boxDeco,
                    height: getH(context),

                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          drawTopLogo(),
                          drawCenterImage(),
                          drawMMText(),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 40, right: 40, top: 5),
                            child: Column(
                              children: [
                                MMBtn(
                                    txt: "Login",
                                    height: getHP(context, 6),
                                    width: getW(context),
                                    callback: () {
                                      navTo(
                                          context: context,
                                          page: () => LoginScreen());
                                    }),
                                SizedBox(height: 10,),
                                Txt(txt: "Or", txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: false,),
                                SizedBox(height: 10,),
                                MMBtn(
                                    txt: "Signup",
                                    height: getHP(context, 6),
                                    width: getW(context),
                                    callback: () {
                                      navTo(
                                          context: context,
                                          page: () => RegScreen());
                                    }),
                              ],
                            ),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    ),
                  ),*/
          )),
    );
  }

  callLoginPage() {
    Timer(Duration(seconds: 1), () {
      try {
        Get.off(() => LoginLandingScreen());
      } catch (e) {
        debugPrint("Welcome screen Error catch ");
      }
      // ignore: unnecessary_statements
    });

    return Container(
      child: Center(
        //color: MyTheme.themeData.accentColor,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Container(
              //width: getWP(context, 70),
              child: Image.asset(
            'assets/images/logo/mm_tm.png',
            fit: BoxFit.cover,
            width: getWP(context, 70),
          )),
        ),
      ),
    );
  }
}
