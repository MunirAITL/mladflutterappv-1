import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/scan_id_doc_base.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class ScanIDDocPage extends StatefulWidget {
  const ScanIDDocPage({Key key}) : super(key: key);

  @override
  _ScanIDDocPageState createState() => _ScanIDDocPageState();
}

class _ScanIDDocPageState extends ScanIDDocBase<ScanIDDocPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Scan identity document"),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return drawRow();
  }
}
