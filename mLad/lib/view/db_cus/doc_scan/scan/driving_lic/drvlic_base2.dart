import 'dart:io';

import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/ScanDocData.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/base_card.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/driving_lic/drvlic_open_cam_page.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/driving_lic/drvlic_review_page.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl_pkg/classes/ImageLib.dart';
import 'package:aitl_pkg/widgets/crop_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../Mixin.dart';

abstract class DrvLic2Base<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();

  drawRow() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 10),
            drawCell(
                0,
                Icon(
                  Icons.drive_eta,
                  color: Colors.green,
                  size: 20,
                ),
                "Yes, English with a transparent window",
                true),
            drawCell(
                1,
                Icon(
                  Icons.drive_eta,
                  color: Colors.green,
                  size: 20,
                ),
                "No, not English and/or no window",
                false),
          ],
        ),
      ),
    );
  }

  drawCell(i, ico, title, isLine) {
    return Container(
      color: MyTheme.greyColor,
      child: ListTile(
        onTap: () async {
          if (Server.isOtp) {
            Get.off(() => OpenCamDrvLicPage());
          } else {
            scanDocData.file_drvlic_front =
                await ImageLib.getImageFromAssets("eid_scan/" + BaseCard.DL);
            await Get.off(() => CropImagePage(
                  file: scanDocData.file_drvlic_front,
                  title: "Crop your driving license",
                  txtColor: Colors.white,
                  appbarColor: MyTheme.statusBarColor,
                )).then((file) {
              if (file != null) {
                scanDocData.file_drvlic_front = file;
                Get.to(() => DrvLicReviewPage());
              }
            });
          }
        },
        minLeadingWidth: 0,
        leading: ico,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            (isLine)
                ? Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: UIHelper().drawLine())
                : SizedBox(height: 20)
          ],
        ),
      ),
    );
  }
}
