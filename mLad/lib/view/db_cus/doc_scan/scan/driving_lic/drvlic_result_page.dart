import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import '../../../../../Mixin.dart';
import '../result_base.dart';

class DrvLicResultPage extends StatefulWidget {
  final PostDocVerifyAPIModel model;
  const DrvLicResultPage({Key key, @required this.model}) : super(key: key);
  @override
  _DrvLicResultPageState createState() => _DrvLicResultPageState();
}

class _DrvLicResultPageState extends ResultBase<DrvLicResultPage> with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title:
              UIHelper().drawAppbarTitle(title: "Identification of a person"),
          centerTitle: false,
        ),
        body: drawLayout(eScanDocType.DL, widget.model),
      ),
    );
  }
}
