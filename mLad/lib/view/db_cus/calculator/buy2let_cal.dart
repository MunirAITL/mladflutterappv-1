import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/input/InputTitleBoxMM4.dart';
import 'package:aitl/view/widgets/slider/SliderThumbShape.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/PriceBox.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/mortgage_cal.dart';
import 'package:aitl/view_model/rx/mortgage_to_let_cal_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:aitl/view/widgets/slider/SliderTrackShapes.dart';
//  https://help.syncfusion.com/flutter/slider/enabled-and-disabled-state

class Buy2LetCalPage extends StatefulWidget {
  const Buy2LetCalPage({Key key}) : super(key: key);

  @override
  State createState() => _Buy2LetCalPageState();
}

class _Buy2LetCalPageState extends State<Buy2LetCalPage> with Mixin {
  //final mortgageCalController = Get.put(MortgageCalController());
  final mortgage2LetController = Get.put(MortgageToLetCalController());

  //  1
  final propertyValue = TextEditingController();
  final rentalIncomeValue = TextEditingController();
  //  2
  final depositValue = TextEditingController();
  final interestRateValue = TextEditingController();

  RegExp reg = RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
  String Function(Match) mathFunc = (Match match) => '${match[1]},';

  @override
  void initState() {
    super.initState();
    initPage();
  }

  initPage() {
    propertyValue.text = mortgage2LetController.propertyValue.value
        .toStringAsFixed(0)
        .replaceAllMapped(reg, mathFunc);
    rentalIncomeValue.text = mortgage2LetController.rentalIncome.value
        .toStringAsFixed(0)
        .replaceAllMapped(reg, mathFunc);
    depositValue.text = mortgage2LetController.depositValue.value
        .toStringAsFixed(0)
        .replaceAllMapped(reg, mathFunc);
    print(mortgage2LetController.interestRate.value.toString());
    interestRateValue.text = mortgage2LetController.interestRate.value
        .toString()
        .replaceAllMapped(reg, mathFunc);
    calculate();
  }

  @override
  void dispose() {
    super.dispose();
  }

  calculate() {
    try {
      mortgage2LetController.maxLoanAmount.value =
          mortgage2LetController.rentalIncome.value * 192;
      mortgage2LetController.stampDutyAmount.value =
          (mortgage2LetController.propertyValue.value * 3) / 100;
    } catch (e) {}
    try {
      if (mortgage2LetController.interestRate.value == 0) {
        mortgage2LetController.lTVAmount.value = 50.0;
        mortgage2LetController.monthlyInterestAmount.value = 0.0;
      } else {
        final balanceOutstanding = (mortgage2LetController.maxLoanAmount.value +
                mortgage2LetController.stampDutyAmount.value) -
            mortgage2LetController.depositValue.value;
        if (balanceOutstanding < 1) {
          mortgage2LetController.lTVAmount.value = 0.0;
          mortgage2LetController.monthlyInterestAmount.value = 0.0;
        } else {
          mortgage2LetController.lTVAmount.value = (balanceOutstanding /
                  mortgage2LetController.propertyValue.value) *
              100;
          if (mortgage2LetController.lTVAmount.value > 99)
            mortgage2LetController.lTVAmount.value = 99;

          final totalInterestAmount =
              (balanceOutstanding * mortgage2LetController.interestRate.value) /
                  100;
          mortgage2LetController.monthlyInterestAmount.value =
              totalInterestAmount / 12;
        }
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          automaticallyImplyLeading: false,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "Buy To Let Calculator"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding:
                const EdgeInsets.only(top: 20, left: 20, right: 20, bottom: 10),
            child: Txt(
              txt: "Calculator",
              txtColor: MyTheme.dBlueAirColor,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
          ),
          draw1BorrowInput(),
          draw1RentalInput(),
          draw1BorrowView(),
          SizedBox(height: 40),
          draw2MonthlyCostInput(),
          draw2InterestRateInput(),
          draw2InterestView(),
          SizedBox(height: 50),
        ],
      )),
    );
  }

  //  ********************************************************  1

  draw1BorrowInput() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
            child: Txt(
              txt: "Find out how much you can borrow",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w500,
              isBold: true,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: InputTitleBoxMM4(
              title: "Property value",
              ph: "0",
              input: propertyValue,
              kbType: TextInputType.number,
              len: 11,
              minLen: 0,
              prefixIco: Text(getCurSign(),
                  style: TextStyle(color: Colors.white, fontSize: 17)),
              tooltipTxt: "",
              isReadonly: true,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(() => Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Text(
                        mortgage2LetController.propertyValue.value
                            .toStringAsFixed(0)
                            .replaceAllMapped(reg, mathFunc),
                        textAlign: TextAlign.start,
                        style: TextStyle(color: Colors.black, fontSize: 17)),
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: SliderTheme(
                  data: SliderThemeData(
                    //overlayShape: SliderComponentShape.noOverlay,
                    thumbColor: MyTheme.dBlueAirColor,
                    activeTrackColor: MyTheme.dBlueAirColor,
                    inactiveTrackColor: Colors.grey,
                    trackHeight: 2,
                    // thumbShape: SliderThumbShape(),
                  ),
                  child: SfSliderTheme(
                    data: SfSliderThemeData(
                      activeTrackHeight: 3,
                      inactiveTrackHeight: 2,
                    ),
                    child: SfSlider(
                      thumbShape: SliderThumbShape(),
                      activeColor: MyTheme.bgColor,
                      inactiveColor: MyTheme.dBlueAirColor.withOpacity(.5),
                      value: mortgage2LetController.propertyValue.value,
                      min: 300000.0,
                      max: 1499990.0,
                      stepSize: 3000,
                      onChanged: (newValue) {
                        mortgage2LetController.propertyValue.value = newValue;
                        propertyValue.text = mortgage2LetController
                            .propertyValue.value
                            .toStringAsFixed(0)
                            .replaceAllMapped(reg, mathFunc);
                        calculate();
                        setState(() {});
                      },
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  draw1RentalInput() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: InputTitleBoxMM4(
              title: "Rental income (monthly)",
              ph: "0",
              input: rentalIncomeValue,
              kbType: TextInputType.number,
              len: 11,
              minLen: 0,
              prefixIco: Text(getCurSign(),
                  style: TextStyle(color: Colors.white, fontSize: 17)),
              tooltipTxt: "",
              isReadonly: true,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(() => Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Text(
                        mortgage2LetController.rentalIncome.value
                            .toStringAsFixed(0)
                            .replaceAllMapped(reg, mathFunc),
                        textAlign: TextAlign.start,
                        style: TextStyle(color: Colors.black, fontSize: 17)),
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: SliderTheme(
                  data: SliderThemeData(
                    //overlayShape: SliderComponentShape.noOverlay,
                    thumbColor: MyTheme.dBlueAirColor,
                    activeTrackColor: MyTheme.dBlueAirColor,
                    inactiveTrackColor: Colors.grey,
                    trackHeight: 2,
                    // thumbShape: SliderThumbShape(),
                  ),
                  child: SfSliderTheme(
                    data: SfSliderThemeData(
                      activeTrackHeight: 3,
                      inactiveTrackHeight: 2,
                    ),
                    child: SfSlider(
                      thumbShape: SliderThumbShape(),
                      activeColor: MyTheme.bgColor,
                      inactiveColor: MyTheme.dBlueAirColor.withOpacity(.5),
                      value: mortgage2LetController.rentalIncome.value,
                      min: 1500.0,
                      max: 4990.0,
                      stepSize: 10,
                      onChanged: (newValue) {
                        mortgage2LetController.rentalIncome.value = newValue;
                        rentalIncomeValue.text = mortgage2LetController
                            .rentalIncome.value
                            .toStringAsFixed(0)
                            .replaceAllMapped(reg, mathFunc);
                        calculate();
                        setState(() {});
                      },
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  draw1BorrowView() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
          width: getW(context),
          decoration: BoxDecoration(
            color: MyTheme.bgColor.withOpacity(.5),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt(
                  txt: "Borrow up to:",
                  txtColor: Colors.white,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ),
                Txt(
                  txt: getCurSign() +
                      (mortgage2LetController.maxLoanAmount.value)
                          .toStringAsFixed(0)
                          .replaceAllMapped(reg, mathFunc),
                  txtColor: MyTheme.brandColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w500,
                  isBold: false,
                ),
                Txt(
                  txt: "Buying? Budget " +
                      getCurSign() +
                      (mortgage2LetController.stampDutyAmount.value)
                          .toStringAsFixed(0)
                          .replaceAllMapped(reg, mathFunc) +
                      " extra for stamp duty.",
                  txtColor: Colors.white,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ),
              ],
            ),
          )),
    );
  }

  //  ********************************************************  2

  draw2MonthlyCostInput() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
            child: Txt(
              txt: "See your monthly mortgage cost",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w500,
              isBold: true,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: InputTitleBoxMM4(
              title: "Deposit",
              ph: "0",
              input: depositValue,
              kbType: TextInputType.number,
              len: 11,
              minLen: 0,
              prefixIco: Text(getCurSign(),
                  style: TextStyle(color: Colors.white, fontSize: 17)),
              tooltipTxt: "",
              isReadonly: true,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(() => Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Text(
                        mortgage2LetController.depositValue.value
                            .toStringAsFixed(0)
                            .replaceAllMapped(reg, mathFunc),
                        textAlign: TextAlign.start,
                        style: TextStyle(color: Colors.black, fontSize: 17)),
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: SfSliderTheme(
                  data: SfSliderThemeData(
                    activeTrackHeight: 3,
                    inactiveTrackHeight: 2,
                  ),
                  child: SfSlider(
                    thumbShape: SliderThumbShape(),
                    activeColor: MyTheme.bgColor,
                    inactiveColor: MyTheme.dBlueAirColor.withOpacity(.5),
                    value: mortgage2LetController.depositValue.value,
                    min: 100000.0,
                    max: 1499990.0,
                    stepSize: 5000,
                    onChanged: (newValue) {
                      mortgage2LetController.depositValue.value = newValue;
                      depositValue.text = mortgage2LetController
                          .depositValue.value
                          .toStringAsFixed(0)
                          .replaceAllMapped(reg, mathFunc);
                      calculate();
                      setState(() {});
                    },
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  draw2InterestRateInput() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: InputTitleBoxMM4(
              title: "Interest rate (%)",
              ph: "0",
              input: interestRateValue,
              kbType: TextInputType.number,
              len: 11,
              minLen: 0,
              prefixIco: Text(getCurSign(),
                  style: TextStyle(color: Colors.white, fontSize: 17)),
              tooltipTxt: "",
              isReadonly: true,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(() => Padding(
                    padding: const EdgeInsets.only(left: 25),
                    child: Text(
                        mortgage2LetController.interestRate.value
                            .toStringAsFixed(1)
                            .replaceAllMapped(reg, mathFunc),
                        textAlign: TextAlign.start,
                        style: TextStyle(color: Colors.black, fontSize: 17)),
                  )),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: SfSliderTheme(
                  data: SfSliderThemeData(
                    activeTrackHeight: 3,
                    inactiveTrackHeight: 2,
                  ),
                  child: SfSlider(
                    thumbShape: SliderThumbShape(),
                    activeColor: MyTheme.bgColor,
                    inactiveColor: MyTheme.dBlueAirColor.withOpacity(.5),
                    //trackShape: SliderTrackShapes(),
                    value: mortgage2LetController.interestRate.value,
                    min: 2.5,
                    max: 10.0,
                    stepSize: .5,
                    //divisions: 1,
                    onChanged: (newValue) {
                      print(newValue);
                      mortgage2LetController.interestRate.value = newValue;
                      interestRateValue.text = mortgage2LetController
                          .interestRate.value
                          .toStringAsFixed(1)
                          .replaceAllMapped(reg, mathFunc);
                      calculate();
                      setState(() {});
                    },
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  draw2InterestView() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
          width: getW(context),
          decoration: BoxDecoration(
            color: MyTheme.bgColor.withOpacity(.5),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Txt(
                  txt: "Interest only:",
                  txtColor: Colors.white,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ),
                Txt(
                  txt: getCurSign() +
                      mortgage2LetController.monthlyInterestAmount.value
                          .toStringAsFixed(0)
                          .replaceAllMapped(reg, mathFunc) +
                      "/month",
                  txtColor: MyTheme.brandColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  fontWeight: FontWeight.w500,
                  isBold: false,
                ),
                Txt(
                  txt: "Loan to value (LTV): " +
                      mortgage2LetController.lTVAmount.value
                          .toStringAsFixed(0) +
                      "%",
                  txtColor: Colors.white,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false,
                ),
              ],
            ),
          )),
    );
  }
}
