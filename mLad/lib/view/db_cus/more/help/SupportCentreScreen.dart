import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/more/help/ResolutionScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';

class SupportCentreScreen extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          automaticallyImplyLeading: true,
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          //automaticallyImplyLeading: false,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "Support"),
          centerTitle: false,
        ),
        /*floatingActionButton: FloatingActionButton(
          backgroundColor: MyTheme.brandColor,
          child: Icon(
            Icons.add,
            color: Colors.white, //The color which you want set.
          ),
          onPressed: () => {
            Get.to(() => ResolutionScreen(),
                transition: Transition.rightToLeft,
                duration:
                    Duration(milliseconds: AppConfig.pageAnimationMilliSecond))
            /*navTo(
              context: context,
              page: () => ResolutionScreen(),
            ).then((value) {
              //callback(route);
            })*/
          },
        ),*/
        body: drawLayout(context),
      ),
    );
  }

  drawLayout(context) {
    return Container(
      width: getW(context),
      height: getH(context),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(30),
              child: GestureDetector(
                onTap: () {
                  launch("mailto:" + AppDefine.SUPPORT_EMAIL);
                },
                child: Container(
                  width: getW(context),
                  height: getHP(context, 7),
                  alignment: Alignment.center,
                  //color: MyTheme.brownColor,
                  decoration: new BoxDecoration(
                    color: MyTheme.brandColor,
                    borderRadius: new BorderRadius.circular(10),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                    child: Txt(
                      txt: "Email: " + AppDefine.SUPPORT_EMAIL,
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 40),
            Container(
                width: getWP(context, 45),
                height: getWP(context, 50),
                child: Image.asset(
                  "assets/images/img/support_bg.png",
                  fit: BoxFit.cover,
                )),
            SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: MMBtn(
                txt: "Call: " + AppDefine.SUPPORT_CALL,
                width: getW(context),
                height: getHP(context, 7),
                radius: 8,
                callback: () {
                  launch("tel://" + AppDefine.SUPPORT_CALL);
                },
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: MMBtn(
                txt: "Send Message",
                bgColor: MyTheme.lGrayColor,
                txtColor: Colors.black,
                width: getW(context),
                height: getHP(context, 7),
                radius: 8,
                callback: () async {
                  Get.to(() => ResolutionScreen(),
                      transition: Transition.rightToLeft,
                      duration: Duration(
                          milliseconds: AppConfig.pageAnimationMilliSecond));

                  /* navTo(
                    context: context,
                    page: () => ResolutionScreen(),
                  ).then((value) {
                    //callback(route);
                  });*/
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
