import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/MoreHelper.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/db_cus/noti/NotiTab.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

/*
String appName = packageInfo.appName;
String packageName = packageInfo.packageName;
String version = packageInfo.version;
String buildNumber = packageInfo.buildNumber;
*/

class MoreTab extends StatefulWidget {
  @override
  State createState() => _MoreTabState();
}

class _MoreTabState extends State<MoreTab> with Mixin {
  PackageInfo packageInfo;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      packageInfo = await PackageInfo.fromPlatform();
      setState(() {});
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "More"),
          centerTitle: false,
        ),
        body: Container(
          //decoration: MyTheme.bgBlueColor,
          width: getW(context),
          height: getH(context),
          child: Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
            child: SingleChildScrollView(
              primary: true,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListView.builder(
                    primary: false,
                    shrinkWrap: true,
                    itemCount: MoreHelper.listMore.length,
                    itemBuilder: (context, index) {
                      Map<String, dynamic> mapMore = MoreHelper.listMore[index];
                      if (Server.isOtp && mapMore['title'] == "EID-Verify")
                        return SizedBox();
                      return GestureDetector(
                        onTap: () async {
                          if (mounted) {
                            Type route = mapMore['route'];
                            if (route != null) {
                              if (identical(route, NotiTab)) {
                                StateProvider().notify(
                                    ObserverState.STATE_CHANGED_tabbar4);
                              } else {
                                await MoreHelper().setRoute(
                                    context: context,
                                    route: route,
                                    callback: (route2) {});
                              }
                            } else {
                              confirmDialog(
                                  msg: "Are you sure, you want to log out ?",
                                  title: "Alert !",
                                  callbackYes: () {
                                    MainCustomerScreenState.currentTab = 0;
                                    StateProvider().notify(
                                        ObserverState.STATE_CHANGED_logout);
                                  },
                                  callbackNo: () {
                                    //Navigator.of(context, rootNavigator: true).pop();
                                  },
                                  context: context);
                            }
                          }
                        },
                        child: Card(
                          elevation: .5,
                          color: MyTheme.bgColor2,
                          child: ListTile(
                              leading:
                                  (mapMore['title'].toString() == 'Live Chat')
                                      ? Icon(Icons.video_call,
                                          color: MyTheme.titleColor, size: 30)
                                      : Image.asset(mapMore['icon'],
                                          width: 30, height: 30),
                              title: Txt(
                                  txt: mapMore['title'].toString(),
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                              trailing:
                                  (index == MoreHelper.listMore.length - 1)
                                      ? SizedBox()
                                      : Icon(
                                          Icons.arrow_forward_ios,
                                          color: Colors.black,
                                          size: 20,
                                        )),
                        ),
                      );
                    },
                  ),
                  packageInfo != null
                      ? Padding(
                          padding: const EdgeInsets.only(
                              left: 20, right: 20, top: 30),
                          child: Container(
                            width: getW(context),
                            //color: Colors.green,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Txt(
                                    txt: "Version",
                                    txtColor: Colors.black87,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    fontWeight: FontWeight.bold,
                                    isBold: false),
                                SizedBox(height: 7),
                                Txt(
                                    txt: packageInfo.version,
                                    txtColor: Colors.grey,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                              ],
                            ),
                          ),
                        )
                      : SizedBox()
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
