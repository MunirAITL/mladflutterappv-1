import 'dart:convert';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_timeline/TaskBiddingHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/review_rating/ReviewRatingAPIModel.dart';
import 'package:aitl/model/json/db_cus/review_rating/ReviewRatingPostAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TaskBiddingAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TaskBiddingModel.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:cached_network_image/cached_network_image.dart';

class ReviewRatingListPage extends StatefulWidget {
  final List<ReviewRatingFormSetups> reviewRatingModel;
  final LocationsModel locationModel;
  const ReviewRatingListPage({
    Key key,
    @required this.reviewRatingModel,
    @required this.locationModel,
  }) : super(key: key);

  @override
  State createState() => _ReviewRatingListPageState();
}

class _ReviewRatingListPageState extends State<ReviewRatingListPage>
    with Mixin {
  List<TextEditingController> listCmt = [];
  List<double> listRate = [];
  TaskBiddingModel taskBiddingModel;

  bool isLoading = true;

  drawRateUI(int i) {
    return RatingBar.builder(
      initialRating: listRate[i],
      itemCount: 5,
      itemSize: 20,
      unratedColor: Colors.grey,
      glowColor: Colors.yellow,
      itemBuilder: (context, index) {
        switch (index) {
          case 0:
            return Icon(
              Icons.sentiment_very_dissatisfied,
              color: Colors.red,
            );
          case 1:
            return Icon(
              Icons.sentiment_dissatisfied,
              color: Colors.orange,
            );
          case 2:
            return Icon(
              Icons.sentiment_neutral,
              color: Colors.amber,
            );
          case 3:
            return Icon(
              Icons.sentiment_satisfied,
              color: Colors.lightGreen,
            );
          case 4:
            return Icon(
              Icons.sentiment_very_satisfied,
              color: Colors.green,
            );
          default:
            return Container();
        }
      },
      onRatingUpdate: (rating) {
        listRate[i] = rating;
        setState(() {});
      },
    );
  }

  drawTxtFieldUI(int i) {
    return Container(
      //margin: const EdgeInsets.only(left: 10.0, right: 10, top: 10),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.grey),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: TextField(
        controller: listCmt[i],
        minLines: 2,
        maxLines: 3,
        //expands: true,
        autocorrect: false,
        maxLength: 255,
        keyboardType: TextInputType.multiline,
        style: TextStyle(
          color: Colors.black,
          fontSize: 14,
        ),
        decoration: InputDecoration(
          isDense: true,
          hintText: 'Your review',
          hintStyle: TextStyle(color: Colors.grey),
          counterText: "",
          //labelText: 'Your message',
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          contentPadding: EdgeInsets.all(10),
        ),
      ),
    );
  }

  reset() {
    for (int i = 0; i < listCmt.length; i++) {
      listCmt[i].clear();
      listRate[i] = 0;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  initPage() async {
    for (int i = 0; i < widget.reviewRatingModel.length; i++) {
      listRate.add(0);
      listCmt.add(TextEditingController());
    }

    await APIViewModel().req<TaskBiddingAPIModel>(
        context: context,
        url: TaskBiddingHelper().getUrl(taskId: widget.locationModel.id),
        reqType: ReqType.Get,
        callback: (model) {
          if (mounted) {
            if (model != null) {
              for (final TaskBiddingModel taskBiddings
                  in model.responseData.taskBiddings) {
                if (taskBiddings.communityId == 9 ||
                    taskBiddings.communityId == 18) {
                  taskBiddingModel = taskBiddings;
                  break;
                }
              }
              if (taskBiddingModel == null) {
                showToast(
                    context: context,
                    msg: "Something, went wrong. try again later");
                Get.back();
              } else {
                setState(() {
                  isLoading = false;
                });
              }
            }
          }
        });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //final icon =
    //NewCaseHelper().getCreateCaseIconByTitle(widget.locationModel.title);

    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          automaticallyImplyLeading: false,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "Task Review and Rating"),
          centerTitle: false,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(getHP(context, 20)),
            child: taskBiddingModel != null
                ? Container(
                    color: Colors.white,
                    width: getW(context),
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 10, left: 10, right: 10, bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircleAvatar(
                            radius: 27,
                            backgroundColor: Colors.transparent,
                            backgroundImage: CachedNetworkImageProvider(
                              MyNetworkImage.checkUrl(
                                  (taskBiddingModel.ownerImageUrl != null)
                                      ? taskBiddingModel.ownerImageUrl
                                      : Server.MISSING_IMG),
                            ),
                          ),
                          Txt(
                            txt: taskBiddingModel.ownerName +
                                    ' - ' +
                                    taskBiddingModel.communityName ??
                                '',
                            txtColor: MyTheme.dBlueAirColor,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            fontWeight: FontWeight.w500,
                            isBold: true,
                          ),
                          Txt(
                            txt: widget.locationModel.title,
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .4,
                            txtAlign: TextAlign.start,
                            isBold: false,
                          ),
                          Txt(
                            txt: "Case No. " +
                                widget.locationModel.id.toString(),
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .4,
                            txtAlign: TextAlign.start,
                            isBold: false,
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.bgColor2,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 10, right: 10, top: 5, bottom: 10),
              child: taskBiddingModel != null
                  ? MMBtn(
                      txt: "Submit",
                      width: getW(context),
                      height: getHP(context, 7),
                      radius: 5,
                      callback: () async {
                        final listRatingDetails = [];
                        double rateAverage = 0;
                        String cmt = '';
                        for (int i = 0;
                            i < widget.reviewRatingModel.length;
                            i++) {
                          final txt = listCmt[i].text.trim();
                          final reviewRatingFormSetupsModel =
                              widget.reviewRatingModel[i];
                          rateAverage += listRate[i];
                          cmt += (txt + " \n");
                          listRatingDetails.add({
                            "Id": 0,
                            "UserId": taskBiddingModel.taskOwnerId,
                            "Status": 101,
                            "CreationDate": DateTime.now().toString(),
                            "Rating": listRate[i],
                            "TaskBiddingId": taskBiddingModel.id,
                            "EmployeeId": taskBiddingModel.userId,
                            "Comments": txt ?? '',
                            "IsPoster": 0,
                            "InitiatorName": userData.userModel.name,
                            "TaskTitle": widget.locationModel.title,
                            "TaskId": widget.locationModel.id,
                            "ReviewRatingId": reviewRatingFormSetupsModel.id
                          });
                        }
                        rateAverage =
                            rateAverage / widget.reviewRatingModel.length;
                        final param = {
                          "Id": 0,
                          "UserId": taskBiddingModel.taskOwnerId,
                          "Status": 101,
                          "CreationDate": DateTime.now().toString(),
                          "Rating": rateAverage.toStringAsFixed(1),
                          "TaskBiddingId": taskBiddingModel.id,
                          "EmployeeId": taskBiddingModel.userId,
                          "Comments": cmt,
                          "IsPoster": 0,
                          "InitiatorName": userData.userModel.name,
                          "TaskTitle": widget.locationModel.title,
                          "TaskId": widget.locationModel.id,
                          "UserRatingDetailList": listRatingDetails,
                        };
                        myLog(param);
                        await APIViewModel().req<ReviewRatingPostAPIModel>(
                            context: context,
                            url: Server.REVIEW_RATING_FORMSETUP_POST_URL,
                            reqType: ReqType.Post,
                            param: param,
                            callback: (model) async {
                              if (mounted) {
                                if (model != null) {
                                  if (model.success) {
                                    reset();
                                    showToast(
                                        context: context,
                                        msg:
                                            "Review and rating submitted successfully",
                                        which: 1);
                                  } else {
                                    final err = model.errorMessages;
                                    showToast(
                                        context: context,
                                        msg: "Error in posting");
                                  }
                                }
                              }
                            });
                      })
                  : SizedBox(),
            ),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: taskBiddingModel != null ? drawLayout() : SizedBox(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
        child: ListView.builder(
            primary: true,
            shrinkWrap: true,
            itemCount: widget.reviewRatingModel.length,
            itemBuilder: (context, i) {
              final ReviewRatingFormSetups model = widget.reviewRatingModel[i];
              return Align(
                alignment: Alignment.centerLeft,
                child: Card(
                  elevation: 2,
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: 3,
                                child: Txt(
                                  txt: model.title,
                                  txtColor: MyTheme.dBlueAirColor,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  isBold: true,
                                  //txtLineSpace: 1.5,
                                ),
                              ),
                              Row(
                                children: [
                                  drawRateUI(i),
                                  SizedBox(width: 5),
                                  Txt(
                                    txt: listRate[i].toStringAsFixed(0) +
                                        "* Rating",
                                    txtColor: Colors.orange,
                                    txtSize: MyTheme.txtSize - .4,
                                    txtAlign: TextAlign.end,
                                    fontWeight: FontWeight.w500,
                                    isBold: false,
                                    //txtLineSpace: 1.5,
                                  ),
                                ],
                              ),

                              /*Flexible(
                                child: Txt(
                                  txt: DateFun.getDate(
                                      model.creationDate, "dd-MMM-yyyy"),
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize - .5,
                                  txtAlign: TextAlign.end,
                                  isBold: false,
                                  //txtLineSpace: 1.5,
                                ),
                              ),*/
                            ],
                          ),
                          model.commentsData != ''
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Txt(
                                    txt: model.commentsData,
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .4,
                                    txtAlign: TextAlign.center,
                                    isBold: false,
                                    //txtLineSpace: 1.5,
                                  ),
                                )
                              : SizedBox(),
                          SizedBox(height: 10),
                          drawTxtFieldUI(i),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }));
  }
}
