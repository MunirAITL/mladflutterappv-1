import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/MoreHelper.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/db_cus/more/help/HelpScreen.dart';
import 'package:aitl/view/db_cus/more/profile/EditProfileScreen.dart';
import 'package:aitl/view/db_cus/more/settings/ChangePwdScreen.dart';
import 'package:aitl/view/db_cus/more/settings/TestNotiScreen.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'NotiSettingsScreen.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> with Mixin {
  var listItems = [];

  @override
  void initState() {
    super.initState();
    listItems = [
      // "Edit Profile",
      {"head": "APP SETTINGS"},
      {
        "head": null,
        "title": "Profile Settings",
        "route": () => EditProfileScreen()
      },
      /*{
        "head": null,
        "title": "Test Notification",
        "route": () => TestNotiScreen()
      },*/
      {
        "head": null,
        "title": "Notification Settings",
        "route": () => NotiSettingsScreen()
      },
      {"head": null, "title": "Change PIN", "route": () => ChangePwdScreen()},
      {"head": "GENERAL INFO"},
      //{"head": null, "title": "Help Center", "route": () => HelpScreen()},
      {
        "head": null,
        "title": "Terms & Conditions",
        "route": () => PDFDocumentPage(
              title: "Privacy",
              url: "https://mortgage-magic.co.uk/assets/img/privacy_policy.pdf",
            ),
      },
      {
        "head": null,
        "title": "Privacy Policy",
        "callback": () => MoreHelper().openWSPrivacyPDF(context),
      },
      //"Test Notification"
    ];
    setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //userModel.countryofResidency = "ffff";
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        //resizeToAvoidBottomPadding: true,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  elevation: 0,
                  //backgroundColor: MyTheme.parallexToolbarColor,
                  iconTheme: IconThemeData(
                      color: (innerBoxIsScrolled)
                          ? Colors.black
                          : Colors.white //change your color here
                      ),
                  leading: IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Icon(Icons.arrow_back)),
                  title: UIHelper().drawAppbarTitle(title: "My Account"),
                  centerTitle: false,
                  expandedHeight: getHP(context, 40),
                  floating: false,
                  pinned: true,
                  snap: false,
                  forceElevated: true,
                  flexibleSpace: FlexibleSpaceBar(
                    collapseMode: CollapseMode.parallax,
                    centerTitle: true,
                    background: drawUserProfileData(),
                  ),
                  //background:
                ),
              ];
            },
            body: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawUserProfileData() {
    final userModel = userData.userModel;
    return Container(
      decoration: BoxDecoration(color: MyTheme.statusBarColor),
      alignment: Alignment.center,
      child: ListView(
        primary: false,
        shrinkWrap: true,
        children: [
          SizedBox(height: 10),
          Container(
            decoration: MyTheme.picEmboseCircleDeco,
            child: CircleAvatar(
              radius: 40,
              backgroundColor: Colors.transparent,
              child: ClipOval(
                child: Image.network(
                  MyNetworkImage.checkUrl(userModel.profileImageURL != null &&
                          userModel.profileImageURL.isNotEmpty
                      ? userModel.profileImageURL
                      : Server.MISSING_IMG),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          SizedBox(height: 15),
          Txt(
              txt: userModel.name.uFirst(),
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: true),
          SizedBox(height: 5),
          Txt(
              txt: userModel.mobileNumber,
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  drawLayout() {
    return Container(
      //decoration: MyTheme.bgBlueColor,
      width: getW(context),
      height: getH(context),
      child: ListView.builder(
        itemCount: listItems.length,
        itemBuilder: (context, index) {
          final item = listItems[index];
          final heading = item['head'];
          if (heading != null) {
            return Container(
              color: MyTheme.lGrayColor,
              width: getW(context),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Txt(
                    txt: heading,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .5,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            );
          } else {
            return GestureDetector(
              onTap: () async {
                if (item['route'] == null) {
                  Function.apply(item['callback'], []);
                } else {
                  Get.to(item['route']);
                }
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Card(
                  elevation: heading != null ? 0 : .5,
                  color: MyTheme.bgColor2,
                  margin: EdgeInsets.zero,
                  clipBehavior: Clip.antiAlias,
                  child: ListTile(
                    title: Txt(
                        txt: item['title'],
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    trailing: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.black,
                      size: 20,
                    ),
                  ),
                ),
              ),
            );
          }
        },
      ),
    );
  }
}
