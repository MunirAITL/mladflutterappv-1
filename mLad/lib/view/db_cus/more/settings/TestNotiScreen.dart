import 'package:aitl/controller/api/db_cus/more/settings/TestNofiAPIMgr.dart';
import 'package:aitl/view/widgets/btn/BtnOutline.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class TestNotiScreen extends StatefulWidget {
  @override
  State createState() => _TestNotiScreenState();
}

class _TestNotiScreenState extends State<TestNotiScreen> with Mixin {
  @mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  appInit() async {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.statusBarColor,
          title: UIHelper().drawAppbarTitle(title: "Test notification"),
          centerTitle: false,
        ),
        body: drawTestUI(),
      ),
    );
  }

  drawTestUI() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: getHP(context, 3)),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Txt(
                      txt: "Is it working?",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: BtnOutline(
                    txt: "Test it",
                    txtColor: MyTheme.brandColor,
                    borderColor: MyTheme.brandColor,
                    callback: () {
                      TestNotiAPIMgr().wsTestNotiAPI(
                        context: context,
                        callback: (model) {
                          if (model != null && mounted) {
                            try {
                              if (model.success) {
                                try {
                                  final msg = model
                                      .responseData.notification.description;
                                  showToast(context: context, msg: msg);
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              } else {
                                try {
                                  final err =
                                      model.messages.pushMessage[0].toString();
                                  showToast(
                                      context: context, msg: err, which: 0);
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              }
                            } catch (e) {
                              myLog(e.toString());
                            }
                          } else {
                            myLog("test notification screen not in");
                          }
                        },
                      );
                    }),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt:
                    "Make sure you're actually getting those all important push notifications.",
                txtColor: Colors.grey,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          drawLine(context: context, w: getW(context)),
        ],
      ),
    );
  }
}
