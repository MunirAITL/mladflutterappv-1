import 'dart:convert';
import 'dart:io';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/db_cus/more/settings/EditProfileAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/more/settings/GetProfileAPIMgr.dart';
import 'package:aitl/controller/api/media/MediaUploadAPIMgr.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/settings/EditProfileHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/UserModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/txt/TxtBox.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../widgets/dialog/DeactivateProfileDialog.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  State createState() => _EditProfileState();
}

enum smokerEnum { yes, no }

class _EditProfileState extends State<EditProfileScreen> with Mixin {
  StateProvider _stateProvider = StateProvider();

  bool isUpdateProfile = false;

  EditProfileHelper editProfileHelper;

  //  personal info
  final _fname = TextEditingController();
  final _mname = TextEditingController();
  final _lname = TextEditingController();
  final _pwd = TextEditingController();
  final _mobile = TextEditingController();
  final _tel = TextEditingController();
  final _nationalInsuranceNumber = TextEditingController();
  final _refSource = TextEditingController();
  final _passport = TextEditingController();
  final TextEditingController _deactivateReason = TextEditingController();
  //
  final focusFname = FocusNode();
  final focusMname = FocusNode();
  final focusLname = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();
  final focusTel = FocusNode();
  final focusPwd = FocusNode();
  final focusNIN = FocusNode();
  final focusPP = FocusNode();
  final focusRef = FocusNode();

  //  Email
  String email;
  String profileImageId = "";
  String coverImageId = "";

  //  DOB
  String dob = "";

  //  Passport
  String passportExp = "";

  smokerEnum _radiosmoker = smokerEnum.no;

  bool isLoading = false;

  updateProfileAPI(
      {String profileImageId = '', String coverImageId = ''}) async {
    try {
      if (validate()) {
        isUpdateProfile = true;
        final param = EditProfileHelper().getParam(
          firstName: _fname.text.trim(),
          lastName: _lname.text.trim(),
          email: email,
          userName: userData.userModel.userName,
          profileImageId: profileImageId.toString(),
          coverImageId: coverImageId.toString(),
          referenceId: userData.userModel.referenceID.toString(),
          referenceType: "",
          //_ref.text.trim(),
          stanfordWorkplaceURL: _refSource.text.toString().trim(),
          //_ref.text.trim(),
          remarks: userData.userModel.remarks,
          cohort: editProfileHelper.optGender.title.trim(),
          communityId: userData.userModel.communityID.toString(),
          isFirstLogin: false,
          mobileNumber: _mobile.text.trim(),
          dateofBirth: dob,
          middleName: _mname.text.trim(),
          namePrefix: editProfileHelper.optTitle.title.trim(),
          areYouASmoker: (_radiosmoker == smokerEnum.no) ? 'No' : 'Yes',
          countryCode: '',
          addressLine1: userData.userModel.addressLine1,
          addressLine2: userData.userModel.addressLine2,
          addressLine3: userData.userModel.addressLine3,
          town: userData.userModel.town,
          county: userData.userModel.county,
          postcode: userData.userModel.postcode,
          telNumber: _tel.text.trim(),
          nationalInsuranceNumber: _nationalInsuranceNumber.text.trim(),
          nationality: editProfileHelper.optNationalities.title,
          countryofBirth: editProfileHelper.optCountriesBirth.title,
          countryofResidency: editProfileHelper.optCountriesResidential.title,
          passportNumber: _passport.text.trim(),
          maritalStatus: editProfileHelper.optMaritalStatus.title,
          occupantType: userData.userModel.occupantType,
          livingDate: userData.userModel.livingDate,
          password: _pwd.text.trim(),
          userCompanyId: userData.userModel.userCompanyID,
          visaExpiryDate: userData.userModel.visaExpiryDate,
          passportExpiryDate: passportExp,
          visaName: userData.userModel.visaName,
          otherVisaName: userData.userModel.otherVisaName,
          id: userData.userModel.id,
        );
        myLog("profile update data passportExp = " + passportExp);
        myLog("profile update data = " + json.encode(param));
        EditProfileAPIMgr().wsUpdateProfileAPI(
          context: context,
          param: param,
          callback: (model) async {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    final msg = model.messages.post_user[0].toString();
                    showToast(context: context, msg: msg, which: 1);
                    await DBMgr.shared
                        .setUserProfile(user: model.responseData.user);
                    await userData.setUserModel();
                    setState(() {});
                  } catch (e) {
                    myLog(e.toString());
                  }
                } else {
                  try {
                    if (mounted) {
                      final err = model.errorMessages.post_user[0].toString();
                      showToast(context: context, msg: err);
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          },
        );
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    editProfileHelper = null;
    _stateProvider = null;

    //  personal info
    _pwd.dispose();
    _fname.dispose();
    _mname.dispose();
    _lname.dispose();
    _mobile.dispose();
    _tel.dispose();
    _nationalInsuranceNumber.dispose();
    _refSource.dispose();
    _passport.dispose();

    //  deactivate
    _deactivateReason.dispose();

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    isLoading = true;
    GetProfileAPIMgr().wsGetProfileAPI(
      context: context,
      callback: (model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                UserModel userModel = model.responseData.user;

                //  personal info
                email = userModel.email;
                profileImageId = userModel.profileImageID.toString();
                _fname.text = userModel.firstName;
                _mname.text = userModel.middleName;
                _lname.text = userModel.lastName;
                _mobile.text =
                    Common.stripCountryCodePhone(userModel.mobileNumber);
                _tel.text = Common.stripCountryCodePhone(userModel.telNumber);

                //  visa info
                _nationalInsuranceNumber.text =
                    userModel.nationalInsuranceNumber;
                _refSource.text = userModel.stanfordWorkplaceURL;
                _passport.text = userModel.passportNumber;
                if (userModel.passportExpiryDate == "0001-01-01T00:00:00" ||
                    userModel.passportExpiryDate == "1970-01-01T00:00:00") {
                  passportExp = '';
                } else {
                  passportExp = DateFormat('dd-MMM-yyyy')
                      .format(DateTime.parse(userModel.passportExpiryDate));
                }

                _radiosmoker = (userModel.areYouASmoker == "No")
                    ? smokerEnum.no
                    : smokerEnum.yes;

                dob = userModel.dateofBirth;

                //  populate countries drop down all 3
                editProfileHelper = EditProfileHelper();
                await editProfileHelper.getCountriesBirth(
                    context: context, cap: userModel.countryofBirth);
                await editProfileHelper.getCountriesResidential(
                    context: context, cap: userModel.countryofResidency);
                await editProfileHelper.getCountriesNationaity(
                    context: context, cap: userModel.nationality);

                //  set data into drop down
                if (userModel.namePrefix != '') {
                  editProfileHelper.optTitle.id = "1";
                  editProfileHelper.optTitle.title = userModel.namePrefix;
                }

                if (userModel.cohort != '') {
                  editProfileHelper.optGender.id = "1";
                  editProfileHelper.optGender.title = userModel.cohort;
                }

                if (userModel.maritalStatus != '') {
                  editProfileHelper.optMaritalStatus.id = "1";
                  editProfileHelper.optMaritalStatus.title =
                      userModel.maritalStatus;
                }

                if (userModel.countryofBirth != '') {
                  editProfileHelper.optCountriesBirth.id = "1";
                  editProfileHelper.optCountriesBirth.title =
                      userModel.countryofBirth;
                }

                if (userModel.countryofResidency != '') {
                  editProfileHelper.optCountriesResidential.id = "1";
                  editProfileHelper.optCountriesResidential.title =
                      userModel.countryofResidency;
                }

                if (userModel.nationality != '') {
                  editProfileHelper.optNationalities.id = "1";
                  editProfileHelper.optNationalities.title =
                      userModel.nationality;
                }

                await DBMgr.shared
                    .setUserProfile(user: model.responseData.user);
                await userData.setUserModel();

                setState(() {
                  isLoading = false;
                });
              } catch (e) {
                myLog("Error UserInfo data parse = " + e.toString());
                setState(() {
                  isLoading = false;
                });
              }
            } else {
              setState(() {
                isLoading = false;
              });

              try {
                if (mounted) {
                  final err = model.errorMessages.post_user[0].toString();
                  showToast(context: context, msg: err);
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          } catch (e) {
            myLog(e.toString());
          }
        }
      },
    );
  }

  validate() {
    if (!UserProfileVal().isFNameOK(context, _fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, _lname)) {
      return false;
    } else if (editProfileHelper.optGender.id == null) {
      showToast(context: context, msg: "Please choose gender from the list");
      return false;
    } else if (dob == "") {
      showToast(context: context, msg: "Please select date of birth");
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, _mobile)) {
      return false;
    } else if (editProfileHelper.optNationalities.id == null) {
      showToast(context: context, msg: "Please choose nationality");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final isCoverBG =
        (MyNetworkImage.isValidUrl(userData.userModel.coverImageURL))
            ? true
            : false;
    return SafeArea(
        child: Scaffold(
            backgroundColor: MyTheme.bgColor2,
            //resizeToAvoidBottomPadding: true,
            body: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: NestedScrollView(
                    headerSliverBuilder:
                        (BuildContext context, bool innerBoxIsScrolled) {
                      return <Widget>[
                        SliverAppBar(
                          automaticallyImplyLeading: true,
                          backgroundColor: MyTheme.statusBarColor,
                          iconTheme: IconThemeData(color: Colors.white),
                          leading: IconButton(
                              onPressed: () {
                                Get.back();
                              },
                              icon: Icon(Icons.arrow_back)),
                          title: UIHelper()
                              .drawAppbarTitle(title: "Edit Your Profile"),
                          centerTitle: false,
                          expandedHeight: getHP(context, 30),
                          pinned: true,
                          elevation: 0,
                          titleSpacing: 0,
                          floating: true,
                          flexibleSpace: FlexibleSpaceBar(
                            collapseMode: CollapseMode.parallax,
                            background: drawHeader(isCoverBG),
                          ),
                        ),
                        //pinned: true,
                        //snap: false,
                        //forceElevated: true,
                      ];
                    },
                    body: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onPanDown: (detail) {
                          FocusScope.of(context).requestFocus(new FocusNode());
                        },
                        onTap: () {
                          FocusScope.of(context).requestFocus(new FocusNode());
                        },
                        child: drawLayout())))));
  }

  drawHeader(isCoverBG) {
    return Container(
      //height: getHP(context, !isPublicUser ? h1 : h2),
      width: getW(context),
      decoration: isCoverBG
          ? BoxDecoration(
              //color: MyTheme.statusBarColor,
              image: DecorationImage(
                image: NetworkImage(
                  userData.userModel.coverImageURL,
                ),
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.7), BlendMode.dstATop),
                fit: BoxFit.cover,
              ),
            )
          : BoxDecoration(
              color: MyTheme.statusBarColor,
              border: Border(
                bottom: BorderSide(
                  color: MyTheme.bgColor2,
                  width: 10,
                ),
              ),
            ),

      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Positioned(
            bottom: 90,
            child: Padding(
              padding: const EdgeInsets.only(right: 10),
              child: MMBtn(
                  txt: "Edit Cover",
                  bgColor: HexColor.fromHex("#FFFFFF").withAlpha(24),
                  width: getWP(context, 22),
                  height: getHP(context, 5),
                  radius: 0,
                  callback: () {
                    CamPicker().showCamDialog(
                      context: context,
                      isRear: false,
                      callback: (File path) {
                        if (path != null) {
                          MediaUploadAPIMgr().wsMediaUploadFileAPI(
                            context: context,
                            file: path,
                            callback: (model) {
                              if (model != null && mounted) {
                                try {
                                  if (model.success) {
                                    coverImageId = model
                                        .responseData.images[0].id
                                        .toString();
                                    updateProfileAPI(
                                        coverImageId: coverImageId.toString());
                                  } else {
                                    final err = model
                                        .errorMessages.upload_pictures[0]
                                        .toString();
                                    showToast(context: context, msg: err);
                                  }
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              }
                            },
                          );
                        }
                      },
                    );
                  }),
            ),
          ),
          Positioned(
              bottom: -5,
              child: Container(
                width: getW(context),
                height: getHP(context, 8),
                color: MyTheme.bgColor2,
              )),
          Stack(
            alignment: Alignment.bottomLeft,
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Container(
                    width: getWP(context, 20),
                    height: getWP(context, 20),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: Colors.white,
                        width: 5,
                      ),
                    ),
                    child: CircleAvatar(
                      radius: 25,
                      backgroundColor: Colors.transparent,
                      backgroundImage: CachedNetworkImageProvider(
                        MyNetworkImage.checkUrl(
                            userData.userModel.profileImageURL != null &&
                                    userData
                                        .userModel.profileImageURL.isNotEmpty
                                ? userData.userModel.profileImageURL
                                : Server.MISSING_IMG),
                      ),
                    ),
                    //borderRadius: BorderRadius.all(Radius.circular(20))),
                  ),
                ),
              ),
              Positioned(
                left: getWP(context, 30) - 40,
                child: Container(
                  width: 25,
                  height: 25,
                  child: MaterialButton(
                    onPressed: () {
                      CamPicker().showCamDialog(
                        context: context,
                        isRear: false,
                        callback: (File path) {
                          if (path != null) {
                            MediaUploadAPIMgr().wsMediaUploadFileAPI(
                              context: context,
                              file: path,
                              callback: (model) {
                                if (model != null && mounted) {
                                  try {
                                    if (model.success) {
                                      profileImageId = model
                                          .responseData.images[0].id
                                          .toString();
                                      updateProfileAPI(
                                          profileImageId:
                                              profileImageId.toString());
                                    } else {
                                      final err = model
                                          .errorMessages.upload_pictures[0]
                                          .toString();
                                      showToast(context: context, msg: err);
                                    }
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              },
                            );
                          }
                        },
                      );
                    },
                    color: HexColor.fromHex("#4F9DAA"),
                    child: Icon(
                      Icons.camera_alt_outlined,
                      size: 15,
                    ),
                    padding: EdgeInsets.all(0),
                    shape: CircleBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: getWP(context, 35)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Txt(
                          txt: "Personal\nInformation",
                          txtColor: MyTheme.titleColor,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                    ),
                    SizedBox(width: 10),
                    Flexible(
                        child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child:
                          Image.asset("assets/images/ico/checked_blue_ico.png"),
                    )),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  drawLayout() {
    return Container(
      child: (isLoading)
          ? SizedBox()
          : Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: ListView(
                shrinkWrap: true,
                children: [
                  //SizedBox(height: 20),
                  //drawCamPicker(),
                  //drawHeader(),
                  SizedBox(height: 10),
                  drawPersonalInfoView(),
                  SizedBox(height: 40),
                  drawVisaInfoView(),
                  SizedBox(height: 40),
                  GestureDetector(
                    onTap: () {
                      updateProfileAPI(
                          profileImageId: profileImageId.toString(),
                          coverImageId: coverImageId.toString());
                    },
                    child: Container(
                      width: getW(context),
                      height: getHP(context, 7),
                      alignment: Alignment.center,
                      //color: MyTheme.brownColor,
                      decoration: new BoxDecoration(
                        color: MyTheme.titleColor,
                        borderRadius: new BorderRadius.circular(10),
                      ),
                      child: Txt(
                        txt: "Save Profile",
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Container(
                    decoration: new BoxDecoration(
                      color: MyTheme.lGrayColor,
                      borderRadius: new BorderRadius.circular(10),
                    ),
                    width: getW(context),
                    height: getHP(context, 7),
                    child: new ElevatedButton(
                        child: Txt(
                            txt: "Deactivate My Account",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                        onPressed: () {
                          showDeactivateProfileDialog(
                              context: context,
                              email: _deactivateReason,
                              callback: (String reason) {
                                //
                                if (reason != null) {
                                  EditProfileAPIMgr().wsDeactivateProfileAPI(
                                    context: context,
                                    reason: reason,
                                    callback: (model) {
                                      if (model != null && mounted) {
                                        try {
                                          if (model.success) {
                                            try {
                                              final msg =
                                                  "Your account has been deleted.\nYou can always sign up again."; //model
                                              //.messages.delete[0]
                                              //.toString();
                                              showToast(
                                                  context: context,
                                                  msg: msg,
                                                  which: 1);
                                              Future.delayed(
                                                  Duration(
                                                      seconds: AppConfig
                                                          .AlertDismisSec), () {
                                                //  signout
                                                _stateProvider.notify(
                                                    ObserverState
                                                        .STATE_CHANGED_logout);
                                              });
                                            } catch (e) {
                                              myLog(e.toString());
                                            }
                                          } else {
                                            try {
                                              if (mounted) {
                                                final err = model
                                                    .messages.delete[0]
                                                    .toString();
                                                showToast(
                                                    context: context, msg: err);
                                              }
                                            } catch (e) {
                                              myLog(e.toString());
                                            }
                                          }
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      }
                                    },
                                  );
                                }
                              });
                        },
                        style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(
                              Colors.transparent,
                            ),
                            elevation: MaterialStateProperty.all<double>(0),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    side: BorderSide(
                                        color: Colors.white, width: 1))))),
                  ),
                  SizedBox(height: 50),
                ],
              ),
            ),
    );
  }

  drawPersonalInfoView() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return Container(
      //width: getW(context),
      child: Column(
        children: [
          /*     Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
            child: DropDownPicker(
              cap: "Choose Title",
              itemSelected: editProfileHelper.optTitle,
              dropListModel: editProfileHelper.ddTitle,
              onOptionSelected: (optionItem) {
                debugPrint("Option Title = ${optionItem}");
                editProfileHelper.optTitle = optionItem;
                setState(() {});
              },
            ),
          ),*/
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  child: Txt(
                txt: "Choose Title",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                isBold: true,
                txtAlign: TextAlign.start,
              )),
              SizedBox(height: 10.0),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optTitle.title,
                ddTitleList: editProfileHelper.ddTitle,
                callback: (optionItem) {
                  editProfileHelper.optTitle = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "First Name",
            input: _fname,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusFname,
            focusNodeNext: focusMname,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Middle Name",
            input: _mname,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusMname,
            focusNodeNext: focusLname,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Last Name",
            input: _lname,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusLname,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          /*         Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: DropDownPicker(
              cap: "Choose Gender",
              itemSelected: editProfileHelper.optGender,
              dropListModel: editProfileHelper.ddGender,
              onOptionSelected: (optionItem) {
                editProfileHelper.optGender = optionItem;
                setState(() {});
              },
            ),
          ),*/
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Choose Gender",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    fontWeight: FontWeight.bold,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optGender.title,
                ddTitleList: editProfileHelper.ddGender,
                callback: (optionItem) {
                  editProfileHelper.optGender = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Choose Marital Status",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    fontWeight: FontWeight.bold,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optMaritalStatus.title,
                ddTitleList: editProfileHelper.ddMaritalStatus,
                callback: (optionItem) {
                  editProfileHelper.optMaritalStatus = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          SizedBox(height: 20),
          DatePickerView(
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            cap: 'Select date of birth',
            dt: (dob == '') ? 'Select date of birth' : dob,
            initialDate: dateDOBlast,
            firstDate: dateDOBfirst,
            lastDate: dateDOBlast,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    dob = DateFormat('dd-MM-yyyy').format(value).toString();
                  } catch (e) {
                    myLog(e.toString());
                  }
                });
              }
            },
          ),
          /*         Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
            child: DropDownPicker(
              cap: "Country of birth",
              itemSelected: editProfileHelper.optCountriesBirth,
              dropListModel: editProfileHelper.ddMaritalCountriesBirth,
              onOptionSelected: (optionItem) {
                editProfileHelper.optCountriesBirth = optionItem;
                setState(() {});
              },
            ),
          ),*/
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Country of birth",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.bold,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optCountriesBirth.title,
                ddTitleList: editProfileHelper.ddMaritalCountriesBirth,
                callback: (optionItem) {
                  editProfileHelper.optCountriesBirth = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          SizedBox(height: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                txt: "Email",
                txtColor: Colors.black,
                fontWeight: FontWeight.bold,
                txtSize: MyTheme.txtSize - .2,
                isBold: false,
                txtAlign: TextAlign.start,
              ),
              SizedBox(height: 10),
              TxtBox(txt: email),
            ],
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Mobile Number",
            input: _mobile,
            kbType: TextInputType.phone,
            inputAction: TextInputAction.next,
            focusNode: focusMobile,
            focusNodeNext: focusTel,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Telephone Number",
            input: _tel,
            kbType: TextInputType.phone,
            inputAction: TextInputAction.next,
            focusNode: focusTel,
            focusNodeNext: focusNIN,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "National insurance Number",
            input: _nationalInsuranceNumber,
            kbType: TextInputType.text,
            inputAction: TextInputAction.next,
            focusNode: focusNIN,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          /*   Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: DropDownPicker(
              cap: "Country of Residence",
              itemSelected: editProfileHelper.optCountriesResidential,
              dropListModel: editProfileHelper.ddMaritalCountriesResidential,
              onOptionSelected: (optionItem) {
                editProfileHelper.optCountriesResidential = optionItem;
                setState(() {});
              },
            ),
          ),*/
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Country of Residence",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.bold,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optCountriesResidential.title,
                ddTitleList: editProfileHelper.ddMaritalCountriesResidential,
                callback: (optionItem) {
                  editProfileHelper.optCountriesResidential = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Nationality",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.bold,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optNationalities.title,
                ddTitleList: editProfileHelper.ddMaritalNationalities,
                callback: (optionItem) {
                  editProfileHelper.optNationalities = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          drawsmoker(),
        ],
      ),
    );
  }

  drawVisaInfoView() {
    final DateTime dateNow = DateTime.now();
    final dateExplast = DateTime(dateNow.year + 50, dateNow.month, dateNow.day);
    final dateExpfirst = DateTime(dateNow.year, dateNow.month + 1, dateNow.day);
    return Container(
      //width: getW(context),
      child: Column(
        children: [
          Container(
            width: getW(context),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Txt(
                          txt: "Visa Information",
                          txtColor: MyTheme.titleColor,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: true),
                    ),
                    SizedBox(width: 5),
                    Container(
                      child:
                          Image.asset("assets/images/ico/checked_blue_ico.png"),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Container(color: Colors.black, height: 0.5)
              ],
            ),
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Passport Number",
            input: _passport,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusPP,
            focusNodeNext: focusRef,
            len: 50,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          DatePickerView(
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            cap: 'Passport Expiry Date',
            dt: (passportExp == '') ? 'Passport Expiry Date' : passportExp,
            initialDate: dateExpfirst,
            firstDate: dateExpfirst,
            lastDate: dateExplast,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    passportExp =
                        DateFormat('dd-MMM-yyyy').format(value).toString();
                  } catch (e) {
                    myLog(e.toString());
                  }
                });
              }
            },
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Reference Source",
            input: _refSource,
            kbType: TextInputType.text,
            inputAction: TextInputAction.done,
            focusNode: focusRef,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),

          /*     Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
            child: DropDownPicker(
              cap: "Visa Status",
              itemSelected: editProfileHelper.optVisaStatus,
              dropListModel: editProfileHelper.ddVisaStatus,
              onOptionSelected: (optionItem) {
                editProfileHelper.optVisaStatus = optionItem;
                setState(() {});
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
            child: DatePickerView(
              cap: 'Visa Expiry Date',
              dt: (visaExp == '') ? 'Visa Expiry Date' : visaExp,
              initialDate: dateExpfirst,
              firstDate: dateExpfirst,
              lastDate: dateExplast,
              callback: (value) {
                if (mounted) {
                  setState(() {
                    try {
                      visaExp =
                          DateFormat('dd-MM-yyyy').format(value).toString();
                    } catch (e) {
                      myLog(e.toString());
                    }
                  });
                }
              },
            ),
          ),*/
        ],
      ),
    );
  }

  drawsmoker() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, top: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Are you a smoker?",
                txtColor: Colors.black,
                fontWeight: FontWeight.bold,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 5),
            Theme(
              data: MyTheme.radioThemeData,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _radiosmoker = smokerEnum.no;
                        });
                      }
                    },
                    child: Radio(
                      visualDensity: VisualDensity(horizontal: 0, vertical: 0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      value: smokerEnum.no,
                      groupValue: _radiosmoker,
                      onChanged: (smokerEnum value) {
                        if (mounted) {
                          setState(() {
                            _radiosmoker = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "No",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.w400,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  SizedBox(width: 20),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _radiosmoker = smokerEnum.yes;
                        });
                      }
                    },
                    child: Radio(
                      visualDensity: VisualDensity(horizontal: 0, vertical: 0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      value: smokerEnum.yes,
                      groupValue: _radiosmoker,
                      onChanged: (smokerEnum value) {
                        if (mounted) {
                          setState(() {
                            _radiosmoker = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Yes",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.w400,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
