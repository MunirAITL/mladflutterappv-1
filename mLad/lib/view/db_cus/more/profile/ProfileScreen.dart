import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/db_cus/more/badge/BadgeAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/more/settings/GetProfileAPIMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/UserModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/UserBadgeModel.dart';
import 'package:aitl/view/db_cus/more/help/HelpScreen.dart';
import 'package:aitl/view/db_cus/more/profile/EditProfileScreen.dart';
import 'package:aitl/view/db_cus/more/settings/ContactPage.dart';
import 'package:aitl/view/db_cus/more/settings/SettingsScreen.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'ProfileAddrBadge.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> with Mixin {
  bool isLoading = true;
  UserModel userModel;

  final menuItems = [
    /*{
      'index': 0,
      'icon': 'assets/images/ico/switch_acc_ico.png',
      'title': 'Switch Accounts'
    },*/
    {
      'icon': 'assets/images/ico/address_ico.png',
      'title': 'Basic Information',
      'route': () => ProfileAddrBadge(),
    },
    {
      'icon': 'assets/images/ico/help_center_ico.png',
      'title': 'Contact',
      'route': () => ContactPage(),
    },
    {
      'icon': 'assets/images/ico/help_center_ico.png',
      'title': 'Help Center',
      'route': () => HelpScreen(),
    },
    {
      'icon': 'assets/images/ico/settings_ico.png',
      'title': 'My Account',
      'route': () => SettingsScreen()
    },
  ];

  getUserProfile() async {
    GetProfileAPIMgr().wsGetProfileAPI(
      context: context,
      callback: (model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                userModel = model.responseData.user;
                myLog("UserInfo data parse = " + model.responseData.toString());

                setState(() async {
                  isLoading = false;
                  await DBMgr.shared
                      .setUserProfile(user: model.responseData.user);
                  await userData.setUserModel();
                });
              } catch (e) {
                myLog("Error UserInfo data parse = " + e.toString());
                setState(() {
                  isLoading = false;
                });
              }
            } else {
              setState(() {
                isLoading = false;
              });

              try {
                if (mounted) {
                  final err = model.errorMessages.post_user[0].toString();
                  showToast(context: context, msg: err);
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          } catch (e) {
            myLog(e.toString());
          }
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    userModel = null;
    super.dispose();
  }

  appInit() async {
    await getUserProfile();
  }

  @override
  Widget build(BuildContext context) {
    //userModel.countryofResidency = "ffff";
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.statusBarColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          //automaticallyImplyLeading: false,
          title: UIHelper().drawAppbarTitle(title: "Profile"),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.to(() => EditProfileScreen());
                },
                icon: Icon(Icons.edit)),
          ],
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.statusBarColor,
          child: Container(
            width: getW(context),
            height: getHP(context, 35),
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(50),
                topRight: const Radius.circular(50),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 30, left: 10, right: 10, bottom: 10),
              child: ListView.builder(
                  shrinkWrap: true,
                  primary: false,
                  itemCount: menuItems.length,
                  itemBuilder: (context, index) {
                    final map = menuItems[index];
                    return GestureDetector(
                      onTap: () {
                        Get.to(map['route']);
                      },
                      child: Card(
                        elevation: 0,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Image.asset(
                                    map['icon'],
                                    width: 20,
                                    height: 20,
                                  ),
                                  SizedBox(width: 20),
                                  Expanded(
                                    child: Txt(
                                      txt: map['title'],
                                      txtColor: MyTheme.dRedColor,
                                      txtSize: MyTheme.txtSize - .2,
                                      txtAlign: TextAlign.start,
                                      isBold: false,
                                    ),
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios,
                                    color: Colors.grey,
                                    size: 20,
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 35, top: 10, bottom: 10),
                                child: Divider(
                                  color: Colors.black,
                                  height: 10,
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            ),
          ),
        ),
        //resizeToAvoidBottomPadding: true,
        body: (isLoading)
            ? SizedBox()
            : Container(
                color: MyTheme.statusBarColor,
                width: getW(context),
                height: getH(context),
                child: Padding(
                  padding: const EdgeInsets.only(left: 30, right: 30),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 10),
                        Container(
                          decoration: MyTheme.picEmboseCircleDeco,
                          child: CircleAvatar(
                            radius: 40,
                            backgroundColor: Colors.transparent,
                            backgroundImage: new CachedNetworkImageProvider(
                              MyNetworkImage.checkUrl(
                                  userModel.profileImageURL != null &&
                                          userModel.profileImageURL.isNotEmpty
                                      ? userModel.profileImageURL
                                      : Server.MISSING_IMG),
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Txt(
                          maxLines: 1,
                          txt: userModel.name.uFirst(),
                          txtColor: MyTheme.lRedColor,
                          txtSize: MyTheme.txtSize + .2,
                          txtAlign: TextAlign.start,
                          isBold: true,
                        ),
                        SizedBox(height: 5),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.phone,
                              color: Colors.cyan,
                              size: 15,
                            ),
                            SizedBox(width: 5),
                            Flexible(
                              child: Txt(
                                maxLines: 1,
                                txt: getPhoneNumber(userModel.mobileNumber),
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 2),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.email,
                              color: Colors.cyan,
                              size: 15,
                            ),
                            SizedBox(width: 5),
                            Flexible(
                              child: Txt(
                                maxLines: 2,
                                txt: userModel.email,
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ),
                          ],
                        ),
                        userModel.address.isNotEmpty
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                      "assets/images/icons/map_pin_icon.png",
                                      width: 15,
                                      height: 15),
                                  SizedBox(width: 5),
                                  Flexible(
                                    child: Txt(
                                      maxLines: 2,
                                      txt: userModel.address ?? '',
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .2,
                                      txtAlign: TextAlign.start,
                                      isBold: false,
                                    ),
                                  ),
                                ],
                              )
                            : SizedBox(),
                        GestureDetector(
                          onTap: () {
                            Get.to(() => EditProfileScreen())
                                .then((value) async {
                              await getUserProfile();
                            });
                          },
                          child: Image.asset(
                            "assets/images/icons/edit_btn_ico.png",
                          ),
                        ),
                      ],
                    ),
                    /*Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 15.0),
                              child: Txt(
                                txt: "Basic Information :",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: true,
                              ),
                            ),
                            userModel.address.isNotEmpty &&
                                    userModel.address != null
                                ? SizedBox(height: 20)
                                : SizedBox(),
                            //address

                            userModel.address.isNotEmpty &&
                                    userModel.address != null
                                ? Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Row(
                                      children: [
                                        // Icon(Icons.home_outlined,color: Colors.black,size: 36,),
                                        Image.asset(
                                          "assets/images/icons/home_icon.png",
                                          width: 36,
                                        ),

                                        SizedBox(width: 10),
                                        Expanded(
                                          child: Txt(
                                            txt: "${userModel.address}",
                                            txtColor: Colors.black,
                                            txtSize: MyTheme.txtSize,
                                            txtAlign: TextAlign.start,
                                            isBold: false,
                                            maxLines: 5,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                : SizedBox(),
                            userModel.countryofResidency.isNotEmpty &&
                                    userModel.countryofResidency != null
                                ? SizedBox(
                                    height: 20,
                                  )
                                : SizedBox(),
//country
                            userModel.countryofResidency.isNotEmpty &&
                                    userModel.countryofResidency != null
                                ? Padding(
                                    padding: const EdgeInsets.only(left: 10),
                                    child: Row(
                                      children: [
                                        // Icon(Icons.flag,color: Colors.black,size: 36,),
                                        Image.asset(
                                          "assets/images/icons/glob_ic.png",
                                          width: 25,
                                          color: Colors.grey,
                                        ),

                                        SizedBox(
                                          width: 10,
                                        ),
                                        Txt(
                                          txt:
                                              "${userModel.countryofResidency}",
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                        ),
                                      ],
                                    ),
                                  )
                                : SizedBox(),
                            userModel.dateofBirth.isNotEmpty &&
                                    userModel.dateofBirth != null
                                ? SizedBox(
                                    height: 20,
                                  )
                                : SizedBox(),
                            //Date of birth

                            userModel.dateofBirth.isNotEmpty &&
                                    userModel.dateofBirth != null
                                ? Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Row(
                                      children: [
                                        // Icon(Icons.date_range,color: Colors.black,size: 36,),

                                        Image.asset(
                                          "assets/images/icons/date_icon.png",
                                          width: 36,
                                        ),

                                        SizedBox(
                                          width: 10,
                                        ),
                                        Txt(
                                          txt: "${userModel.dateofBirth}",
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                        ),
                                      ],
                                    ),
                                  )
                                : SizedBox(),

                            userModel.maritalStatus.isNotEmpty &&
                                    userModel.maritalStatus != null
                                ? SizedBox(
                                    height: 20,
                                  )
                                : SizedBox(),
                            //marital status

                            userModel.maritalStatus.isNotEmpty &&
                                    userModel.maritalStatus != null
                                ? Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Row(
                                      children: [
                                        // Icon(Icons.update,color: Colors.black,size: 36,),
                                        Image.asset(
                                          "assets/images/icons/gender_icon.png",
                                          width: 36,
                                        ),

                                        SizedBox(
                                          width: 10,
                                        ),
                                        Txt(
                                          txt: "${userModel.maritalStatus}",
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                        ),
                                      ],
                                    ),
                                  )
                                : SizedBox(),
                            SizedBox(
                              height: 20,
                            ),
                            Container(
                              padding: EdgeInsets.all(10.0),
                              // margin: EdgeInsets.all(8.0),
                              color: Colors.grey[200],
                              child: Row(
                                // crossAxisAlignment: CrossAxisAlignment.s,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Txt(
                                    txt: "Badges",
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.start,
                                    isBold: false,
                                  ),
                                  SizedBox(
                                    width: 10,
                                  ),
                                  Btn(
                                      txt: "Add More",
                                      txtColor: Colors.white,
                                      bgColor: MyTheme.brandColor,
                                      width: null, //getWP(context, 35),
                                      height: getHP(context, 5),
                                      callback: () {
                                        navTo(
                                                context: context,
                                                page: () => BadgeScreen())
                                            .then((value) {});
                                      }),
                                ],
                              ),
                            ),

                            ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: listUserBadgeModelShowList.length,
                              itemBuilder: (context, index) {
                                var item;
                                debugPrint(
                                    "badge type  ${listUserBadgeModelShowList[index].type}");

                                if (listUserBadgeModelShowList[index].type ==
                                    "Mobile") {
                                  item = listItems[0];
                                }
                                if (listUserBadgeModelShowList[index].type ==
                                    "Email") {
                                  item = listItems[1];
                                }
                                if (listUserBadgeModelShowList[index].type ==
                                    "Passport") {
                                  item = listItems[2];
                                }
                                if (listUserBadgeModelShowList[index].type ==
                                    "Facebook") {
                                  item = listItems[3];
                                }
                                if (listUserBadgeModelShowList[index].type ==
                                    "CustomerPrivacy") {
                                  item = listItems[4];
                                }
                                if (listUserBadgeModelShowList[index].type ==
                                    "NationalIDCard") {
                                  item = listItems[5];
                                }
                                if (listUserBadgeModelShowList[index].type ==
                                    "ElectricId") {
                                  item = listItems[5];
                                }
                                return (item == null)
                                    ? SizedBox()
                                    : Card(
                                        elevation: 0,
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              top: 10, bottom: 10),
                                          child: ListTile(
                                            // leading: Image.asset(item['icon']),
                                            title: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Container(
                                                    width: getWP(context, 8),
                                                    height: getWP(context, 8),
                                                    child: Image.asset(
                                                        item['icon'])),
                                                SizedBox(width: 10),
                                                Expanded(
                                                  child: Txt(
                                                      txt: item["title"],
                                                      txtColor: Colors.black,
                                                      txtSize:
                                                          MyTheme.txtSize + .4,
                                                      txtAlign: TextAlign.start,
                                                      isOverflow: true,
                                                      isBold: false),
                                                ),
                                              ],
                                            ),
                                            subtitle: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 10, bottom: 10),
                                                  child: Txt(
                                                      txt: item["title"] ==
                                                              "Customer Privacy"
                                                          ? item["desc"] +
                                                              " at ${DateFormat('dd-MMM-yyyy').format(DateTime.parse(listUserBadgeModelShowList[index].creationDate.toString()))}"
                                                          : item["desc"],
                                                      txtColor: Colors.grey,
                                                      txtSize:
                                                          MyTheme.txtSize - .2,
                                                      txtAlign: TextAlign.start,
                                                      //txtLineSpace: 1.4,
                                                      isBold: false),
                                                ),
                                                Container(
                                                  color: Colors.grey[200],
                                                  height: 1,
                                                  width: getW(context),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                              },
                            ),
                          ],
                        ),
                      ),*/
                  ),
                ),
              ),
      ),
    );
  }

  String getPhoneNumber(String mobileNumber) {
    var phoneNumber = mobileNumber;
    try {
      debugPrint("Phone number substring(0,2)= " + mobileNumber[0]);
      if (mobileNumber[0] == "+") {
        if (mobileNumber.substring(0, 3) == "+88") {
          phoneNumber = mobileNumber.substring(3);
        } else if (mobileNumber.substring(0, 3) == "+44") {
          phoneNumber = mobileNumber.substring(3);
        }
      } else {
        if (mobileNumber.substring(0, 2) == "88") {
          phoneNumber = mobileNumber.substring(2);
        } else if (mobileNumber.substring(0, 2) == "44") {
          phoneNumber = mobileNumber.substring(2);
        }
      }
      //remove double 00
      debugPrint("Phonenumber substring(0,2) 2= " + phoneNumber);
      if (phoneNumber.substring(0, 2) == "00") {
        phoneNumber = phoneNumber.substring(1);
      }
      // add a single  0
      //     if (phoneNumber.substring(0, 1) != "0") {
      //       phoneNumber = "0" + phoneNumber;
      //     }
    } catch (e) {}

    return phoneNumber;
  }
}
