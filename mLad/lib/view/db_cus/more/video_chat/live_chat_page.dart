import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'live_chat_base.dart';

class LiveChatPage extends StatefulWidget {
  const LiveChatPage({Key key}) : super(key: key);

  @override
  _LiveChatPageState createState() => _LiveChatPageState();
}

class _LiveChatPageState extends BaseLiveChat<LiveChatPage> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: brandColor));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFFFEDD8),
        resizeToAvoidBottomInset: true,
        /*appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: brandColor,
          iconTheme: MyTheme.themeData.iconTheme,
          leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () {
                Get.back();
              }),
          title: UIHelper().drawAppbarTitle(title: 'Live video call'),
          centerTitle: false,
        ),*/
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10),
                  drawSupportTeamUI(),
                  SizedBox(height: 20),
                  drawGraphUI(),
                  SizedBox(height: 10),
                  drawListingItemBorrow(),
                  SizedBox(height: 20),
                ],
              ),
            ),
            drawApplyMortgageUI(),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
