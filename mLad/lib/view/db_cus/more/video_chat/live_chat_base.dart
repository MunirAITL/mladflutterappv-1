import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:chart_sparkline/chart_sparkline.dart';

abstract class BaseLiveChat<T extends StatefulWidget> extends State<T>
    with Mixin {
  final brandColor = Color(0xFFEC407A);

  final listSupportTeam = [
    {
      'icon': 'assets/images/ico_chat/adobe_live.png',
      'name': 'Adobe Live',
      'status': 'Live',
    },
    {
      'icon': 'assets/images/ico_chat/roman_lel.png',
      'name': 'Roman Lel',
      'status': 'Live',
    },
    {
      'icon': 'assets/images/ico_chat/dianna.png',
      'name': 'Dianna',
      'status': 'Live',
    },
    {
      'icon': 'assets/images/ico_chat/marian.png',
      'name': 'Marian',
      'status': 'Live',
    },
    {
      'icon': 'assets/images/ico_chat/adobe_live.png',
      'name': 'MD Sir',
      'status': 'Live',
    },
    {
      'icon': 'assets/images/ico_chat/adobe_live.png',
      'name': 'Adobe Live',
      'status': 'Live',
    },
    {
      'icon': 'assets/images/ico_chat/roman_lel.png',
      'name': 'Roman Lel',
      'status': 'Live',
    },
  ];

  //
  final listRate = [
    {
      'name': 'Santandar',
      'icon': 'assets/images/ico_chat/rate_santandar.png',
      'price': '0.9',
      'rate1': '+12',
      'rate2': '4',
      'rate3': '49',
    },
    {
      'name': 'Santandar',
      'icon': 'assets/images/ico_chat/rate_barclays.png',
      'price': '0.9',
      'rate1': '+12',
      'rate2': '4',
      'rate3': '49',
    },
    {
      'name': 'Test',
      'icon': 'assets/images/ico_chat/rate_null.png',
      'price': '0.9',
      'rate1': '+12',
      'rate2': '4',
      'rate3': '49',
    }
  ];

  //
  final listItemBorrow = [
    {
      'title': 'How much can i borrow?',
      'subTitle': 'Use the mortgage calculator',
    },
    {
      'title': 'Compare Best Rates',
      'subTitle': 'Check live rates',
    },
  ];
  //
  final listMortgageGrid = [{}];

  drawLayout();

  _drawLineText(txt1, txt2) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Text(AppDefine.CUR_SIGN + txt1,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 12,
                  decoration: TextDecoration.lineThrough)),
        ),
        Flexible(
          child: Text(AppDefine.CUR_SIGN + txt2,
              style: TextStyle(
                  color: Color(0xFF77838F),
                  fontSize: 12,
                  decoration: TextDecoration.lineThrough)),
        ),
      ],
    );
  }

  _drawRect4MoreBox() {
    final boxW = getWP(context, 3);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
          child: Container(
            width: boxW,
            height: boxW,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.red, width: 1),
                shape: BoxShape.rectangle,
                color: Color(0xFFC6DFFF)),
          ),
        ),
        SizedBox(width: 2),
        Flexible(
          child: Container(
            width: boxW,
            height: boxW,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.red, width: 1),
                shape: BoxShape.rectangle,
                color: Color(0xFFC6DFFF)),
          ),
        ),
      ],
    );
  }

  _drawSparkLineGraph() {
    var data = [0.0, 1.0, 1.5, 2.0, 0.0, 0.0, -0.5, -1.0, -0.5, 0.0, 0.0];
    return Container(
      width: double.infinity,
      height: getWP(context, 10),
      child: Sparkline(
        data: data,
        fillMode: FillMode.below,
        lineWidth: 1,
        lineColor: Colors.green,
        fillGradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Color(0xFFB5F8C6), Color(0xFFFFFFFF)],
        ),
      ),
    );
  }

  drawSupportTeamUI() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Txt(
            txt: "Video live chat right now",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: true),
        SizedBox(height: 5),
        Card(
          elevation: 5,
          child: Container(
            width: getW(context),
            alignment: Alignment.center,
            height: getHP(context, 12),
            color: Colors.transparent,
            padding: EdgeInsets.only(left: 10, right: 10),
            child: ListView.separated(
              physics: BouncingScrollPhysics(),
              shrinkWrap: true,
              primary: false,
              scrollDirection: Axis.horizontal,
              itemCount: listSupportTeam.length,
              separatorBuilder: (BuildContext context, int index) =>
                  Container(width: 20),
              itemBuilder: (BuildContext context, int index) {
                final map = listSupportTeam[index];
                final name = map['name'];
                final icon = map['icon'];
                final status = map['status'];
                return GestureDetector(
                  child: Container(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: getWP(context, 11),
                        height: getWP(context, 11),
                        padding: EdgeInsets.all(2),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.transparent,
                          border: Border.all(
                            width: 1,
                            color: Colors.red,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: CircleAvatar(
                          radius: 20,
                          backgroundImage: AssetImage(
                            icon,
                          ),
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(0, -12),
                        child: Container(
                          margin: const EdgeInsets.all(0),
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: Colors.red,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 5, right: 5),
                            child: Txt(
                                txt: status,
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .8,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                        ),
                      ),
                      Transform.translate(
                        offset: Offset(0, -7),
                        child: Container(
                          child: Txt(
                              txt: name,
                              txtColor: Color(0xFF656565),
                              txtSize: MyTheme.txtSize - .7,
                              txtAlign: TextAlign.center,
                              isOverflow: true,
                              isBold: false),
                        ),
                      ),
                    ],
                  )),
                  onTap: () {},
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  drawGraphUI() {
    final boxSize = getHP(context, 23);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Txt(
            txt: "Check Live Rates",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: true),
        SizedBox(height: 5),
        Container(
          width: getW(context),
          alignment: Alignment.centerLeft,
          height: boxSize,
          color: Colors.transparent,
          child: ListView.separated(
            physics: BouncingScrollPhysics(),
            shrinkWrap: true,
            primary: false,
            scrollDirection: Axis.horizontal,
            itemCount: listRate.length,
            separatorBuilder: (BuildContext context, int index) =>
                Container(width: 10),
            itemBuilder: (BuildContext context, int index) {
              final map = listRate[index];
              final name = map['name'];
              final icon = map['icon'];
              final price = map['price'];
              final rate1 = map['rate1'];
              final rate2 = map['rate2'];
              final rate3 = map['rate2'];
              return GestureDetector(
                child: Container(
                  color: Colors.transparent,
                  width: boxSize,
                  child: Card(
                    elevation: 5,
                    child: Container(
                        child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(child: Image.asset(icon)),
                          SizedBox(height: 15),
                          _drawLineText(price, rate1),
                          SizedBox(height: 5),
                          _drawLineText(rate2, rate3),
                          SizedBox(height: 5),
                          _drawSparkLineGraph(),
                        ],
                      ),
                    )),
                  ),
                ),
                onTap: () {},
              );
            },
          ),
        ),
      ],
    );
  }

  drawListingItemBorrow() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListView.builder(
            shrinkWrap: true,
            primary: false,
            scrollDirection: Axis.vertical,
            itemCount: listItemBorrow.length,
            itemBuilder: (BuildContext context, int index) {
              final map = listItemBorrow[index];
              final title = map['title'];
              final subTitle = map['subTitle'];
              return Padding(
                padding: const EdgeInsets.only(top: 8),
                child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: getWP(context, 10),
                            height: getWP(context, 10),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color(0xFFF3616B)),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Txt(
                                    txt: title,
                                    txtColor: Color(0xFF434343),
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    fontWeight: FontWeight.w500,
                                    isBold: false),
                                SizedBox(height: 5),
                                Txt(
                                    txt: subTitle,
                                    txtColor: Color(0xFF919191),
                                    txtSize: MyTheme.txtSize - .5,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                              ],
                            ),
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.black54,
                            size: 20,
                          ),
                        ],
                      ),
                    )),
              );
            },
          ),
        ],
      ),
    );
  }

  drawApplyMortgageUI() {
    final boxSize = (getW(context) / 4) - 60;
    final aRatio = getW(context) - 60 / 4;
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Container(
              width: getW(context),
              child: Padding(
                padding: const EdgeInsets.all(10),
                child: Txt(
                    txt: "Apply for a mortgage now",
                    txtColor: Colors.black54,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
            ),
          ),
          SizedBox(height: 2),
          Divider(color: Colors.grey, height: 1),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Container(
              child: GridView.builder(
                shrinkWrap: true,
                primary: false,
                physics: BouncingScrollPhysics(),
                itemCount: 8, //NewCaseCfg.listCreateNewCase.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    //crossAxisSpacing: 4,
                    childAspectRatio: MediaQuery.of(context).size.height /
                        (MediaQuery.of(context).size.height + 100)),
                itemBuilder: (BuildContext context, int index) {
                  if (index < 7) {
                    final map = NewCaseCfg.listCreateNewCase[index];
                    final icon = map["url"];
                    final title = map["title"];
                    final bgColor = map["bgColor"];
                    return Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          new Container(
                            width: boxSize,
                            height: boxSize,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: bgColor),
                            child: Padding(
                              padding: const EdgeInsets.all(1),
                              child: Image.asset(icon, color: brandColor),
                            ),
                          ),
                          SizedBox(height: 10),
                          Txt(
                              txt: title,
                              txtColor: Color(0xFF969696),
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.center,
                              maxLines: 2,
                              isBold: false)
                        ],
                      ),
                    );
                  } else {
                    return Container(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(height: 20),
                        Container(
                            width: boxSize,
                            height: boxSize,
                            child: Column(
                              children: [
                                _drawRect4MoreBox(),
                                SizedBox(height: 2),
                                _drawRect4MoreBox(),
                              ],
                            )),
                        Txt(
                            txt: "More",
                            txtColor: Color(0xFF969696),
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.center,
                            maxLines: 2,
                            isBold: false)
                      ],
                    ));
                  }
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
