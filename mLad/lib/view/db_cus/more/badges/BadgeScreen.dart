import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/PrivacyPolicyAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/controller/api/db_cus/more/badge/BadgeAPIMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/ScanDocData.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/UserBadgeModel.dart';
import 'package:aitl/view/AnimatedListItem.dart';
import 'package:aitl/view/auth/otp/Sms3Screen.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/selfie/selfie_page.dart';
import 'package:aitl/view/db_cus/more/badges/BadgePhotoIDScreen.dart';
import 'package:aitl/view/widgets/btn/BtnOutline.dart';
import 'package:aitl/view/widgets/btn/BtnOutlineBadge.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
import 'package:intl/intl.dart';

class BadgeScreen extends StatefulWidget {
  @override
  State createState() => _BadgeScreenState();
}

class _BadgeScreenState extends State<BadgeScreen> with Mixin {
  List<dynamic> listItems = [];
  List<UserBadgeModel> listUserBadgeModel = [];
  UserBadgeModel userBadgeModel;
  bool isMobileVerified = false;
  bool isEmailVerified = false;
  bool isPrivacyAccepted = false;
  String privacyAcceptedDate = DateTime.now().toString();
  bool isEIDVerified = false;

  updateUserBadges() async {
    if (mounted) {
      BadgeAPIMgr().wsGetUserBadge(
        context: context,
        userId: userData.userModel.id,
        callback: (model) {
          if (model != null) {
            try {
              if (model.success) {
                listUserBadgeModel = model.responseData.userBadges;

                for (int i = 0; i < listUserBadgeModel.length; i++) {
                  debugPrint(
                      "position $i type = " + listUserBadgeModel[i].type);
                  debugPrint("position $i status = " +
                      listUserBadgeModel[i].isVerified.toString());

                  if (listUserBadgeModel[i].type == "Mobile" &&
                      listUserBadgeModel[i].isVerified) {
                    setState(() {
                      isMobileVerified = true;
                      debugPrint("position $i isMobileVerified = " +
                          isMobileVerified.toString());
                    });
                  }

                  if (listUserBadgeModel[i].type == "Email" &&
                      listUserBadgeModel[i].isVerified) {
                    setState(() {
                      isEmailVerified = true;
                    });
                  }
                  if (listUserBadgeModel[i].type == "CustomerPrivacy" &&
                      listUserBadgeModel[i].isVerified) {
                    setState(() {
                      isPrivacyAccepted = true;
                    });
                    setState(() {
                      privacyAcceptedDate =
                          listUserBadgeModel[i].creationDate.toString();
                    });
                  }

                  if (listUserBadgeModel[i].type == "ElectricId" &&
                      listUserBadgeModel[i].isVerified) {
                    setState(() {
                      isEIDVerified = true;
                    });
                  }
                }

                try {
                  setState(() {
                    listItems = [
                      {
                        "icon":
                            "assets/images/screens/db_cus/more/badge/badge_phone_icon.png",
                        "title": "Mobile",
                        "desc":
                            "You will receive this badge when you mobile number is verified. This will also allow you to receive Case related notifications and make calls* from the portal.",
                        "btn": isMobileVerified
                            ? BtnOutlineBadge(
                                txt: "✓ Verified",
                                txtColor: Color(0xFF00C938),
                                borderColor: Color(0xFF263F5D),
                                callback: () {})
                            : BtnOutlineBadge(
                                txt: "Verify",
                                txtColor: Color(0xFF263F5D),
                                borderColor: Color(0xFF263F5D),
                                callback: () async {
                                  String countryCode = "+44";
                                  String countryName = "GB";
                                  var cn = await PrefMgr.shared
                                      .getPrefStr("countryName");
                                  var cd = await PrefMgr.shared
                                      .getPrefStr("countryCode");
                                  if (cn != null &&
                                      cd != null &&
                                      cn.toString().isNotEmpty &&
                                      cd.toString().isNotEmpty) {
                                    setState(() {
                                      countryName = cn;
                                      countryCode = cd;
                                    });
                                  }

                                  Sms2APIMgr().wsLoginMobileOtpPostAPI(
                                    context: context,
                                    countryCode: countryCode,
                                    mobile: userData.userModel.mobileNumber,

                                    // mobile: getFullPhoneNumber(countryCodeTxt: countryCode,number: _phoneController.text.trim()),
                                    callback: (model) {
                                      if (model != null && mounted) {
                                        try {
                                          if (model.success) {
                                            try {
                                              //final msg = model.messages.postUserotp[0].toString();
                                              //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: msg, which: 1);
                                              if (mounted) {
                                                Sms2APIMgr().wsSendOtpNotiAPI(
                                                    context: context,
                                                    otpId: model.responseData
                                                        .userOTP.id,
                                                    callback: (model) {
                                                      if (model != null &&
                                                          mounted) {
                                                        try {
                                                          if (model.success) {
                                                            Get.to(
                                                              () => Sms3Screen(
                                                                isBack: true,
                                                                mobileUserOTPModel:
                                                                    model
                                                                        .responseData
                                                                        .userOTP,
                                                              ),
                                                            );
                                                          } else {
                                                            try {
                                                              if (mounted) {
                                                                final err = model
                                                                    .messages
                                                                    .postUserotp[
                                                                        0]
                                                                    .toString();
                                                                showToast(
                                                                    context:
                                                                        context,
                                                                    msg: err);
                                                              }
                                                            } catch (e) {
                                                              myLog(
                                                                  e.toString());
                                                            }
                                                          }
                                                        } catch (e) {
                                                          myLog(e.toString());
                                                        }
                                                      }
                                                    });
                                              }
                                            } catch (e) {
                                              myLog(e.toString());
                                            }
                                          } else {
                                            try {
                                              if (mounted) {
                                                final err = model
                                                    .messages.postUserotp[0]
                                                    .toString();
                                                showToast(
                                                    context: context, msg: err);
                                              }
                                            } catch (e) {
                                              myLog(e.toString());
                                            }
                                          }
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      }
                                    },
                                  );
                                }),
                      },
                      {
                        "icon":
                            "assets/images/screens/db_cus/more/badge/badge_email_icon.png",
                        "title": "Email",
                        "desc":
                            "You receive this badge upon verification of your email address. This will also allow you to receive case related notifications and send or receive emails from the portal.",
                        "btn": isEmailVerified
                            ? BtnOutlineBadge(
                                txt: "✓ Verified",
                                txtColor: Color(0xFF00C938),
                                borderColor: Color(0xFF263F5D),
                                callback: () {})
                            : BtnOutlineBadge(
                                txt: "Send Email",
                                txtColor: Color(0xFF263F5D),
                                borderColor: Color(0xFF263F5D),
                                callback: () {
                                  //
                                  BadgeAPIMgr().wsPostEmailBadge(
                                      context: context,
                                      callback: (BadgeEmailAPIModel model) {
                                        if (mounted) {
                                          if (model.success) {
                                            showToast(
                                                context: context,
                                                msg:
                                                    "We have sent an email for verification, please check your email",
                                                which: 1);
                                          } else {
                                            showSnake("Alert!",
                                                "Sorry, something went wrong");
                                          }
                                        }
                                      });
                                }),
                      },
                      /*   {
          "icon": "assets/images/screens/db_cus/more/badge/badge_pp_icon.png",
          "title": "Passport / Photo ID",
          "desc":
              "You receive this badge when your ID is verified with your Passport, Driving License or any other acceptable form of ID.",
          "btn": BtnOutline(
              txt: "Add",
              txtColor: Colors.black87,
              borderColor: Colors.black87,
              callback: () {
                Get.to(() => BadgePhotoIDScreen(
                      listUserBadgeModel: listUserBadgeModel,
                    )).then((value) => updateUserBadges());
              }),
        },
        {
          "icon": "assets/images/screens/db_cus/more/badge/badge_fb_icon.png",
          "title": "Facebook",
          "desc":
              "You receive this badge when you connect your Facebook account.",
          "btn": BtnOutline(
              txt: "Add",
              txtColor: Colors.black87,
              borderColor: Colors.black87,
              callback: () {
                //
              }),
        },*/
                      {
                        "icon":
                            "assets/images/screens/db_cus/more/badge/badge_prv_icon.png",
                        "title": "Customer Privacy",
                        "desc":
                            "You receive this badge when you agree to the Customer Privacy Statement. Customer Privacy accepted at ${DateFormat('dd-MMM-yyyy').format(DateTime.parse(privacyAcceptedDate))}.",
                        "btn": isPrivacyAccepted
                            ? BtnOutlineBadge(
                                txt: "✓ Accepted",
                                txtColor: Color(0xFF00C938),
                                borderColor: Color(0xFF263F5D),
                                callback: () {
                                  if (mounted) {
                                    PrivacyPolicyAPIMgr().wsOnLoad(
                                      context: context,
                                      callback: (model) {
                                        if (model != null) {
                                          try {
                                            if (model.success) {
                                              debugPrint(
                                                  "size = ${model.responseData.termsPrivacyNoticeSetupsList.length}");

                                              for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                                                  in model.responseData
                                                      .termsPrivacyNoticeSetupsList) {
                                                if (termsPrivacyPoliceSetups
                                                        .type
                                                        .toString() ==
                                                    "Customer Privacy Notice") {
                                                  Get.to(
                                                          () => PDFDocumentPage(
                                                                title:
                                                                    "Privacy",
                                                                url:
                                                                    termsPrivacyPoliceSetups
                                                                        .webUrl,
                                                              ),
                                                          transition: Transition
                                                              .rightToLeft,
                                                          duration: Duration(
                                                              milliseconds:
                                                                  AppConfig
                                                                      .pageAnimationMilliSecond))
                                                      .then((value) {
                                                    //callback(route);
                                                  });
                                                  break;
                                                }
                                              }
                                            } else {
                                              //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: model.errorMessages.toString());
                                              showToast(
                                                  context: context,
                                                  msg:
                                                      "Sorry, something went wrong");
                                            }
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        } else {
                                          myLog("dashboard screen not in");
                                        }
                                      },
                                    );
                                  }
                                })
                            : BtnOutlineBadge(
                                txt: "View",
                                txtColor: Color(0xFF263F5D),
                                borderColor: Color(0xFF263F5D),
                                callback: () {
                                  if (mounted) {
                                    PrivacyPolicyAPIMgr().wsOnLoad(
                                      context: context,
                                      callback: (model) {
                                        if (model != null) {
                                          try {
                                            if (model.success) {
                                              debugPrint(
                                                  "size = ${model.responseData.termsPrivacyNoticeSetupsList.length}");

                                              for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                                                  in model.responseData
                                                      .termsPrivacyNoticeSetupsList) {
                                                if (termsPrivacyPoliceSetups
                                                        .type
                                                        .toString() ==
                                                    "Customer Privacy Notice") {
                                                  Get.to(
                                                          () => PDFDocumentPage(
                                                                title:
                                                                    "Privacy",
                                                                url:
                                                                    termsPrivacyPoliceSetups
                                                                        .webUrl,
                                                              ),
                                                          transition: Transition
                                                              .rightToLeft,
                                                          duration: Duration(
                                                              milliseconds:
                                                                  AppConfig
                                                                      .pageAnimationMilliSecond))
                                                      .then((value) {
                                                    //callback(route);
                                                  });
                                                  break;
                                                }
                                              }
                                            } else {
                                              //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: model.errorMessages.toString());
                                              showToast(
                                                  context: context,
                                                  msg:
                                                      "Sorry, something went wrong");
                                            }
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        } else {
                                          myLog("dashboard screen not in");
                                        }
                                      },
                                    );
                                  }
                                }),
                      },
                      {
                        "icon":
                            "assets/images/screens/db_cus/more/badge/badge_eid_icon.png",
                        "title": "Electronic ID Verification",
                        "desc":
                            "You receive this badge when your ID is verified through an EID system.",
                        "btn": isEIDVerified
                            ? BtnOutlineBadge(
                                txt: "✓ Verified",
                                txtColor: Color(0xFF00C938),
                                borderColor: Color(0xFF263F5D),
                                callback: () {})
                            : BtnOutlineBadge(
                                txt: "Verify",
                                txtColor: Color(0xFF263F5D),
                                borderColor: Color(0xFF263F5D),
                                callback: () {
                                  //
                                  /*Get.to(
                                      () => WebScreen(
                                          ewebType: eWebType.WEB,
                                          url: Server.EID_URL,
                                          title: "E-ID Verification"),
                                      fullscreenDialog: true,
                                      transition: Transition.rightToLeft,
                                      duration: Duration(
                                          milliseconds: AppConfig
                                              .pageAnimationMilliSecond));*/

                                  Get.to(() => SelfiePage()).then((value) {
                                    StateProvider().notify(ObserverState
                                        .STATE_CHANGED_tabbar1_reload_case_api);
                                  });
                                }),
                      },
                      //"Application tutorial"
                    ];
                  });
                } catch (e) {}
              } else {
                //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: model.errorMessages.toString());
                showToast(context: context, msg: "Sorry, something went wrong");
              }
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            myLog(" badge screen not in");
          }
        },
      );
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    listItems = null;
    listUserBadgeModel = null;
    super.dispose();
  }

  appInit() async {
    try {
      updateUserBadges();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "Badges"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        child: ListView(
          shrinkWrap: true,
          primary: true,
          children: [
            ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: listItems.length,
              itemBuilder: (context, index) {
                bool isOn = false;
                switch (index) {
                  case 0:
                    isOn = isMobileVerified;
                    break;
                  case 1:
                    isOn = isEmailVerified;
                    break;
                  case 2:
                    isOn = isPrivacyAccepted;
                    break;
                  case 3:
                    isOn = isEIDVerified;
                    break;
                  default:
                }
                final item = listItems[index];
                final btn = item['btn'] as BtnOutlineBadge;
                var widgetItem = Card(
                  color: MyTheme.bgColor2,
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      // leading: Image.asset(item['icon']),
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            /*Container(
                                width: getWP(context, 10),
                                height: getWP(context, 10),
                                child: Image.asset(item['icon'])),*/
                            Image.asset(
                              "assets/images/icons/badge_" +
                                  (isOn ? 'on' : 'off') +
                                  ".png",
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              child: Txt(
                                  txt: item["title"],
                                  txtColor: MyTheme.titleColor,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isOverflow: true,
                                  isBold: true),
                            ),
                            IconButton(
                                onPressed: () {
                                  showToolTips(
                                      context: context,
                                      txt:
                                          "Badges are issued when specific requirements are met. A green tick shows that the verification is currently active.");
                                },
                                icon: Icon(
                                  Icons.info,
                                  color: Colors.grey,
                                ))
                          ],
                        ),
                        Txt(
                            txt: item["desc"],
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            //txtLineSpace: 1.4,
                            isBold: false),
                        Align(
                            alignment: Alignment.centerRight,
                            child: btn ?? SizedBox()),
                      ],
                    ),
                  ),
                );

                return AnimatedListItem(
                  index: index,
                  itemDesign: widgetItem,
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
