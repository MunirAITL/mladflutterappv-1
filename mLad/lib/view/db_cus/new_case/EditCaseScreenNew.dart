import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/api/db_cus/new_case/EditCaseAPIMgr.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/controller/helper/db_cus/tab_mycases/CaseDetailsWebHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/EditCaseHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseInfoEntityModelListModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/OtherApplicantSwitchView.dart';
import 'package:aitl/view/widgets/views/SPVSwitchView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownPickerForEditCase.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/controller/api/db_cus/new_case/MortgageCaseInfosAPIMgr.dart';

class EditCaseScreenNew extends StatefulWidget {
  final LocationsModel caseModel;

  const EditCaseScreenNew({
    Key key,
    @required this.caseModel,
  }) : super(key: key);

  @override
  State createState() => _EditCaseScreenState();
}

class _EditCaseScreenState extends State<EditCaseScreenNew> with Mixin {
  //  OtherApplicant input fields
  List<TextEditingController> listOApplicantInputFieldsCtr = [];
  List<FocusNode> listFocusNode = [];

  //  SPVSwitchView input fields
  final _compName = TextEditingController();
  final _regNo = TextEditingController();
  final _localAddress = TextEditingController();
  final focusCompName = FocusNode();
  final focusAddr = FocusNode();
  final focusRegNo = FocusNode();

  String _regAddr = "";
  String regDate = "";

  var title = "";
  var subTitle = "";

  bool isOtherApplicantSwitchShow = false;
  bool isSPVSwitchShow = false;
  bool isOtherApplicantSwitch = false;
  bool isSPVSwitch = false;
  bool pickAddress = false;
  int otherApplicantRadioIndex = 1;
  int totalOtherApplicantRadio = 0;

  Map mapCaseCfg;

  //  dropdown title
  DropListModel caseDD = DropListModel([]);
  OptionItemSelectNewCaseEdit caseOpt;

  MortgageCaseInfoEntityModelListModel mortgageCaseInfoEntityModelListModel;
  List<dynamic> mortgageCaseInfoEntityModelList = [];

  validate() {
    if (isOtherApplicantSwitch) {
      int i = 0;
      while (i < otherApplicantRadioIndex) {
        if (!UserProfileVal().isEmailOK(
            context,
            listOApplicantInputFieldsCtr[i],
            "Invalid " +
                AppDefine.ORDINAL_NOS[i + 1] +
                " applicant email address")) {
          return false;
        }
        i++;
      }
    }
    if (isSPVSwitch) {
      if (UserProfileVal()
          .isEmpty(context, _compName, "Missing company name")) {
        return false;
      } else if (pickAddress) {
        if (_regAddr == "") {
          showToast(
              context: context, msg: "Missing company registered address");
          return false;
        }
      } else if (!pickAddress) {
        _regAddr = _localAddress.text.toString();
        if (_regAddr == "") {
          showToast(
              context: context, msg: "Missing company registered address");
          return false;
        }
      } else if (_regNo.text.isEmpty) {
        showToast(
            context: context, msg: "Missing company registeration number");
        return false;
      } else if (regDate == "") {
        showToast(context: context, msg: "Please select registered date");
        return false;
      }
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    subTitle = null;
    _compName.dispose();
    _regNo.dispose();
    _localAddress.dispose();
    listOApplicantInputFieldsCtr = null;
    listFocusNode = null;
    // mortgageCaseInfoEntityModelListModel = null;
    mapCaseCfg = null;
    caseDD = null;
    caseOpt = null;
    title = null;
    _regAddr = null;
    regDate = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listFocusNode.add(FocusNode());
      listFocusNode.add(FocusNode());
      listFocusNode.add(FocusNode());
    } catch (e) {}

    try {
      MortgageCaseInfosAPIMgr.wsGetUserCaseInfos(
          context: context,
          taskID: widget.caseModel.id.toString(),
          callback: (model) {
            print(
                "Response Model - = ${model.responseData.caseInfosEntityModel.length.toString()}");
            if (model.success) {
              mortgageCaseInfoEntityModelList =
                  model.responseData.caseInfosEntityModel;

              for (var info in mortgageCaseInfoEntityModelList) {
                if (info.userId == widget.caseModel.userId) {
                  mortgageCaseInfoEntityModelListModel = info;
                }
              }

              print(
                  "Response Model - = ${mortgageCaseInfoEntityModelListModel.userId.toString()}");
            } else {
              // print("Response Model not Success - = ${model.toJson()}");
            }
          });
    } catch (e) {
      print("Response error - = ${e.toString()}");
    }

    try {
      title = widget.caseModel.title;
    } catch (e) {}
    try {
      subTitle = widget.caseModel.description;
    } catch (e) {}
    try {} catch (e) {}
    try {
      mapCaseCfg = NewCaseHelper().getCaseByTitle(title);
      if (mapCaseCfg["isOtherApplicant"]) {
        isOtherApplicantSwitchShow = true;
      }
      if (mapCaseCfg["isSPV"]) {
        isSPVSwitchShow = true;
      }
      caseOpt = OptionItemSelectNewCaseEdit(
          id: null, title: title, subId: null, subTitle: subTitle);
    } catch (e) {}
    try {
      int i = 0;
      List<OptionItem> list = [];
      for (var map in NewCaseCfg.listCreateNewCase) {
        list.add(OptionItem(id: (i++).toString(), title: map['title']));
      }
      caseDD = DropListModel(list);
    } catch (e) {}
    try {
      _compName.addListener(() {
        myLog(_compName.text);
      });
      _regNo.addListener(() {
        myLog(_regNo.text);
      });
    } catch (e) {}

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    print("model json get =  ${widget.caseModel.toJson()}");
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      //Get.off
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.brandColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: UIHelper().drawAppbarTitle(title: "Edit Case"),
                ),
                SizedBox(
                  width: 30,
                )
              ],
            ),
          ),
          iconTheme: IconThemeData(color: MyTheme.brandColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      //height: getH(context),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10),
          child: Column(
            children: [
              SizedBox(height: getHP(context, 5)),
              drawCaseType(),
              SizedBox(height: 30),
              // drawCaseNote(),
              SizedBox(height: 30),
              (isOtherApplicantSwitchShow)
                  ? OtherApplicantSwitchView(
                      listOApplicantInputFieldsCtr:
                          listOApplicantInputFieldsCtr,
                      listFocusNode: listFocusNode,
                      isSwitch: isOtherApplicantSwitch,
                      totalOtherApplicantRadio: totalOtherApplicantRadio,
                      callback: (value) {
                        otherApplicantRadioIndex = value;
                        myLog(value);
                      },
                      callbackSwitch: (isSwitch_1) {
                        isOtherApplicantSwitch = isSwitch_1;
                      },
                    )
                  : SizedBox(),
              (isSPVSwitchShow)
                  ? Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: SPVSwitchView(
                        focusCompName: focusCompName,
                        focusAddr: focusAddr,
                        focusRegNo: focusRegNo,
                        compName: _compName,
                        regAddr: _regAddr,
                        regNo: _regNo,
                        regDate: regDate,
                        isSwitch: isSPVSwitch,
                        localAddress: _localAddress,
                        pickAddress: pickAddress,
                        callback: (value) {
                          regDate = value;
                        },
                        callbackSwitch: (isSwitch_2) {
                          isSPVSwitch = isSwitch_2;
                        },
                        callbackAddress: (address) {
                          _regAddr = address;
                          setState(() {});
                        },
                        callbackSwitchAddress: (localAddressPick) {
                          _regAddr = "";
                          pickAddress = localAddressPick;
                        },
                      ),
                    )
                  : SizedBox(),
              Padding(
                padding: const EdgeInsets.only(
                    top: 40, left: 20, right: 20, bottom: 20),
                child: MMBtn(
                  txt: "Update Case",
                  height: getHP(context, 6),
                  width: getW(context),
                  callback: () {
                    if (validate()) {
                      /*      final param = EditCaseHelper().getParam(
                        caseModel: widget.caseModel,
                        isOtherApplicantSwitch: isOtherApplicantSwitch,
                        isSPVSwitch: isSPVSwitch,
                        listOApplicantInputFieldsCtr: listOApplicantInputFieldsCtr,
                        compName: _compName.text.trim(),
                        regAddr: _regAddr,
                        regDate: regDate.trim(),
                        regNo: _regNo.text.trim(),
                        title: caseOpt.title.trim(),
                        note: caseOpt.subTitle.trim(),
                      );*/

                      MortgageCaseInfoEntity mortgageCaseInfoEntity = new MortgageCaseInfoEntity(
                          id: mortgageCaseInfoEntityModelListModel.id,
                          userId: mortgageCaseInfoEntityModelListModel.userId,
                          companyId:
                              mortgageCaseInfoEntityModelListModel.companyId,
                          status: 0,
                          creationDate: widget.caseModel.creationDate,
                          updatedDate: widget.caseModel.updatedDate,
                          versionNumber: widget.caseModel.versionNumber,
                          caseType: "",
                          customerType: "",
                          isSmoker: "NO",
                          isDoYouHaveAnyFinancialDependants: "",
                          remarks: "",
                          isAnyOthers: (isOtherApplicantSwitch) ? "Yes" : "No",
                          taskId: widget.caseModel.entityId,
                          coapplicantUserId: 0,
                          customerName:
                              mortgageCaseInfoEntityModelListModel.customerName,
                          customerEmail: mortgageCaseInfoEntityModelListModel
                              .customerEmail,
                          customerMobileNumber: mortgageCaseInfoEntityModelListModel
                              .customerMobileNumber,
                          customerAddress: (mortgageCaseInfoEntityModelListModel
                                      .customerAddress
                                      .toString() +
                                  ' ' +
                                  mortgageCaseInfoEntityModelListModel.customerAddress1
                                      .toString() +
                                  ' ' +
                                  mortgageCaseInfoEntityModelListModel
                                      .customerAddress2
                                      .toString() +
                                  ' ' +
                                  mortgageCaseInfoEntityModelListModel
                                      .customerAddress3
                                      .toString())
                              .trim(),
                          profileImageUrl: mortgageCaseInfoEntityModelListModel
                              .profileImageUrl,
                          namePrefix:
                              mortgageCaseInfoEntityModelListModel.namePrefix,
                          areYouBuyingThePropertyInNameOfASPV:
                              (isSPVSwitch) ? "Yes" : "No",
                          companyName: _compName.text.trim(),
                          registeredAddress: _regAddr,
                          dateRegistered: regDate,
                          companyRegistrationNumber: _regNo.text.trim(),
                          applicationNumber: null,
                          customerEmail1: listOApplicantInputFieldsCtr[0].text,
                          customerEmail2: listOApplicantInputFieldsCtr[1].text,
                          customerEmail3: listOApplicantInputFieldsCtr[2].text,
                          isDoYouHaveAnyBTLPortfulio: "",
                          isClientAgreement: "",
                          adminFee: 0,
                          adminFeeWhenPayable: "",
                          adviceFee: 0,
                          adviceFeeWhenPayable: "",
                          isFeesRefundable: "",
                          feesRefundable: "",
                          iPAdddress: "",
                          iPLocation: "",
                          deviceType: "",
                          reportLogo: "",
                          officePhoneNumber: "",
                          reportFooter: "",
                          adviceFeeType: "",
                          clientAgreementStatus: "",
                          recommendationAgreementStatus: "",
                          recommendationAgreementSignature: "",
                          isThereAnySavingsOrInvestments: "",
                          isThereAnyExistingPolicy: "",
                          taskTitleUrl: widget.caseModel.taskTitleUrl,
                          customerAddress1: mortgageCaseInfoEntityModelListModel
                              .customerAddress1,
                          customerAddress2: mortgageCaseInfoEntityModelListModel
                              .customerAddress2,
                          customerAddress3: mortgageCaseInfoEntityModelListModel
                              .customerAddress3,
                          customerPostcode:
                              mortgageCaseInfoEntityModelListModel.customerPostcode,
                          customerTown: mortgageCaseInfoEntityModelListModel.customerTown,
                          customerLastName: mortgageCaseInfoEntityModelListModel.customerLastName,
                          customerDateofBirth: mortgageCaseInfoEntityModelListModel.customerDateofBirth,
                          customerGender: mortgageCaseInfoEntityModelListModel.customerGender,
                          customerAreYouASmoker: mortgageCaseInfoEntityModelListModel.customerAreYouASmoker,
                          occupationCode: "");

                      print("mortgageCaseInfoEntity = " +
                          mortgageCaseInfoEntity.toJson().toString());

                      final param = EditCaseHelper(
                          userId: userData.userModel.id,
                          status: 0,
                          creationDate:
                              widget.caseModel.creationDate.toString(),
                          updatedDate: widget.caseModel.updatedDate.toString(),
                          versionNumber: widget.caseModel.versionNumber,
                          title: caseOpt.title.trim(),
                          description: caseOpt.subTitle.trim(),
                          isInPersonOrOnline:
                              widget.caseModel.isInPersonOrOnline,
                          dutDateType: widget.caseModel.dutDateType,
                          deliveryDate: widget.caseModel.deliveryDate,
                          deliveryTime: widget.caseModel.deliveryTime,
                          workerNumber: widget.caseModel.workerNumber,
                          skill: widget.caseModel.skill,
                          isFixedPrice: widget.caseModel.isFixedPrice,
                          hourlyRate: widget.caseModel.hourlyRate,
                          fixedBudgetAmount: widget.caseModel.fixedBudgetAmount,
                          netTotalAmount: widget.caseModel.netTotalAmount,
                          paidAmount: widget.caseModel.paidAmount,
                          dueAmount: widget.caseModel.dueAmount,
                          jobCategory: widget.caseModel.jobCategory,
                          employeeId: widget.caseModel.employeeId,
                          totalBidsNumber: widget.caseModel.totalBidsNumber,
                          isArchive: widget.caseModel.isArchive,
                          preferedLocation: widget.caseModel.preferedLocation,
                          latitude: widget.caseModel.latitude,
                          longitude: widget.caseModel.longitude,
                          ownerName: widget.caseModel.ownerName,
                          thumbnailPath: widget.caseModel.thumbnailPath,
                          remarks: widget.caseModel.remarks,
                          taskReferenceNumber:
                              widget.caseModel.taskReferenceNumber,
                          imageServerUrl: widget.caseModel.imageServerUrl,
                          ownerImageUrl: widget.caseModel.ownerImageUrl,
                          requirements: widget.caseModel.requirements,
                          totalHours: widget.caseModel.totalHours,
                          taskTitleUrl: widget.caseModel.taskTitleUrl,
                          ownerProfileUrl: widget.caseModel.ownerImageUrl,
                          totalAcceptedNumber:
                              widget.caseModel.totalAcceptedNumber,
                          totalCompletedNumber:
                              widget.caseModel.totalCompletedNumber,
                          companyId: widget.caseModel.companyId,
                          companyName: widget.caseModel.companyName,
                          entityId: widget.caseModel.entityId,
                          entityName: widget.caseModel.entityName,
                          /*
                          introducerFeeShareAmount: widget.caseModel.introducerFeeShareAmount,
                          introducerPaymentedAmount: widget.caseModel.introducerPaymentedAmount,
                          adviserAmount: widget.caseModel.adviserAmount,
                          adviserPaymentedAmount: widget.caseModel.adviserPaymentedAmount,*/
                          introducerFeeShareAmount: 0,
                          introducerPaymentedAmount: 0,
                          adviserAmount: 0,
                          adviserPaymentedAmount: 0,
                          notificationUnreadTaskCount:
                              widget.caseModel.notificationUnreadTaskCount,
                          notificationTaskCount:
                              widget.caseModel.notificationTaskCount,
                          addressOfPropertyToBeMortgaged:
                              widget.caseModel.addressOfPropertyToBeMortgaged,
                          overAllCaseGrade: widget.caseModel.overAllCaseGrade,
                          caseObservationAssessmentId:
                              widget.caseModel.caseObservationAssessmentId,
                          namePrefix: widget.caseModel.namePrefix,
                          lastName: widget.caseModel.lastName,
                          caseStatus: widget.caseModel.status,
                          areYouChargingAFee: "No",
                          chargeFeeAmount: 0,
                          chargingFeeWhenPayable: null,
                          chargingFeeRefundable: null,
                          areYouChargingAnotherFee: "No",
                          chargeAnotherFeeAmount: 0,
                          chargingAnotherFeeWhenPayable: null,
                          chargingAnotherFeeRefundable: null,
                          id: widget.caseModel.id,
                          mortgageCaseInfoEntity: mortgageCaseInfoEntity);
                      EditCaseAPIMgr().wsOnPutCase(
                        context: context,
                        param: param,
                        callback: (model) {
                          if (model != null && mounted) {
                            try {
                              final LocationsModel caseModel =
                                  model.responseData.task;

                              Get.to(
                                      () => WebScreen(
                                            title: caseOpt.title,
                                            url: CaseDetailsWebHelper().getLink(
                                              title: title,
                                              taskId: caseModel.id,
                                            ),
                                            caseID: caseModel.id,
                                          ),
                                      transition: Transition.rightToLeft,
                                      duration: Duration(
                                          milliseconds: AppConfig
                                              .pageAnimationMilliSecond))
                                  .then((value) {
                                try {
                                  Navigator.pop(context);
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              });
                            } catch (e) {
                              myLog(e.toString());
                            }
                          }
                        },
                      );
                    }
                  },
                ),
              ),
              SizedBox(height: getHP(context, 10)),
            ],
          ),
        ),
      ),
    );
  }

  drawCaseType() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Case Type",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 10),
            //TxtBox(txt: title, height: 5),
            (caseOpt != null)
                ? DropDownPickerForEditCase(
                    cap: null,
                    bgColor: Colors.white,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    ddTitleSize: 0,
                    ddRadius: 5,
                    itemSelected: caseOpt,
                    dropListModel: caseDD,
                    onOptionSelected: (optionItem) {
                      caseOpt = optionItem;
                      setState(() {});
                    },
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
/*
  drawCaseNote() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Case Note",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 10),
            Container(
              //padding: const EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: TextFormField(
                  controller: _note,
                  minLines: 5,
                  maxLines: 10,
                  autocorrect: false,
                  keyboardType: TextInputType.multiline,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:
                        getTxtSize(context: context, txtSize: MyTheme.txtSize),
                  ),
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: " Case Note",
                    hintStyle: new TextStyle(
                      color: Colors.grey,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                    ),
                    contentPadding: const EdgeInsets.symmetric(vertical: 0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }*/
}
