import 'dart:convert';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/api/db_cus/new_case/PostCaseAPIMgr.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/controller/helper/db_cus/tab_mycases/CaseDetailsWebHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/PostNewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';
import 'package:aitl/model/json/misc/CommonAPIModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/txt/TxtBox.dart';
import 'package:aitl/view/widgets/views/OtherApplicantSwitchView.dart';
import 'package:aitl/view/widgets/views/SPVSwitchView.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class PostNewCaseScreen extends StatefulWidget {
  final int indexCase;
  final int subIndexCase;

  const PostNewCaseScreen(
      {Key key, @required this.indexCase, this.subIndexCase})
      : super(key: key);

  @override
  State createState() => _PostNewCaseScreenState();
}

class _PostNewCaseScreenState extends State<PostNewCaseScreen> with Mixin {
  final TextEditingController _note = TextEditingController();

  //  OtherApplicant input fields
  List<TextEditingController> listOApplicantInputFieldsCtr = [];
  List<FocusNode> listFocusNode = [];

  //  SPVSwitchView input fields
  final _compName = TextEditingController();
  final _regNo = TextEditingController();
  final _localAddress = TextEditingController();
  final focusCompName = FocusNode();
  final focusAddr = FocusNode();
  final focusRegNo = FocusNode();

  String _regAddr = "";
  String regDate = "";

  var title = "";
  String subTitle = "";

  bool isOtherApplicantSwitchShow = false;
  bool isSPVSwitchShow = false;
  bool isOtherApplicantSwitch = false;
  bool isSPVSwitch = false;
  bool pickAddress = false;
  int otherApplicantRadioIndex = 1;

  StateProvider _stateProvider;

  validate() {
    /*if (_note.text.trim().length == 0) {
      showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor, msg: "Please enter case note");
      return false;
    }*/
    if (isOtherApplicantSwitch) {
      int i = 0;
      while (i < otherApplicantRadioIndex) {
        if (!UserProfileVal().isEmailOK(
            context,
            listOApplicantInputFieldsCtr[i],
            "Invalid " +
                AppDefine.ORDINAL_NOS[i + 1] +
                " applicant email address")) {
          return false;
        }
        i++;
      }
    }
    if (isSPVSwitch) {
      if (UserProfileVal()
          .isEmpty(context, _compName, "Missing company name")) {
        return false;
      } else if (pickAddress) {
        if (_regAddr == "") {
          showToast(
              context: context, msg: "Missing company registered address");
          return false;
        }
      } else if (!pickAddress) {
        _regAddr = _localAddress.text.toString();
        if (_regAddr == "") {
          showToast(
              context: context, msg: "Missing company registered address");
          return false;
        }
      } else if (_regNo.text.isEmpty) {
        showToast(
            context: context, msg: "Missing company registeration number");
        return false;
      } else if (regDate == "") {
        showToast(context: context, msg: "Please select registered date");
        return false;
      }
    }
    return true;
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider = null;
    // _note.dispose();
    _compName.dispose();
    _regNo.dispose();
    _localAddress.dispose();
    listOApplicantInputFieldsCtr = null;
    listFocusNode = null;
    _regAddr = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    _stateProvider = new StateProvider();
    try {
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listOApplicantInputFieldsCtr.add(TextEditingController());
      listFocusNode.add(FocusNode());
      listFocusNode.add(FocusNode());
      listFocusNode.add(FocusNode());
    } catch (e) {}
    try {
      final map = NewCaseCfg.listCreateNewCase[widget.indexCase];

      //final icon = map["url"];
      title = map["title"];

      try {
        subTitle = map["subItem"][widget.subIndexCase]["title"];
        print("Subtiled willbe placed here  = " + subTitle);
      } catch (e) {}

      // _note.text = subTitle;

      // _note.text= "subTitle";
      if (map["isOtherApplicant"]) {
        isOtherApplicantSwitchShow = true;
      }
      if (map["isSPV"]) {
        isSPVSwitchShow = true;
      }
    } catch (e) {}
    try {
      _compName.addListener(() {
        myLog(_compName.text);
      });
      _regNo.addListener(() {
        myLog(_regNo.text);
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgGrayColor,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          automaticallyImplyLeading: false,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "Create New"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      //height: getH(context),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.white,
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  children: [
                    SizedBox(height: getHP(context, 5)),
                    drawCaseType(),
                    /*   SizedBox(height: 30),
                drawCaseNote(),*/
                    SizedBox(height: 30),
                    (isOtherApplicantSwitchShow)
                        ? OtherApplicantSwitchView(
                            listOApplicantInputFieldsCtr:
                                listOApplicantInputFieldsCtr,
                            listFocusNode: listFocusNode,
                            isSwitch: isOtherApplicantSwitch,
                            callback: (value) {
                              otherApplicantRadioIndex = value;
                              myLog(value);
                            },
                            callbackSwitch: (isSwitch_1) {
                              isOtherApplicantSwitch = isSwitch_1;
                            },
                          )
                        : SizedBox(),
                    (isSPVSwitchShow)
                        ? Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: SPVSwitchView(
                              focusCompName: focusCompName,
                              focusAddr: focusAddr,
                              focusRegNo: focusRegNo,
                              compName: _compName,
                              regAddr: _regAddr,
                              regNo: _regNo,
                              regDate: regDate,
                              isSwitch: isSPVSwitch,
                              localAddress: _localAddress,
                              pickAddress: pickAddress,
                              callback: (value) {
                                regDate = value;
                              },
                              callbackSwitch: (isSwitch_2) {
                                isSPVSwitch = isSwitch_2;
                              },
                              callbackSwitchAddress: (localAddressPick) {
                                _regAddr = "";
                                pickAddress = localAddressPick;
                              },
                              callbackAddress: (address) {
                                _regAddr = address;
                                setState(() {});
                              },
                            ),
                          )
                        : SizedBox(),
                    SizedBox(height: 40),
                    /*Padding(
                        padding: const EdgeInsets.only(
                            top: 40, left: 20, right: 20, bottom: 50),
                        child: MMBtn(
                            txt: "Import Credit Report",
                            height: getHP(context, 6),
                            width: getW(context),
                            callback: () {})),*/
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 50, left: 20, right: 20, bottom: 20),
              child: MMBtn(
                  txt: "Post Case",
                  height: getHP(context, 6),
                  width: getWP(context, 40),
                  callback: () {
                    if (validate()) {
                      final param = PostNewCaseHelper().getParam(
                        isOtherApplicantSwitch: isOtherApplicantSwitch,
                        isSPVSwitch: isSPVSwitch,
                        listOApplicantInputFieldsCtr:
                            listOApplicantInputFieldsCtr,
                        compName: _compName.text.trim(),
                        regAddr: _regAddr,
                        regDate: regDate.trim(),
                        regNo: _regNo.text.trim(),
                        title: title.trim(),
                        descriptionNote: subTitle,
                        // note: _note.text.trim(),
                      );
                      myLog(json.encode(param));
                      PostCaseAPIMgr().wsOnPostCase(
                        context: context,
                        param: param,
                        callback: (model) async {
                          if (model != null && mounted) {
                            try {
                              final LocationsModel caseModel =
                                  model.responseData.task;
                              //  email notification api call
                              await APIViewModel().req<CommonAPIModel>(
                                context: context,
                                url: Server.CASE_EMAI_NOTI_GET_URL.replaceAll(
                                    "#caseId#", caseModel.id.toString()),
                                isLoading: true,
                                reqType: ReqType.Get,
                              );
                              Get.to(
                                      () => WebScreen(
                                            caseID: caseModel.id.toString(),
                                            title: title,
                                            url: CaseDetailsWebHelper().getLink(
                                              title: title,
                                              taskId: caseModel.id,
                                            ),
                                          ),
                                      transition: Transition.rightToLeft,
                                      duration: Duration(
                                          milliseconds: AppConfig
                                              .pageAnimationMilliSecond))
                                  .then((value) {
                                try {
                                  Get.back();

                                  _stateProvider.notify(
                                      ObserverState.STATE_CHANGED_tabbar2);
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              });
                            } catch (e) {
                              myLog(e.toString());
                            }
                          }
                        },
                      );
                    }
                  }),
            ),
            SizedBox(height: getHP(context, 10)),
          ],
        ),
      ),
    );
  }

  drawCaseType() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Case Type",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 10),
            // TxtBox(txt: title, height: 5),
            Container(
              //height: getHP(context, height),
              width: double.infinity,
              //color: Colors.white,
              decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: Colors.grey),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 20, bottom: 20, left: 5, right: 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      flex: 3,
                      child: Txt(
                        txt: title,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false,
                        maxLines: 5,
                      ),
                    ),
                    // SizedBox(width: 5),
                    subTitle.isNotEmpty
                        ? Flexible(
                            flex: 2,
                            child: Container(
                              padding: EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                color: MyTheme.brandColor,
                                borderRadius: new BorderRadius.only(
                                  topLeft: const Radius.circular(10.0),
                                  topRight: const Radius.circular(10.0),
                                  bottomLeft: const Radius.circular(10.0),
                                  bottomRight: const Radius.circular(10.0),
                                ),
                              ),
                              child: Center(
                                child: Txt(
                                  txt: subTitle,
                                  txtColor: Colors.white,
                                  txtSize: MyTheme.txtSize - .5,
                                  txtAlign: TextAlign.start,
                                  isBold: false,
                                  maxLines: 5,
                                ),
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  drawCaseNote() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Case Note",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
            SizedBox(height: 10),
            Container(
              //padding: const EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: TextFormField(
                  controller: _note,
                  minLines: 5,
                  maxLines: 10,
                  autocorrect: false,
                  keyboardType: TextInputType.multiline,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:
                        getTxtSize(context: context, txtSize: MyTheme.txtSize),
                  ),
                  decoration: new InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: " Case Note",
                    hintStyle: new TextStyle(
                      color: Colors.grey,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                    ),
                    contentPadding: const EdgeInsets.symmetric(vertical: 0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
