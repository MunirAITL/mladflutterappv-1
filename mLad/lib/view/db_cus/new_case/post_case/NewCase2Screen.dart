import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import '../PostNewCaseScreen.dart';
import 'package:get/get.dart';

class NewCase2Screen extends StatefulWidget {
  @override
  State createState() => _NewCase2ScreenState();
}

class _NewCase2ScreenState extends State<NewCase2Screen> with Mixin {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.themeData.accentColor,
        appBar: AppBar(
          centerTitle: true,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: Container(
            width: getW(context),
            //color: Colors.yellow,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  //color: Colors.black,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Row(
                      children: [
                        Icon(Icons.arrow_back, size: 20),
                        Txt(
                            txt: "Back",
                            txtColor: MyTheme.brandColor,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: UIHelper().drawAppbarTitle(title: "Create New"),
                ),
                SizedBox(
                  width: 30,
                )
                /*IconButton(
                  iconSize: 30,
                  icon: Image.asset("assets/images/icons/help_circle_icon.png"),
                  onPressed: () {
                    // do something
                    Get.to(
                      () => WebScreen(
                        title: "Help",
                        url: Server.HELP_INFO_URL,
                      ),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                )*/
              ],
            ),
          ),
          iconTheme: IconThemeData(color: MyTheme.brandColor),
          backgroundColor: MyTheme.themeData.accentColor,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
        //width: double.infinity,
        //height: getH(context),
        child: ListView.builder(
            shrinkWrap: true,
            itemCount: NewCaseCfg.listCreateNewCase.length,
            itemBuilder: (context, index) {
              final Map<String, dynamic> map =
                  NewCaseCfg.listCreateNewCase[index];
              final icon = map["url"];
              final title = map["title"];
              return GestureDetector(
                onTap: () {
                  Get.off(
                    () => PostNewCaseScreen(
                      indexCase: index,
                    ),
                  );
                },
                child: Padding(
                  padding:
                      const EdgeInsets.only(left: 10, right: 10, bottom: 5),
                  child: Card(
                    elevation: 2,
                    //margin: EdgeInsets.symmetric(vertical: 10),
                    //color: Colors.black,
                    //height: getHP(context, 12),
                    /*decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white38,
                          //spreadRadius: 0,
                          //blurRadius: 0,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),*/
                    //color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ListTile(
                            //crossAxisAlignment: CrossAxisAlignment.center,
                            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            //children: [
                            title: Txt(
                                txt: title,
                                txtColor: MyTheme.welcomeTitleColor,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: true),

                            trailing: Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Container(
                                //width: boxW,
                                //height: boxW,
                                child: CircleAvatar(
                                  backgroundImage: AssetImage(icon),
                                  radius: 30,
                                  backgroundColor: Colors.transparent,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 5),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }
}
