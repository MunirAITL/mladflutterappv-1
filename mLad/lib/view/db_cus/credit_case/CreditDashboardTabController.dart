import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/db_cus/credit_case/CreditDashboardAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/credit_case/GetSummeryReportAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/credit_case/creditTabDataControllerLogic.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/model/json/db_cus/credit_case/GetUserValidationOldUserResponse.dart';
import 'package:aitl/model/json/db_cus/credit_case/getSummaryResponse.dart';
import 'package:aitl/view/db_cus/credit_case/TabScreen/CreditAlertsScreen.dart';
import 'package:aitl/view/db_cus/credit_case/TabScreen/CreditEduScreen.dart';
import 'package:aitl/view/db_cus/credit_case/TabScreen/CreditReportScreen.dart';
import 'package:aitl/view/db_cus/credit_case/TabScreen/CreditScoreDashboard.dart';
import 'package:aitl/view/db_cus/credit_case/TabScreen/DisputeStatusScreen.dart';
import 'package:aitl/view/db_cus/credit_case/TabScreen/ScoreSimulatorScreen.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CreditDashBoardTabController extends StatefulWidget {
  final responseData;
  static var validationIdentifier;
  CreditDashBoardTabController({Key key, @required this.responseData}) {
    validationIdentifier =
        responseData.validateNewUser.validateNewUserResult.validationIdentifier;
  }

  @override
  State<CreditDashBoardTabController> createState() =>
      CreditDashBoardTabControllerState();
}

class CreditDashBoardTabControllerState
    extends State<CreditDashBoardTabController>
    with Mixin, StateListener, SingleTickerProviderStateMixin {
  static CreditDashBoardReport creditDashBoardReport;
  static GetSummaryResponseData getSummaryResponse;

  List<Widget> selectedPageList = [
    CreditDashboard(),
    CreditReportScreen(),
    ScoreSimulatorScreen(),
    CreditAlertsScreen(),
    CreditEduScreen(),
    DisputeStatusScreen(),
  ];

  List<CRAddressLinkDetailList> cRAddressLinkDetailList = [];

  List<CRBankAccountList> cRBankAccountList = [];

  List<CRJudgmentList> cRJudgmentList = [];

  List<CRMonthlyStatusDataList> cRMonthlyStatusDataList = [];

  List<CRMonthlyDataList> cRMonthlyDataList = [];

  var creditTabDataControllerLogic = Get.put(CreditTabDataControllerLogic());

  TabController _tabController;

  StateProvider _stateProvider;
  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_full_report_credit_report) {
      setState(() {
        _tabController.index = 1;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      _tabController =
          new TabController(vsync: this, length: selectedPageList.length);
      _tabController.index = 0;
    } catch (e) {}
    try {
      appInitApiCall(context);
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    _tabController.dispose();
    _tabController = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  Future<void> appInitApiCall(BuildContext context) async {
    creditTabDataControllerLogic.isLoadingCreditOverview.value = true;
    await GetSummeryReportApiMgr().getSummeryReport(
        context: context,
        validationIdentifier: widget.responseData.validateNewUser
            .validateNewUserResult.validationIdentifier,
        userId: userData.userModel.id,
        companyId: userData.userModel.userCompanyID,
        callback: (model) {
          if (model != null) {
            getSummaryResponse = model.responseData;
            creditTabDataControllerLogic.isLoadingCreditOverview.value = false;
          } else {
            creditTabDataControllerLogic.isLoadingCreditOverview.value = false;
          }
        });

    await CreditCaseDashboardApiMgr().creditCaseDashboardLoad(
        context: context,
        userIdentifier: widget.responseData.validateNewUser
            .validateNewUserResult.validationIdentifier,
        userID: userData.userModel.id,
        userCompanyId: userData.userModel.userCompanyID,
        callback: (model) {
          print("imaran ${model.responseData.toJson()}");
          if (model != null) {
            creditTabDataControllerLogic.isLoadingDashboardOverview.value =
                false;
            creditDashBoardReport = model.responseData;
          } else {
            creditTabDataControllerLogic.isLoadingDashboardOverview.value =
                false;
          }
        });
    if (mounted)
      setState(() {
        _tabController.index = 0;
      });
  }

  /*@override
  Widget build(BuildContext context) {
/*
    CRElectoralRoll crElectoralRoll = CRElectoralRoll(
        status: 101,
        creationDate: "2021-08-12T09:38:19.477",
        userId: 120979,
        userCompanyId: 1003,
        address: null,
        currentStatus: "Present",
        electoralRollId: "137312854",
        endDate: "1970-01-01T00:00:00",
        name: "Ensom  Muller",
        notices: "",
        startDate: "2021-08-12T09:59:27.067",
        remarks: null,
        id: 2);
    CRUserManage crUserManage = new CRUserManage(
        status: 101,
        creationDate: "2021-08-10T13:58:32.693",
        updatedDate: "2021-08-10T13:58:32.693",
        userId: 120979,
        userCompanyId: 1003,
        userIdentifier: "f756449e-fc9b-40dd-9442-3b9229a9b58c",
        remarks: "Verified",
        reportId: "38559170",
        searchReference: "c26671b5-8efd-47a0-b88d-147840518851",
        ratingValue: 3,
        score: 584,
        id: 4);
    CRAccountHolderDetail accountHolderDetail = new CRAccountHolderDetail(
        userId: 120979,
        status: 101,
        creationDate: "2021-08-12T09:38:20.15",
        userCompanyId: 1003,
        userIdentifier: "17fbe086-3057-40ac-b13f-60df66690594",
        accountHolderId: null,
        accountNumber: "",
        address: "5 , Fisher Lane, Test Town X9 8HJ",
        dateOfBirth: "2021-08-12T09:59:27.297",
        name: "Mr Ensom  Muller",
        startDate: "2018-09-14T00:00:00",
        dateReportedCreated: "2021-08-12T09:59:27.51",
        accountHolderDetailStatus: null,
        searchReference: null,
        remarks: null,
        id: 5);

    CRAddressLinkDetailList crAddressLinkDetails = new CRAddressLinkDetailList(
        status: 101,
        creationDate: "2021-08-12T09:38:18.987",
        userId: 120979,
        userCompanyId: 1003,
        addressFrom: "http://schemas.microsoft.com/2003/10/Serialization/Arrays",
        addressLinkId: "",
        addressTo: null,
        earliestConfirmationOn: null,
        lastConfirmationOn: null,
        mostRecentSource: null,
        notices: null,
        remarks: null,
        id: 2);
    cRAddressLinkDetailList.add(crAddressLinkDetails);

    CRBankAccountList cRBankAccount = new CRBankAccountList(
        userId: 120979,
        status: 101,
        creationDate: "2021-08-12T09:38:30.153",
        userCompanyId: 1003,
        name: "Loan (unspecified type)",
        address: null,
        userIdentifier: "17fbe086-3057-40ac-b13f-60df66690594",
        dateOfBirth: "1970-01-01T00:00:00",
        accountEndDate: null,
        accountId: "2147483561",
        accountNumber: "**************3561 1",
        accountStartDate: "2021-08-12T09:59:27.533",
        accountType: "LN",
        balance: 500,
        currencyCode: "GBP",
        defaultBalance: 0,
        defaultDate: null,
        lenderName: "Test Lender",
        memberPortId: "286",
        minimumPayment: 0,
        openingBalance: 100,
        paymentStartDate: "2021-08-12T09:59:27.833",
        promotionalRate: "false",
        regularPaymentAmount: 0,
        repaymentFrequency: "Monthly",
        statusSubjectiveLevel: "Good",
        updatedDate: "2021-08-12T09:59:28.157",
        cRBankAccountStatus: "Up to date",
        remarks: "false",
        id: 2);
    cRBankAccountList.add(cRBankAccount);

    CRJudgmentList crJudgment = new CRJudgmentList(
        status: 101,
        creationDate: "2021-08-12T09:58:54.41",
        userId: 120979,
        userCompanyId: 1003,
        address: "5 Fisher Lane Test Town X9 8HJ",
        amount: 2222,
        caseNumber: "TEST 7825220",
        casePerId: "8145911",
        courtName: "Testtown",
        courtType: "County Court Judgment",
        currencyCode: "GBP",
        judgmentDate: "2021-08-12T09:59:28.167",
        name: "Ensom  Muller",
        notices: "",
        satisfiedDate: null,
        judgmentStatus: "Active",
        remarks: null,
        id: 2);
    cRJudgmentList.add(crJudgment);

    CRMonthlyStatusDataList cRMonthlyStatusData = new CRMonthlyStatusDataList(
        status: 101,
        creationDate: "2021-08-12T09:43:24.123",
        userId: 120979,
        userCompanyId: 1003,
        accountStatus: "OK",
        month: "10",
        paymentStatus: "0",
        paymentStatusDescription: "account payments up to date",
        year: "2018",
        remarks: null,
        id: 20);
    cRMonthlyStatusDataList.add(cRMonthlyStatusData);

    CRMonthlyDataList crMonthlyData = new CRMonthlyDataList(
        status: 101,
        creationDate: "2021-08-12T09:43:24.157",
        userId: 120979,
        userCompanyId: 1003,
        month: "10",
        monthlyValue: "500",
        year: "2018",
        type: "BalanceHistory",
        remarks: null,
        id: 20);
    cRMonthlyDataList.add(crMonthlyData);

    Report report = new Report(getReportResult: null);

    creditDashBoardReport = new CreditDashBoardReport(
        cRElectoralRoll: crElectoralRoll,
        cRUserManage: crUserManage,
        cRAccountHolderDetail: accountHolderDetail,
        cRAddressLinkDetailList: cRAddressLinkDetailList,
        cRBankAccountList: cRBankAccountList,
        cRFinancialConnectionList: [],
        cRJudgmentList: cRJudgmentList,
        cRMonthlyStatusDataList: cRMonthlyStatusDataList,
        cRMonthlyDataList: cRMonthlyDataList,
        report: report);*/

    return DefaultTabController(
      length: tabs.length,
      //initialIndex: initialIndex,
      // The Builder widget is used to have a different BuildContext to access
      // closest DefaultTabController.
      child: Builder(builder: (BuildContext context) {
        final TabController tabController = DefaultTabController.of(context);
        tabController.index = tabIndex;
      
        tabController.addListener(() {
          tabIndex = tabController.index;
        });

        return Scaffold(
          resizeToAvoidBottomInset: true,
          backgroundColor: MyTheme.themeData.accentColor,
          body: NestedScrollView(headerSliverBuilder:
              (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                bottom: TabBar(
                  unselectedLabelColor: Colors.black87,
                  labelColor: MyTheme.brandColor,
                  indicatorColor: MyTheme.brandColor,
                  tabs: tabs,
                  isScrollable: true,
                ),
                elevation: MyTheme.appbarElevation,
                toolbarHeight: getHP(context, 8),
                backgroundColor: MyTheme.themeData.accentColor,
                iconTheme: IconThemeData(color: MyTheme.brandColor),
                pinned: true,

                //floating: false,
                //snap: true,
                //forceElevated: false,
                centerTitle: true,
                title: Txt(
                    txt: "My Credit Report",
                    txtColor: MyTheme.brandColor,
                    txtSize: MyTheme.appbarTitleFontSize - .4,
                    txtAlign: TextAlign.center,
                    isBold: false),
                actions: <Widget>[],

                /*  bottom: PreferredSize(
                  preferredSize: new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
                  child: (isLoading || isBaseLoading || isCaseReviewLoading || isPrivacyPolicyFetch)
                      ? AppbarBotProgBar(
                          backgroundColor: MyTheme.appbarProgColor,
                        )
                      : SizedBox()),*/
              ),
            ];
          }, body: Obx(() {
            return Container(
              child:
                  creditTabDataControllerLogic.isLoadingCreditOverview.value ||
                          creditTabDataControllerLogic
                              .isLoadingDashboardOverview.value
                      ? Container()
                      : TabBarView(
                          controller: tabController,
                          children: selectedPageList,
                        ),
            );
          })),
        );
      }),
    );
  }*/

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: selectedPageList.length,
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor2,
          appBar: AppBar(
            elevation: MyTheme.appbarElevation,
            iconTheme: IconThemeData(color: Colors.white),
            backgroundColor: MyTheme.statusBarColor,
            title: UIHelper().drawAppbarTitle(title: "My Credit Report"),
            centerTitle: false,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(getHP(context, 6)),
              child: TabBar(
                controller: _tabController,
                isScrollable: true,
                indicatorColor: Colors.white,
                unselectedLabelColor: Colors.grey,
                labelColor: Colors.white,
                /*indicator: UnderlineTabIndicator(
                              borderSide:
                                  BorderSide(width: 5.0, color: Colors.white),
                              insets: EdgeInsets.symmetric(horizontal: 16.0)),*/
                tabs: [
                  Tab(
                      child: Txt(
                          txt: "Dashboard",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                  Tab(
                      child: Txt(
                          txt: "Credit Reports",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                  Tab(
                      child: Txt(
                          txt: "Score Simulator",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                  Tab(
                      child: Txt(
                          txt: "Credit Alerts",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                  Tab(
                      child: Txt(
                          txt: "Credit Education",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                  Tab(
                      child: Txt(
                          txt: "Dispute Status",
                          txtColor: null,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.center,
                          isBold: true)),
                ],
              ),
            ),
          ),
          body: TabBarView(
            physics: AlwaysScrollableScrollPhysics(),
            controller: _tabController,
            children: <Widget>[
              CreditDashboard(),
              selectedPageList[1],
              selectedPageList[2],
              selectedPageList[3],
              selectedPageList[4],
              selectedPageList[5],
            ],
          ),
        ),
      ),
    );
  }
}
