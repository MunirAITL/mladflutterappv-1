import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/db_cus/credit_case/CreditInfoPostAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/PrivacyPolicyAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/settings/EditProfileHelper.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoParms.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoPostResponse.dart';
import 'package:aitl/model/json/db_cus/credit_case/GetUserValidationOldUserResponse.dart';
import 'package:aitl/view/db_cus/credit_case/MultipleChoiceQuestions.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class CreditUserInformation extends StatefulWidget {
  final GetUserValidationOldUser responseData;
  CreditUserInformation({Key key, @required this.responseData})
      : super(key: key);

  @override
  _CreditUserInformationState createState() => _CreditUserInformationState();
}

class _CreditUserInformationState extends State<CreditUserInformation>
    with Mixin {
  EditProfileHelper editProfileHelper;

  //  personal info
  final _fname = TextEditingController();
  final _mname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _postCode = TextEditingController();
  final _address = TextEditingController();
  final _town = TextEditingController();
  final _mobile = TextEditingController();

  String dob = "";
  bool isTermsAndConditionRead = false;

  @override
  void initState() {
    appInit();
    super.initState();
  }

  appInit() async {
    _fname.text = userData.userModel.name;
    _mname.text = userData.userModel.middleName;
    _lname.text = userData.userModel.lastName;
    _email.text = userData.userModel.email;
    _postCode.text = userData.userModel.postcode;
    _address.text = userData.userModel.address;
    _town.text = userData.userModel.town;
    _mobile.text = userData.userModel.mobileNumber;
    dob = userData.userModel.dateofBirth;

    editProfileHelper = EditProfileHelper();
    editProfileHelper.optTitle =
        OptionItem(id: null, title: userData.userModel.namePrefix);

    await editProfileHelper.getCountriesBirth(context: context, cap: "");
    await editProfileHelper.getCountriesResidential(context: context, cap: "");
    await editProfileHelper.getCountriesNationaity(context: context, cap: "");
  }

  validate() {
    if (!UserProfileVal().isFNameOK(context, _fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, _lname)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(context, _email, "Invalid email address")) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, _mobile)) {
      return false;
    } else if (dob.length == 0) {
      showToast(context: context, msg: "Missing date of birth");
      return false;
    } else if (_postCode.text.trim().length == 0) {
      showToast(context: context, msg: "Missing post code");
      return false;
    } else if (_address.text.trim().length == 0) {
      showToast(context: context, msg: "Missing address");
      return false;
    } else if (_town.text.trim().length == 0) {
      showToast(context: context, msg: "Missing town");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(getHP(context, 12)),
            child: Padding(
              padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Txt(
                    txt: "Credit User Information",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize + 1,
                    txtAlign: TextAlign.start,
                    isBold: true,
                  ),
                  SizedBox(height: 10),
                  Container(
                      width: MediaQuery.of(context).size.width,
                      child: Txt(
                        txt: "Please fill in the details below to get started.",
                        txtColor: MyTheme.lightTxtColor,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.left,
                        fontWeight: FontWeight.w300,
                        txtLineSpace: 1.4,
                        isBold: false,
                      )),
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          elevation: 0,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 5, bottom: 10),
              child: Btn(
                  txt: "Next",
                  txtColor: Colors.white,
                  bgColor: (isTermsAndConditionRead)
                      ? MyTheme.brandColor
                      : Colors.grey,
                  width: getW(context),
                  height: 45,
                  callback: () {
                    if (!isTermsAndConditionRead) return;
                    if (!validate()) return;
                    //hide keyboard if is it focus able
                    if (FocusScope.of(context).hasFocus) {
                      FocusScope.of(context).unfocus();
                    }
                    AddressInfo addressInfoParams = AddressInfo(
                        abodeNumber: "1",
                        address1: _address.text.trim(),
                        address2: "",
                        address3: "",
                        postcode: _postCode.text.trim(),
                        town: _town.text.trim());
                    Device deviceParams = Device(
                        blackBox:
                            "04003hQUMXGB0poNf94lis1ztmW5GgQ+xBQ+cvNMV/W52q6osBZcUljnE2vm03g5nuwyXNN+cc",
                        ip: "127.0.0.1");
                    IndividualDetails individualDetails = IndividualDetails(
                        dateOfBirth: dob,
                        email: _email.text.trim(),
                        forename: _fname.text.trim(),
                        middleNames: _mname.text.trim(),
                        phoneNumber: _mobile.text.trim(),
                        surname: _lname.text.trim(),
                        title: editProfileHelper.optTitle.title ?? '',
                        userCompanyId: userData.userModel.userCompanyID,
                        userId: userData.userModel.id);
                    CreditInfoParms creditInfoParams = CreditInfoParms(
                        address: addressInfoParams,
                        device: deviceParams,
                        individualDetails: individualDetails,
                        riskLevel: "CardlessHigh");

                    print("credit information =  ${creditInfoParams.toJson()}");

                    CreditInfoPostApiMgr().creditUserInfoPost(
                        context: context,
                        creditInfoParms: creditInfoParams,
                        callback: (model) {
                          print("model.responseData =  ${model.success}");
                          if (model != null) {
                            CreditInfoPostResponseData creditInfoPostResponse =
                                model.responseData;

                            /*if (creditInfoPostResponse
                                      .validateNewUser
                                      .validateNewUserResult
                                      .identityValidationOutcome ==
                                  CreditReportCfg.IdentityValidationOutcome10) {*/
                            PrefMgr.shared.setPrefStr(
                                "validationIdentifier",
                                creditInfoPostResponse
                                    .validateNewUser
                                    .validateNewUserResult
                                    .validationIdentifier);
                            Get.to(
                                () => MultipleChoiceQuestions(
                                    responseData: widget.responseData),
                                transition: Transition.rightToLeft,
                                duration: Duration(
                                    milliseconds:
                                        AppConfig.pageAnimationMilliSecond));
                            //} else {
                            //showAlert(context: context,msg: "");
                            //}
                          }
                        });

                    /*return;
                      if (_fname.text.isEmpty) {
                        showToast(context: context,
                            txtColor: Colors.white,
                            bgColor: MyTheme.brandColor,
                            msg: "First name should not be empty",
                            which: 1);
                        return;
                      }
                      if (!GetUtils.isEmail(_email.text)) {
                        showToast(context: context,
                            txtColor: Colors.white,
                            bgColor: MyTheme.brandColor,
                            msg: "Email is not valid",
                            which: 1);
                        return;
                      }
                      if (_postCode.text.isEmpty) {
                        showToast(context: context,
                            txtColor: Colors.white,
                            bgColor: MyTheme.brandColor,
                            msg: "Post code should not be empty",
                            which: 1);
                        return;
                      }
                      if (_town.text.isEmpty) {
                        showToast(context: context,
                            txtColor: Colors.white,
                            bgColor: MyTheme.brandColor,
                            msg: "Town name should not be empty",
                            which: 1);
                        return;
                      }
                      if (_mobile.text.isEmpty) {
                        showToast(context: context,
                            txtColor: Colors.white,
                            bgColor: MyTheme.brandColor,
                            msg: "Mobile number should not be empty",
                            which: 1);
                        return;
                      }
                      if (dob.toString().isEmpty) {
                        showToast(context: context,
                            txtColor: Colors.white,
                            bgColor: MyTheme.brandColor,
                            msg: "Date of birth should not be empty",
                            which: 1);
                        return;
                      }
                      if (!isTermsAndConditionRead) {
                        showToast(context: context,
                            txtColor: Colors.white,
                            bgColor: MyTheme.brandColor,
                            msg: "Check the terms and condition",
                            which: 1);
                        return;
                      }*/
                  }),
            ),
          ),
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return Container(
      child: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                    width: getW(context),
                    child: Txt(
                      txt: "Choose Title",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      isBold: true,
                      txtAlign: TextAlign.start,
                    )),
                SizedBox(height: 10),
                DropDownListDialog(
                  context: context,
                  title: editProfileHelper.optTitle.title,
                  ddTitleList: editProfileHelper.ddTitle,
                  callback: (optionItem) {
                    editProfileHelper.optTitle = optionItem;
                    setState(() {});
                  },
                ),
                SizedBox(height: 10),
                inputFieldWithText(
                    controller: _fname,
                    titleText: "First Name",
                    hintText: "First Name"),
                SizedBox(height: 10),
                inputFieldWithText(
                    controller: _mname,
                    titleText: "Middle Name",
                    hintText: "Middle Name"),
                SizedBox(height: 10),
                inputFieldWithText(
                    controller: _lname,
                    titleText: "Last Name",
                    hintText: "First Name"),
                SizedBox(height: 20),
                DatePickerView(
                  txtColor: Colors.black,
                  cap: 'Select date of birth',
                  dt: (dob == '') ? 'Select date of birth' : dob,
                  initialDate: dateDOBlast,
                  firstDate: dateDOBfirst,
                  lastDate: dateDOBlast,
                  callback: (value) {
                    if (mounted) {
                      setState(() {
                        try {
                          dob =
                              DateFormat('dd-MM-yyyy').format(value).toString();
                        } catch (e) {
                          myLog(e.toString());
                        }
                      });
                    }
                  },
                ),
                SizedBox(height: 10),
                inputFieldWithText(
                    controller: _email,
                    titleText: "Email",
                    hintText: "Email Address",
                    keyBoardType: TextInputType.emailAddress),
                SizedBox(height: 10),
                inputFieldWithText(
                    controller: _postCode,
                    titleText: "Post Code",
                    hintText: "Post Code"),
                SizedBox(height: 10),
                inputFieldWithText(
                    controller: _address,
                    titleText: "Address",
                    hintText: "Address"),
                SizedBox(height: 10),
                inputFieldWithText(
                    controller: _town,
                    titleText: "Town",
                    hintText: "Town Name"),
                SizedBox(height: 10),
                inputFieldWithText(
                    controller: _mobile,
                    titleText: "Mobile Number",
                    hintText: "Mobile Number",
                    keyBoardType: TextInputType.number),
                SizedBox(height: 5),
                Txt(
                    txt:
                        "Your email address and mobile number will be used to login to your Client Portal.",
                    txtColor: Colors.grey,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
                SizedBox(height: 10),
                getTermsAndConditionPage(),
                SizedBox(height: 40),
                SizedBox(height: 50),
              ],
            ),
          ),
        ),
      ),
    );
  }

  inputFieldWithText(
      {TextEditingController controller,
      String titleText,
      String hintText,
      var keyBoardType}) {
    if (keyBoardType == null) {
      keyBoardType = TextInputType.text;
    }
    return Container(
      width: getW(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Txt(
              txt: titleText,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          SizedBox(
            height: 10,
          ),
          Container(
            width: getW(context),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border(
                    left: BorderSide(color: Colors.grey, width: 1),
                    right: BorderSide(color: Colors.grey, width: 1),
                    top: BorderSide(color: Colors.grey, width: 1),
                    bottom: BorderSide(color: Colors.grey, width: 1)),
                color: Colors.white),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: new TextFormField(
                    controller: controller,
                    textInputAction: TextInputAction.next,
                    decoration: new InputDecoration(
                      hintText: hintText,
                      hintStyle: TextStyle(color: Colors.grey),
                      fillColor: Colors.black,
                    ),
                    validator: (val) {
                      if (val.length == 0) {
                        return "Name cannot be empty";
                      } else {
                        return null;
                      }
                    },
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: MyTheme.txtSize + 16,
                    ),
                    keyboardType: keyBoardType,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return MyTheme.statusBarColor;
  }

  getTermsAndConditionPage() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 15,
          ),
          Txt(
              txt:
                  "Terms and Conditions and Privacy Notice - you need to read these.",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
          SizedBox(
            height: 15,
          ),
          GestureDetector(
            onTap: () {
              //hide keyboard if is it focus able
              if (FocusScope.of(context).hasFocus) {
                FocusScope.of(context).unfocus();
              }

              setState(() {
                isTermsAndConditionRead
                    ? isTermsAndConditionRead = false
                    : isTermsAndConditionRead = true;
              });
            },
            child: Row(
              children: [
                Checkbox(
                    value: isTermsAndConditionRead,
                    fillColor: MaterialStateProperty.resolveWith(getColor),
                    focusColor: MyTheme.statusBarColor,
                    checkColor: Colors.white,
                    onChanged: (value) {
                      //hide keyboard if is it focus able
                      if (FocusScope.of(context).hasFocus) {
                        FocusScope.of(context).unfocus();
                      }
                      setState(() {
                        isTermsAndConditionRead
                            ? isTermsAndConditionRead = false
                            : isTermsAndConditionRead = true;
                      });
                    }),
                Txt(
                    txt: "I confirm that I have",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40),
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(
                  text: '* Read and agreed to the ',
                  style: TextStyle(
                      height: MyTheme.txtLineSpace,
                      color: Colors.black,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                      fontWeight: FontWeight.normal),
                ),
                TextSpan(
                    text: 'Terms and Conditions',
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        height: MyTheme.txtLineSpace,
                        color: MyTheme.brandColor,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize),
                        fontWeight: FontWeight.bold),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        if (mounted) {
                          PrivacyPolicyAPIMgr().wsOnLoad(
                            context: context,
                            callback: (model) {
                              if (model != null) {
                                try {
                                  if (model.success) {
                                    debugPrint(
                                        "size = ${model.responseData.termsPrivacyNoticeSetupsList.length}");

                                    for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                                        in model.responseData
                                            .termsPrivacyNoticeSetupsList) {
                                      if (termsPrivacyPoliceSetups.type
                                              .toString() ==
                                          "Customer Privacy Notice") {
                                        Get.to(
                                                () => PDFDocumentPage(
                                                      title: "Privacy",
                                                      url:
                                                          termsPrivacyPoliceSetups
                                                              .webUrl,
                                                    ),
                                                transition:
                                                    Transition.rightToLeft,
                                                duration: Duration(
                                                    milliseconds: AppConfig
                                                        .pageAnimationMilliSecond))
                                            .then((value) {
                                          //callback(route);
                                        });
                                        break;
                                      }
                                    }
                                  } else {
                                    //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: model.errorMessages.toString());
                                    showToast(
                                        context: context,
                                        msg: "Sorry, something went wrong");
                                  }
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              } else {
                                myLog("dashboard screen not in");
                              }
                            },
                          );
                        }
                      })
              ]),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 40),
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(
                  text: '* Read the ',
                  style: TextStyle(
                      height: MyTheme.txtLineSpace,
                      color: Colors.black,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                      fontWeight: FontWeight.normal),
                ),
                TextSpan(
                    text: 'privacy policy.',
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        height: MyTheme.txtLineSpace,
                        color: MyTheme.brandColor,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize),
                        fontWeight: FontWeight.bold),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        // navigate to desired screen
                        Get.to(
                                () => PDFDocumentPage(
                                      title: "Privacy",
                                      url:
                                          "https://mortgage-magic.co.uk/assets/img/privacy_policy.pdf",
                                    ),
                                transition: Transition.rightToLeft,
                                duration: Duration(
                                    milliseconds:
                                        AppConfig.pageAnimationMilliSecond))
                            .then((value) {
                          //callback(route);
                        });
                      })
              ]),
            ),
          ),
        ],
      ),
    );
  }
}
