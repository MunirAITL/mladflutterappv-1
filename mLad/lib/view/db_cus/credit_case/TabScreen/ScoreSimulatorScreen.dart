import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/credit_case/CreditDashboardTabController.dart';
import 'package:aitl/view/db_cus/credit_case/widget/ScoreSimulatorScreenWidget/CurrentScoreWidget.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:flutter/material.dart';
import 'base/ScoreSimulatorBase.dart';

class ScoreSimulatorScreen extends StatefulWidget {
  @override
  _ScoreSimulatorScreenState createState() => _ScoreSimulatorScreenState();
}

class _ScoreSimulatorScreenState
    extends ScoreSimulatorBase<ScoreSimulatorScreen> {
  @override
  void initState() {
    super.initState();
    try {
      creditDashBoardReport =
          CreditDashBoardTabControllerState.creditDashBoardReport;
      getSummaryResponse = CreditDashBoardTabControllerState.getSummaryResponse;
      addCCLenderName4DD();
      addLoan4DD();
      addAll4DD();
      addMissedPayment4DD();
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    creditDashBoardReport = null;
    getSummaryResponse = null;
    ddCC = null;
    optCC = null;
    ddLoan = null;
    optLoan = null;
    ddAll = null;
    optAll = null;
    ddMissedPayment = null;
    optMissedPayment = null;
    scrollController.dispose();
    limitCC.dispose();
    balanceCC.dispose();
    limitCCamend.dispose();
    balanceCCamend.dispose();
    balanceLoan.dispose();
    balanceLoanAmend.dispose();
    balanceMortgage.dispose();
    modelSimulated = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyTheme.bgColor2,
      body: SingleChildScrollView(
        controller: scrollController,
        child: Container(
          padding: EdgeInsets.all(10),
          color: Colors.white,
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CurrentStoreWidget(
                    radius: (modelSimulated != null) ? 120 : 160.0,
                    isTxtSmall: (modelSimulated != null) ? true : false,
                    cRUserManage: creditDashBoardReport.cRUserManage,
                    title: "Current Score",
                    scenerioTxt: "As of " +
                        DateFun.getDate(
                            creditDashBoardReport.cRUserManage.creationDate,
                            "dd MMMM yyyy"),
                  ),
                  (modelSimulated != null)
                      ? CurrentStoreWidget(
                          radius: 120.0,
                          isTxtSmall: true,
                          cRUserManage: null,
                          simulatedScore:
                              modelSimulated.responseData.simulatedScore,
                          title: "Simulated Score",
                          scenerioTxt: totalScenerioRunning.toString() +
                              " scenerio running",
                        )
                      : SizedBox(),
                ],
              ),
              SizedBox(height: 20),
              cardItemView(
                context: context,
                iconLeft: Icons.credit_card,
                titleTxt: "Add a new credit card",
                e: eOtp.ADD_NEW_CC,
              ),
              cardItemView(
                context: context,
                iconLeft: Icons.credit_card,
                titleTxt: "Amend an existing credit card",
                e: eOtp.AMEND_EXISTING_CC,
              ),
              cardItemView(
                context: context,
                iconLeft: Icons.price_change,
                titleTxt: "Add a new loan",
                e: eOtp.ADD_NEW_LOAN,
              ),
              cardItemView(
                context: context,
                iconLeft: Icons.home,
                titleTxt: "Amend an existing loan",
                e: eOtp.AMEND_EXISTING_LOAN,
              ),
              cardItemView(
                context: context,
                iconLeft: Icons.home,
                titleTxt: "Add a new mortgage",
                e: eOtp.ADD_NEW_MORTGAGE,
              ),
              cardItemView(
                context: context,
                iconLeft: Icons.home,
                titleTxt: "Amend my electoral register Status",
                e: eOtp.AMEND_ELECTORAL_REG_STATUS,
              ),
              cardItemView(
                context: context,
                iconLeft: Icons.done_all,
                titleTxt: "Add missed payment",
                e: eOtp.ADD_MISSED_PAYMENT,
              ),
              drawAbout(),
              drawBtnReset(callback: () {}, clr: Colors.grey, isArrow: false),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  cardItemView(
      {BuildContext context, iconLeft, String titleTxt, eOtp e, callback}) {
    return Container(
      margin: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black87),
          borderRadius: BorderRadius.all(
            Radius.circular(7),
          )),
      child: ExpansionTile(
        onExpansionChanged: (value) {
          //print(value);
          /*Get.dialog(
            AlrtDialogImran(
                txtColor: Colors.black,
                bgColor: MyTheme.lGrayColor,
                msg: "You account is not authorised to use this feature.",
                which: 1),
            transitionDuration: Duration(milliseconds: 400),
          );*/
        },
        title: Row(
          children: [
            Expanded(
                flex: 1,
                child: Icon(
                  iconLeft,
                  color: Colors.grey,
                  size: 30,
                )),
            Expanded(
                flex: 5,
                child: Txt(
                    txt: "$titleTxt",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false)),
          ],
        ),
        iconColor: Colors.black,
        collapsedIconColor: Colors.black,
        children: <Widget>[
          drawExpandableDiv(e),
        ],
      ),
    );
  }

  drawExpandableDiv(eOtp e) {
    switch (e) {
      case eOtp.ADD_NEW_CC:
        return drawAddNewCreditCard();
        break;
      case eOtp.AMEND_EXISTING_CC:
        return drawAmendCreditCard();
        break;
      case eOtp.ADD_NEW_LOAN:
        return drawAddNewLoan();
        break;
      case eOtp.AMEND_EXISTING_LOAN:
        return drawAmendLoan();
        break;
      case eOtp.ADD_NEW_MORTGAGE:
        return drawAddNewMortgage();
        break;
      case eOtp.AMEND_ELECTORAL_REG_STATUS:
        return drawAmendElectoralRegStatus();
        break;
      case eOtp.ADD_MISSED_PAYMENT:
        return drawAddMissedPayment();
        break;
      default:
        return Container();
    }
  }
}
