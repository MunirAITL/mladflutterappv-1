import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

import '../../../../../Mixin.dart';

abstract class DisputeStatusBase<T extends StatefulWidget> extends State<T>
    with Mixin {
  drawAbout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Txt(
                  txt: "Dispute Status",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
              SizedBox(height: 20),
              Txt(
                  txt:
                      "This page displays the status of any disputes you have raised when you have questioned the information on your credit report.",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              Txt(
                  txt: "We will email you when a dispute has been closed.",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              Txt(
                  txt: "You have no disputes.",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ],
          ),
        ),
      ),
    );
  }
}
