import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/view/db_cus/credit_case/CreditDashboardTabController.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/AddressLinks.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/CIFASWidget.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/CreditReportRating.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/CreditReportSearchHistory.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/FinancialAccountInformation.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/MODAWidget.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/PersonalInfomationWidget.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class CreditReportScreen extends StatefulWidget {
  @override
  State<CreditReportScreen> createState() => _CreditReportScreenState();
}

class _CreditReportScreenState extends State<CreditReportScreen> with Mixin {
  CreditDashBoardReport creditDashBoardReport;

  @override
  Widget build(BuildContext context) {
    creditDashBoardReport =
        CreditDashBoardTabControllerState.creditDashBoardReport;

    return Scaffold(
      backgroundColor: MyTheme.themeData.accentColor,
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              CreditReportRating(
                  cRUserManage: creditDashBoardReport.cRUserManage),
              PersonalInformation(creditDashBoardReport.cRAccountHolderDetail),
              FinancialAccountInformation(),
              MODA(),
              CreditReportSearchHistory(),
              AddressLinks(creditDashBoardReport.cRAddressLinkDetailList),
              CIFAS(),
            ],
          ),
        ),
      ),
    );
  }
}
