import 'dart:io';

import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:pluto_menu_bar/pluto_menu_bar.dart';
import 'base/CreditEduBase.dart';

class CreditEduScreen extends StatefulWidget {
  const CreditEduScreen({Key key}) : super(key: key);
  @override
  State createState() => _CreditEduScreenState();
}

class _CreditEduScreenState extends CreditEduBase<CreditEduScreen> {
  //dynamic callback;

  String title = "Credit Report Information";
  final url = "https://app.mortgage-magic.co.uk/apps/credit-education-report";

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    cookieStr = null;

    //webView = null;
    super.dispose();
  }

  appInit() async {
    try {
      /*CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();*/

      CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();
      myLog(cookieStr);
      if (cookieStr.length > 0) {
        cookieManager.setCookie(
          url: Uri.parse(Server.BASE_URL),
          name: listCookies[0].name,
          value: listCookies[0].value,
          domain: listCookies[0].domain,
          path: listCookies[0].path,
          maxAge: listCookies[0].maxAge,
          //expiresDate: listCookies[0].expires.,
          isSecure: true,
        );
        setState(() {});
      }
    } catch (e) {
      cookieStr = "";
      if (mounted) setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyTheme.bgColor2,
        resizeToAvoidBottomInset: true,
        bottomNavigationBar: BottomAppBar(
          child: PlutoMenuBar(
            textStyle: TextStyle(color: MyTheme.brandColor, fontSize: 12),
            menuIconColor: MyTheme.brandColor,
            menuIconSize: 20,
            menus: [
              MenuItem(
                title: 'Credit basic',
                icon: Icons.arrow_drop_up,
                children: [
                  MenuItem(
                    title: 'Credit Report',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-education-report")));
                    },
                  ),
                  MenuItem(
                    title: 'Credit Score Recipe',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-score-recipe")));
                    },
                  ),
                  MenuItem(
                    title: 'Credit Misconceptions',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-misconceptions")));
                    },
                  ),
                  MenuItem(
                    title: 'How to Get Credit',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-how-to-get-credit")));
                    },
                  ),
                ],
              ),
              MenuItem(
                title: 'Managing your money',
                icon: Icons.arrow_drop_up,
                children: [
                  MenuItem(
                    title: 'Fixing Mistakes',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-fixing-mistakes")));
                    },
                  ),
                  MenuItem(
                    title: 'Budgeting Tips',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-budgeting-tips")));
                    },
                  ),
                  MenuItem(
                    title: 'Savings',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-savings")));
                    },
                  ),
                ],
              ),
              MenuItem(
                title: 'Life events',
                icon: Icons.arrow_drop_up,
                children: [
                  MenuItem(
                    title: 'Planning',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-planning")));
                    },
                  ),
                ],
              ),
              MenuItem(
                title: 'Identity & Safety',
                icon: Icons.arrow_drop_up,
                children: [
                  MenuItem(
                    title: 'Cifas',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-cifas")));
                    },
                  ),
                  MenuItem(
                    title: 'Identity Theft Protection',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-identity-theft-protection")));
                    },
                  ),
                  MenuItem(
                    title: 'Spotting Identity Theft',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-spotting-identity-theft")));
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
        body: Stack(
          children: <Widget>[
            InAppWebView(
              gestureRecognizers: Set()
                ..add(Factory<VerticalDragGestureRecognizer>(
                    () => VerticalDragGestureRecognizer())),
              initialUrlRequest: URLRequest(url: Uri.parse(url)),
              initialOptions: options,
              onWebViewCreated: (controller) {
                webViewController = controller;
              },
              onLoadStart: (controller, url) {
                //setState(() {
                //this.url = url.toString();
                isLoading = true;
                //urlController.text = this.url;
                //});
              },
              androidOnPermissionRequest:
                  (controller, origin, resources) async {
                return PermissionRequestResponse(
                    resources: resources,
                    action: PermissionRequestResponseAction.GRANT);
              },
              shouldOverrideUrlLoading: (controller, navigationAction) async {
                return NavigationActionPolicy.ALLOW;
              },
              onLoadStop: (controller, url) async {
                //setState(() {
                //this.url = url.toString();
                isLoading = false;

                //urlController.text = this.url;
                //});
              },
              onLoadError: (controller, url, code, message) {},
              onProgressChanged: (controller, progress) {
                if (progress == 100) {}
                setState(() {
                  this.progress = progress / 100;
                  //urlController.text = this.url;
                });
              },
              onConsoleMessage: (controller, consoleMessage) {
                print(consoleMessage);
              },
            ),
            isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Stack(),
          ],
        ));
  }
}
