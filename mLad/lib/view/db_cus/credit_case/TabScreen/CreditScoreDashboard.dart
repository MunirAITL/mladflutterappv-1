import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/model/json/db_cus/credit_case/getSummaryResponse.dart';
import 'package:aitl/view/db_cus/credit_case/CreditDashboardTabController.dart';
import 'package:aitl/view/db_cus/credit_case/widget/CreditDashboardWidget/accountAndAlertsWidget.dart';
import 'package:aitl/view/db_cus/credit_case/widget/CreditDashboardWidget/myCreditOverViewWidget.dart';
import 'package:aitl/view/db_cus/credit_case/widget/CreditDashboardWidget/myCreditScoreWidget.dart';
import 'package:aitl/view/db_cus/credit_case/widget/CreditDashboardWidget/myCurrentScoreWidget.dart';
import 'package:aitl/view/db_cus/credit_case/widget/CreditDashboardWidget/myScoreHistoryGraphWidget.dart';
import 'package:flutter/material.dart';

class CreditDashboard extends StatefulWidget {
  const CreditDashboard({Key key}) : super(key: key);

  @override
  _CreditDashboardState createState() => _CreditDashboardState();
}

class _CreditDashboardState extends State<CreditDashboard> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  CreditDashBoardReport creditDashBoardReport;
  GetSummaryResponseData getSummaryResponse;

  @override
  Widget build(BuildContext context) {
    creditDashBoardReport =
        CreditDashBoardTabControllerState.creditDashBoardReport;
    getSummaryResponse = CreditDashBoardTabControllerState.getSummaryResponse;

    return Scaffold(
      backgroundColor: MyTheme.themeData.accentColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Container(
            child: Column(
              children: [
                (creditDashBoardReport != null)
                    ? MyCreditScore(creditDashBoardReport.cRUserManage)
                    : SizedBox(),
                SizedBox(
                  height: 10,
                ),
                (creditDashBoardReport != null)
                    ? MyCreditOverView(
                        creditDashBoardReport, getSummaryResponse)
                    : SizedBox(),
                SizedBox(
                  height: 10,
                ),
                AccountAndAlerts(),
                SizedBox(
                  height: 10,
                ),
                (getSummaryResponse != null)
                    ? MyScoreHistoryGraph()
                    : SizedBox(),
                SizedBox(
                  height: 10,
                ),
                MyCurrentScore()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
