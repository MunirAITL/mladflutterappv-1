import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import 'base/CreditAlertsBase.dart';

class CreditAlertsScreen extends StatefulWidget {
  const CreditAlertsScreen({Key key}) : super(key: key);

  @override
  _CreditAlertsScreenState createState() => _CreditAlertsScreenState();
}

class _CreditAlertsScreenState extends CreditAlertsBase<CreditAlertsScreen> {
  @override
  void initState() {
    super.initState();
    try {} catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyTheme.bgColor2,
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              drawAbout(),
            ],
          ),
        ),
      ),
    );
  }
}
