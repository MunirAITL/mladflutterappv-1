import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';

import 'base/DisputeStatusBase.dart';

class DisputeStatusScreen extends StatefulWidget {
  const DisputeStatusScreen({Key key}) : super(key: key);

  @override
  State createState() => _DisputeStatusScreenState();
}

class _DisputeStatusScreenState extends DisputeStatusBase<DisputeStatusScreen> {
  @override
  void initState() {
    super.initState();
    try {} catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyTheme.bgColor2,
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              drawAbout(),
            ],
          ),
        ),
      ),
    );
  }
}
