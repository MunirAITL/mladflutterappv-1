import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AlertDialogWhereDoYouStand extends StatelessWidget with Mixin {
  const AlertDialogWhereDoYouStand({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getWP(context, 70),
      height: getHP(context, 40),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                //color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt:
                                  "Here’s what your credit score is saying about your credit history.",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt:
                                  "Below is a brief explanation on where you may stand when it comes to obtaining credit.",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt: "Excellent",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt:
                                  "It’s unlikely that your credit applications will be rejected and you’re most likely to be accepted"
                                  " for the best deals on the market. Lenders are likely to see you as a low risk and if you continue making your "
                                  "repayments on time and using your credit sensibly, you’ll be able to maintain your high score."
                                  "",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt: "Good",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt:
                                  "This is a healthy credit score and signals that lenders may view you as low risk."
                                  " You’re more likely to be accepted for credit and have more freedom to choose between different credit providers."
                                  "",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt: "Fair",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt:
                                  "You may occasionally be rejected by lenders and may also find yourself excluded from the very best deals on the market or offered a higher interest rate.",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt: "Poor",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt:
                                  "You may sometimes be rejected for credit. Similar to people with a rating of Very Poor, you may find you are subjected to higher interest rates than most people.",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt: "Very Poor",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(
                            height: 20,
                          ),
                          Txt(
                              txt:
                                  "You’re likely to find it difficult to obtain credit. If you are able to obtain credit, you may find your interest rates are higher than most people’s.",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          /*Container(
            width: MediaQuery.of(context).size.width,
            child: Btn(
                txt: "Ok",
                txtColor: Colors.white,
                bgColor: MyTheme.brandColor,
                width: 100,
                height: 50,
                callback: () {
                  Get.back();
                }),
          )*/
        ],
      ),
    );
  }
}
