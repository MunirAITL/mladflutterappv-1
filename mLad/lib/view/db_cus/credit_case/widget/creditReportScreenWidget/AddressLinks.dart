import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/txt/TxtBox.dart';
import 'package:flutter/material.dart';

class AddressLinks extends StatelessWidget with Mixin {
  List<CRAddressLinkDetailList> cRAddressLinkDetailList;
  AddressLinks(this.cRAddressLinkDetailList, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Container(
                width: getW(context),
                child: Txt(
                    txt: "Address Links",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.left,
                    isBold: true)),
            SizedBox(height: 10),
            Container(
                width: getW(context),
                child: Txt(
                    txt:
                        "Addresses that have been linked to your personal details; "
                        "in most cases these are previous address(es) and/or "
                        "correspondence address(es).",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.left,
                    isBold: false)),
            SizedBox(height: 10),
            Container(
                padding: EdgeInsets.all(10),
                color: HexColor.fromHex("#EDEDED"),
                width: getW(context),
                child: Txt(
                    txt: "Address Details",
                    txtColor: Colors.black54,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.center,
                    isBold: true)),
            for (var model in cRAddressLinkDetailList)
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(
                    width: getW(context),
                    child: Card(
                      elevation: 1,
                      color: Colors.white,
                      child: Txt(
                          txt: model.addressFrom ?? '',
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.left,
                          isBold: false),
                    )),
              ),
            SizedBox(height: 10),
            Container(
                padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                color: HexColor.fromHex("#EDEDED"),
                width: getW(context),
                child: Txt(
                    txt: "Link Details",
                    txtColor: Colors.black54,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: true)),
            for (var model in cRAddressLinkDetailList)
              Container(
                padding: const EdgeInsets.all(5),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey, width: .5)),
                child: Column(
                  children: [
                    fromItem(context, "From", model.addressFrom ?? ''),
                    fromItem(context, "Most recent source of link",
                        model.mostRecentSource ?? ''),
                    fromItem(context, "Last confirmed",
                        model.lastConfirmationOn ?? ''),
                    fromItem(context, "Earliest confirmation",
                        model.earliestConfirmationOn ?? ''),
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }

  fromItem(context, leftTxt, rightTxt) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: Container(
                      child: Txt(
                          txt: "$leftTxt",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .3,
                          txtAlign: TextAlign.start,
                          isBold: true))),
              SizedBox(width: 10),
              Expanded(
                  child: Container(
                      child: Txt(
                          txt: rightTxt ?? '',
                          txtColor: Colors.black87,
                          txtSize: MyTheme.txtSize - .4,
                          txtAlign: TextAlign.start,
                          isBold: false)))
            ],
          ),
          (leftTxt == "From")
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Btn(
                      txt: "Raise a dispute",
                      txtColor: Colors.white,
                      txtSize: 1.5,
                      bgColor: Colors.blue,
                      width: null,
                      height: getHP(context, 5),
                      radius: 5,
                      callback: () {}),
                )
              : SizedBox()
        ],
      ),
    );
  }
}
