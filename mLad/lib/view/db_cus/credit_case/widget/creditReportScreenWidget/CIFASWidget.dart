import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CIFAS extends StatelessWidget with Mixin {
  const CIFAS({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 10,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Container(
                width: getW(context),
                child: Txt(
                    txt: "CIFAS",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: getW(context),
                child: RichText(
                  textAlign: TextAlign.start,
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                      text:
                          'Details of any warnings registered at your address(es) by Cifas, the UK’s Fraud Prevention Service.\n\n'
                          'Cifas is the UK’s fraud prevention service. If you have taken out Protective Registration with Cifas,'
                          ' or one of the organisations that works with Cifas has registered you as a victim of fraud,'
                          ' this information will appear on your report. This information does not affect your credit rating,'
                          ' it acts as a warning that you could be at higher risk of identity theft and prompts'
                          ' organisations working with Cifas to take extra care with applications in your name.\n\n'
                          'To find out more about Cifas, visit ',
                      style: TextStyle(
                          height: MyTheme.txtLineSpace,
                          color: Colors.black,
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize),
                          fontWeight: FontWeight.normal),
                    ),
                    TextSpan(
                        text: 'www.cifas.org.uk',
                        style: TextStyle(
                            decoration: TextDecoration.underline,
                            height: MyTheme.txtLineSpace,
                            color: MyTheme.brandColor,
                            fontSize: getTxtSize(
                                context: context, txtSize: MyTheme.txtSize),
                            fontWeight: FontWeight.bold),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                                    () => WebScreen(
                                          title: "cifas",
                                          url: "https://www.cifas.org.uk/",
                                        ),
                                    transition: Transition.rightToLeft,
                                    duration: Duration(
                                        milliseconds:
                                            AppConfig.pageAnimationMilliSecond))
                                .then((value) {
                              //callback(route);
                            });
                          }),
                    TextSpan(
                      text:
                          '\n\nThere are no Cifas warnings registered at your address(es).',
                      style: TextStyle(
                          height: MyTheme.txtLineSpace,
                          color: Colors.black,
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize),
                          fontWeight: FontWeight.normal),
                    ),
                  ]),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ));
  }
}
