import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class CreditReportRating extends StatelessWidget with Mixin {
  final CRUserManage cRUserManage;
  const CreditReportRating({Key key, @required this.cRUserManage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ratingValue = cRUserManage.ratingValue ?? 0;

    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
      child: Column(
        children: [
          Container(color: Colors.green.shade300, height: 3.5),
          Container(
            color: HexColor.fromHex("#80ff00").withOpacity(.3),
            child: Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
              child: Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Txt(
                            txt: "Credit Rating",
                            txtColor: Colors.green,
                            txtSize: MyTheme.txtSize - .4,
                            txtAlign: TextAlign.start,
                            isBold: true),
                      ),
                      IconButton(
                          onPressed: () {
                            showToolTips(
                                context: context, txt: "Credit Rating");
                          },
                          icon: Icon(
                            Icons.info_outlined,
                            color: Colors.green,
                          )),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Row(
                          children: [
                            for (int i = 0; i < 5; i++)
                              Padding(
                                padding: const EdgeInsets.only(right: 5),
                                child: Container(
                                  width: 15,
                                  height: 15,
                                  decoration: BoxDecoration(
                                    color: (i < ratingValue)
                                        ? Colors.green
                                        : Colors.white, // border color
                                    shape: BoxShape.circle,
                                  ),
                                ),
                              ),
                          ],
                        ),
                      ),
                      Text.rich(
                        TextSpan(
                          children: [
                            TextSpan(
                                text: ratingValue.toString() ?? '',
                                style: TextStyle(
                                    color: Colors.green,
                                    fontSize: 17,
                                    fontWeight: FontWeight.bold)),
                            TextSpan(
                              text: '/5',
                              style: TextStyle(
                                color: Colors.green,
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Container(
                    color: Colors.green,
                    height: 1,
                  ),
                  SizedBox(height: 10),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Txt(
                            txt: "Credit Score",
                            txtColor: Colors.green,
                            txtSize: MyTheme.txtSize - .4,
                            txtAlign: TextAlign.start,
                            isBold: true),
                      ),
                      Flexible(
                        child: Txt(
                            txt: cRUserManage.score.toString() ?? '0',
                            txtColor: Colors.green,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: true),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
