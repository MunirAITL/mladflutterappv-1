import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class MODA extends StatelessWidget with Mixin {
  const MODA({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 10,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Container(
                width: getW(context),
                child: Txt(
                    txt: "MODA",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize + .2,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: getW(context),
                child: Txt(
                    txt:
                        "MODA is a database of credit account Information held by TransUnion which is updated on a more timely basis."
                        " MODA shows recent significant events that will affect your credit file.It will therefore help enable"
                        " lenders to make more timely and accurate assessments of credit risk,"
                        " affordability and fraud risk. Where available MODA data is submitted"
                        " to TransUnion and updated on a daily basis.\n\nPlease note, that any accounts"
                        " showing in this section will also appear in the Financial Account"
                        " Information section. This allows lenders to see"
                        " the daily updates for a short period of time (accounts remain in the"
                        " MODA section for 50 days) while seeing the"
                        " information on a more long-term basis through Financial Account"
                        " Information (where accounts remain for six years).\n\nYou have no MODA data on your report.",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ));
  }
}
