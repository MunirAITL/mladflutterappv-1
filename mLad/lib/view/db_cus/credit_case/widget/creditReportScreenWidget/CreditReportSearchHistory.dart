import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/OtherSeaches.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/YourSearches.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class CreditReportSearchHistory extends StatefulWidget {
  @override
  State<CreditReportSearchHistory> createState() =>
      _CreditReportSearchHistoryState();
}

class _CreditReportSearchHistoryState extends State<CreditReportSearchHistory>
    with Mixin {
  bool isOthersSearches = true;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Container(
        padding: EdgeInsets.all(10),
        width: getW(context),
        child: Column(
          children: [
            Container(
                width: getW(context),
                child: Txt(
                    txt: "Credit Report Search History",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true)),
            SizedBox(height: 10),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Btn(
                      txt: "Other's Searches",
                      txtSize: 1.5,
                      width: getWP(context, 45),
                      height: getHP(context, 5),
                      bgColor: (isOthersSearches)
                          ? MyTheme.statusBarColor
                          : Colors.grey,
                      txtColor:
                          (isOthersSearches) ? Colors.white : Colors.black,
                      callback: () {
                        setState(() {
                          isOthersSearches = true;
                        });
                      }),
                ),
                Flexible(
                  child: Btn(
                      txt: "Your Searches",
                      txtSize: 1.5,
                      width: getWP(context, 45),
                      height: getHP(context, 5),
                      bgColor: (!isOthersSearches)
                          ? MyTheme.statusBarColor
                          : Colors.grey,
                      txtColor:
                          (!isOthersSearches) ? Colors.white : Colors.black,
                      callback: () {
                        setState(() {
                          isOthersSearches = false;
                        });
                      }),
                ),
              ],
            ),
            SizedBox(height: 20),
            Txt(
                txt: isOthersSearches
                    ? "Searches on your details through TransUnion within the last 24 months. These searches may be seen by third parties when they check your details with TransUnion."
                    : "Searches made by you on your Credit Report through TransUnion within the last 24 months. These searches will not be seen by lenders when you make an application for credit.",
                txtColor: Colors.black87,
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
            isOthersSearches ? OtherSeaches() : YourSeaches()
          ],
        ),
      ),
    );
  }
}
