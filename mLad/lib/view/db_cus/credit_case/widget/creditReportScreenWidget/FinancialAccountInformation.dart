import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/ClosedAccounts.dart';
import 'package:aitl/view/db_cus/credit_case/widget/creditReportScreenWidget/OpenAccount.dart';
import 'package:aitl/view/widgets/btn/BSBtn.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class FinancialAccountInformation extends StatefulWidget {
  @override
  State<FinancialAccountInformation> createState() =>
      _FinancialAccountInformationState();
}

class _FinancialAccountInformationState
    extends State<FinancialAccountInformation> with Mixin {
  bool isOpenAccount = true;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Container(
        padding: EdgeInsets.all(10),
        width: getW(context),
        child: Column(
          children: [
            Container(
                width: getW(context),
                child: Txt(
                    txt: "Financial Account Information",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true)),
            SizedBox(height: 10),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Flexible(
                  child: Btn(
                      txt: "Open Accounts",
                      txtSize: 1.5,
                      width: getWP(context, 45),
                      height: getHP(context, 5),
                      bgColor: (isOpenAccount)
                          ? MyTheme.statusBarColor
                          : Colors.grey,
                      txtColor: (isOpenAccount) ? Colors.white : Colors.black,
                      callback: () {
                        setState(() {
                          isOpenAccount = true;
                        });
                      }),
                ),
                Flexible(
                  child: Btn(
                      txt: "Closed Accounts",
                      txtSize: 1.5,
                      width: getWP(context, 45),
                      height: getHP(context, 5),
                      bgColor: (!isOpenAccount)
                          ? MyTheme.statusBarColor
                          : Colors.grey,
                      txtColor: (!isOpenAccount) ? Colors.white : Colors.black,
                      callback: () {
                        setState(() {
                          isOpenAccount = false;
                        });
                      }),
                ),
              ],
            ),
            SizedBox(height: 10),
            isOpenAccount ? OpenAccounts() : ClosedAccounts()
          ],
        ),
      ),
    );
  }
}
