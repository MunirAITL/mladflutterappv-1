import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class PersonalInformation extends StatelessWidget with Mixin {
  CRAccountHolderDetail cRAccountHolderDetail;

  PersonalInformation(this.cRAccountHolderDetail, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 10,
        child: Container(
          padding: EdgeInsets.all(10),
          width: getW(context),
          child: Column(
            children: [
              Container(
                width: getW(context),
                child: Txt(
                    txt: "Personal Information",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: getW(context),
                child: Txt(
                    txt:
                        "These are the details you gave us when you asked for your credit report. We’ve used this information to provide your report.",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize - .3,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              userInfoItem(title: "Name", value: cRAccountHolderDetail.name),
              userInfoItem(
                  title: "Date of birth",
                  value: DateFormat('dd-MMM-yyyy').format(
                      DateTime.parse(cRAccountHolderDetail.dateOfBirth))),
              userInfoItem(
                  title: "Date report created",
                  value: DateFormat('dd-MMM-yyyy').format(
                      DateTime.parse(cRAccountHolderDetail.creationDate))),
              userInfoItem(
                  title: "Search reference",
                  value: cRAccountHolderDetail.searchReference ?? ''),
              userInfoItem(
                  title: "Current address",
                  value: cRAccountHolderDetail.address +
                      cRAccountHolderDetail.address),
              SizedBox(
                height: 20,
              )
            ],
          ),
        ));
  }

  userInfoItem({String title, String value}) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Txt(
              txt: "$title: ",
              txtColor: Colors.black87,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: true),
          Expanded(
              child: Txt(
                  txt: "$value",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .3,
                  txtAlign: TextAlign.start,
                  isBold: false))
        ],
      ),
    );
  }
}
