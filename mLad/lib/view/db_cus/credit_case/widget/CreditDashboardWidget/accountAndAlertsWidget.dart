import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/credit_case/AlertDialog/AlertDialogAccountAndAlerts.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AccountAndAlerts extends StatelessWidget with Mixin {
  const AccountAndAlerts({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getW(context),
      child: Card(
        elevation: 10,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              GestureDetector(
                onTap: () {
                  var txt =
                      "The following information is presented on this panel.\n\n";
                  txt += "• The number of days to the next report\n\n";
                  txt += "• The expiry date of the account\n\n";
                  txt +=
                      "• Information about credit alerts, total number and any unread alerts";
                  showToolTips(context: context, txt: txt);

                  /*Get.dialog(
                    AlertDialogAccountAndAlerts(
                      titleTxt:
                          "The following information is presented on this panel.",
                      bodyTxt1: "The number of days to the next report",
                      bodyTxt2: "The expiry date of the account",
                      bodyTxt3:
                          "Information about credit alerts, total number and any unread alerts",
                    ),
                    transitionDuration: Duration(milliseconds: 400),
                  );*/
                },
                child: Row(
                  children: [
                    Icon(
                      Icons.warning,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Txt(
                        txt: "Account and Alerts",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: true),
                    SizedBox(
                      width: 10,
                    ),
                    Icon(
                      Icons.info_outlined,
                      color: Colors.grey,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: getW(context),
                child: Padding(
                  padding: const EdgeInsets.only(left: 28.0),
                  child: Txt(
                      txt: "Your next credit report is due in 15 days",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: getW(context),
                child: Padding(
                  padding: const EdgeInsets.only(left: 28.0),
                  child: Txt(
                      txt: "Account expiry date: 20 Jun 2021",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ),
              SizedBox(
                height: 10,
              )
            ],
          ),
        ),
      ),
    );
  }
}
