import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/model/json/db_cus/credit_case/getSummaryResponse.dart';
import 'package:aitl/view/db_cus/credit_case/AlertDialog/AlertDialogWhereDoYouStand.dart';
import 'package:aitl/view/db_cus/credit_case/Controller/ScoreHelper.dart';
import 'package:aitl/view/db_cus/credit_case/widget/CreditDashboardWidget/getProgressItemWidget.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

// ignore: must_be_immutable
class MyCreditScore extends StatelessWidget with Mixin {
  CRUserManage cRUserManage;

  MyCreditScore(this.cRUserManage, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Row(
        children: [
          //start let side
          Expanded(
              flex: 1,
              child: Container(
                height: 305,
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                          width: getW(context),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                            child: GestureDetector(
                              onTap: () {
                                showToolTips(
                                    context: context,
                                    txt:
                                        "Your credit score is a numerical representation of your credit history. It’s calculated and influenced by the information within your credit report over the last 6 years.");
                                /*showToast(context: context,
                                    bgColor: MyTheme.brandColor,
                                    msg:
                                        "Your credit score is a numerical representation of your credit history. It’s calculated and influenced by the information within your credit report over the last 6 years.",
                                    txtColor: Colors.white,
                                    isToast: false,
                                    which: 1);*/
                              },
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Txt(
                                      txt: "My Credit Score",
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize - .2,
                                      txtAlign: TextAlign.left,
                                      isBold: true),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Icon(
                                    Icons.info_outlined,
                                    color: Colors.grey,
                                  )
                                ],
                              ),
                            ),
                          )),
                    ),
                    Expanded(
                      flex: 5,
                      child: Container(
                        width: getW(context),
                        child: new CircularPercentIndicator(
                          radius: 160.0,
                          animation: true,
                          animationDuration: 1200,
                          lineWidth: 15.0,
                          percent:
                              (cRUserManage.score / 710) /*.toPrecision(2)*/,
                          center: Txt(
                              txt: cRUserManage.score.toString(),
                              txtColor: Colors.green,
                              txtSize: MyTheme.txtSize + 2,
                              txtAlign: TextAlign.center,
                              isBold: false),
                          circularStrokeCap: CircularStrokeCap.butt,
                          backgroundColor: Colors.grey,
                          progressColor: ScoreHelper()
                              .getCircleBarColor(cRUserManage.score),
                        ),
                      ),
                    ),
                  ],
                ),
              )),
          //start right side
          Expanded(
              flex: 1,
              child: Container(
                height: 305,
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                          width: getW(context),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0, top: 8),
                            child: GestureDetector(
                              onTap: () {
                                showToolTips(
                                    context: context,
                                    w: AlertDialogWhereDoYouStand());
                                /*Get.dialog(
                                  AlertDialogWhereDoYouStand(),
                                  transitionDuration:
                                      Duration(milliseconds: 400),
                                );*/
                              },
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Expanded(
                                      child: Txt(
                                          txt: "Where Do You Stand",
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .2,
                                          txtAlign: TextAlign.left,
                                          isBold: true)),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Icon(
                                    Icons.info_outlined,
                                    color: Colors.grey,
                                  )
                                ],
                              ),
                            ),
                          )),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        children: [
                          GetProgressItem(
                              status: "Excellent",
                              value: "628-710",
                              statusColor: "#128B49"),
                          GetProgressItem(
                              status: "Good",
                              value: "604-627",
                              statusColor: "#55A84E"),
                          GetProgressItem(
                              status: "Fair",
                              value: "566-603",
                              statusColor: "#FAD937"),
                          GetProgressItem(
                              status: "Poor",
                              value: "511-565",
                              statusColor: "#F17731"),
                          GetProgressItem(
                              status: "Very poor",
                              value: "0-550",
                              statusColor: "#C41C2B"),
                        ],
                      ),
                    )
                  ],
                ),
              )),
        ],
      ),
    );
  }
}
