import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/model/json/db_cus/credit_case/getSummaryResponse.dart';
import 'package:aitl/view/db_cus/credit_case/CreditDashboardTabController.dart';
import 'package:aitl/view/db_cus/credit_case/widget/CreditDashboardWidget/showCreditOverViewItemWidget.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MyCreditOverView extends StatelessWidget with Mixin {
  CreditDashBoardReport creditDashBoardReport;
  GetSummaryResponseData getSummaryResponse;

  MyCreditOverView(this.creditDashBoardReport, this.getSummaryResponse,
      {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var totalBalance = 0.0;
    if (getSummaryResponse.summaryReport != null) {
      for (var report in getSummaryResponse
          .summaryReport.getSummaryReportResult.metrics.summaryReportMetric) {
        if (report.title == "Short Term Debt" ||
            report.title == "Long Term Debt") {
          totalBalance += double.parse(report.value).round();
        }
      }
    }

    //totalBalance = totalBalance + subBalance;

    return (creditDashBoardReport.cRBankAccountList.length > 0)
        ? Card(
            elevation: 10,
            child: Column(
              children: [
                Container(
                    width: getW(context),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                      child: GestureDetector(
                        onTap: () {
                          showToolTips(
                              context: context,
                              txt:
                                  "Your credit report at a glance. For more detail, or to dispute any inaccuracies, please see your credit report.\n\n Remember, your current balances represent the combined sum of short term and long-term debt.");
                          /*showToast(context: context,
                              bgColor: MyTheme.brandColor,
                              msg:
                                  "Your credit report at a glance. For more detail, or to dispute any inaccuracies, please see your credit report.\n\n Remember, your current balances represent the combined sum of short term and long-term debt.",
                              txtColor: Colors.white,
                              isToast: false,
                              which: 1);*/
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.apps_outlined,
                              color: Colors.black,
                            ),
                            SizedBox(width: 10),
                            Txt(
                                txt: "My Credit Overview",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.left,
                                isBold: true),
                            SizedBox(
                              width: 5,
                            ),
                            Icon(
                              Icons.info_outlined,
                              color: Colors.grey,
                            )
                          ],
                        ),
                      ),
                    )),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Card(
                    elevation: 20,
                    child: Container(
                      decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Colors.black54,
                            blurRadius: 15.0,
                            offset: Offset(5.0, 0),
                          )
                        ],
                        color: HexColor.fromHex("#00A6CA"),
                      ),
                      width: 250,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 15,
                          ),
                          Txt(
                              txt: "Current Balances",
                              //"${creditDashBoardReport.cRBankAccountList[0].name}",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: true),
                          SizedBox(
                            height: 15,
                          ),
                          Txt(
                              txt: AppDefine.CUR_SIGN +
                                  " " +
                                  totalBalance.toStringAsFixed(0),
                              //"${creditDashBoardReport.cRBankAccountList[0].balance.toString()} ${creditDashBoardReport.cRBankAccountList[0].currencyCode.toString()}",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: true),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                            color: Colors.white,
                            width: double.infinity,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Txt(
                                  txt:
                                      "${DateFormat('dd MMMM yyyy').format(DateTime.parse(creditDashBoardReport.cRBankAccountList[0].updatedDate.toString())).toString()}",
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                (getSummaryResponse.summaryReport != null)
                    ? GridView.count(
                        crossAxisCount: 2,
                        crossAxisSpacing: 4.0,
                        mainAxisSpacing: 8.0,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        children: List.generate(
                            getSummaryResponse
                                .summaryReport
                                .getSummaryReportResult
                                .metrics
                                .summaryReportMetric
                                .length, (index) {
                          return ShowCreditOverViewItem(
                              firstTitle: getSummaryResponse
                                  .summaryReport
                                  .getSummaryReportResult
                                  .metrics
                                  .summaryReportMetric[index]
                                  .title,
                              firstPrice: getSummaryResponse
                                  .summaryReport
                                  .getSummaryReportResult
                                  .metrics
                                  .summaryReportMetric[index]
                                  .value);
                        }),
                      )
                    : SizedBox(),
                // ShowCreditOverViewItem(firstTitle: "Short Term Debt", firstPrice: "£ 1,356", secondTitle: "Long Term Debt", secondPrice: "£ 207,205"),
                // ShowCreditOverViewItem(firstTitle: "County Court Judgments", firstPrice: "0", secondTitle: "Accounts in arrears", secondPrice: "0"),
                // ShowCreditOverViewItem(firstTitle: "Present on Electoral Register?", firstPrice: "Y", secondTitle: "Accounts opened in last 2 years", secondPrice: "3"),
                SizedBox(
                  height: 15,
                ),
                GestureDetector(
                  onTap: () {
                    StateProvider().notify(
                        ObserverState.STATE_CHANGED_full_report_credit_report);
                  },
                  child: Text(
                    'View Full Report',
                    style: TextStyle(
                      fontSize: MyTheme.txtSize + 12,
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          )
        : SizedBox();
  }
}
