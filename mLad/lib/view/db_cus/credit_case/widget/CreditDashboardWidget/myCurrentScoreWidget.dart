import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/credit_case/widget/CreditDashboardWidget/storeItemWidget.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class MyCurrentScore extends StatelessWidget with Mixin {
  const MyCurrentScore({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                showToolTips(
                    context: context,
                    txt:
                        "A breakdown of the biggest influences on your overall credit score to help give you an idea of what you are doing well and what you could improve on.");
                /*showToast(context: context,
                    msg: "A breakdown of the biggest influences on your overall credit score to help give you an idea of what you are doing well and what you could improve on.",
                    bgColor: MyTheme.brandColor,
                    txtColor: Colors.white,
                    isToast: false,
                    which: 1);*/
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(Icons.bar_chart, color: Colors.black),
                  SizedBox(width: 10),
                  Expanded(
                      child: Txt(
                          txt: "Main factor(s) influencing your current score",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: true)),
                  SizedBox(
                    width: 10,
                  ),
                  Icon(
                    Icons.info_outlined,
                    color: Colors.grey,
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),
            Container(
              width: getW(context),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Txt(
                        txt: "Things that you are doing well",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: true),
                    StoreItem(txtData: "You have an active mortgage"),
                    StoreItem(
                        txtData:
                            "You have financial accounts that have been open for more than 12 months which can positively impact your credit score"),
                    /*StoreItem(
                        txtData:
                            "You do not have any CCJ's, Bankruptcies, IVA's or accounts in default"),
                    StoreItem(txtData: "You have an active mortgage"),
                    StoreItem(
                        txtData:
                            "You have financial accounts that have been open for more than 12 months which can positively impact your credit score"),
                    StoreItem(
                        txtData:
                            "You are using less than 50% of your available credit, which can positively impact your credit score"),*/
                  ],
                ),
              ),
            ),
            Container(
              width: getW(context),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Txt(
                        txt: "Things that you could improve",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: true),
                    /*StoreItem(
                        txtData:
                            "You have missed payments on one or more of your financial accounts"),*/
                    StoreItem(
                        txtData:
                            "You have recently missed a payment on one or more of your financial accounts"),
                    StoreItem(
                        txtData:
                            "You have more than one account where you are using over 50% of the available credit which could negatively impact your credit score"),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
