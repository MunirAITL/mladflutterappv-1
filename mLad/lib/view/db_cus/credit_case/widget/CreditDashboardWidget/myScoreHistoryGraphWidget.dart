import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/model/json/db_cus/credit_case/getSummaryResponse.dart';
import 'package:aitl/view/db_cus/credit_case/AlertDialog/AlertDialogAccountAndAlerts.dart';
import 'package:aitl/view/db_cus/credit_case/CreditDashboardTabController.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class MyScoreHistoryGraph extends StatelessWidget with Mixin {
  GetSummaryResponseData getSummaryResponse;

  TooltipBehavior _tooltipBehavior;

  List<SalesData> listData = [];

  @override
  Widget build(BuildContext context) {
    getSummaryResponse = CreditDashBoardTabControllerState.getSummaryResponse;
    _tooltipBehavior = TooltipBehavior(
      enable: true,
      color: Colors.white,
      canShowMarker: true,
      shouldAlwaysShow: true,
    );

    listData.clear();
    if (getSummaryResponse.summaryReport != null) {
      for (var list in getSummaryResponse
          .summaryReport.getSummaryReportResult.scoreHistory.score) {
        final yy = DateFun.getDate(list.scoreDate, "MMM");
        listData.add(SalesData(yy, int.parse(list.scoreValue)));
      }
    }

    final txtStyle = TextStyle(
      color: Colors.black,
      fontFamily: 'Roboto',
      fontSize: 12,
      fontStyle: FontStyle.italic,
    );
    return Card(
      elevation: 10,
      child: Container(
        width: getW(context),
        child: Column(
          children: [
            SizedBox(
              height: 15,
            ),

            GestureDetector(
              onTap: () {
                var txt =
                    "The following information is presented on this panel.\n\n";
                txt += "• The number of days to the next report\n\n";
                txt += "• The expiry date of the account\n\n";
                txt +=
                    "• Information about credit alerts, total number and any unread alerts";
                showToolTips(context: context, txt: txt);
                /*Get.dialog(
                  AlertDialogAccountAndAlerts(
                    titleTxt: "The following information is presented on this panel.",
                    bodyTxt1: "The number of days to the next report",
                    bodyTxt2: "The expiry date of the account",
                    bodyTxt3: "Information about credit alerts, total number and any unread alerts",
                  ),
                  transitionDuration: Duration(milliseconds: 400),
                );*/
              },
              child: Container(
                //color: HexColor.fromHex("#00A6CA"),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Icon(Icons.timer_rounded, color: Colors.black),
                      SizedBox(width: 10),
                      Txt(
                          txt: "My Score History",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.center,
                          isBold: true),
                      SizedBox(width: 10),
                      Icon(Icons.info_outlined, color: Colors.grey),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: SfCartesianChart(
                    backgroundColor:
                        Colors.white, //HexColor.fromHex("#00A6CA"),

                    primaryXAxis: CategoryAxis(
                      isInversed: false,
                      labelStyle: txtStyle,
                      tickPosition: TickPosition.inside,
                      axisLine: AxisLine(color: Colors.black, width: 1),
                      majorGridLines: MajorGridLines(
                        width: .5,
                        color: Colors.black,
                      ),
                    ),
                    primaryYAxis: NumericAxis(
                        axisLine: AxisLine(color: Colors.black, width: 1),
                        isInversed: false,
                        labelStyle: txtStyle,
                        minimum: 0,
                        maximum: 600,
                        rangePadding: ChartRangePadding.none,
                        majorGridLines: MajorGridLines(
                            width: .5,
                            color: Colors.black,
                            dashArray: <double>[1, 1]),
                        minorGridLines: MinorGridLines(
                          width: .5,
                          color: Colors.black,
                        )),
                    // Enable tooltip
                    tooltipBehavior: _tooltipBehavior,
                    series: <LineSeries<SalesData, String>>[
                      LineSeries<SalesData, String>(
                          color: Colors.red,
                          markerSettings: MarkerSettings(
                              isVisible: true, color: Colors.red),
                          dataSource: listData,
                          xValueMapper: (SalesData sales, _) => sales.year,
                          yValueMapper: (SalesData sales, _) => sales.sales,
                          // Enable data label
                          dataLabelSettings: DataLabelSettings(
                              isVisible: true, textStyle: txtStyle))
                    ])),

            // SizedBox(height: 15,),
          ],
        ),
      ),
    );
  }
}

class SalesData {
  SalesData(this.year, this.sales);
  final String year;
  final int sales;
}
