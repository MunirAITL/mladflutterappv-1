import 'package:aitl/view/db_cus/doc_scan/utils/main_mixin.dart';
import 'package:flutter/material.dart';

class ScoreHelper {
  Color getCircleBarColor(int score) {
    return score > 627
        ? HexColor.fromHex("#128B49")
        : score < 628 && score > 603
            ? HexColor.fromHex("#55A84E")
            : score < 604 && score > 565
                ? HexColor.fromHex("#FAD937")
                : score < 566 && score > 510
                    ? HexColor.fromHex("#F17731")
                    : HexColor.fromHex("#C41C2B");
  }

  String getCircleStatus(int score) {
    return score > 627
        ? "Excellent"
        : score < 628 && score > 603
            ? "Good"
            : score < 604 && score > 565
                ? "Fair"
                : score < 566 && score > 510
                    ? "Poor"
                    : "Very Poor";
  }
}
