import 'dart:convert';
import 'dart:ui';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/db_cus/credit_case/GetKbaCreditQuestionAPIMgr.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/credit_case/AnswerParams.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoPostResponse.dart';
import 'package:aitl/model/json/db_cus/credit_case/GetUserValidationOldUserResponse.dart';
import 'package:aitl/model/json/db_cus/credit_case/KbaQuestionResponse.dart';
import 'package:aitl/model/json/db_cus/credit_case/PostKbaCrditQAAPIModel.dart';
import 'package:aitl/view/db_cus/credit_case/CreditDashboardTabController.dart';
import 'package:aitl/view/db_cus/credit_case/TabScreen/CreditScoreDashboard.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_string/json_string.dart';

class MultipleChoiceQuestions extends StatefulWidget {
  final GetUserValidationOldUser responseData;
  MultipleChoiceQuestions({
    Key key,
    @required this.responseData,
  }) : super(key: key);

  @override
  State createState() => _MultipleChoiceQuestionsState();
}

class _MultipleChoiceQuestionsState extends State<MultipleChoiceQuestions>
    with Mixin {
  var isCheckedList = [];
  KbaQuestionResponseData _kbaQuestionResponseData;
  bool _loading = true;

  @override
  void initState() {
    appInit();
    super.initState();
  }

  void appInit() {
    _loading = true;
    GetKbaCreditQuestionApiMgr().getKbaQuestionList(
        context: context,
        // validationIdentifier: widget.creditInfoPostResponse.validateNewUser.validateNewUserResult.validationIdentifier,
        validationIdentifier: widget.responseData.validateNewUser
            .validateNewUserResult.validationIdentifier,
        callback: (model) {
          print("response data = ${model.success}");
          if (model != null) {
            setState(() {
              _kbaQuestionResponseData = model.responseData;
              isCheckedList.length = _kbaQuestionResponseData.kbaQuestions
                  .getKbaQuestionsResult.questions.kbaQuestionItem.length;

              _loading = false;
            });
          } else {
            setState(() {
              _loading = false;
            });
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: MyTheme.themeData.accentColor,
      appBar: AppBar(
        elevation: MyTheme.appbarElevation,
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: MyTheme.statusBarColor,
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(getHP(context, 35)),
          child: Padding(
            padding: const EdgeInsets.only(left: 30, right: 30, bottom: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Txt(
                  txt: "Let us verify your\naccount with TransUnion",
                  txtColor: Colors.white,
                  txtSize: MyTheme.txtSize + 1,
                  txtAlign: TextAlign.start,
                  isBold: true,
                ),
                SizedBox(height: 10),
                Container(
                    width: MediaQuery.of(context).size.width,
                    child: Txt(
                      txt:
                          "Please select the correct answers for the questions below. If we are not able to verify with the correct answers, we will not be able to provide access to your credit profile. If wrong answer selected three times, you will have to call our customer services to obtain access.",
                      txtColor: MyTheme.lightTxtColor,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.left,
                      fontWeight: FontWeight.w300,
                      txtLineSpace: 1.4,
                      isBold: false,
                    )),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 5, bottom: 10),
              child: Btn(
                  txt: "Next",
                  txtColor: Colors.white,
                  bgColor: MyTheme.brandColor,
                  width: getW(context),
                  height: 45,
                  radius: 10,
                  callback: () async {
                    bool selectedIsNull = false;
                    _kbaQuestionResponseData.kbaQuestions.getKbaQuestionsResult
                        .questions.kbaQuestionItem
                        .forEach((element) {
                      if (element.selectedAnswer == null) {
                        selectedIsNull = true;
                        return;
                      }
                    });
                    if (selectedIsNull) {
                      showToast(
                        context: context,
                        msg: "Answer All Question Please.",
                      );
                    } else {
                      List<QuestionsAnswerList> questionsAnswerList = [];
                      _kbaQuestionResponseData.kbaQuestions
                          .getKbaQuestionsResult.questions.kbaQuestionItem
                          .forEach((element) {
                        debugPrint(
                            "selected anser id = ${element.selectedAnswer}");
                        AnswersItem anserItem = AnswersItem(
                            kbaAnswerOption: _kbaQuestionResponseData
                                .kbaQuestions
                                .getKbaQuestionsResult
                                .questions
                                .kbaQuestionItem);
                        QuestionsAnswerList questionsAnswer =
                            new QuestionsAnswerList(
                                answers: anserItem,
                                id: element.id,
                                text: element.text,
                                selectedAnswer: element.selectedAnswer);
                        questionsAnswerList.add(questionsAnswer);
                      });

                      final validationIdentifier = await PrefMgr.shared
                          .getPrefStr("validationIdentifier");
                      AnswerParams answerParams = AnswerParams(
                          kbaAnswerItem: [],
                          questionsAnswerList: questionsAnswerList,
                          validationIdentifier: validationIdentifier,
                          userCompanyId: userData.userModel.userCompanyID,
                          userId: userData.userModel.id);
                      final jsonString =
                          JsonString(json.encode(answerParams.toJson()));
                      myLog(jsonString.source);

                      await APIViewModel().req<PostKbaCrditQAAPIModel>(
                          context: context,
                          url: Server.POST_KBA_QUESTIONS_URL,
                          reqType: ReqType.Post,
                          param: answerParams.toJson(),
                          callback: (model) async {
                            if (mounted && model != null) {
                              if (model.success) {
                                Get.off(
                                    CreditDashBoardTabController(
                                      responseData: widget.responseData,
                                    ),
                                    transition: Transition.rightToLeft,
                                    duration: Duration(
                                        milliseconds: AppConfig
                                            .pageAnimationMilliSecond));
                              } else {
                                showToast(
                                    context: context,
                                    msg: "Something went wrong");
                              }
                            }
                          });
                    }
                  }),
            ),
          )),
      body: drawLayout(),
    );
  }

  drawLayout() {
    return Container(
        width: getW(context),
        height: getH(context),
        child: Padding(
          padding: const EdgeInsets.only(left: 30, right: 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              !_loading
                  ? Expanded(
                      flex: 13,
                      child: ListView.builder(
                        itemCount: _kbaQuestionResponseData
                            .kbaQuestions
                            .getKbaQuestionsResult
                            .questions
                            .kbaQuestionItem
                            .length,
                        itemBuilder: (context, index) {
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(left: 10.0),
                                child: Txt(
                                    txt:
                                        "${index + 1}. ${_kbaQuestionResponseData.kbaQuestions.getKbaQuestionsResult.questions.kbaQuestionItem[index].text}",
                                    txtColor: Colors.white,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.start,
                                    isBold: true),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: _kbaQuestionResponseData
                                      .kbaQuestions
                                      .getKbaQuestionsResult
                                      .questions
                                      .kbaQuestionItem[index]
                                      .answers
                                      .kbaAnswerOption
                                      .length,
                                  itemBuilder: (context, subIndex) {
                                    return GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          isCheckedList[subIndex] = true;
                                          _kbaQuestionResponseData
                                                  .kbaQuestions
                                                  .getKbaQuestionsResult
                                                  .questions
                                                  .kbaQuestionItem[index]
                                                  .selectedAnswer =
                                              _kbaQuestionResponseData
                                                  .kbaQuestions
                                                  .getKbaQuestionsResult
                                                  .questions
                                                  .kbaQuestionItem[index]
                                                  .answers
                                                  .kbaAnswerOption[subIndex]
                                                  .id
                                                  .toString();
                                        });
                                      },
                                      child: radioButtonitem(
                                          text:
                                              "${_kbaQuestionResponseData.kbaQuestions.getKbaQuestionsResult.questions.kbaQuestionItem[index].answers.kbaAnswerOption[subIndex].text}",
                                          bgColor: _kbaQuestionResponseData.kbaQuestions.getKbaQuestionsResult.questions.kbaQuestionItem[index].selectedAnswer != null &&
                                                  _kbaQuestionResponseData
                                                          .kbaQuestions
                                                          .getKbaQuestionsResult
                                                          .questions
                                                          .kbaQuestionItem[
                                                              index]
                                                          .selectedAnswer ==
                                                      _kbaQuestionResponseData.kbaQuestions.getKbaQuestionsResult.questions.kbaQuestionItem[index].answers.kbaAnswerOption[subIndex].id
                                                          .toString()
                                              ? MyTheme.dBlueAirColor
                                              : Color(0xFFFFF),
                                          textColor: _kbaQuestionResponseData
                                                          .kbaQuestions
                                                          .getKbaQuestionsResult
                                                          .questions
                                                          .kbaQuestionItem[
                                                              index]
                                                          .selectedAnswer !=
                                                      null &&
                                                  _kbaQuestionResponseData
                                                          .kbaQuestions
                                                          .getKbaQuestionsResult
                                                          .questions
                                                          .kbaQuestionItem[
                                                              index]
                                                          .selectedAnswer ==
                                                      _kbaQuestionResponseData
                                                          .kbaQuestions
                                                          .getKbaQuestionsResult
                                                          .questions
                                                          .kbaQuestionItem[index]
                                                          .answers
                                                          .kbaAnswerOption[subIndex]
                                                          .id
                                                          .toString()
                                              ? Colors.white
                                              : Colors.black),
                                    );
                                  }),
                              SizedBox(
                                height: 20,
                              ),
                              /*  GestureDetector(
                      onTap: () {
                        setState(() {
                          isCheckedList[index] = true;
                        });
                      },
                      child: radioButtonitem(
                          text: "Yes",
                          bgColor: isCheckedList[index] != null && isCheckedList[index] ? MyTheme.dBlueAirColor : Color(0xFFFFF),
                          textColor: isCheckedList[index] != null && isCheckedList[index] ? Colors.white : Colors.black),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          isCheckedList[index] = false;
                        });
                      },
                      child: radioButtonitem(
                          text: "No",
                          bgColor: isCheckedList[index] != null && !isCheckedList[index] ? MyTheme.dBlueAirColor : Color(0xFFFFF),
                          textColor: isCheckedList[index] != null && !isCheckedList[index] ? Colors.white : Colors.black),
                    ),
                    SizedBox(
                      height: 40,
                    ),*/
                            ],
                          );
                        },
                      ),
                    )
                  : SizedBox(),
            ],
          ),
        ));
  }

  radioButtonitem({String text, Color bgColor, Color textColor}) {
    return Container(
      width: getW(context),
      padding: EdgeInsets.only(left: 10),
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border(
              left: BorderSide(color: Colors.grey, width: 1),
              right: BorderSide(color: Colors.grey, width: 1),
              top: BorderSide(color: Colors.grey, width: 1),
              bottom: BorderSide(color: Colors.grey, width: 1)),
          color: bgColor),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 25,
            height: 25,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border(
                    left: BorderSide(color: Colors.grey, width: 1),
                    right: BorderSide(color: Colors.grey, width: 1),
                    top: BorderSide(color: Colors.grey, width: 1),
                    bottom: BorderSide(color: Colors.grey, width: 1)),
                color: Colors.white),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    border: Border(
                        left: BorderSide(color: Colors.grey, width: 1),
                        right: BorderSide(color: Colors.grey, width: 1),
                        top: BorderSide(color: Colors.grey, width: 1),
                        bottom: BorderSide(color: Colors.grey, width: 1)),
                    color: Colors.grey),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(12),
              child: Txt(
                  txt: text,
                  txtColor: textColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ),
        ],
      ),
    );
  }
}
