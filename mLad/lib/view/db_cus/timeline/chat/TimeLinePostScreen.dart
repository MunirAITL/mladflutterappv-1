import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/PubNubCfg.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/utils/tokbox_cfg.dart';
import 'package:aitl/controller/api/db_cus/timeline/TimelineChatAPIMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/GetCaseGroupChatAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimelineUserModel.dart';
import 'package:aitl/view/db_cus/timeline/tokbox/video_call_page.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/chat_controller.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:aitl/controller/helper/db_cus/tab_timeline/TimeLinePostHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLinePostModel.dart';
import 'package:aitl/view/AnimatedListItem.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:pubnub/pubnub.dart';
import '../../../../Mixin.dart';
import 'package:jiffy/jiffy.dart';
import 'package:flutter/services.dart';
//import 'package:flutter_twilio_voice/flutter_twilio_voice.dart';
import 'package:get/get.dart';
import 'package:json_string/json_string.dart';

class TimeLinePostScreen extends StatefulWidget {
  final CaseGroupMessageData groupMsg;
  final TimelineUserModel timelineUserModel;

  TimeLinePostScreen({
    Key key,
    this.groupMsg,
    this.timelineUserModel,
  }) : super(key: key);

  @override
  State createState() => _TimeLinePostScreenState();
}

class _TimeLinePostScreenState extends State<TimeLinePostScreen> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final TextEditingController textController = TextEditingController();
  ScrollController _scrollController = new ScrollController();

  List<TimeLinePostModel> listTimeLineModel = [];

  final chatController = Get.put(ChatController());

  var pubnub;
  var myChannel;

  Timer timerGetTimeLine;
  Timer timerScrollToBottom;
  static const int callTimelineSec = 35;
  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;

  int pageStart = 1;
  int pageCount = AppConfig.page_limit;

  String msgTmp = '';

  String _platformVersion = 'Unknown';
  String _eventMessage;

  // Platform messages are asynchronous, so we initialize in an async method.
  /*Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await FlutterTwilioVoice.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  void _onEvent(Object event) {
    setState(() {
      _eventMessage = "status: ${event == 'charging' ? '' : 'dis'}charging.";
    });
  }

  void _onError(Object error) {
    setState(() {
      _eventMessage = 'status: unknown.';
      print(_eventMessage);
    });
  }*/

  postTimelineAPI() async {
    try {
      isLoading = true;
      int receiverId = 0;
      int taskId = 0;
      if (widget.timelineUserModel != null) {
        receiverId = widget.timelineUserModel.id;
      } else {
        taskId = widget.groupMsg.id;
      }
      final param = TimeLinePostHelper().getParam(
        message: msgTmp,
        postTypeName: "status",
        additionalAttributeValue: null,
        inlineTags: [],
        checkin: "",
        fromLat: 0,
        fromLng: 0,
        smallImageName: "",
        receiverId: receiverId, //widget.adviserID,
        ownerId: 0,
        taskId: taskId, //widget.taskId,
        isPrivate: true,
      );
      final jsonString = JsonString(json.encode(param));
      myLog(jsonString.source);
      TimelineChatAPIMgr().wsPostTimelineAPI(
          context: context,
          param: param,
          callback: (model) async {
            if (model != null && mounted) {
              try {
                debugPrint(
                    "post a message = ${model.responseData.post.toJson()}");
                if (model.success) {
                  User user = new User(
                      id: userData.userModel.id, name: userData.userModel.name);

                  TimeLinePostModel linePostModel = model.responseData.post;
                  Data2 data2 = new Data2(
                      id: linePostModel.id,
                      ownerId: linePostModel.ownerId,
                      ownerEntityType: linePostModel.ownerEntityType,
                      ownerImageUrl: linePostModel.ownerImageUrl,
                      ownerProfileUrl: linePostModel.ownerProfileUrl,
                      ownerName: linePostModel.ownerName,
                      postTypeName: linePostModel.postTypeName,
                      isSponsored: linePostModel.isSponsored,
                      message: linePostModel.message,
                      additionalAttributeValue:
                          linePostModel.additionalAttributeValue,
                      dateCreatedUtc: linePostModel.dateCreatedUtc,
                      dateUpdatedUtc: linePostModel.dateUpdatedUtc,
                      dateCreated: linePostModel.dateCreated,
                      dateUpdated: linePostModel.dateUpdated,
                      totalLikes: linePostModel.totalLikes,
                      totalComments: linePostModel.totalComments,
                      canDelete: linePostModel.canDelete,
                      publishDateUtc: linePostModel.publishDateUtc,
                      publishDate: linePostModel.publishDate,
                      likeStatus: linePostModel.likeStatus,
                      isOwner: linePostModel.isOwner,
                      checkin: linePostModel.checkin,
                      fromLat: linePostModel.fromLat,
                      fromLng: linePostModel.fromLng,
                      userCommentPublicModelList:
                          linePostModel.userCommentPublicModelList,
                      receiverId: linePostModel.receiverId,
                      senderId: linePostModel.senderId,
                      taskId: linePostModel.taskId,
                      userCompanyId: userData.userModel.userCompanyID,
                      isOnline: true);

                  Data data = new Data(data2: data2, user: user);
                  postAMessage(data);
                  onPageLoad(1);
                } else {}
              } catch (e) {
                myLog(e.toString());
              }
            }
          });
    } catch (e) {
      myLog(e.toString());
      isLoading = false;
    }
  }

  onPageLoad(int startPage) async {
    try {
      setState(() {
        isLoading = true;
      });
      int senderId = 0;
      int receiverId = 0;
      int taskId = 0;
      if (widget.timelineUserModel != null) {
        senderId = userData.userModel.id;
        receiverId = widget.timelineUserModel.id;
      } else {
        taskId = widget.groupMsg.id;
      }
      var url = Server.TIMELINE_URL;
      url = url.replaceAll("#isPrivate#", "true");
      url = url.replaceAll(
          "#receiverId#", receiverId.toString()); //receiverId.toString());
      url = url.replaceAll("#senderId#", senderId.toString());
      url = url.replaceAll("#taskId#", taskId.toString());
      url = url.replaceAll("#Count#", pageCount.toString());
      url = url.replaceAll("#count#", pageCount.toString());
      url = url.replaceAll("#customerId#", "0");
      url = url.replaceAll("#page#", pageStart.toString());
      url = url.replaceAll("#timeLineId#", "0");
      TimelineChatAPIMgr().wsOnPageLoad(
        context: context,
        url: url,
        callback: (model) {
          if (model != null && mounted) {
            msgTmp = '';
            try {
              if (model.success) {
                try {
                  final List<dynamic> timeLines =
                      model.responseData.timelinePosts;
                  if (timeLines != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility

                    try {
                      setState(() {
                        isLoading = false;
                      });
                      if (listTimeLineModel.length > 0) {
                        try {
                          TimeLinePostModel tlModel1 = listTimeLineModel[0];
                          TimeLinePostModel tlModel2 =
                              model.responseData.timelinePosts[0];
                          if (tlModel1.id == tlModel2.id) {
                            return;
                          }
                        } catch (e) {
                          myLog(e.toString());
                        }
                      }

                      //await _removeFromMeMsg();
                      if (startPage == 1) {
                        listTimeLineModel.clear();
                        //await _removeFromMeMsg();
                      }
                      for (TimeLinePostModel timeLine in timeLines) {
                        //if (!listTimeLineModel.contains(timeLine))
                        listTimeLineModel.add(timeLine);
                      }
                      setState(() {
                        if (timeLines.length != pageCount) {
                          isPageDone = true;
                        }
                      });
                    } catch (e) {
                      myLog(e.toString());
                    }
                    myLog(listTimeLineModel.toString());
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              }
            } catch (e) {
              myLog(e.toString());
            }
          }
        },
      );
    } catch (e) {
      myLog(e.toString());
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  void initState() {
    try {
      appInit();
    } catch (e) {}
    super.initState();
  }

  /*_removeFromMeMsg() async {
    try {
      for (TimeLinePostModel tlModel in listTimeLineModel) {
        try {
          if (tlModel.isFromMe) {
            listTimeLineModel.remove(tlModel);
          }
        } catch (e) {}
      }
    } catch (e) {}
  }*/

  //  ******************* Pub Nub Start Here....

  /*initPubNub() async {
    userModel =  DashBoardScreenState.userModel;
    final pubnub = pn.PubNub(
        defaultKeyset: Keyset(
            subscribeKey: PubNubCfg.subscribeKey,
            publishKey: PubNubCfg.publishKey,
            uuid: UUID(await Common.getUDID(context))));

    final param = TimeLinePostHelper().getParam(
      message: "testinggggg pubnub",
      postTypeName: "status",
      additionalAttributeValue: null,
      inlineTags: [],
      checkin: "",
      fromLat: 0,
      fromLng: 0,
      smallImageName: "",
      receiverId: widget.receiverId,
      ownerId: 0,
      senderId: userModel.id,
      taskId: widget.taskId,
      isPrivate: true,
    );

    final myChannel = pubnub.channel(userModel.id.toString());
    myChannel.publish({
      'data': param,
      'user': userModel.id,
    });

    var subscription = pubnub.subscribe(channels: {userModel.id.toString()});

    subscription.messages.listen((envelope) {
      print('${envelope.uuid} sent a message: ${envelope.payload}');
    });

    var history = myChannel.history(chunkSize: 50);
    myLog(history.toString());

    appInit();
    /*myChannel.subscribe().messages.listen((envelope) {
      print(envelope.payload);
    });*/
  }*/

  //  ******************* Pub Nub End Here....
  @override
  void dispose() {
    listTimeLineModel = null;
    textController.dispose();
    _scrollController.dispose();
    _scrollController = null;
    try {
      timerScrollToBottom = null;
      timerGetTimeLine?.cancel();
      timerGetTimeLine = null;
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      //initPlatformState();
      await onPageLoad(1);
      pageStart++;
      /* timerGetTimeLine =
          Timer.periodic(Duration(seconds: callTimelineSec), (Timer t) {
            //  call to ws for getting any new msg of the sender
            if (!isLoading && msgTmp.length == 0) {
              onPageLoad(1);
            }
          });*/
    } catch (e) {
      myLog(e.toString());
    }
    try {
      initPubNub();
    } catch (e) {
      debugPrint("Error pubnub = ${e.toString()}");
    }
  }

  Widget buildSingleMessage(index) {
    try {
      TimeLinePostModel timelineModel = listTimeLineModel[index];

      bool fromMe = false;
      bool isMe =
          (timelineModel.senderId == userData.userModel.id) ? true : false;

      //bool isFromMeSent = false;
      /*String lastMsgDate;
      DateTime date1;
      try {
        //if (index  <= listTimeLineModel.length) {
        TimeLinePostModel timelineModel2 = listTimeLineModel[(index + 1)];
        //var jiffy1 = Jiffy(timelineModel2.dateCreated);
        //var jiffy2 = Jiffy(timelineModel2.dateCreated)..date;

        date1 = Jiffy(timelineModel.dateCreated).dateTime;
        DateTime date2 = Jiffy(timelineModel2.dateCreated).dateTime;
        //final days = (jiffy2.diff(jiffy1.date));
        if (date1.day > date2.day) {
          // myLog(days.toString());
          var inputDate = DateTime.parse(date1.toString());
          var outputFormat = DateFormat('dd-MMMM-yyyy');
          lastMsgDate = outputFormat.format(inputDate);
          //isFromMeSent = true;
        }
        //}
      } catch (e) {
        myLog(e.toString());
      }*/

      return Padding(
        padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            /*(fromMe && lastMsgDate != null)
                ? Padding(
                    padding: const EdgeInsets.all(20),
                    child: Center(
                      child: Txt(
                          txt: lastMsgDate,
                          txtColor: Colors.grey,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                  )
                : SizedBox(),*/
            Container(
              //color: Colors.black,
              child: Row(
                crossAxisAlignment: (fromMe)
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  (!fromMe)
                      ? getImage(timelineModel.ownerImageUrl)
                      : SizedBox(),
                  SizedBox(width: 10),
                  Expanded(
                    child: Container(
                      //color: Colors.white,
                      // margin: margins,
                      child: Column(
                        crossAxisAlignment: (fromMe)
                            ? CrossAxisAlignment.end
                            : CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: 5),
                          Row(
                            crossAxisAlignment: (fromMe)
                                ? CrossAxisAlignment.start
                                : CrossAxisAlignment.end,
                            mainAxisAlignment: (fromMe)
                                ? MainAxisAlignment.end
                                : MainAxisAlignment.start,
                            children: [
                              Txt(
                                  txt: (fromMe)
                                      ? DateFun.getDate(
                                              timelineModel.dateCreated,
                                              "HH:mm") ??
                                          ''
                                      : timelineModel.ownerName
                                          .toString()
                                          .trim(),
                                  txtColor: (fromMe)
                                      ? Colors.grey
                                      : Color(0xFF3D3D3D),
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                              SizedBox(width: 10),
                              Txt(
                                  txt: (fromMe)
                                      ? timelineModel.ownerName
                                          .toString()
                                          .trim()
                                      : DateFun.getDate(
                                              timelineModel.dateCreated,
                                              "HH:mm") ??
                                          '',
                                  txtColor: (fromMe)
                                      ? Color(0xFF3D3D3D)
                                      : Colors.grey,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                            ],
                          ),
                          SizedBox(height: 5),
                          Stack(
                            alignment: (fromMe)
                                ? Alignment.bottomRight
                                : Alignment.bottomLeft,
                            children: [
                              Padding(
                                padding:
                                    const EdgeInsets.only(right: 3, bottom: 5),
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 10, right: 10, bottom: 2),
                                  decoration: BoxDecoration(
                                    color: isMe
                                        ? MyTheme.bgColor
                                        : Color(0xFFE8E8E8),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: Txt(
                                      txt: timelineModel.message
                                          .toString()
                                          .trim(),
                                      txtColor: isMe
                                          ? Colors.white
                                          : Color(0xFF3D3D3D),
                                      txtSize: MyTheme.txtSize - .5,
                                      txtAlign: TextAlign.end,
                                      isBold: false),
                                ),
                              ),
                              /*(timelineModel.senderId == userData.userModel.id)
                                  ? Positioned(
                                      bottom: 0,
                                      child: Container(
                                        padding: EdgeInsets.all(2),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: MyTheme.titleColor,
                                          //borderRadius: BorderRadius.circular(100),
                                        ),
                                        child: Txt(
                                            txt: "R",
                                            txtColor: Colors.white,
                                            txtSize: MyTheme.txtSize - 1,
                                            txtAlign: TextAlign.center,
                                            isBold: false),
                                      ),
                                    )
                                  : SizedBox(),*/
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: 10),
                  (fromMe) ? getImage(timelineModel.ownerImageUrl) : SizedBox(),
                ],
              ),
            ),
          ],
        ),
      );
    } catch (e) {
      return Container();
    }
  }

  getImage(url) {
    return CircleAvatar(
      radius: 20,
      backgroundColor: Colors.transparent,
      backgroundImage:
          new CachedNetworkImageProvider(MyNetworkImage.checkUrl(url)),
    );
  }

  Widget buildMessageList() {
    return Flexible(
      child: NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            if (!isLoading && !isPageDone && msgTmp.length == 0) {
              onPageLoad(pageStart++);
            }
          }
          return true;
        },
        child: ListView.builder(
          controller: _scrollController,
          padding: new EdgeInsets.all(8.0),
          //reverse: true,
          itemCount: listTimeLineModel.length,
          itemBuilder: (BuildContext context, int index) {
            var itemWidget = buildSingleMessage(index);
            return AnimatedListItem(
              index: index,
              itemDesign: itemWidget,
            );
          },
        ),
      ),
    );
  }

  _bottomChatArea() {
    return Obx(() => Container(
          padding: EdgeInsets.all(10.0),
          child: Row(
            children: <Widget>[
              _chatTextArea(),
              /*GestureDetector(
            onLongPressStart: (details) async {
              bool hasPermission = await checkPermission();
              if (hasPermission) {
                myLog("start recording");
                // Check and request permission
                Directory tempDir = await getTemporaryDirectory();
                String tempPath = tempDir.path;
                // https://pub.dev/packages/record_mp3
                //start record
                RecordMp3.instance.start(tempPath, (type) {
                  // record fail callback
                  myLog(type);
                });
              } else {
                myLog("cannot start recording:: permission not allowed");
              }
            },
            onLongPressEnd: (details) {
              myLog("end recording");
              //complete record and export a record file
              RecordMp3.instance.stop();
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Container(
                child: Image.asset(
                  "assets/images/icons/mic_icon.png",
                  width: 60,
                  height: 60,
                ),
              ),
            ),
          ),*/
              IconButton(
                iconSize: 30,
                icon: Icon(
                  Icons.send,
                  color:
                      chatController.isWrite.value ? Colors.black : Colors.grey,
                ),
                onPressed: () async {
                  chatController.isWrite.value = false;
                  FocusScope.of(context).requestFocus(FocusNode());
                  onSendClicked();
                },
              ),
            ],
          ),
        ));
  }

  onSendClicked() async {
    try {
      if (textController.text.isNotEmpty) {
        //Add the message to the list
        String formattedDate = DateFormat('dd-MMM-yyyy').format(DateTime.now());
        if (mounted) {
          //Scrolldown the list to show the latest message
          msgTmp = textController.text.trim();
          final TimeLinePostModel timeLinePostModel = TimeLinePostModel();
          timeLinePostModel.senderId = userData.userModel.id;
          timeLinePostModel.ownerId = userData.userModel.id;
          timeLinePostModel.ownerImageUrl = userData.userModel.profileImageURL;
          timeLinePostModel.ownerName = userData.userModel.name;
          timeLinePostModel.message = msgTmp;
          timeLinePostModel.dateCreatedUtc = formattedDate;
          timeLinePostModel.isFromMe = true;
          listTimeLineModel.insert(0, timeLinePostModel);
          textController.text = '';
          _scrollController.animateTo(
            0.0,
            curve: Curves.easeOut,
            duration: const Duration(milliseconds: 300),
          );
          //_chatListScrollToBottom();
          //setState(() {});
          //Future.delayed(Duration(seconds: 5), () async {
          pageStart = 1;
          await postTimelineAPI();
          //});
        }
      }
    } catch (e) {}
  }

  _chatTextArea() {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Container(
          decoration: new BoxDecoration(
              color: Color(0xFFE9E9E9),
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(40.0),
                topRight: const Radius.circular(40.0),
                bottomLeft: const Radius.circular(40.0),
                bottomRight: const Radius.circular(40.0),
              )),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: IconButton(
                    iconSize: 25,
                    icon: Icon(
                      Icons.tag_faces,
                      color: Colors.black54,
                    ),
                    onPressed: () {}),
              ),
              Expanded(
                child: TextField(
                  controller: textController,
                  textInputAction: TextInputAction.send,
                  textAlign: TextAlign.center,
                  onSubmitted: (value) {
                    onSendClicked();
                  },
                  onChanged: (v) {
                    chatController.isWrite.value =
                        v.trim().length > 0 ? true : false;
                  },
                  maxLength: 255,
                  autocorrect: false,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:
                        getTxtSize(context: context, txtSize: MyTheme.txtSize),
                  ),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintStyle: TextStyle(color: Colors.black54),
                    counter: Offstage(),
                    contentPadding: EdgeInsets.fromLTRB(0, 20.0, 20.0, 10.0),
                    hintText: 'Type a message...',
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        //resizeToAvoidBottomPadding: false,
        backgroundColor: MyTheme.bgColor2,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: (mounted) ? drawChatUI() : SizedBox(),
        ),
      ),
    );
  }

  drawChatUI() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Column(
        children: <Widget>[
          drawHeader(),
          buildMessageList(),
          _bottomChatArea(),
        ],
      ),
    );
  }

  drawHeader() {
    var caseIcon;
    var name;
    var isOnline;
    if (widget.groupMsg != null) {
      caseIcon =
          NewCaseHelper().getCreateCaseIconByTitle(widget.groupMsg.title);
      name = widget.groupMsg.title;
      isOnline = false; //widget.groupMsg.isInPersonOrOnline;
    } else {
      name = widget.timelineUserModel.name;
      isOnline = widget.timelineUserModel.isOnline;
    }

    final width = getW(context);

    return Container(
      child: Card(
        margin: EdgeInsets.zero,
        elevation: 2,
        child: Padding(
          padding:
              const EdgeInsets.only(left: 10, right: 10, top: 15, bottom: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        color: Colors.black,
                      )),
                  caseIcon == null
                      ? CircleAvatar(
                          radius: 18,
                          backgroundColor: Colors.transparent,
                          backgroundImage: new CachedNetworkImageProvider(
                            MyNetworkImage.checkUrl(
                                (widget.timelineUserModel.profileImageUrl !=
                                        null)
                                    ? widget.timelineUserModel.profileImageUrl
                                    : Server.MISSING_IMG),
                          ),
                        )
                      : Container(
                          width: width * 0.08,
                          height: width * 0.08,
                          decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(5),
                              gradient: LinearGradient(
                                  colors: [
                                    HexColor.fromHex("#2B4564"),
                                    HexColor.fromHex("#112B4A"),
                                  ],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(0.0, 0.0),
                                  stops: [0.0, 1.0],
                                  tileMode: TileMode.clamp)),
                          child: Image.asset(
                            caseIcon,
                          ),
                        ),
                  SizedBox(width: 15),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Txt(
                            txt: name,
                            txtColor: MyTheme.titleColor,
                            txtSize: MyTheme.txtSize - .1,
                            txtAlign: TextAlign.start,
                            isBold: true),
                        //SizedBox(height: 5),
                        caseIcon == null
                            ? Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  drawCircle(
                                      context: context,
                                      color: isOnline
                                          ? Color(0xFF00FF38)
                                          : Colors.redAccent,
                                      size: 1.3),
                                  SizedBox(width: 5),
                                  Flexible(
                                    child: Txt(
                                        txt: isOnline ? 'Active' : 'Offline',
                                        txtColor: Color(0xFF7B7B7B),
                                        txtSize: MyTheme.txtSize - .8,
                                        txtAlign: TextAlign.start,
                                        isBold: true),
                                  ),
                                ],
                              )
                            : Txt(
                                txt: "Case No: ${widget.groupMsg.id}",
                                txtColor: Color(0xFF3D3D3D),
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: false),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  initPubNub() async {
    myLog("init pub nub call =");

    pubnub = PubNub(
        defaultKeyset: Keyset(
            subscribeKey: PubNubCfg.subscribeKey,
            publishKey: PubNubCfg.publishKey,
            uuid: UUID(userData.userModel.id.toString())));

    Subscription subscription =
        pubnub.subscribe(channels: {userData.userModel.id.toString()});

    print('receive message id : ${userData.userModel.id}');

    subscription.messages.listen((envelope) {
      print('sent a message full : ${envelope.payload}');
      print('receive message id : ${userData.userModel.id}');
      onPageLoad(1);
    });
  }

  void postAMessage(Data data) {
    var id = 0;
    if (widget.groupMsg != null)
      id = widget.groupMsg.id;
    else
      id = widget.timelineUserModel.id;

    myChannel = pubnub.channel(id);
    print('sent a message id : ${id.toString()}');
    myChannel.publish({
      'data': data.toJson(),
      'user': userData.userModel.id,
    });
  }
}

class Data {
  Data2 data2;
  User user;

  Data({this.data2, this.user});

  Data.fromJson(Map<String, dynamic> json) {
    data2 = json['data'] != null ? new Data2.fromJson(json['data']) : null;
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data2 != null) {
      data['data'] = this.data2.toJson();
    }
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class Data2 {
  int id;
  int ownerId;
  String ownerEntityType;
  String ownerImageUrl;
  String ownerProfileUrl;
  String ownerName;
  String postTypeName;
  bool isSponsored;
  String message;
  dynamic additionalAttributeValue;
  String dateCreatedUtc;
  String dateUpdatedUtc;
  String dateCreated;
  String dateUpdated;
  int totalLikes;
  int totalComments;
  bool canDelete;
  String publishDateUtc;
  String publishDate;
  int likeStatus;
  bool isOwner;
  String checkin;
  double fromLat;
  double fromLng;
  dynamic userCommentPublicModelList;
  int receiverId;
  int senderId;
  int taskId;
  int userCompanyId;
  bool isOnline;

  Data2(
      {this.id,
      this.ownerId,
      this.ownerEntityType,
      this.ownerImageUrl,
      this.ownerProfileUrl,
      this.ownerName,
      this.postTypeName,
      this.isSponsored,
      this.message,
      this.additionalAttributeValue,
      this.dateCreatedUtc,
      this.dateUpdatedUtc,
      this.dateCreated,
      this.dateUpdated,
      this.totalLikes,
      this.totalComments,
      this.canDelete,
      this.publishDateUtc,
      this.publishDate,
      this.likeStatus,
      this.isOwner,
      this.checkin,
      this.fromLat,
      this.fromLng,
      this.userCommentPublicModelList,
      this.receiverId,
      this.senderId,
      this.taskId,
      this.userCompanyId,
      this.isOnline});

  Data2.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    ownerId = json['OwnerId'];
    ownerEntityType = json['OwnerEntityType'];
    ownerImageUrl = json['OwnerImageUrl'];
    ownerProfileUrl = json['OwnerProfileUrl'];
    ownerName = json['OwnerName'];
    postTypeName = json['PostTypeName'];
    isSponsored = json['IsSponsored'];
    message = json['Message'];
    additionalAttributeValue = json['AdditionalAttributeValue'];
    dateCreatedUtc = json['DateCreatedUtc'];
    dateUpdatedUtc = json['DateUpdatedUtc'];
    dateCreated = json['DateCreated'];
    dateUpdated = json['DateUpdated'];
    totalLikes = json['TotalLikes'];
    totalComments = json['TotalComments'];
    canDelete = json['CanDelete'];
    publishDateUtc = json['PublishDateUtc'];
    publishDate = json['PublishDate'];
    likeStatus = json['LikeStatus'];
    isOwner = json['IsOwner'];
    checkin = json['Checkin'];
    fromLat = json['FromLat'];
    fromLng = json['FromLng'];
    userCommentPublicModelList = json['UserCommentPublicModelList'];
    receiverId = json['ReceiverId'];
    senderId = json['SenderId'];
    taskId = json['TaskId'];
    userCompanyId = json['UserCompanyId'];
    isOnline = json['IsOnline'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['OwnerId'] = this.ownerId;
    data['OwnerEntityType'] = this.ownerEntityType;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    data['OwnerName'] = this.ownerName;
    data['PostTypeName'] = this.postTypeName;
    data['IsSponsored'] = this.isSponsored;
    data['Message'] = this.message;
    data['AdditionalAttributeValue'] = this.additionalAttributeValue;
    data['DateCreatedUtc'] = this.dateCreatedUtc;
    data['DateUpdatedUtc'] = this.dateUpdatedUtc;
    data['DateCreated'] = this.dateCreated;
    data['DateUpdated'] = this.dateUpdated;
    data['TotalLikes'] = this.totalLikes;
    data['TotalComments'] = this.totalComments;
    data['CanDelete'] = this.canDelete;
    data['PublishDateUtc'] = this.publishDateUtc;
    data['PublishDate'] = this.publishDate;
    data['LikeStatus'] = this.likeStatus;
    data['IsOwner'] = this.isOwner;
    data['Checkin'] = this.checkin;
    data['FromLat'] = this.fromLat;
    data['FromLng'] = this.fromLng;
    data['UserCommentPublicModelList'] = this.userCommentPublicModelList;
    data['ReceiverId'] = this.receiverId;
    data['SenderId'] = this.senderId;
    data['TaskId'] = this.taskId;
    data['UserCompanyId'] = this.userCompanyId;
    data['IsOnline'] = this.isOnline;
    return data;
  }
}

class User {
  int id;
  String name;

  User({this.id, this.name});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    return data;
  }
}
