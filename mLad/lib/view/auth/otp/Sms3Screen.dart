import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/controller/api/auth/otp/Sms3APIMgr.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOTPModel.dart';
import 'package:aitl/view/NoAccess.dart';
import 'package:aitl/view/auth/LoginLandingScreen.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Sms3Screen extends StatefulWidget {
  final bool isBack;
  final MobileUserOTPModel mobileUserOTPModel;

  const Sms3Screen({
    Key key,
    @required this.mobileUserOTPModel,
    this.isBack = false,
  }) : super(key: key);

  @override
  State createState() => new _Sms3ScreenState();
}

class _Sms3ScreenState extends State<Sms3Screen>
    with SingleTickerProviderStateMixin, Mixin {
  // final _scaffoldKey = GlobalKey<ScaffoldState>();

  String countryCode = "+44";
  String countryName = "GB";

  bool isLoading = false;

  // Variables
  static const int SMS_AUTH_CODE_LEN = 6;
  int _currentDigit;
  int _firstDigit;
  int _secondDigit;
  int _thirdDigit;
  int _fourthDigit;
  int _fiveDigit;
  int _sixDigit;

  var userId = 0;
  var otpCode = "";

  getFullPhoneNumber({String countryCodeTxt, String number}) {
    var fullNumber = countryCodeTxt + number;

    return fullNumber;
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    super.dispose();
  }

  void clearOtp() {
    _sixDigit = null;
    _fiveDigit = null;
    _fourthDigit = null;
    _thirdDigit = null;
    _secondDigit = null;
    _firstDigit = null;
    if (mounted) {
      setState(() {});
    }
  }

  appInit() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;

          countryCode = cd;
        });
      }
    } catch (e) {
      print("Sms2 screen country code and name problem ");
    }
    try {
      userId = userData.userModel.id;
    } catch (e) {
      debugPrint("User id getting problem $userId = " + e.toString());
    }
  }

  // Return "OTP" input field
  get _getInputField {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _otpTextField(_firstDigit),
        _otpTextField(_secondDigit),
        _otpTextField(_thirdDigit),
        _otpTextField(_fourthDigit),
        _otpTextField(_fiveDigit),
        _otpTextField(_sixDigit),
      ],
    );
  }

  // Returns "Resend" button
  get _getResendButton {
    return Center(
      child: Padding(
        padding:
            const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
        child: MMBtn(
            //bgColor: ,
            //txtColor: Colors.black,
            txt: (!widget.isBack) ? "Continue" : "Back",
            height: getHP(context, 7),
            width: getW(context) / 3,
            radius: 5,
            callback: () {
              if (!widget.isBack) {
                Get.offAll(() => LoginLandingScreen());
              } else {
                Get.back();
              }
            }),
      ),

      /* child: Txt(
        txt: "Resend code",
        txtColor: Colors.black,
        txtSize: MyTheme.txtSize - .2,
        txtAlign: TextAlign.center,
        isBold: false,
      ),*/
    );
  }

  // Returns "Otp" keyboard
  get _getOtpKeyboard {
    return new Container(
        height: getW(context) - 140,
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "1",
                      onPressed: () {
                        _setCurrentDigit(1);
                      }),
                  _otpKeyboardInputButton(
                      label: "2",
                      onPressed: () {
                        _setCurrentDigit(2);
                      }),
                  _otpKeyboardInputButton(
                      label: "3",
                      onPressed: () {
                        _setCurrentDigit(3);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "4",
                      onPressed: () {
                        _setCurrentDigit(4);
                      }),
                  _otpKeyboardInputButton(
                      label: "5",
                      onPressed: () {
                        _setCurrentDigit(5);
                      }),
                  _otpKeyboardInputButton(
                      label: "6",
                      onPressed: () {
                        _setCurrentDigit(6);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "7",
                      onPressed: () {
                        _setCurrentDigit(7);
                      }),
                  _otpKeyboardInputButton(
                      label: "8",
                      onPressed: () {
                        _setCurrentDigit(8);
                      }),
                  _otpKeyboardInputButton(
                      label: "9",
                      onPressed: () {
                        _setCurrentDigit(9);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardActionButton(
                      label: new Icon(
                        Icons.backspace,
                        color: Color(0xFF8C8C8C),
                        size: 30,
                      ),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            otpCode = "";
                            if (_sixDigit != null) {
                              _sixDigit = null;
                            } else if (_fiveDigit != null) {
                              _fiveDigit = null;
                            } else if (_fourthDigit != null) {
                              _fourthDigit = null;
                            } else if (_thirdDigit != null) {
                              _thirdDigit = null;
                            } else if (_secondDigit != null) {
                              _secondDigit = null;
                            } else if (_firstDigit != null) {
                              _firstDigit = null;
                            }
                          });
                        }
                      }),
                  _otpKeyboardInputButton(
                      label: "0",
                      onPressed: () {
                        _setCurrentDigit(0);
                      }),
                  _otpKeyboardActionButton(
                      label: Container(
                        decoration: BoxDecoration(
                          color: Color(0xFF8C8C8C),
                          shape: BoxShape.circle,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: new Icon(
                            Icons.keyboard_return,
                            color: Colors.white,
                            size: 25,
                          ),
                        ),
                      ),
                      onPressed: () {
                        if (mounted) {
                          if (otpCode.length == SMS_AUTH_CODE_LEN) {
                            callAllApi();
                          }
                        }
                      }),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          iconTheme: IconThemeData(color: MyTheme.titleColor),
          backgroundColor: MyTheme.bgColor2,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          centerTitle: true,
        ),
        bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          child: _getOtpKeyboard,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        // width: getW(context),
        // height: getH(context),
        child: Column(
          // mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Column(
                children: [
                  Txt(
                    txt: "Verify yourself",
                    txtColor: MyTheme.titleColor,
                    txtSize: MyTheme.txtSize + 1.2,
                    txtAlign: TextAlign.center,
                    isBold: true,
                  ),
                  SizedBox(height: 20),
                  Center(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: new TextSpan(
                        children: <TextSpan>[
                          new TextSpan(
                              text:
                                  "We have sent a code in your mobile number.\nPlease enter the code here.\n\n${widget.mobileUserOTPModel.mobileNumber.toString().substring(0, 3)}****${widget.mobileUserOTPModel.mobileNumber.toString().substring(widget.mobileUserOTPModel.mobileNumber.length - 5)}   ",
                              style: new TextStyle(
                                  fontSize: getTxtSize(
                                      context: context,
                                      txtSize: MyTheme.txtSize),
                                  color: MyTheme.inputColor,
                                  fontWeight: FontWeight.normal,
                                  height: 1.5)),
                          TextSpan(
                            children: [
                              TextSpan(
                                text: 'Resend code',
                                recognizer: new TapGestureRecognizer()
                                  ..onTap = () {
                                    final mobile =
                                        widget.mobileUserOTPModel.mobileNumber;
                                    if (getFullPhoneNumber(
                                                countryCodeTxt: countryCode,
                                                number: mobile)
                                            .length >=
                                        UserProfileVal.PHONE_LIMIT) {
                                      Sms2APIMgr().wsLoginMobileOtpPostAPI(
                                        context: context,
                                        countryCode: countryCode,
                                        mobile: mobile,

                                        // mobile: getFullPhoneNumber(countryCodeTxt: countryCode,number: _phoneController.text.trim()),
                                        callback: (model) {
                                          if (model != null && mounted) {
                                            try {
                                              if (model.success) {
                                                try {
                                                  //final msg = model.messages.postUserotp[0].toString();
                                                  //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: msg, which: 1);
                                                  if (mounted) {
                                                    Sms2APIMgr()
                                                        .wsSendOtpNotiAPI(
                                                            context: context,
                                                            otpId: model
                                                                .responseData
                                                                .userOTP
                                                                .id,
                                                            callback: (model) {
                                                              if (model !=
                                                                      null &&
                                                                  mounted) {
                                                                try {
                                                                  if (model
                                                                      .success) {
                                                                    try {
                                                                      showToast(
                                                                          context:
                                                                              context,
                                                                          msg:
                                                                              "OTP has been sent");
                                                                      /* navTo(
                                          context: context,
                                          page: () => Sms3Screen(
                                            mobileUserOTPModel: model.responseData.userOTP,
                                          ),
                                        ).then((value) {
                                          //callback(route);
                                        });*/
                                                                    } catch (e) {
                                                                      myLog(e
                                                                          .toString());
                                                                    }
                                                                  } else {
                                                                    try {
                                                                      if (mounted) {
                                                                        final err = model
                                                                            .messages
                                                                            .postUserotp[0]
                                                                            .toString();
                                                                        showToast(
                                                                            context:
                                                                                context,
                                                                            msg:
                                                                                err);
                                                                      }
                                                                    } catch (e) {
                                                                      myLog(e
                                                                          .toString());
                                                                    }
                                                                  }
                                                                } catch (e) {
                                                                  myLog(e
                                                                      .toString());
                                                                }
                                                              }
                                                            });
                                                  }
                                                } catch (e) {
                                                  myLog(e.toString());
                                                }
                                              } else {
                                                try {
                                                  if (mounted) {
                                                    final err = model
                                                        .messages.postUserotp[0]
                                                        .toString();
                                                    showToast(
                                                        context: context,
                                                        msg: err);
                                                  }
                                                } catch (e) {
                                                  myLog(e.toString());
                                                }
                                              }
                                            } catch (e) {
                                              myLog(e.toString());
                                            }
                                          }
                                        },
                                      );
                                    } else {
                                      showToast(
                                          context: context,
                                          msg:
                                              'Please enter your valid phone number');
                                    }
                                  },
                                style: new TextStyle(
                                  decoration: TextDecoration.underline,
                                  fontSize: getTxtSize(
                                      context: context,
                                      txtSize: MyTheme.txtSize),
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            _getInputField,
            _getResendButton,
            /*SizedBox(height: 20),
            Center(
              child: CupertinoButton(
                onPressed: () => {Get.offAll(() => RegScreen())},
                color: MyTheme.brandColor,
                borderRadius: new BorderRadius.circular(20.0),
                child: new Text(
                  "Continue",
                  textAlign: TextAlign.center,
                  style: new TextStyle(color: Colors.white),
                ),
              ),
            ),
            SizedBox(height: 20),*/
          ],
        ),
      ),
    );
  }

  // Returns "Otp custom text field"
  Widget _otpTextField(int digit) {
    int boxSpace = 2 * 6;
    double boxW = (getWP(context, 100) / SMS_AUTH_CODE_LEN) - boxSpace;
    return new Container(
      width: boxW,
      height: boxW,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        //borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.black, width: .5),
        //color: MyTheme.brandColor,
      ),
      child: Text(
        digit != null ? digit.toString() : "",
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.black,
          fontSize: 30,
          fontWeight: FontWeight.bold,
          fontFamily: "Dosis Regular",
        ),
      ),
    );
  }

  // Returns "Otp keyboard input Button"
  Widget _otpKeyboardInputButton({String label, VoidCallback onPressed}) {
    return new Material(
      color: Colors.transparent,
      child: new InkWell(
        onTap: onPressed,
        borderRadius: new BorderRadius.circular(40.0),
        child: new Container(
          height: 80.0,
          width: 80.0,
          decoration: new BoxDecoration(shape: BoxShape.rectangle),
          child: new Center(
            child: Text(
              label,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFF8C8C8C),
                fontSize: 30,
                fontWeight: FontWeight.bold,
                fontFamily: "Dosis Bold",
              ),
            ),
          ),
        ),
      ),
    );
  }

  // Returns "Otp keyboard action Button"
  _otpKeyboardActionButton({Widget label, VoidCallback onPressed}) {
    return new InkWell(
      onTap: onPressed,
      borderRadius: new BorderRadius.circular(40.0),
      child: new Container(
        height: 80.0,
        width: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: new Center(
          child: label,
        ),
      ),
    );
  }

  // Current digit
  void _setCurrentDigit(int i) {
    if (mounted) {
      setState(() {
        _currentDigit = i;
        if (_firstDigit == null) {
          _firstDigit = _currentDigit;
        } else if (_secondDigit == null) {
          _secondDigit = _currentDigit;
        } else if (_thirdDigit == null) {
          _thirdDigit = _currentDigit;
        } else if (_fourthDigit == null) {
          _fourthDigit = _currentDigit;
        } else if (_fiveDigit == null) {
          _fiveDigit = _currentDigit;
        } else if (_sixDigit == null) {
          _sixDigit = _currentDigit;
          final otpCode2 = _firstDigit.toString() +
              _secondDigit.toString() +
              _thirdDigit.toString() +
              _fourthDigit.toString() +
              _fiveDigit.toString() +
              _sixDigit.toString();
          if (otpCode2.length == SMS_AUTH_CODE_LEN) {
            otpCode = otpCode2;
            callAllApi();
          }
        }
      });
    }
  }

  void callAllApi() {
    Sms3APIMgr().wsLoginMobileOtpPutAPI(
      context: context,
      otpCode: otpCode,
      userID: userId,
      mobile: widget.mobileUserOTPModel.mobileNumber,
      callback: (model) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              //final msg = model.messages.postUserotp[0].toString();
              //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: msg, which: 1);
              Sms3APIMgr().wsLoginMobileFBPostAPI(
                context: context,
                mobileUserOTPModel: model.responseData.userOTP,
                otpCode: otpCode,
                callback: (model) async {
                  if (model != null && mounted) {
                    try {
                      if (model.success) {
                        try {
                          await DBMgr.shared
                              .setUserProfile(user: model.responseData.user);
                          await userData.setUserModel();
                          if (userData.userModel.communityID.toString() ==
                              "1") {
                            Get.offAll(
                              () => MainCustomerScreen(),
                            );
                          } else {
                            Get.off(() => NoAccess());
                          }
                        } catch (e) {
                          myLog(e.toString());
                        }
                      } else {
                        try {
                          if (mounted) {
                            final err = model.errorMessages.login[0].toString();
                            debugPrint("Error message = ${err}");
                            /* Get.defaultDialog(
                                    title: "Alert !",
                                    content: Text(
                                        "User not found with this number ${widget.mobileUserOTPModel.mobileNumber}.Press Ok to create account"),
                                    textConfirm: "Ok",
                                    onConfirm: () {
                                      navTo(context: context, isRep: true, page: () => RegScreen(phoneNumber:widget.mobileUserOTPModel.mobileNumber.toString().substring(2)));
                                    });*/

                            Get.off(RegScreen(
                              phoneNumber: widget
                                  .mobileUserOTPModel.mobileNumber
                                  .toString()
                                  .substring(2),
                            ));
                            // navTo(context: context, isRep: false, page: () => RegScreen(phoneNumber:widget.mobileUserOTPModel.mobileNumber.toString().substring(2),));

                            /*  Get.dialog(AlrtDialogUserNotFound(
                                  bgColor: MyTheme.brandColor,
                                  mobileNumber: widget.mobileUserOTPModel.mobileNumber.toString(),
                                msg: "User not found with this number.\nPress Ok to create account",));*/

                            /*   showToast(context: context,
                                    txtColor: Colors.white,
                                    bgColor: MyTheme.brandColor,
                                    msg:
                                        "We were unable to verify you. You may have entered a wrong code."
                                        "If you do not have access to the registered mobile phone, please contact your adviser/ company admin.");*/
                          }
                        } catch (e) {
                          myLog(e.toString());
                        }
                      }
                    } catch (e) {
                      myLog(e.toString());
                    }
                  }
                },
              );
            }
          } catch (e) {
            myLog(e.toString());
          }
        } else {
          try {
            if (mounted) {
              final err = model.errorMessages.postUserotp[0];
              showToast(context: context, msg: err, which: 0);
            }
          } catch (e) {
            myLog(e.toString());
          }
        }
      },
    );
  }
}
