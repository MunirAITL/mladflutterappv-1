import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'email/signin_page.dart';

class LoginLandingScreen extends StatefulWidget {
  @override
  State createState() => _LoginLandingScreenState();
}

class _LoginLandingScreenState extends State<LoginLandingScreen>
    with Mixin, StateListener {
  StateProvider _stateProvider;
  @override
  void onStateChanged(ObserverState state) async {
    try {} catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIMode(SystemUiMode.leanBack);
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: Container(
            width: getW(context),
            height: getH(context),
            decoration: BoxDecoration(
              image: DecorationImage(
                image:
                    AssetImage('assets/images/screens/welcome/welcome_bg.png'),
                fit: BoxFit.fill,
              ),
            ),
            child: Stack(
              children: [
                Positioned(
                  top: getHP(context, 40),
                  child: Container(
                    height: getHP(context, 20),
                    width: getW(context),
                    child: Image.asset(
                      "assets/images/logo/mm_tm.png",
                      //fit: BoxFit.fitWidth,
                      //height: getHP(context, 12),
                      //width: getW(context),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 20,
                  left: 20,
                  right: 20,
                  child: Container(
                    //color: Colors.black,
                    //height: getHP(context, 20),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 0, right: 0),
                      child: Column(
                        children: [
                          MMBtn(
                              txt: "Log in",
                              txtColor: Colors.white,
                              bgColor: null,
                              height: getHP(context, 7),
                              width: getW(context),
                              radius: 10,
                              callback: () {
                                Get.to(() => SigninPage());
                              }),
                          SizedBox(height: 10),
                          GestureDetector(
                            onTap: () {
                              Get.to(() => RegScreen());
                            },
                            child: Txt(
                                txt: "Open an account",
                                txtColor: HexColor.fromHex("#06152B"),
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: true),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
