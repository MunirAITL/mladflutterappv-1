import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/auth/hreflogin/PostEmailOtpAPIModel.dart';
import 'package:aitl/view/auth/email/get_email_page.dart';
import 'package:aitl/view/auth/otp/Sms3Screen.dart';
import 'package:aitl/view/auth/otp/Sms4Screen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../base_app.dart';
import 'package:open_mail_app/open_mail_app.dart';

class CheckEmailPage extends StatefulWidget {
  final UserOTP userOTP;
  final String email;
  const CheckEmailPage({Key key, @required this.email, @required this.userOTP})
      : super(key: key);

  @override
  State createState() => _CheckEmailPageState();
}

class _CheckEmailPageState extends BaseApp<CheckEmailPage> {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //extendBodyBehindAppBar: true,
        backgroundColor: MyTheme.bgColor,
        appBar: drawAppbar(
          onBack: () {
            Get.back();
          },
          backTitle: "Back",
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Column(
                children: [
                  SizedBox(height: 20),
                  Image.asset("assets/images/img/email_image.png"),
                  SizedBox(height: 20),
                  Txt(
                    txt: "Check your email",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize + .5,
                    txtAlign: TextAlign.center,
                    fontWeight: FontWeight.w500,
                    isBold: true,
                  ),
                  SizedBox(height: 20),
                  Txt(
                    txt: "We've sent an email to",
                    txtColor: Color(0xFFCCCCCC),
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  Txt(
                    txt: widget.email,
                    txtColor: Color(0xFFCCCCCC),
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  SizedBox(height: 5),
                  GestureDetector(
                    onTap: () {
                      Get.off(() => GetEmailPage(
                            email: widget.email,
                          ));
                    },
                    child: Center(
                      child: Text(
                        "I didn't receive my email",
                        style: TextStyle(
                            decoration: TextDecoration.underline,
                            fontSize: 16,
                            color: Color(0xFFCCCCCC),
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Txt(
                    txt: "It has a magic link that'll be using you in to",
                    txtColor: Color(0xFFCCCCCC),
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  Txt(
                    txt: "Mortgage Magic",
                    txtColor: Color(0xFFCCCCCC),
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: true,
                  ),
                ],
              ),
            ),
            SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: MMBtn(
                txt: "Open email app",
                txtColor: Colors.white,
                height: getHP(context, 7),
                width: getW(context),
                radius: 10,
                callback: () async {
                  //openUrl(context, "mailto:" + widget.email);
                  var result = await OpenMailApp.openMailApp(
                    nativePickerTitle:
                        'Select email app to proceed your login via link',
                  );

                  // If no mail apps found, show error
                  if (!result.didOpen && !result.canOpen) {
                    //showToast(context:context, msg: "Sorry, failed to open app");
                    openUrl(context, "mailto:" + widget.email);
                    // iOS: if multiple mail apps found, show dialog to select.
                    // There is no native intent/default app system in iOS so
                    // you have to do it yourself.
                  } else if (!result.didOpen && result.canOpen) {
                    showDialog(
                      context: context,
                      builder: (_) {
                        return MailAppPickerDialog(
                          mailApps: result.options,
                        );
                      },
                    );
                  }
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: MMBtn(
                txt: "Next",
                txtColor: Colors.white,
                height: getHP(context, 7),
                width: getW(context),
                radius: 10,
                callback: () async {
                  Get.to(() => Sms4Screen(userOTP: widget.userOTP));
                },
              ),
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
