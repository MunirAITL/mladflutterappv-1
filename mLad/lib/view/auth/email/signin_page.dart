import 'dart:convert';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/auth/LoginApiMgr.dart';
import 'package:aitl/controller/api/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/hreflogin/LoginAccExistsByEmailApiModel.dart';
import 'package:aitl/model/json/auth/hreflogin/PostEmailOtpAPIModel.dart';
import 'package:aitl/model/json/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import 'package:aitl/model/json/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/auth/otp/Sms3Screen.dart';
import 'package:aitl/view/base_app.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/href_login_dialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '../../NoAccess.dart';
import 'check_email_page.dart';

class SigninPage extends StatefulWidget {
  final hrefLoginData;
  const SigninPage({Key key, this.hrefLoginData}) : super(key: key);

  @override
  _SigninPageState createState() => _SigninPageState();
}

class _SigninPageState extends BaseApp<SigninPage> {
  final email = TextEditingController();

  callCheckUserEmailAPI() async {
    try {
      await APIViewModel().req<LoginAccExistsByEmailApiModel>(
          context: context,
          url: Server.LOGINACCEXISTSBYEMAIL_POST_URL,
          reqType: ReqType.Post,
          param: {"Email": email.text.trim()},
          callback: (model1) async {
            if (mounted) {
              if (model1 != null) {
                if (model1.success) {
                  await APIViewModel().req<PostEmailOtpAPIModel>(
                    context: context,
                    url: Server.POSTEMAILOTP_POST_URL,
                    reqType: ReqType.Post,
                    param: {
                      "Email": email.text.trim(),
                      "Status": "101",
                    },
                    callback: (model2) async {
                      if (mounted) {
                        if (model2 != null) {
                          if (model2.success) {
                            await APIViewModel().req<SendUserEmailOtpAPIModel>(
                                context: context,
                                url: Server.SENDUSEREMAILOTP_GET_URL.replaceAll(
                                    "#otpId#",
                                    model2.responseData.userOTP.id.toString()),
                                reqType: ReqType.Get,
                                callback: (model3) async {
                                  if (mounted) {
                                    if (model3 != null) {
                                      if (model3.success) {
                                        Get.to(() => CheckEmailPage(
                                            userOTP:
                                                model3.responseData.userOTP,
                                            email: email.text.trim()));
                                      } else {
                                        final err = model1.messages.login[0];
                                        showToast(
                                            context: context,
                                            msg: err,
                                            which: 0);
                                      }
                                    }
                                  }
                                });
                          } else {
                            final err = model1.messages.login[0];
                            showToast(context: context, msg: err, which: 0);
                          }
                        }
                      }
                    },
                  );
                } else {
                  Get.dialog(HrefLoginDialog(
                      context: context,
                      callback: () {
                        Get.off(
                            () => RegScreen(emailAddress: email.text.trim()));
                      }));
                }
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      FocusScope.of(context).requestFocus(FocusNode());
      //testLogin();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: false,
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Row(
                children: [
                  SizedBox(width: 20),
                  Icon(Icons.arrow_back, color: Colors.white),
                  SizedBox(width: 5),
                  /*Txt(
                    txt: "Back", //"Sign in to Access My File",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),*/
                ],
              ),
            ),
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 60),
                    Center(
                      child: Txt(
                        txt:
                            "Enter the email address associated with your account.",
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        fontWeight: FontWeight.w500,
                        isBold: false,
                      ),
                    ),
                    SizedBox(height: 40),
                    Txt(
                      txt: "Email Address:",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      fontWeight: FontWeight.w400,
                      isBold: false,
                    ),
                    TextField(
                        controller: email,
                        autofocus: true,
                        maxLength: 50,
                        autocorrect: false,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize + .5),
                          height: MyTheme.txtLineSpace,
                        ),
                        keyboardType: TextInputType.emailAddress,
                        decoration: new InputDecoration(
                          isDense: true,
                          contentPadding:
                              EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                          counterText: '',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.black, width: .5),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.black, width: .5),
                          ),
                          border: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.white, width: .5),
                          ),
                        )),
                    SizedBox(height: 50),
                    MMBtn(
                        txt: "Next",
                        txtColor: Colors.white,
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          //if (!Server.isOtp) {
                          //testLogin();
                          //} else {
                          if (RegExp(UserProfileVal.EMAIL_REG)
                              .hasMatch(email.text.trim())) {
                            callCheckUserEmailAPI();
                          } else {
                            showToast(
                                context: context,
                                msg: "Invalid email address entered");
                          }
                        }),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  testLogin() {
    LoginAPIMgr().wsLoginAPI(
      context: context,
      email: email.text =
          "anisur5001@yopmail.com", //"rc.cust.16@cool.fr.nf", // "anisur5001@yopmail.com", //"rc.cust.16@cool.fr.nf"; //LIVE
      pwd: "123456",
      callback: (model) async {
        if (model != null && mounted) {
          if (model.success) {
            await DBMgr.shared.setUserProfile(user: model.responseData.user);
            await userData.setUserModel();
            if (userData.userModel.communityID.toString() == "1") {
              Get.offAll(() => MainCustomerScreen());
            } else {
              Get.off(() => NoAccess());
            }
          } else {
            try {
              if (mounted) {
                final err = model.errorMessages.login[0].toString();
                showToast(context: context, msg: err);
              }
            } catch (e) {
              myLog(e.toString());
            }
          }
        }
      },
    );
  }
}
