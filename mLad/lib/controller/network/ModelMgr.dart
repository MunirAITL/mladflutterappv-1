import 'package:aitl/controller/api/db_cus/noti/DeleteNotification.dart';
import 'package:aitl/model/json/auth/hreflogin/LoginAccExistsByEmailApiModel.dart';
import 'package:aitl/model/json/auth/hreflogin/PostEmailOtpAPIModel.dart';
import 'package:aitl/model/json/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import 'package:aitl/model/json/db_cus/action_req/GetMortgageCaseInfoLocTaskidUseridAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDertailsAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoPostAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/GetSimulatedScoreUseridAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/GetUserValidationOldUserApiModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/PostKbaCrditQAAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/getKbaQuestionAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/getSummeryAPIModel.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocByBase64BitDataAPIModel.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostSelfieByBase64DataAPIModel.dart';
import 'package:aitl/model/json/db_cus/open_banking/GetForeCasts4PersonalAccountsAPIModel.dart';
import 'package:aitl/model/json/db_cus/open_banking/GetInsight4PersonnelAccountsAPIModel.dart';
import 'package:aitl/model/json/db_cus/review_rating/ReviewRatingAPIModel.dart';
import 'package:aitl/model/json/db_cus/review_rating/ReviewRatingPostAPIModel.dart';
import 'package:aitl/model/json/db_cus/submit_case/SubmitCaseAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_dash/PrivacyPolicyAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgePhotoIDAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgePhotoIDDelAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/settings/ChangePwdAPIModel.dart';
import 'package:aitl/model/json/auth/ForgotAPIModel.dart';
import 'package:aitl/model/json/auth/LoginAPIModel.dart';
import 'package:aitl/model/json/auth/RegAPIModel.dart';
import 'package:aitl/model/json/auth/email/VerifyEmailAPIModel.dart';
import 'package:aitl/model/json/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/model/json/auth/otp/SendOtpNotiAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/CaseReviewAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/CaseInfosAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/GetCaseGroupChatAPIModel.dart';
import 'package:aitl/model/json/db_intr/caselead/CasePayInfoAPIModel.dart';
import 'package:aitl/model/json/db_intr/caselead/GetlLeadStatusIntrwiseReportDatabyIntrAPIModel.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/model/json/misc/CommonAPIModel.dart';
import 'package:aitl/model/json/misc/FcmDeviceInfoAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/edit_profile/DeactivateProfileAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/noti/FcmTestNotiAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/noti/NotiSettingsAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/noti/NotiSettingsPostAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/support/ResolutionAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/EditCaseAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/PostCaseAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_mycases/TaskInfoSearchAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/edit_profile/UserProfileAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNoteByEntityAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNotePutAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_noti/NotiAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TaskBiddingAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLineAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLineAdvisorAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLinePostAPIModel.dart';
import 'package:aitl/model/json/db_cus/case_lead/CaseLeadNegotiatorUserAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/MessageTypeAdvisorAPIModel.dart';

//  https://stackoverflow.com/questions/56271651/how-to-pass-a-generic-type-as-a-parameter-to-a-future-in-flutter
class ModelMgr {
  final listModels = [
    FcmDeviceInfoAPIModel,
    CommonAPIModel,
    LoginAPIModel,
    MobileUserOtpPostAPIModel,
    SendOtpNotiAPIModel,
    MobileUserOtpPutAPIModel,
    VerifyEmailAPIModel,
    LoginRegOtpFBAPIModel,
    ForgotAPIModel,
    ChangePwdAPIModel,
    RegAPIModel,
    DeactivateProfileAPIModel,
    UserProfileAPIModel,
    UserNoteByEntityAPIModel,
    UserNotePutAPIModel,
    TaskInfoSearchAPIModel,
    PostCaseAPIModel,
    EditCaseAPIModel,
    NotiAPIModel,
    GetCaseGroupChatAPIModel,
    TimeLineAdvisorAPIModel,
    TimeLineAPIModel,
    TimeLinePostAPIModel,
    TaskBiddingAPIModel,
    MediaUploadFilesAPIModel,
    ResolutionAPIModel,
    NotiSettingsAPIModel,
    NotiSettingsPostAPIModel,
    FcmTestNotiAPIModel,
    GetMortgageCaseInfoLocTaskidUseridAPIModel,
    BadgeAPIModel,
    BadgeEmailAPIModel,
    BadgePhotoIDAPIModel,
    BadgePhotoIDDelAPIModel,
    DeleteNotificationModel,
    CaseReviewAPIModel,
    MessageTypeAdvisorAPIModel,
    CreditDetailsAPIModel,
    GetUserValidationOldUserApiModel,
    CreditInfoPostAPIModel,
    GetKbaQuestionAPIModel,
    PostKbaCrditQAAPIModel,
    GetSummeryAPIModel,
    GetSimulatedScoreUseridAPIModel,
//  open banking
    GetInsight4PersonnelAccountsAPIModel,
    GetForeCasts4PersonalAccountsAPIModel,
//  db intr
    SubmitCaseAPIModel,
    CaseLeadNegotiatorUserAPIModel,
    CasePayInfoAPIModel,
    GetlLeadStatusIntrwiseReportDatabyIntrAPIModel,
    CaseInfosApiModel,
    PrivacyPolicyAPIModel,
//  eid
    PostSelfieByBase64DataAPIModel,
    PostDocByBase64BitAPIModel,
    PostDocVerifyAPIModel,
//  login by email
    LoginAccExistsByEmailApiModel,
    PostEmailOtpAPIModel,
    SendUserEmailOtpAPIModel,
//  review and rating
    ReviewRatingAPIModel,
    ReviewRatingPostAPIModel,
  ];

  /// If T is a List, K is the subtype of the list.
  Future<T> fromJson<T, K>(dynamic json) async {
    if (identical(T, FcmDeviceInfoAPIModel)) {
      return FcmDeviceInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, CommonAPIModel)) {
      return CommonAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPostAPIModel)) {
      return MobileUserOtpPostAPIModel.fromJson(json) as T;
    } else if (identical(T, SendOtpNotiAPIModel)) {
      return SendOtpNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPutAPIModel)) {
      return MobileUserOtpPutAPIModel.fromJson(json) as T;
    } else if (identical(T, VerifyEmailAPIModel)) {
      return VerifyEmailAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginRegOtpFBAPIModel)) {
      return LoginRegOtpFBAPIModel.fromJson(json) as T;
    } else if (identical(T, ForgotAPIModel)) {
      return ForgotAPIModel.fromJson(json) as T;
    } else if (identical(T, ChangePwdAPIModel)) {
      return ChangePwdAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, DeactivateProfileAPIModel)) {
      return DeactivateProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserProfileAPIModel)) {
      return UserProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNoteByEntityAPIModel)) {
      return UserNoteByEntityAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNotePutAPIModel)) {
      return UserNotePutAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskInfoSearchAPIModel)) {
      return TaskInfoSearchAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCaseAPIModel)) {
      return PostCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, EditCaseAPIModel)) {
      return EditCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiAPIModel)) {
      return NotiAPIModel.fromJson(json) as T;
    } else if (identical(T, GetCaseGroupChatAPIModel)) {
      return GetCaseGroupChatAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAdvisorAPIModel)) {
      return TimeLineAdvisorAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAPIModel)) {
      return TimeLineAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLinePostAPIModel)) {
      return TimeLinePostAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingAPIModel)) {
      return TaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, ResolutionAPIModel)) {
      return ResolutionAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsAPIModel)) {
      return NotiSettingsAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsPostAPIModel)) {
      return NotiSettingsPostAPIModel.fromJson(json) as T;
    } else if (identical(T, FcmTestNotiAPIModel)) {
      return FcmTestNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, GetMortgageCaseInfoLocTaskidUseridAPIModel)) {
      return GetMortgageCaseInfoLocTaskidUseridAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgeAPIModel)) {
      return BadgeAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgeEmailAPIModel)) {
      return BadgeEmailAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgePhotoIDAPIModel)) {
      return BadgePhotoIDAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgePhotoIDDelAPIModel)) {
      // return BadgePhotoIDDelAPIModel.fromJson(json) as T;
    } else if (identical(T, DeleteNotificationModel)) {
      return DeleteNotificationModel.fromJson(json) as T;
    } else if (identical(T, CaseReviewAPIModel)) {
      return CaseReviewAPIModel.fromJson(json) as T;
    } else if (identical(T, MessageTypeAdvisorAPIModel)) {
      return MessageTypeAdvisorAPIModel.fromJson(json) as T;
    } else if (identical(T, CreditDetailsAPIModel)) {
      return CreditDetailsAPIModel.fromJson(json) as T;
    } else if (identical(T, GetUserValidationOldUserApiModel)) {
      return GetUserValidationOldUserApiModel.fromJson(json) as T;
    } else if (identical(T, CreditInfoPostAPIModel)) {
      return CreditInfoPostAPIModel.fromJson(json) as T;
    } else if (identical(T, GetKbaQuestionAPIModel)) {
      return GetKbaQuestionAPIModel.fromJson(json) as T;
    } else if (identical(T, PostKbaCrditQAAPIModel)) {
      return PostKbaCrditQAAPIModel.fromJson(json) as T;
    } else if (identical(T, GetSummeryAPIModel)) {
      return GetSummeryAPIModel.fromJson(json) as T;
    } else if (identical(T, GetSimulatedScoreUseridAPIModel)) {
      return GetSimulatedScoreUseridAPIModel.fromJson(json) as T;
    }

    //  Open Banking
    else if (identical(T, GetInsight4PersonnelAccountsAPIModel)) {
      return GetInsight4PersonnelAccountsAPIModel.fromJson(json) as T;
    } else if (identical(T, GetForeCasts4PersonalAccountsAPIModel)) {
      return GetForeCasts4PersonalAccountsAPIModel.fromJson(json) as T;
    }

    //  ********************************************  DB_INTR
    else if (identical(T, SubmitCaseAPIModel)) {
      return SubmitCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, CaseLeadNegotiatorUserAPIModel)) {
      return CaseLeadNegotiatorUserAPIModel.fromJson(json) as T;
    } else if (identical(T, CasePayInfoAPIModel)) {
      return CasePayInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, GetlLeadStatusIntrwiseReportDatabyIntrAPIModel)) {
      return GetlLeadStatusIntrwiseReportDatabyIntrAPIModel.fromJson(json) as T;
    } else if (identical(T, CaseInfosApiModel)) {
      return CaseInfosApiModel.fromJson(json) as T;
    } else if (identical(T, PrivacyPolicyAPIModel)) {
      return PrivacyPolicyAPIModel.fromJson(json) as T;
    } else if (identical(T, PostSelfieByBase64DataAPIModel)) {
      return PostSelfieByBase64DataAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDocByBase64BitAPIModel)) {
      return PostDocByBase64BitAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDocVerifyAPIModel)) {
      return PostDocVerifyAPIModel.fromJson(json) as T;
    }

    //  ******************************************* HREF LOGIN BY EMAIL
    else if (identical(T, LoginAccExistsByEmailApiModel)) {
      return LoginAccExistsByEmailApiModel.fromJson(json) as T;
    } else if (identical(T, PostEmailOtpAPIModel)) {
      return PostEmailOtpAPIModel.fromJson(json) as T;
    } else if (identical(T, SendUserEmailOtpAPIModel)) {
      return SendUserEmailOtpAPIModel.fromJson(json) as T;
    }
    //  ******************************************  REVIEW RATING
    else if (identical(T, ReviewRatingAPIModel)) {
      return ReviewRatingAPIModel.fromJson(json) as T;
    } else if (identical(T, ReviewRatingPostAPIModel)) {
      return ReviewRatingPostAPIModel.fromJson(json) as T;
    }

    /*if (json is Iterable) {
      return _fromJsonList<K>(json) as T;
    } else if (identical(T, LoginModel)) {
      return LoginModel.fromJson(json) as T;
    } else if (T == bool ||
        T == String ||
        T == int ||
        T == double ||
        T == Map) {
      // primitives
      return json;
    } else {
      throw Exception("Unknown class");
    }*/
  }

  /*List<K> _fromJsonList<K>(List<dynamic> jsonList) {
    return jsonList
        ?.map<K>((dynamic json) => fromJson<K, void>(json))
        ?.toList();
  }*/
}
