//  https://github.com/flutterchina/dio
import 'dart:convert';
import 'dart:io';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/FromJsonModel.dart';
import 'package:aitl/view_model/rx/UploadProgController.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import '../../Mixin.dart';
import 'ModelMgr.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'CookieMgr.dart';
import 'package:http_parser/http_parser.dart';
import 'package:http/http.dart' show get;
import 'package:mime_type/mime_type.dart';
import 'package:json_string/json_string.dart';
import 'package:data_connection_checker/data_connection_checker.dart';

//typedef mapValue = Function(Map<String, dynamic>);

enum NetEnum { Available, NotAvailable, ServerDown }

enum ReqType {
  Get,
  Post,
  Put,
  Delete,
  Head,
  Patch,
  Copy,
  Options,
  Link,
  UnLink,
  Purge,
  Lock,
  UnLock,
  PropFind,
  View,
}

class NetworkMgr with Mixin {
  static final NetworkMgr shared = NetworkMgr._internal();
  factory NetworkMgr() {
    return shared;
  }
  NetworkMgr._internal();

  Future<T> req<T, K>(
      {context,
      url,
      param,
      reqType = ReqType.Post,
      isLoading = true,
      isCookie = false,
      headers,
      UploadProgController progController}) async {
    final NetEnum netEnum = await hasNetwork();
    if (netEnum == NetEnum.Available) {
      try {
        if (isLoading) {
          startLoading();
        }
        myLog("ws::" + reqType.toString() + "==================" + url);
        try {
          for (var p in param.entries) {
            myLog(p.key + '=' + p.value.toString());
          }
        } catch (e) {}

        var _dio = Dio();
        _dio.options.headers = headers ?? CookieMgr.headers;

        //if (isCookie) {
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        if (isCookie) {
          cj.saveFromResponse(
              Uri.parse(url), await cj.loadForRequest(Uri.parse(url)));
          print(cj.loadForRequest(Uri.parse(url)));
        }

        var response;
        if (reqType == ReqType.Post)
          response = await _dio.post(
            url,
            data: param,
            onSendProgress: (int sent, int total) {
              final progress = sent / total;
              if (progController != null)
                progController.progress.value = progress;
              //print('progress: $progress ($sent/$total)');
            },
            onReceiveProgress: (int sent, int total) {
              //final progress = sent / total;
              //progController.progress.value = progress;
              //print('progress: $progress ($sent/$total)');
            },
          );
        else if (reqType == ReqType.Put)
          response = await _dio.put(
            url,
            data: param,
            onSendProgress: (int sent, int total) {
              final progress = sent / total;
              if (progController != null)
                progController.progress.value = progress;
              //print('progress: $progress ($sent/$total)');
            },
          );
        else if (reqType == ReqType.Delete)
          response = await _dio.delete(url);
        else if (reqType == ReqType.Get)
          response = await _dio.get(url, queryParameters: param ?? {});
        else if (reqType == ReqType.Head)
          response = await _dio.head(url);
        else if (reqType == ReqType.Patch) response = await _dio.patch(url);
        _dio.close();

        final jsonString = JsonString(json.encode(response.data));
        myLog(jsonString.source);
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          //final m = FromJsonModel.typeOf<T>();
          //print((m as T)(mapRes));
          //.fromJson(mapRes);
          return ModelMgr().fromJson(mapRes);

          //final c = BaseModel.typeOf<T>().toString();
          //print(t);
          //final m = c.fromJson(mapRes);
          //return m as T;
          //return t;
        } else {
          return null;
        }
      } catch (e) {
        myLog(
            "******************** NETWORK ERROR *****************************");
        myLog(e.toString());
        myLog(
            "***************************************************************");
        if (isLoading) {
          stopLoading();
        }
      }
    } else if (netEnum == NetEnum.NotAvailable) {
      showSnake("Alert!", "Sorry, internet is not available. try again later");
    } else if (netEnum == NetEnum.ServerDown) {
      showSnake("Alert!", "Sorry, web server is down please try later. thanks");
    }
    return null;
  }

  //  files only
  Future<T> uploadFiles<T, K>(
      {context,
      url,
      List<File> files,
      isLoading = true,
      UploadProgController progController}) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          startLoading();
        }
        myLog("ws::uploadFiles:: files only  ==================" + url);
        var formData = FormData();
        for (var file in files) {
          //final String mimeStr = lookupMimeType(file.path);
          //var fileType = mimeStr.split('/');
          //myLog(fileType[0]);
          //myLog('file type ${mimeStr}, ${fileType}');
          //final String fileName = file.path.split('/').last;
          //myLog(MediaType(fileType[0], mimeStr));
          /*formData.files.addAll(
            [
              MapEntry(
                  "file",
                  await MultipartFile.fromFile(file.path,
                      filename: basename(file.path),
                      contentType: MediaType(fileType[0], mimeStr)))
            ],
          );*/

          String mimeType = mime(file.path);
          String mimee = mimeType.split('/')[0];
          String type = mimeType.split('/')[1];

          formData = FormData.fromMap({
            "files": [
              await MultipartFile.fromFile(
                file.path,
                filename: file.path,
                contentType: MediaType(mimee, type),
              )
            ],
          });
        }

        var _dio = Dio();
        _dio.options.headers = {
          'Content-type': 'multipart/form-data',
        };
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(
            Uri.parse(url), await cj.loadForRequest(Uri.parse(url)));
        print(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.post(
          url,
          data: formData,
          onSendProgress: (int sent, int total) {
            final progress = sent / total;
            if (progController != null)
              progController.progress.value = progress;
            //print('progress: $progress ($sent/$total)');
          },
        );
        _dio.close();
        myLog(response.data.toString());
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        showSnake(
            "Alert!", "Sorry, internet is not available. try again later");
      } else if (netEnum == NetEnum.ServerDown) {
        showSnake(
            "Alert!", "Sorry, web server is down please try later. thanks");
      }
    } catch (e) {
      myLog("******************** NETWORK ERROR *****************************");
      myLog(e.toString());
      myLog("***************************************************************");
      if (isLoading) {
        stopLoading();
      }
    }
  }

  //  multipart
  Future<T> postMultiPart<T, K>({context, url, param, isLoading = true}) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          startLoading();
        }
        myLog("ws::postFile :: multipart  ==================" + url);
        var _dio = Dio();
        _dio.options.headers = CookieMgr.headers;
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(
            Uri.parse(url), await cj.loadForRequest(Uri.parse(url)));
        //myLog(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.post(url, data: FormData.fromMap(param));
        _dio.close();
        myLog(response.data.toString());
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        showSnake(
            "Alert!", "Sorry, internet is not available. try again later");
      } else if (netEnum == NetEnum.ServerDown) {
        showSnake(
            "Alert!", "Sorry, web server is down please try later. thanks");
      }
    } catch (e) {
      myLog("******************** NETWORK ERROR *****************************");
      myLog(e.toString());
      myLog("***************************************************************");
      if (isLoading) {
        stopLoading();
      }
    }
  }

  downloadFile({
    BuildContext context,
    String url,
    isLoading = true,
    Function(File) callback,
  }) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          startLoading();
        }
        var response = await get(Uri.parse(url));
        if (response.statusCode == 200) {
          myLog("ws::downloadFile ==================" + url);
          final String fileName = url.split('/').last;
          final dir = "/MortgageMagic/";
          Directory directory;
          if (Platform.isIOS) {
            directory = await getApplicationDocumentsDirectory();
          } else {
            directory = await getExternalStorageDirectory();
          }

          directory = Directory(directory.path + dir);
          if (!await directory.exists()) {
            await directory.create(recursive: true);
          }
          if (await directory.exists()) {
            String fullPath = directory.path + fileName;
            File file = File(fullPath);
            var raf = file.openSync(mode: FileMode.write);
            raf.writeFromSync(response.bodyBytes);
            await raf.close();
            callback(file);
          }
        }

        if (isLoading) {
          stopLoading();
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        showSnake(
            "Alert!", "Sorry, internet is not available. try again later");
      } else if (netEnum == NetEnum.ServerDown) {
        showSnake(
            "Alert!", "Sorry, web server is down please try later. thanks");
      }
    } catch (e) {
      myLog("******************** NETWORK ERROR *****************************");
      myLog(e.toString());
      myLog("***************************************************************");
      if (isLoading) {
        stopLoading();
      }
    }
  }

  dispose() {
    try {
      stopLoading();
      //_dio.close();
      //_dio = null;
    } catch (e) {}
  }

  //  0=ok
  //  1=internet not available
  //  2=server is down
  Future<NetEnum> hasNetwork() async {
    /*var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return await isServerLive();
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return await isServerLive();
    }
    return NetEnum.NotAvailable;*/

    return (await DataConnectionChecker().hasConnection)
        ? NetEnum.Available
        : NetEnum.NotAvailable;
  }

  Future<NetEnum> isServerLive() async {
    try {
      final result = await InternetAddress.lookup(
          Server.BASE_URL.replaceAll("https://", ""));
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('server connected');
        return NetEnum.Available;
      }
    } on SocketException catch (_) {
      print('server not connected');
    }
    return NetEnum.ServerDown;
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
