import 'dart:io';

class LoginMobOtpPutHelper {
  getParam({
    String mobileNumber,
    String otpCode = '',
    var userId ="",
  }) {
    return {
      "MobileNumber": mobileNumber,
      "OTPCode": otpCode,
      "UserId":userId,
      "DeviceType":(Platform.isAndroid) ? 'Android' : 'iOS',
      "Persist":true,
    };
  }
}
