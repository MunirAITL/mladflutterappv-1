import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';
import 'package:flutter/material.dart';

/*
class EditCaseHelper {
  getParam({
    LocationsModel caseModel,
    bool isOtherApplicantSwitch,
    bool isSPVSwitch,
    List<TextEditingController> listOApplicantInputFieldsCtr,
    String compName,
    String regAddr,
    String regDate,
    String regNo,
    String title,
    String note,
  }) {
    try {
      final paramMortgageCaseInfoEntityModelList = [];
      String customerEmail1 = "";
      String customerEmail2 = "";
      String customerEmail3 = "";
      try {
        customerEmail1 = listOApplicantInputFieldsCtr[0].text;
      } catch (e) {}
      try {
        customerEmail2 = listOApplicantInputFieldsCtr[1].text;
      } catch (e) {}
      try {
        customerEmail3 = listOApplicantInputFieldsCtr[2].text;
      } catch (e) {}

      final paramMortgageCaseInfoEntity = {
        "UserId": userData.userModel.id.toString(),
        "CompanyId": userData.userModel.userCompanyID,
        "TaskId": caseModel.entityID,
        "Status": 0,
        "CreationDate": caseModel.creationDate,
        "UpdatedDate": caseModel.updatedDate,
        "VersionNumber": 0,
        "CaseType": "",
        "CustomerType": "",
        "IsSmoker": "No",
        "Remarks": "",
        "IsAnyOthers": (isOtherApplicantSwitch) ? "Yes" : "No",
        "CustomerEmail1": customerEmail1,
        "CustomerEmail2": customerEmail2,
        "CustomerEmail3": customerEmail3,
        "IsCurrentProperty": "No",
        "CustomerName": userData.userModel.name,
        "CustomerEmail": userData.userModel.email,
        "CustomerMobileNumber": userData.userModel.mobileNumber,
        "CustomerAddress": (userData.userModel.address +
                ' ' +
                userData.userModel.addressLine1 +
                ' ' +
                userData.userModel.addressLine2 +
                ' ' +
                userData.userModel.addressLine3)
            .trim(),
        "CoapplicantUserId": 0,
        "AreYouBuyingThePropertyInNameOfASPV": (isSPVSwitch) ? "Yes" : "No",
        "CompanyName": compName.trim(),
        "RegisteredAddress": regAddr.trim(),
        "DateRegistered": regDate,
        "CompanyRegistrationNumber": regNo.trim(),
        "DateRegistered1": "",
        "DateRegistered2": "",
        "DateRegistered3": "",
        "AdminFee": 0,
        "AdminFeeWhenPayable": "",
        "AdviceFee": 0,
        "AdviceFeeWhenPayable": "",
        "IsFeesRefundable": "",
        "FeesRefundable": ""
      };

      return {
        "Id": caseModel.id,
        "Status": caseModel.status,
        "Title": title,
        "Description": note,
        "IsInPersonOrOnline": caseModel.isInPersonOrOnline,
        "DutDateType": caseModel.dutDateType,
        "DeliveryDate": caseModel.deliveryDate,
        "DeliveryTime": caseModel.deliveryTime,
        "WorkerNumber": caseModel.workerNumber,
        "Skill": caseModel.skill,
        "IsFixedPrice": caseModel.isFixedPrice,
        "HourlyRate": caseModel.hourlyRate,
        "FixedBudgetAmount": caseModel.fixedBudgetAmount,
        "NetTotalAmount": caseModel.netTotalAmount,
        "JobCategory": caseModel.jobCategory,
        "PreferedLocation": caseModel.preferedLocation,
        "Requirements": caseModel.requirements,
        "TotalHours": caseModel.totalHours,
        "Latitude": caseModel.latitude,
        "Longitude": caseModel.longitude,
        "TaskReferenceNumber": caseModel.taskReferenceNumber,
        "ReferenceTaskerId": caseModel.taskReferenceNumber,
        "MortgageCaseInfoEntityModelList": paramMortgageCaseInfoEntityModelList,
        "UserId": userData.userModel.id.toString(),
        "EntityId": userData.userModel.userCompanyInfo.entityID,
        "EntityName": userData.userModel.userCompanyInfo.entityName,
        "CompanyId": userData.userModel.userCompanyID,
        "MortgageCaseInfoEntity": paramMortgageCaseInfoEntity,
      };
    } catch (e) {}
  }
}
*/

class EditCaseHelper {
  int userId;
  int status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  String title;
  String description;
  bool isInPersonOrOnline;
  int dutDateType;
  String deliveryDate;
  String deliveryTime;
  int workerNumber;
  String skill;
  bool isFixedPrice;
  dynamic hourlyRate;
  dynamic fixedBudgetAmount;
  dynamic netTotalAmount;
  dynamic paidAmount;
  dynamic dueAmount;
  String jobCategory;
  int employeeId;
  int totalBidsNumber;
  bool isArchive;
  String preferedLocation;
  dynamic latitude;
  dynamic longitude;
  String ownerName;
  String thumbnailPath;
  String remarks;
  String taskReferenceNumber;
  String imageServerUrl;
  String ownerImageUrl;
  String requirements;
  dynamic totalHours;
  String taskTitleUrl;
  String ownerProfileUrl;
  int totalAcceptedNumber;
  int totalCompletedNumber;
  int companyId;
  String companyName;
  int entityId;
  String entityName;
  double introducerFeeShareAmount;
  double introducerPaymentedAmount;
  double adviserAmount;
  double adviserPaymentedAmount;
  int notificationUnreadTaskCount;
  int notificationTaskCount;
  String addressOfPropertyToBeMortgaged;
  String overAllCaseGrade;
  int caseObservationAssessmentId;
  String namePrefix;
  String lastName;
  int caseStatus;
  String areYouChargingAFee;
  int chargeFeeAmount;
  String chargingFeeWhenPayable;
  String chargingFeeRefundable;
  String areYouChargingAnotherFee;
  int chargeAnotherFeeAmount;
  String chargingAnotherFeeWhenPayable;
  String chargingAnotherFeeRefundable;
  int id;
  MortgageCaseInfoEntity mortgageCaseInfoEntity;

  EditCaseHelper({this.userId, this.status, this.creationDate, this.updatedDate, this.versionNumber, this.title, this.description, this.isInPersonOrOnline, this.dutDateType, this.deliveryDate, this.deliveryTime, this.workerNumber, this.skill, this.isFixedPrice, this.hourlyRate, this.fixedBudgetAmount, this.netTotalAmount, this.paidAmount, this.dueAmount, this.jobCategory, this.employeeId, this.totalBidsNumber, this.isArchive, this.preferedLocation, this.latitude, this.longitude, this.ownerName, this.thumbnailPath, this.remarks, this.taskReferenceNumber, this.imageServerUrl, this.ownerImageUrl, this.requirements, this.totalHours, this.taskTitleUrl, this.ownerProfileUrl, this.totalAcceptedNumber, this.totalCompletedNumber, this.companyId, this.companyName, this.entityId, this.entityName, this.introducerFeeShareAmount, this.introducerPaymentedAmount, this.adviserAmount, this.adviserPaymentedAmount, this.notificationUnreadTaskCount, this.notificationTaskCount, this.addressOfPropertyToBeMortgaged, this.overAllCaseGrade, this.caseObservationAssessmentId, this.namePrefix, this.lastName, this.caseStatus, this.areYouChargingAFee, this.chargeFeeAmount, this.chargingFeeWhenPayable, this.chargingFeeRefundable, this.areYouChargingAnotherFee, this.chargeAnotherFeeAmount, this.chargingAnotherFeeWhenPayable, this.chargingAnotherFeeRefundable, this.id, this.mortgageCaseInfoEntity});

  EditCaseHelper.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    updatedDate = json['UpdatedDate'];
    versionNumber = json['VersionNumber'];
    title = json['Title'];
    description = json['Description'];
    isInPersonOrOnline = json['IsInPersonOrOnline'];
    dutDateType = json['DutDateType'];
    deliveryDate = json['DeliveryDate'];
    deliveryTime = json['DeliveryTime'];
    workerNumber = json['WorkerNumber'];
    skill = json['Skill'];
    isFixedPrice = json['IsFixedPrice'];
    hourlyRate = json['HourlyRate'];
    fixedBudgetAmount = json['FixedBudgetAmount'];
    netTotalAmount = json['NetTotalAmount'];
    paidAmount = json['PaidAmount'];
    dueAmount = json['DueAmount'];
    jobCategory = json['JobCategory'];
    employeeId = json['EmployeeId'];
    totalBidsNumber = json['TotalBidsNumber'];
    isArchive = json['IsArchive'];
    preferedLocation = json['PreferedLocation'];
    latitude = json['Latitude'];
    longitude = json['Longitude'];
    ownerName = json['OwnerName'];
    thumbnailPath = json['ThumbnailPath'];
    remarks = json['Remarks'];
    taskReferenceNumber = json['TaskReferenceNumber'];
    imageServerUrl = json['ImageServerUrl'];
    ownerImageUrl = json['OwnerImageUrl'];
    requirements = json['Requirements'];
    totalHours = json['TotalHours'];
    taskTitleUrl = json['TaskTitleUrl'];
    ownerProfileUrl = json['OwnerProfileUrl'];
    totalAcceptedNumber = json['TotalAcceptedNumber'];
    totalCompletedNumber = json['TotalCompletedNumber'];
    companyId = json['CompanyId'];
    companyName = json['CompanyName'];
    entityId = json['EntityId'];
    entityName = json['EntityName'];
    introducerFeeShareAmount = json['IntroducerFeeShareAmount'];
    introducerPaymentedAmount = json['IntroducerPaymentedAmount'];
    adviserAmount = json['AdviserAmount'];
    adviserPaymentedAmount = json['AdviserPaymentedAmount'];
    notificationUnreadTaskCount = json['NotificationUnreadTaskCount'];
    notificationTaskCount = json['NotificationTaskCount'];
    addressOfPropertyToBeMortgaged = json['AddressOfPropertyToBeMortgaged'];
    overAllCaseGrade = json['OverAllCaseGrade'];
    caseObservationAssessmentId = json['CaseObservationAssessmentId'];
    namePrefix = json['NamePrefix'];
    lastName = json['LastName'];
    caseStatus = json['CaseStatus'];
    areYouChargingAFee = json['AreYouChargingAFee'];
    chargeFeeAmount = json['ChargeFeeAmount'];
    chargingFeeWhenPayable = json['ChargingFeeWhenPayable'];
    chargingFeeRefundable = json['ChargingFeeRefundable'];
    areYouChargingAnotherFee = json['AreYouChargingAnotherFee'];
    chargeAnotherFeeAmount = json['ChargeAnotherFeeAmount'];
    chargingAnotherFeeWhenPayable = json['ChargingAnotherFeeWhenPayable'];
    chargingAnotherFeeRefundable = json['ChargingAnotherFeeRefundable'];
    id = json['Id'];
    mortgageCaseInfoEntity = json['MortgageCaseInfoEntity'] != null ? new MortgageCaseInfoEntity.fromJson(json['MortgageCaseInfoEntity']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['Title'] = this.title;
    data['Description'] = this.description;
    data['IsInPersonOrOnline'] = this.isInPersonOrOnline;
    data['DutDateType'] = this.dutDateType;
    data['DeliveryDate'] = this.deliveryDate;
    data['DeliveryTime'] = this.deliveryTime;
    data['WorkerNumber'] = this.workerNumber;
    data['Skill'] = this.skill;
    data['IsFixedPrice'] = this.isFixedPrice;
    data['HourlyRate'] = this.hourlyRate;
    data['FixedBudgetAmount'] = this.fixedBudgetAmount;
    data['NetTotalAmount'] = this.netTotalAmount;
    data['PaidAmount'] = this.paidAmount;
    data['DueAmount'] = this.dueAmount;
    data['JobCategory'] = this.jobCategory;
    data['EmployeeId'] = this.employeeId;
    data['TotalBidsNumber'] = this.totalBidsNumber;
    data['IsArchive'] = this.isArchive;
    data['PreferedLocation'] = this.preferedLocation;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['OwnerName'] = this.ownerName;
    data['ThumbnailPath'] = this.thumbnailPath;
    data['Remarks'] = this.remarks;
    data['TaskReferenceNumber'] = this.taskReferenceNumber;
    data['ImageServerUrl'] = this.imageServerUrl;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['Requirements'] = this.requirements;
    data['TotalHours'] = this.totalHours;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    data['TotalAcceptedNumber'] = this.totalAcceptedNumber;
    data['TotalCompletedNumber'] = this.totalCompletedNumber;
    data['CompanyId'] = this.companyId;
    data['CompanyName'] = this.companyName;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['IntroducerFeeShareAmount'] = this.introducerFeeShareAmount;
    data['IntroducerPaymentedAmount'] = this.introducerPaymentedAmount;
    data['AdviserAmount'] = this.adviserAmount;
    data['AdviserPaymentedAmount'] = this.adviserPaymentedAmount;
    data['NotificationUnreadTaskCount'] = this.notificationUnreadTaskCount;
    data['NotificationTaskCount'] = this.notificationTaskCount;
    data['AddressOfPropertyToBeMortgaged'] = this.addressOfPropertyToBeMortgaged;
    data['OverAllCaseGrade'] = this.overAllCaseGrade;
    data['CaseObservationAssessmentId'] = this.caseObservationAssessmentId;
    data['NamePrefix'] = this.namePrefix;
    data['LastName'] = this.lastName;
    data['CaseStatus'] = this.caseStatus;
    data['AreYouChargingAFee'] = this.areYouChargingAFee;
    data['ChargeFeeAmount'] = this.chargeFeeAmount;
    data['ChargingFeeWhenPayable'] = this.chargingFeeWhenPayable;
    data['ChargingFeeRefundable'] = this.chargingFeeRefundable;
    data['AreYouChargingAnotherFee'] = this.areYouChargingAnotherFee;
    data['ChargeAnotherFeeAmount'] = this.chargeAnotherFeeAmount;
    data['ChargingAnotherFeeWhenPayable'] = this.chargingAnotherFeeWhenPayable;
    data['ChargingAnotherFeeRefundable'] = this.chargingAnotherFeeRefundable;
    data['Id'] = this.id;
    if (this.mortgageCaseInfoEntity != null) {
      data['MortgageCaseInfoEntity'] = this.mortgageCaseInfoEntity.toJson();
    }
    return data;
  }
}

class MortgageCaseInfoEntity {
  int id;
  int userId;
  int companyId;
  int status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  String caseType;
  String customerType;
  String isSmoker;
  String isDoYouHaveAnyFinancialDependants;
  String remarks;
  String isAnyOthers;
  int taskId;
  int coapplicantUserId;
  String customerName;
  String customerEmail;
  String customerMobileNumber;
  String customerAddress;
  String profileImageUrl;
  String namePrefix;
  String areYouBuyingThePropertyInNameOfASPV;
  String companyName;
  String registeredAddress;
  String dateRegistered;
  String companyRegistrationNumber;
  int applicationNumber;
  String customerEmail1;
  String customerEmail2;
  String customerEmail3;
  String isDoYouHaveAnyBTLPortfulio;
  String isClientAgreement;
  int adminFee;
  String adminFeeWhenPayable;
  int adviceFee;
  String adviceFeeWhenPayable;
  String isFeesRefundable;
  String feesRefundable;
  String iPAdddress;
  String iPLocation;
  String deviceType;
  String reportLogo;
  String officePhoneNumber;
  String reportFooter;
  String adviceFeeType;
  String clientAgreementStatus;
  String recommendationAgreementStatus;
  String recommendationAgreementSignature;
  String isThereAnySavingsOrInvestments;
  String isThereAnyExistingPolicy;
  String taskTitleUrl;
  String customerAddress1;
  String customerAddress2;
  String customerAddress3;
  String customerPostcode;
  String customerTown;
  String customerLastName;
  String customerDateofBirth;
  String customerGender;
  String customerAreYouASmoker;
  String occupationCode;

  MortgageCaseInfoEntity({this.id, this.userId, this.companyId, this.status, this.creationDate, this.updatedDate, this.versionNumber, this.caseType, this.customerType, this.isSmoker, this.isDoYouHaveAnyFinancialDependants, this.remarks, this.isAnyOthers, this.taskId, this.coapplicantUserId, this.customerName, this.customerEmail, this.customerMobileNumber, this.customerAddress, this.profileImageUrl, this.namePrefix, this.areYouBuyingThePropertyInNameOfASPV, this.companyName, this.registeredAddress, this.dateRegistered, this.companyRegistrationNumber, this.applicationNumber, this.customerEmail1, this.customerEmail2, this.customerEmail3, this.isDoYouHaveAnyBTLPortfulio, this.isClientAgreement, this.adminFee, this.adminFeeWhenPayable, this.adviceFee, this.adviceFeeWhenPayable, this.isFeesRefundable, this.feesRefundable, this.iPAdddress, this.iPLocation, this.deviceType, this.reportLogo, this.officePhoneNumber, this.reportFooter, this.adviceFeeType, this.clientAgreementStatus, this.recommendationAgreementStatus, this.recommendationAgreementSignature, this.isThereAnySavingsOrInvestments, this.isThereAnyExistingPolicy, this.taskTitleUrl, this.customerAddress1, this.customerAddress2, this.customerAddress3, this.customerPostcode, this.customerTown, this.customerLastName, this.customerDateofBirth, this.customerGender, this.customerAreYouASmoker, this.occupationCode});

  MortgageCaseInfoEntity.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    userId = json['UserId'];
    companyId = json['CompanyId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    updatedDate = json['UpdatedDate'];
    versionNumber = json['VersionNumber'];
    caseType = json['CaseType'];
    customerType = json['CustomerType'];
    isSmoker = json['IsSmoker'];
    isDoYouHaveAnyFinancialDependants = json['IsDoYouHaveAnyFinancialDependants'];
    remarks = json['Remarks'];
    isAnyOthers = json['IsAnyOthers'];
    taskId = json['TaskId'];
    coapplicantUserId = json['CoapplicantUserId'];
    customerName = json['CustomerName'];
    customerEmail = json['CustomerEmail'];
    customerMobileNumber = json['CustomerMobileNumber'];
    customerAddress = json['CustomerAddress'];
    profileImageUrl = json['ProfileImageUrl'];
    namePrefix = json['NamePrefix'];
    areYouBuyingThePropertyInNameOfASPV = json['AreYouBuyingThePropertyInNameOfASPV'];
    companyName = json['CompanyName'];
    registeredAddress = json['RegisteredAddress'];
    dateRegistered = json['DateRegistered'];
    companyRegistrationNumber = json['CompanyRegistrationNumber'];
    applicationNumber = json['ApplicationNumber'];
    customerEmail1 = json['CustomerEmail1'];
    customerEmail2 = json['CustomerEmail2'];
    customerEmail3 = json['CustomerEmail3'];
    isDoYouHaveAnyBTLPortfulio = json['IsDoYouHaveAnyBTLPortfulio'];
    isClientAgreement = json['IsClientAgreement'];
    adminFee = json['AdminFee'];
    adminFeeWhenPayable = json['AdminFeeWhenPayable'];
    adviceFee = json['AdviceFee'];
    adviceFeeWhenPayable = json['AdviceFeeWhenPayable'];
    isFeesRefundable = json['IsFeesRefundable'];
    feesRefundable = json['FeesRefundable'];
    iPAdddress = json['IPAdddress'];
    iPLocation = json['IPLocation'];
    deviceType = json['DeviceType'];
    reportLogo = json['ReportLogo'];
    officePhoneNumber = json['OfficePhoneNumber'];
    reportFooter = json['ReportFooter'];
    adviceFeeType = json['AdviceFeeType'];
    clientAgreementStatus = json['ClientAgreementStatus'];
    recommendationAgreementStatus = json['RecommendationAgreementStatus'];
    recommendationAgreementSignature = json['RecommendationAgreementSignature'];
    isThereAnySavingsOrInvestments = json['IsThereAnySavingsOrInvestments'];
    isThereAnyExistingPolicy = json['IsThereAnyExistingPolicy'];
    taskTitleUrl = json['TaskTitleUrl'];
    customerAddress1 = json['CustomerAddress1'];
    customerAddress2 = json['CustomerAddress2'];
    customerAddress3 = json['CustomerAddress3'];
    customerPostcode = json['CustomerPostcode'];
    customerTown = json['CustomerTown'];
    customerLastName = json['CustomerLastName'];
    customerDateofBirth = json['CustomerDateofBirth'];
    customerGender = json['CustomerGender'];
    customerAreYouASmoker = json['CustomerAreYouASmoker'];
    occupationCode = json['OccupationCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['UserId'] = this.userId;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['CaseType'] = this.caseType;
    data['CustomerType'] = this.customerType;
    data['IsSmoker'] = this.isSmoker;
    data['IsDoYouHaveAnyFinancialDependants'] = this.isDoYouHaveAnyFinancialDependants;
    data['Remarks'] = this.remarks;
    data['IsAnyOthers'] = this.isAnyOthers;
    data['TaskId'] = this.taskId;
    data['CoapplicantUserId'] = this.coapplicantUserId;
    data['CustomerName'] = this.customerName;
    data['CustomerEmail'] = this.customerEmail;
    data['CustomerMobileNumber'] = this.customerMobileNumber;
    data['CustomerAddress'] = this.customerAddress;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['NamePrefix'] = this.namePrefix;
    data['AreYouBuyingThePropertyInNameOfASPV'] = this.areYouBuyingThePropertyInNameOfASPV;
    data['CompanyName'] = this.companyName;
    data['RegisteredAddress'] = this.registeredAddress;
    data['DateRegistered'] = this.dateRegistered;
    data['CompanyRegistrationNumber'] = this.companyRegistrationNumber;
    data['ApplicationNumber'] = this.applicationNumber;
    data['CustomerEmail1'] = this.customerEmail1;
    data['CustomerEmail2'] = this.customerEmail2;
    data['CustomerEmail3'] = this.customerEmail3;
    data['IsDoYouHaveAnyBTLPortfulio'] = this.isDoYouHaveAnyBTLPortfulio;
    data['IsClientAgreement'] = this.isClientAgreement;
    data['AdminFee'] = this.adminFee;
    data['AdminFeeWhenPayable'] = this.adminFeeWhenPayable;
    data['AdviceFee'] = this.adviceFee;
    data['AdviceFeeWhenPayable'] = this.adviceFeeWhenPayable;
    data['IsFeesRefundable'] = this.isFeesRefundable;
    data['FeesRefundable'] = this.feesRefundable;
    data['IPAdddress'] = this.iPAdddress;
    data['IPLocation'] = this.iPLocation;
    data['DeviceType'] = this.deviceType;
    data['ReportLogo'] = this.reportLogo;
    data['OfficePhoneNumber'] = this.officePhoneNumber;
    data['ReportFooter'] = this.reportFooter;
    data['AdviceFeeType'] = this.adviceFeeType;
    data['ClientAgreementStatus'] = this.clientAgreementStatus;
    data['RecommendationAgreementStatus'] = this.recommendationAgreementStatus;
    data['RecommendationAgreementSignature'] = this.recommendationAgreementSignature;
    data['IsThereAnySavingsOrInvestments'] = this.isThereAnySavingsOrInvestments;
    data['IsThereAnyExistingPolicy'] = this.isThereAnyExistingPolicy;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['CustomerAddress1'] = this.customerAddress1;
    data['CustomerAddress2'] = this.customerAddress2;
    data['CustomerAddress3'] = this.customerAddress3;
    data['CustomerPostcode'] = this.customerPostcode;
    data['CustomerTown'] = this.customerTown;
    data['CustomerLastName'] = this.customerLastName;
    data['CustomerDateofBirth'] = this.customerDateofBirth;
    data['CustomerGender'] = this.customerGender;
    data['CustomerAreYouASmoker'] = this.customerAreYouASmoker;
    data['OccupationCode'] = this.occupationCode;
    return data;
  }
}
