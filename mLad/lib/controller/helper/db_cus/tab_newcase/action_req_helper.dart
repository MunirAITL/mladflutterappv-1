import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_cus/UserNotesCfg.dart';
import 'package:aitl/controller/api/db_cus/dash/PrivacyPolicyAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/controller/api/db_cus/more/badge/BadgeAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/new_case/CaseDigitalSignByCustomerURLhelper.dart';
import 'package:aitl/controller/api/db_cus/new_case/CaseReviewAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/new_case/ClientAgreementURLhelper.dart';
import 'package:aitl/controller/api/db_cus/usercases/UserCaseAPIMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/DashBoardListType.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/action_req/GetMortgageCaseInfoLocTaskidUseridAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/UserBadgeModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseInfos.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseRecomendationInfos.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNotesModel.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/dialog/TaskDetailsDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dialog/PrivacyPoliceAcceptDialog.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/rx/ActionReqController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class ActionReqHelper {
  final showMoreCount = 3;
  final actReqControler = Get.put(ActionReqController());

  wsActionReqAPIcall(
      {context,
      Function(List<DashBoardListType>, List<dynamic>,
              List<MortgageCaseRecomendationInfos>, List<MortgageCaseInfos>)
          callback}) async {
    final List<DashBoardListType> list = [];
    List<dynamic> listUserNotesModel = [];
    List<MortgageCaseRecomendationInfos> caseReviewModelList = [];
    List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList = [];
    await privacyPolicyFetchAPI(context);
    await userNotesAPI(context, (lst, listUserNotesModel2) {
      listUserNotesModel = listUserNotesModel2;
      for (var l in lst) list.add(l);
    });
    await caseReviewAPI(context,
        (lst, caseReviewModelList2, caseMortgageAgreementReviewInfosList2) {
      caseReviewModelList = caseReviewModelList2;
      caseMortgageAgreementReviewInfosList =
          caseMortgageAgreementReviewInfosList2;
      for (var l in lst) list.add(l);
    });
    print(list);
    await updateUserBadgesAPI(context, (lst) {
      for (var l in lst) list.add(l);
    });
    actReqControler.isCaseChecked.clear();
    for (int i = 0; i < list.length; i++) {
      actReqControler.isCaseChecked.add(false);
    }
    print(list);
    print(listUserNotesModel);
    print(caseReviewModelList);
    print(caseMortgageAgreementReviewInfosList);
    callback(list, listUserNotesModel, caseReviewModelList,
        caseMortgageAgreementReviewInfosList);
    listUserNotesModel = null;
    caseReviewModelList = null;
    caseMortgageAgreementReviewInfosList = null;
  }

  privacyPolicyFetchAPI(context) async {
    var value = await PrefMgr.shared.getPrefStr("Accepted");
    if (value == null) {
      if (userData.userModel.isCustomerPrivacy == 0 ||
          userData.userModel.isCustomerPrivacy == 907) {
        await PrivacyPolicyAPIMgr().wsOnLoad(
          context: context,
          callback: (model) {
            if (model != null) {
              try {
                if (model.success) {
                  final termsPrivacyNoticeSetupsList =
                      model.responseData.termsPrivacyNoticeSetupsList;
                  for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                      in termsPrivacyNoticeSetupsList) {
                    if (termsPrivacyPoliceSetups.type.toString() ==
                        "Customer Privacy Notice") {
                      Get.dialog(
                        PrivacyPolicyDialog(termsPrivacyPoliceSetups.webUrl),
                        barrierDismissible: false,
                        barrierColor: Colors.transparent,
                        transitionDuration: Duration(milliseconds: 400),
                      );
                      break;
                    }
                  }
                } else {}
              } catch (e) {}
            }
          },
        );
      }
    }
  }

  updateUserBadgesAPI(
      context, Function(List<DashBoardListType>) callback) async {
    await BadgeAPIMgr().wsGetUserBadge(
      context: context,
      userId: userData.userModel.id,
      callback: (model) {
        if (model != null) {
          try {
            if (model.success) {
              final List<DashBoardListType> listUserList = [];
              final List<UserBadgeModel> listUserBadgeModel =
                  model.responseData.userBadges;
              bool emailExitStatus;
              bool eIDExitStatus;
              for (UserBadgeModel userBaseModel in listUserBadgeModel) {
                if (userBaseModel.type == "Email" &&
                    userBaseModel.isVerified == true) {
                  emailExitStatus = true;
                }
                if (userBaseModel.type == "Email" &&
                    userBaseModel.isVerified == false) {
                  emailExitStatus = false;
                }
                if (userBaseModel.type == "ElectricId" &&
                    userBaseModel.isVerified == true) {
                  eIDExitStatus = true;
                }
                if (userBaseModel.type == "ElectricId" &&
                    userBaseModel.isVerified == false) {
                  eIDExitStatus = false;
                }
              }
              if (emailExitStatus == null || emailExitStatus == false) {
                listUserList.add(new DashBoardListType(type: "email"));
              }
              if (eIDExitStatus == null || eIDExitStatus == false) {
                listUserList.add(new DashBoardListType(type: "eid"));
              }
              callback(listUserList);
            } else {}
          } catch (e) {}
        } else {}
      },
    );
  }

  caseReviewAPI(
      context,
      Function(
              List<DashBoardListType>,
              List<MortgageCaseRecomendationInfos> caseDocumentReviewModelList,
              List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList)
          callback) async {
    await CaseReviewAPIMgr().wsOnLoad(
      context: context,
      callback: (model) {
        if (model != null) {
          try {
            if (model.success) {
              final List<DashBoardListType> listUserList = [];
              final caseReviewModelList =
                  model.responseData.caseDocumentReviewModelList;
              final caseMortgageAgreementReviewInfosList =
                  model.responseData.caseMortgageAgreementReviewInfosList;

              for (MortgageCaseRecomendationInfos info in caseReviewModelList) {
                listUserList.add(new DashBoardListType(
                    type: "caseReviewModelList",
                    taskID: info.taskId.toString()));
              }

              for (MortgageCaseInfos info2
                  in caseMortgageAgreementReviewInfosList) {
                listUserList.add(new DashBoardListType(
                    type: "caseMortgageAgreementReviewInfosList",
                    taskID: info2.taskId.toString()));
              }

              callback(listUserList, caseReviewModelList,
                  caseMortgageAgreementReviewInfosList);
            }
          } catch (e) {}
        }
      },
    );
  }

  userNotesAPI(context,
      Function(List<DashBoardListType>, List<dynamic>) callback) async {
    await UserCaseAPIMgr().wsUserNotesAPI(
      context: context,
      status: UserNotesCfg.PENDING,
      callback: (model) {
        if (model != null) {
          try {
            if (model.success) {
              final List<DashBoardListType> listUserList = [];
              final listUserNotesModel = model.responseData.userNotes;
              if (listUserNotesModel.length > 0) {
                for (UserNotesModel userNotesModel in listUserNotesModel) {
                  listUserList.add(new DashBoardListType(
                      type: "UserNotes",
                      taskID: userNotesModel.entityId.toString(),
                      title: userNotesModel.title,
                      initiatorName: userNotesModel.initiatorName,
                      comments: userNotesModel.comments));
                }
                callback(listUserList, listUserNotesModel);
              }
            }
          } catch (e) {}
        }
      },
    );
  }

  drawActionRequiredItems({
    BuildContext context,
    List<DashBoardListType> listUserList,
    List<dynamic> listUserNotesModel,
    List<MortgageCaseRecomendationInfos> caseReviewModelList,
    List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList,
    Function(Map<String, dynamic>) callbackUserBadges,
    Function(UserNotesModel) callbackUserNotes,
    Function callbackReload,
    Function callbackRefresh, //  only use setstate
  }) {
    double width = MediaQuery.of(context).size.width;
    //double height = MediaQuery.of(context).size.height;

    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
        child: Container(
            width: width,
            child: Column(
              children: [
                Container(
                  width: width,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Txt(
                          txt: "Action Required",
                          txtColor: MyTheme.titleColor,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.left,
                          //fontWeight: FontWeight.w600,
                          isBold: true,
                        ),
                      ),
                      IconButton(
                          onPressed: () {
                            callbackReload();
                          },
                          icon: Icon(
                            Icons.refresh,
                            color: MyTheme.titleColor,
                          )),
                    ],
                  ),
                ),
                listUserList != null
                    ? GetBuilder<ActionReqController>(
                        builder: (_dx) => ListView.builder(
                          primary: false,
                          shrinkWrap: true,
                          itemCount: listUserList.length > showMoreCount &&
                                  !_dx.expanad.value
                              ? showMoreCount
                              : listUserList.length,
                          itemBuilder: (context, index) {
                            _dx.isCaseChecked.add(false);
                            if (listUserList[index].type == "email") {
                              return Obx(() => Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        _dx.isCaseChecked[index] =
                                            !_dx.isCaseChecked[index];
                                        if (_dx.isCaseChecked[index]) {
                                          showEmailConfirmDialog(context, index,
                                              callbackUserBadges);
                                        }
                                      },
                                      child: Container(
                                        color: Colors.transparent,
                                        width: width,
                                        child: Row(
                                          children: [
                                            drawCB(
                                              _dx,
                                              index,
                                              (value) {
                                                _dx.isCaseChecked[index] =
                                                    value;
                                                if (value) {
                                                  showEmailConfirmDialog(
                                                      context,
                                                      index,
                                                      callbackUserBadges);
                                                }
                                              },
                                            ),
                                            SizedBox(width: 8),
                                            Txt(
                                              txt: "Email verification",
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize - .2,
                                              txtAlign: TextAlign.start,
                                              isBold: false,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ));
                            } else if (listUserList[index].type == "eid") {
                              return Obx(() => Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        _dx.isCaseChecked[index] =
                                            !_dx.isCaseChecked[index];
                                        if (_dx.isCaseChecked[index]) {
                                          showEIDConfirmDialog(context, index,
                                              callbackUserBadges);
                                        }
                                      },
                                      child: Container(
                                        color: Colors.transparent,
                                        width: width,
                                        child: Row(
                                          children: [
                                            drawCB(
                                              _dx,
                                              index,
                                              (value) {
                                                _dx.isCaseChecked[index] =
                                                    value;
                                                if (value) {
                                                  showEIDConfirmDialog(
                                                      context,
                                                      index,
                                                      callbackUserBadges);
                                                }
                                              },
                                            ),
                                            SizedBox(width: 8),
                                            Txt(
                                              txt: "E-ID verification",
                                              txtColor: Colors.black,
                                              txtSize: MyTheme.txtSize - .2,
                                              txtAlign: TextAlign.start,
                                              isBold: false,
                                            ),
                                            SizedBox(
                                              height: 20,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ));
                            } else if (listUserList[index].type ==
                                "caseReviewModelList") {
                              return Obx(() => Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        _dx.isCaseChecked[index] =
                                            !_dx.isCaseChecked[index];
                                        if (_dx.isCaseChecked[index]) {
                                          dialogCaseReview(
                                            context,
                                            listUserList,
                                            caseReviewModelList,
                                            index,
                                            () {
                                              callbackReload();
                                            },
                                          );
                                        }
                                      },
                                      child: Container(
                                        color: Colors.transparent,
                                        width: width,
                                        child: Row(
                                          children: [
                                            drawCB(
                                              _dx,
                                              index,
                                              (value) {
                                                _dx.isCaseChecked[index] =
                                                    value;
                                                if (value) {
                                                  dialogCaseReview(
                                                    context,
                                                    listUserList,
                                                    caseReviewModelList,
                                                    index,
                                                    () {
                                                      callbackReload();
                                                    },
                                                  );
                                                }
                                              },
                                            ),
                                            SizedBox(width: 8),
                                            Expanded(
                                              child: new RichText(
                                                text: new TextSpan(
                                                  style: new TextStyle(
                                                    fontSize:
                                                        MyTheme.txtSize - .1,
                                                    color: Colors.black,
                                                  ),
                                                  children: <TextSpan>[
                                                    new TextSpan(
                                                        text: 'Case no:',
                                                        style: new TextStyle(
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                    new TextSpan(
                                                        text:
                                                            ' ${listUserList[index].taskID},',
                                                        style: new TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Colors
                                                                .blueAccent,
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                    new TextSpan(
                                                        text:
                                                            ' You have NOT Signed the document yet.',
                                                        style: new TextStyle(
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                    new TextSpan(
                                                        text: 'Click here ',
                                                        style: new TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Colors
                                                                .blueAccent,
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                    new TextSpan(
                                                        text: 'to review',
                                                        style: new TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ));
                            } else if (listUserList[index].type ==
                                "caseMortgageAgreementReviewInfosList") {
                              return Obx(() => Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        _dx.isCaseChecked[index] =
                                            !_dx.isCaseChecked[index];
                                        if (_dx.isCaseChecked[index]) {
                                          dialogCaseMortgageAgreementReview(
                                            context,
                                            listUserList,
                                            caseMortgageAgreementReviewInfosList,
                                            index,
                                            () {
                                              callbackReload();
                                            },
                                          );
                                        }
                                      },
                                      child: Row(
                                        children: [
                                          drawCB(
                                            _dx,
                                            index,
                                            (value) {
                                              _dx.isCaseChecked[index] = value;
                                              if (value) {
                                                dialogCaseMortgageAgreementReview(
                                                  context,
                                                  listUserList,
                                                  caseMortgageAgreementReviewInfosList,
                                                  index,
                                                  () {
                                                    callbackReload();
                                                  },
                                                );
                                              }
                                            },
                                          ),
                                          SizedBox(width: 8),
                                          Expanded(
                                            child: new RichText(
                                              text: new TextSpan(
                                                style: new TextStyle(
                                                  fontSize:
                                                      MyTheme.txtSize - .1,
                                                  color: Colors.black,
                                                ),
                                                children: <TextSpan>[
                                                  new TextSpan(
                                                      text: 'Case no:',
                                                      style: new TextStyle(
                                                          color:
                                                              Colors.blueAccent,
                                                          fontSize: ResponsiveFlutter
                                                                  .of(context)
                                                              .fontSize(MyTheme
                                                                      .txtSize -
                                                                  .1))),
                                                  new TextSpan(
                                                      text:
                                                          ' ${listUserList[index].taskID},',
                                                      style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color:
                                                              Colors.blueAccent,
                                                          fontSize: ResponsiveFlutter
                                                                  .of(context)
                                                              .fontSize(MyTheme
                                                                      .txtSize -
                                                                  .1))),
                                                  new TextSpan(
                                                      text:
                                                          ' You have NOT signed Client Agreement yet.',
                                                      style: new TextStyle(
                                                          fontSize: ResponsiveFlutter
                                                                  .of(context)
                                                              .fontSize(MyTheme
                                                                      .txtSize -
                                                                  .1))),
                                                  new TextSpan(
                                                      text: 'Click here ',
                                                      style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color:
                                                              Colors.blueAccent,
                                                          fontSize: ResponsiveFlutter
                                                                  .of(context)
                                                              .fontSize(MyTheme
                                                                      .txtSize -
                                                                  .1))),
                                                  new TextSpan(
                                                      text: 'to review it',
                                                      style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.normal,
                                                          fontSize: ResponsiveFlutter
                                                                  .of(context)
                                                              .fontSize(MyTheme
                                                                      .txtSize -
                                                                  .1))),
                                                ],
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ));
                            } else if (listUserList[index].type ==
                                "UserNotes") {
                              return Obx(() => Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        _dx.isCaseChecked[index] =
                                            !_dx.isCaseChecked[index];
                                        if (_dx.isCaseChecked[index]) {
                                          dialogUserNotes(
                                              context,
                                              listUserList,
                                              listUserNotesModel,
                                              index, (data) {
                                            callbackUserNotes(data);
                                          });
                                        }
                                      },
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(bottom: 8.0),
                                        child: Row(
                                          children: [
                                            drawCB(
                                              _dx,
                                              index,
                                              (value) {
                                                _dx.isCaseChecked[index] =
                                                    value;
                                                if (value) {
                                                  dialogUserNotes(
                                                      context,
                                                      listUserList,
                                                      listUserNotesModel,
                                                      index,
                                                      callbackUserNotes);
                                                }
                                              },
                                            ),
                                            SizedBox(width: 8),
                                            Expanded(
                                              child: new RichText(
                                                text: new TextSpan(
                                                  style: new TextStyle(
                                                    fontSize:
                                                        MyTheme.txtSize - .1,
                                                    color: Colors.black,
                                                  ),
                                                  children: <TextSpan>[
                                                    new TextSpan(
                                                        text: 'Assign Task: ',
                                                        style: new TextStyle(
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                    new TextSpan(
                                                        text:
                                                            '${listUserList[index].title}',
                                                        style: new TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                    new TextSpan(
                                                        text: ' by ',
                                                        style: new TextStyle(
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                    new TextSpan(
                                                        text:
                                                            '${listUserList[index].initiatorName}.',
                                                        style: new TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                    new TextSpan(
                                                        text: 'Click here',
                                                        style: new TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Colors
                                                                .blueAccent,
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                    new TextSpan(
                                                        text:
                                                            ' to complete it.',
                                                        style: new TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            fontSize: ResponsiveFlutter
                                                                    .of(context)
                                                                .fontSize(MyTheme
                                                                        .txtSize -
                                                                    .1))),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ));
                            } else {
                              return Container(height: 20);
                            }
                          },
                        ),
                      )
                    : SizedBox(),
                listUserList.length > showMoreCount
                    ? Obx(() => GestureDetector(
                        onTap: () {
                          if (actReqControler.expanad.value) {
                            actReqControler.expanad.value = false;
                          } else {
                            actReqControler.expanad.value = true;
                          }
                          callbackRefresh();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Txt(
                                txt: actReqControler.expanad.value
                                    ? "Show less"
                                    : "Show more",
                                txtColor: MyTheme.brandColor,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.right,
                                isBold: false),
                            Icon(
                              actReqControler.expanad.value
                                  ? Icons.arrow_drop_up
                                  : Icons.arrow_drop_down,
                              color: MyTheme.brandColor,
                            )
                          ],
                        )))
                    : SizedBox()
              ],
            )),
      ),
    );
  }

  drawCB(dx, index, Function(bool) callback) {
    return SizedBox(
        width: 24,
        height: 24,
        child: Container(
            margin: const EdgeInsets.all(0),
            padding: const EdgeInsets.all(0),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.red, width: 1),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Checkbox(
                splashRadius: 20,
                fillColor: MaterialStateProperty.all(Colors.transparent),
                focusColor: Colors.red,
                checkColor: Colors.black,
                value: dx.isCaseChecked[index],
                onChanged: (value) {
                  callback(value);
                })));
  }

  void callSendEmailAPi({BuildContext context, Function(bool) callback}) {
    BadgeAPIMgr().wsPostEmailBadge(
      context: context,
      callback: (BadgeEmailAPIModel model) {
        if (model.success) {
          callback(true);
        } else {
          callback(false);
        }
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
  }

  void showEmailConfirmDialog(BuildContext context, int index,
      Function(Map<String, dynamic>) callback) {
    confirmDialog(
        msg: "Press \'Yes\' to verify your email.",
        title: "Email Verification !",
        callbackYes: () {
          actReqControler.isCaseChecked[index] = true;
          callSendEmailAPi(callback: (val) {
            callback({"email": val});
          });
          //Navigator.of(context, rootNavigator: true).pop();
        },
        callbackNo: () {
          actReqControler.isCaseChecked[index] = false;
          callback({"email": null});
          //Navigator.of(context, rootNavigator: true).pop();
        },
        context: context);
  }

  void showEIDConfirmDialog(BuildContext context, int index,
      Function(Map<String, dynamic>) callback) {
    confirmDialog(
        msg: "Press \'Yes\' to verify E-ID.",
        title: "E-ID Verification !",
        callbackYes: () {
          //Navigator.of(context, rootNavigator: true).pop();
          actReqControler.isCaseChecked[index] = true;
          callback({"eid": true});
        },
        callbackNo: () {
          //Navigator.of(context, rootNavigator: true).pop();
          actReqControler.isCaseChecked[index] = false;
          callback({"eid": false});
        },
        context: context);
  }

  dialogCaseReview(
      context,
      listUserList,
      List<MortgageCaseRecomendationInfos> caseReviewModelList,
      index,
      callbackReload) {
    confirmDialog(
        callbackYes: () async {
          //Navigator.of(context, rootNavigator: true).pop();
          var url = Server.MORTGAGE_CASEINFO_LOCATIONTASKUSERID_GET;
          url = url.replaceAll("#caseId#", listUserList[index].taskID);
          url = url.replaceAll("#userId#", userData.userModel.id.toString());
          await APIViewModel().req<GetMortgageCaseInfoLocTaskidUseridAPIModel>(
            context: context,
            url: url,
            reqType: ReqType.Get,
            callback: (model) async {
              if (model != null) {
                if (model.success) {
                  final url =
                      model.responseData.mortgageCaseDocumentInfo.documentUrl;
                  if (url != "") {
                    Get.to(
                            () => PDFDocumentPage(
                                  title: "Case Document",
                                  url: url,
                                  isRightArrow: true,
                                ),
                            transition: Transition.rightToLeft,
                            duration: Duration(
                                milliseconds:
                                    AppConfig.pageAnimationMilliSecond))
                        .then((value) {
                      String url = CaseDigitalSignByCustomerURLHelper.getUrl(
                          caseID: listUserList[index].taskID.toString());
                      Get.to(
                        () => WebScreen(
                          caseID: listUserList[index].taskID.toString(),
                          title: "Case ${listUserList[index].taskID} Signature",
                          // url: Server.CaseDigitalSignByCustomer + caseReviewModelList[index].taskId.toString(),
                          url: url,
                        ),
                      ).then((value) => callbackReload);
                    });
                  } else {}
                }
              }
            },
          );

          actReqControler.isCaseChecked[index] = false;
        },
        callbackNo: () {
          //Navigator.of(context, rootNavigator: true).pop();
          actReqControler.isCaseChecked[index] = false;
        },
        context: context,
        msg: "Do you want to signature now ?",
        title: "Case ${listUserList[index].taskID}");
  }

  dialogCaseMortgageAgreementReview(
      context,
      listUserList,
      List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList,
      index,
      callbackReload) {
    confirmDialog(
        callbackYes: () {
          //Navigator.of(context, rootNavigator: true).pop();
          String url = ClientAgreementURLHelper.getUrl(
              caseID: listUserList[index].taskID.toString());
          Get.to(
            () => WebScreen(
              caseID: listUserList[index].taskID.toString(),
              title: "Case ${listUserList[index].taskID} Agreement",
              url: url,
            ),
          ).then((value) => callbackReload);
          actReqControler.isCaseChecked[index] = false;
        },
        callbackNo: () {
          //Navigator.of(context, rootNavigator: true).pop();
          actReqControler.isCaseChecked[index] = false;
        },
        context: context,
        msg: "Do you want to Review the Agreement?",
        title: "Case ${listUserList[index].taskID}");
  }

  dialogUserNotes(
      context,
      List<DashBoardListType> listUserList,
      List<dynamic> listUserNotesModel,
      int index,
      Function(UserNotesModel) callbackListUserNotes) {
    taskDetailsDialog(
      caseID: listUserList[index].taskID.toString(),
      caseTitle: listUserList[index].title,
      caseDescription: listUserList[index].comments,
      caseAssignedBy: listUserList[index].initiatorName,
      callbackYes: () {
        //Navigator.of(context, rootNavigator: true).pop();
        final userNotesModel = NewCaseHelper().getUserNotesModelByTitle(
            listUserNotesModel: listUserNotesModel,
            title: listUserList[index].title);
        if (userNotesModel != null) {
          //  api call
          UserCaseAPIMgr().wsCaseNoteDone(
            context: context,
            userNoteModel: userNotesModel,
            callback: (model) {
              if (model != null) {
                if (model.success) {
                  callbackListUserNotes(model.responseData.userNote);
                }
              }
            },
          );
        }
      },
      callbackNo: () {
        //Navigator.of(context, rootNavigator: true).pop();
        actReqControler.isCaseChecked[index] = false;
      },
      context: context,
      msg:
          "For Completing the task, please click the \'Yes\' button otherwise click the \'No\' button",
      title: "Task Details",
    );
  }
}
