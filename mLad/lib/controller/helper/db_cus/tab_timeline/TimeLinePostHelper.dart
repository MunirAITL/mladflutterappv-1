import 'package:aitl/model/data/UserData.dart';

class TimeLinePostHelper {
  getParam({
    String message,
    String postTypeName,
    String additionalAttributeValue,
    List<dynamic> inlineTags,
    String checkin,
    int fromLat,
    int fromLng,
    String smallImageName,
    int receiverId,
    int ownerId,
    int taskId,
    bool isPrivate,
  }) {
    return {
      "message": message,
      "postTypeName": postTypeName,
      "additionalAttributeValue": additionalAttributeValue,
      "inlineTags": inlineTags,
      "checkin": checkin,
      "fromLat": fromLat,
      "fromLng": fromLng,
      "smallImageName": smallImageName,
      "receiverId": receiverId,
      "ownerId": ownerId,
      "senderId": userData.userModel.id,
      "taskId": taskId,
      "isPrivate": isPrivate,
    };
  }
}
