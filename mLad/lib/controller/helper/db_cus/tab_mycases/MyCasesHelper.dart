import 'dart:developer' as dev;
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/db_cus/tab_mycases/CaseDetailsWebHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/review_rating/ReviewRatingAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/LocationsModel.dart';
import 'package:aitl/view/AnimatedListItem.dart';
import 'package:aitl/view/db_cus/doc_scan/utils/main_mixin.dart';
import 'package:aitl/view/db_cus/my_cases/MyCaseTab.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyCasesHelper {
  drawRecentCaseItem({
    BuildContext context,
    LocationsModel caseModel,
    bool isDashboard = false,
    Function(ReviewRatingAPIModel) callbackModel,
  }) {
    double width = MediaQuery.of(context).size.width;

    try {
      if (caseModel == null) return SizedBox();
      final icon = NewCaseHelper().getCreateCaseIconByTitle(caseModel.title);
      if (icon == null) return SizedBox();
      final isShowEditButton = (caseModel.status == 903);

      return GestureDetector(
        onTap: () {
          if (caseModel.status == 903) {
            Get.to(
                    () => WebScreen(
                          title: caseModel.title,
                          url: CaseDetailsWebHelper().getLink(
                            title: caseModel.title,
                            taskId: caseModel.id,
                          ),
                          caseID: caseModel.id.toString(),
                        ),
                    transition: Transition.rightToLeft,
                    duration: Duration(
                        milliseconds: AppConfig.pageAnimationMilliSecond))
                .then((value) {
              try {
                StateProvider().notify(ObserverState.STATE_CHANGED_tabbar2);
              } catch (e) {
                dev.log(e.toString());
              }
            });
          } else {
            Get.to(
                () => WebScreen(
                      caseID: caseModel.id.toString(),
                      title: caseModel.title,
                      url: CaseDetailsWebHelper().getLink(
                          title: caseModel.title, taskId: caseModel.id),
                    ),
                transition: Transition.rightToLeft,
                duration:
                    Duration(milliseconds: AppConfig.pageAnimationMilliSecond));
          }
        },
        child: Padding(
          padding: const EdgeInsets.only(
            left: 10,
            right: 10,
          ),
          child: Container(
            decoration: BoxDecoration(
              //color: Colors.white,
              border: Border(
                bottom: BorderSide(
                  color: Colors.grey,
                  width: .5,
                ),
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: IntrinsicHeight(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Container(
                            //color: Colors.blue,
                            width: width * (isDashboard ? 0.13 : 0.09),
                            child: Column(
                              //mainAxisSize: MainAxisSize.max,
                              children: [
                                Align(
                                  alignment: FractionalOffset.topCenter,
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 2),
                                    child: Container(
                                      //width: width * (isDashboard ? 0.13 : 0.09),
                                      //height:
                                      //width * (isDashboard ? 0.13 : 0.09),
                                      decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        gradient: LinearGradient(
                                            colors: [
                                              HexColor.fromHex("#2B4564"),
                                              HexColor.fromHex("#112B4A"),
                                            ],
                                            begin: const FractionalOffset(
                                                0.0, 0.0),
                                            end: const FractionalOffset(
                                                0.0, 0.0),
                                            stops: [0.0, 1.0],
                                            tileMode: TileMode.clamp),
                                      ),
                                      child: Image.asset(icon),
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5),
                                Align(
                                  alignment: FractionalOffset.bottomCenter,
                                  child: Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Txt(
                                        maxLines: 2,
                                        txt: CaseStatusTxt(
                                                caseModel.status.toString())
                                            .toUpperCase(),
                                        txtColor: Color(0xFF1F3548),
                                        txtSize: MyTheme.txtSize - 1,
                                        txtAlign: TextAlign.center,
                                        isBold: false),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),

                        Expanded(
                          flex: 5,
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 15, top: 5, bottom: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Txt(
                                    txt: "Case ID: ${caseModel.id}",
                                    txtColor: MyTheme.timelineTitleColor,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    maxLines: 5,
                                    isBold: true),
                                Txt(
                                    txt: "${caseModel.title}",
                                    txtColor: Color(0xFF9097A5),
                                    txtSize: MyTheme.txtSize - .4,
                                    txtAlign: TextAlign.start,
                                    fontWeight: FontWeight.bold,
                                    maxLines: 2,
                                    isBold: false),
                                caseModel.description.isNotEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.only(top: 5),
                                        child: Txt(
                                            txt: "${caseModel.description}",
                                            txtColor:
                                                MyTheme.timelineTitleColor,
                                            txtSize: MyTheme.txtSize - .7,
                                            txtAlign: TextAlign.start,
                                            maxLines: 5,
                                            isBold: false),
                                      )
                                    : SizedBox(),
                                caseModel.creationDate.isNotEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: [
                                            Image.asset(
                                                "assets/images/ico/date_ico.png"),
                                            SizedBox(width: 5),
                                            Flexible(
                                              child: Txt(
                                                  txt: DateFun.getDate(
                                                      caseModel.creationDate,
                                                      "dd MMM yyyy"),
                                                  txtColor: Color(0xFF8D8D8D),
                                                  txtSize: MyTheme.txtSize - .8,
                                                  txtAlign: TextAlign.start,
                                                  maxLines: 5,
                                                  isBold: false),
                                            ),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                caseModel.addressOfPropertyToBeMortgaged
                                        .isNotEmpty
                                    ? Padding(
                                        padding: const EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: [
                                            Image.asset(
                                                "assets/images/ico/case_addr_ico.png"),
                                            SizedBox(width: 5),
                                            Flexible(
                                              child: Txt(
                                                  txt:
                                                      "${caseModel.addressOfPropertyToBeMortgaged}",
                                                  txtColor: Color(0xFF8D8D8D),
                                                  txtSize: MyTheme.txtSize - .8,
                                                  txtAlign: TextAlign.start,
                                                  maxLines: 5,
                                                  isBold: false),
                                            ),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                Container(
                                  child: caseModel.adviserName.isNotEmpty
                                      ? Padding(
                                          padding:
                                              const EdgeInsets.only(top: 5),
                                          child: Row(
                                            children: [
                                              Image.asset(
                                                  "assets/images/icons/adviser_icon.png"),
                                              SizedBox(width: 5),
                                              Flexible(
                                                child: Txt(
                                                    txt: caseModel.adviserName,
                                                    txtColor: Color(0xFF8D8D8D),
                                                    txtSize:
                                                        MyTheme.txtSize - .5,
                                                    txtAlign: TextAlign.start,
                                                    maxLines: 5,
                                                    isBold: false),
                                              ),
                                              SizedBox(width: 5),
                                              Flexible(
                                                child: GestureDetector(
                                                    onTap: () async {
                                                      await APIViewModel().req<
                                                              ReviewRatingAPIModel>(
                                                          context: context,
                                                          url: Server
                                                              .REVIEW_RATING_FORMSETUP_GET_URL
                                                              .replaceAll(
                                                                  "#userCompanyId#",
                                                                  userData
                                                                      .userModel
                                                                      .userCompanyID
                                                                      .toString()),
                                                          reqType: ReqType.Get,
                                                          callback:
                                                              (model) async {
                                                            callbackModel(
                                                                model);
                                                          });
                                                    },
                                                    child: Icon(
                                                      Icons
                                                          .rate_review_outlined,
                                                      color: MyTheme.bgColor,
                                                    )),
                                              ),
                                            ],
                                          ),
                                        )
                                      : Container(),
                                ),
                                Container(
                                  child: caseModel.introducerName.isNotEmpty
                                      ? Padding(
                                          padding:
                                              const EdgeInsets.only(top: 5),
                                          child: Row(
                                            children: [
                                              Image.asset(
                                                "assets/images/icons/adviser_icon.png",
                                                fit: BoxFit.fitHeight,
                                              ),
                                              SizedBox(width: 5),
                                              Flexible(
                                                child: Txt(
                                                    txt: caseModel
                                                        .introducerName,
                                                    txtColor: Color(0xFF8D8D8D),
                                                    txtSize:
                                                        MyTheme.txtSize - .5,
                                                    txtAlign: TextAlign.start,
                                                    maxLines: 5,
                                                    isBold: false),
                                              ),
                                            ],
                                          ),
                                        )
                                      : Container(),
                                ),
                              ],
                            ),
                          ),
                        ),
                        //SizedBox(width: 20),
                        Flexible(
                          child: Container(
                            //color: Colors.black,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                (isShowEditButton)
                                    ? IconButton(
                                        onPressed: () {
                                          Get.to(
                                                  () => WebScreen(
                                                        title: caseModel.title,
                                                        url:
                                                            CaseDetailsWebHelper()
                                                                .getLink(
                                                          title:
                                                              caseModel.title,
                                                          taskId: caseModel.id,
                                                        ),
                                                        caseID: caseModel.id
                                                            .toString(),
                                                      ),
                                                  transition:
                                                      Transition.rightToLeft,
                                                  duration: Duration(
                                                      milliseconds: AppConfig
                                                          .pageAnimationMilliSecond))
                                              .then((value) {
                                            try {
                                              // Navigator.pop(context);
                                              StateProvider().notify(
                                                  ObserverState
                                                      .STATE_CHANGED_tabbar2);
                                            } catch (e) {
                                              dev.log(e.toString());
                                            }
                                          });

                                          /* navTo(
                                              context: context,
                                              page: () => EditCaseScreenNew(
                                                    caseModel: caseModel,
                                                  )).then((value) {
                                            //callback(route);
                                          });*/
                                        },
                                        icon: Image.asset(
                                          "assets/images/icons/edit_icon.png",
                                          color: MyTheme.titleColor,
                                          width: 25,
                                          height: 25,
                                        ),
                                      )
                                    : SizedBox(),
                                SizedBox(height: (isShowEditButton) ? 5 : 0),
                                isShowEditButton
                                    ? SizedBox()
                                    : Padding(
                                        padding: EdgeInsets.only(
                                            top: (isShowEditButton) ? 0 : 25,
                                            bottom:
                                                (isShowEditButton) ? 0 : 25),
                                        child: IconButton(
                                          onPressed: () {
                                            Get.to(
                                                    () => WebScreen(
                                                          caseID: caseModel.id
                                                              .toString(),
                                                          title:
                                                              caseModel.title,
                                                          url: CaseDetailsWebHelper()
                                                              .getLink(
                                                                  title:
                                                                      caseModel
                                                                          .title,
                                                                  taskId:
                                                                      caseModel
                                                                          .id),
                                                        ),
                                                    transition:
                                                        Transition.rightToLeft,
                                                    duration: Duration(
                                                        milliseconds: AppConfig
                                                            .pageAnimationMilliSecond))
                                                .then((value) {
                                              //callback(route);
                                            });
                                          },
                                          icon: Image.asset(
                                              "assets/images/icons/visible_icon.png"),
                                          //color: Colors.grey.shade400,
                                          iconSize: 25,
                                        ),
                                      ),
                              ],
                            ),
                          ),
                        ),
                        //SizedBox(width: 5),
                      ],
                    ),
                  ),
                ),
                //SizedBox(height: 10),
              ],
            ),
          ),
        ),
      );
    } catch (e) {
      dev.log(e.toString());
    }
  }
}
