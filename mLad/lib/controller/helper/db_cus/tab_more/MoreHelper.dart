import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/controller/api/db_cus/dash/PrivacyPolicyAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/dash/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/selfie/selfie_page.dart';
import 'package:aitl/view/db_cus/more/badges/BadgeScreen.dart';
import 'package:aitl/view/db_cus/more/help/HelpScreen.dart';
import 'package:aitl/view/db_cus/more/profile/ProfileScreen.dart';
import 'package:aitl/view/db_cus/more/profile/ProfileAddrBadge.dart';
import 'package:aitl/view/db_cus/more/reviews/ReviewsScreen.dart';
import 'package:aitl/view/db_cus/more/settings/SettingsScreen.dart';
import 'package:aitl/view/db_cus/more/video_chat/live_chat_page.dart';
import 'package:aitl/view/db_cus/noti/NotiTab.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MoreHelper with Mixin {
  static const List<Map<String, dynamic>> listMore = [
    {
      "icon": "assets/images/screens/db_cus/more/profile_ico.png",
      "title": "Profile",
      "route": ProfileAddrBadge
    },
    //{"title": "Reviews", "route": ReviewsScreen},
    // {"title": "Badges", "route": BadgeScreen},
    {
      "icon": "assets/images/screens/db_cus/more/noti_ico.png",
      "title": "Notifications",
      "route": NotiTab
    },
    {
      "icon": "assets/images/screens/db_cus/more/help_ico.png",
      "title": "Help",
      "route": HelpScreen
    },
    {
      "icon": "assets/images/screens/db_cus/more/settings_ico.png",
      "title": "Settings",
      "route": SettingsScreen
    },

    /* {
      "title": "Terms and conditions",
      "route": WebScreenTermsAndCondition
    },*/
    {
      "icon": "assets/images/screens/db_cus/more/privacy_ico.png",
      "title": "Privacy",
      "route": PDFDocumentPage
    },
    {
      "icon": "assets/images/ico/cam_ico.png",
      "title": "Live Chat",
      "route": LiveChatPage
    },
    {
      "icon": "assets/images/screens/db_cus/more/privacy_ico.png",
      "title": "EID-Verify",
      "route": SelfiePage
    },
    {
      "icon": "assets/images/screens/db_cus/more/logout_ico.png",
      "title": "Logout",
      "route": null
    },
  ];

  setRoute({
    Type route,
    BuildContext context,
    Function callback,
  }) async {
    try {
      if (identical(route, ProfileAddrBadge)) {
        Get.to(() => ProfileAddrBadge()).then((value) {
          callback(route);
        });
      } else if (identical(route, BadgeScreen)) {
        Get.to(() => BadgeScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, ReviewsScreen)) {
        Get.to(() => ReviewsScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, SettingsScreen)) {
        Get.to(() => SettingsScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, HelpScreen)) {
        Get.to(() => HelpScreen()).then((value) {
          callback(route);
        });
      } else if (identical(route, SelfiePage)) {
        Get.to(() => SelfiePage()).then((value) {
          callback(route);
        });
      } else if (identical(route, LiveChatPage)) {
        Get.to(() => LiveChatPage()).then((value) {
          callback(route);
        });
      }
      /*else if (identical(route, WebScreenTermsAndCondition)) {
        navTo(context: context, page: () => WebScreenTermsAndCondition(
          title: "Terms & Conditions",
          url: Server.TC_URL,
        )).then((value) {
          callback(route);
        });
      }*/
      else if (identical(route, PDFDocumentPage)) {
        openWSPrivacyPDF(context);
        /* navTo(
            context: context,
            page: () => WebScreenPrivacy(
                  title: "Privacy",
                  url: Server.PRIVACY_URL,
                )).then((value) {
          callback(route);
        });*/
      }
    } catch (e) {}
  }

  openWSPrivacyPDF(context) {
    PrivacyPolicyAPIMgr().wsOnLoad(
      context: context,
      callback: (model) {
        if (model != null) {
          try {
            if (model.success) {
              debugPrint(
                  "size = ${model.responseData.termsPrivacyNoticeSetupsList.length}");

              for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                  in model.responseData.termsPrivacyNoticeSetupsList) {
                if (termsPrivacyPoliceSetups.type.toString() ==
                        "Customer Privacy Notice" ||
                    termsPrivacyPoliceSetups.type.toString() ==
                        "Customer Case Agreement") {
                  Get.to(
                          () => PDFDocumentPage(
                                title: "Privacy",
                                url: termsPrivacyPoliceSetups.webUrl,
                              ),
                          transition: Transition.rightToLeft,
                          duration: Duration(
                              milliseconds: AppConfig.pageAnimationMilliSecond))
                      .then((value) {
                    //callback(route);
                  });
                  break;
                }
              }
            } else {
              //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: model.errorMessages.toString());
              showToast(context: context, msg: "Sorry, something went wrong");
            }
          } catch (e) {
            myLog(e.toString());
          }
        } else {
          myLog("dashboard screen not in");
        }
      },
    );
  }
}
