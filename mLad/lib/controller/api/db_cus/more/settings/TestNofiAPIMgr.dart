import 'package:aitl/controller/helper/db_cus/tab_more/settings/FcmTestNotiHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/noti/FcmTestNotiAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class TestNotiAPIMgr with Mixin {
  static final TestNotiAPIMgr _shared = TestNotiAPIMgr._internal();

  factory TestNotiAPIMgr() {
    return _shared;
  }

  TestNotiAPIMgr._internal();

  wsTestNotiAPI({
    BuildContext context,
    Function(FcmTestNotiAPIModel) callback,
  }) async {
    try {
      final url = FcmTestNotiHelper().getUrl(
        userId: userData.userModel.id,
      );
      myLog(url);
      await NetworkMgr()
          .req<FcmTestNotiAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: true,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
