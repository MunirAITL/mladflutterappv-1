import 'dart:convert';

import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/UserNotesHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNoteByEntityAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNotePutAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNotesModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class UserCaseAPIMgr with Mixin {
  static final UserCaseAPIMgr _shared = UserCaseAPIMgr._internal();

  factory UserCaseAPIMgr() {
    return _shared;
  }

  UserCaseAPIMgr._internal();

  wsUserNotesAPI({
    BuildContext context,
    int status,
    Function(UserNoteByEntityAPIModel) callback,
  }) async {
    try {
      final url = UserNotesHelper().getUrl(
        status: status.toString(),
      );
      myLog(url);
      await NetworkMgr()
          .req<UserNoteByEntityAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  wsCaseNoteDone({
    BuildContext context,
    UserNotesModel userNoteModel,
    Function(UserNotePutAPIModel) callback,
  }) async {
    try {
      final param = UserNotesHelper().getParam(userNoteModel: userNoteModel);
      myLog(json.encode(param));
      await NetworkMgr()
          .req<UserNotePutAPIModel, Null>(
        context: context,
        url: Server.USERNOTE_PUT_URL,
        reqType: ReqType.Put,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
