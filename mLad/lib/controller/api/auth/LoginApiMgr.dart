import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/auth/LoginHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/auth/LoginAPIModel.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class LoginAPIMgr with Mixin {
  static final LoginAPIMgr _shared = LoginAPIMgr._internal();

  factory LoginAPIMgr() {
    return _shared;
  }

  LoginAPIMgr._internal();

  wsLoginAPI({
    BuildContext context,
    String email,
    String pwd,
    Function(LoginAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<LoginAPIModel, Null>(
        context: context,
        url: Server.LOGIN_URL,
        param: LoginHelper().getParam(email: email, pwd: pwd),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
