import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class UserProfileVal with Mixin {
  static const int PHONE_LIMIT = 8;
  static const EMAIL_REG =
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+\.[a-zA-Z]+";
  static const PHONE_REG = r'(^(?:[+0])?[0-9]{10,12}$)';
  isEmpty(BuildContext context, TextEditingController tf, arg) {
    if (tf.text.isEmpty) {
      showToast(context: context, msg: arg);
      return true;
    }
    return false;
  }

  isFullName(BuildContext context, String name) {
    try {
      final isOk = (name.trim().contains(" ")) ? true : false;
      if (!isOk) {
        showToast(context: context, msg: "Invalid full name");
      }
      return isOk;
    } catch (e) {
      return false;
    }
  }

  isFNameOK(BuildContext context, TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(context: context, msg: "Invalid first name");
      return false;
    }
    return true;
  }

  isLNameOK(BuildContext context, TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(context: context, msg: "Invalid last name");
      return false;
    }
    return true;
  }

  isEmailOK(BuildContext context, TextEditingController tf, String msg) {
    if (!RegExp(EMAIL_REG).hasMatch(tf.text.trim())) {
      showToast(context: context, msg: msg);
      return false;
    } /*else if (tf.text.trim().contains("@help.mortgage-magic.co.uk")) {
      showToast(context: context,
          txtColor: Colors.white,
          bgColor: bgColor,
          msg: "Email address does not exists");
      return false;
    }*/
    return true;
  }

  isPhoneOK(BuildContext context, TextEditingController tf) {
    if (tf.text.length < PHONE_LIMIT) {
      showToast(context: context, msg: "Invalid Mobile Number");
      return false;
    }
    return true;
  }

  isPwdOK(BuildContext context, TextEditingController tf) {
    if (tf.text.length < 3) {
      showToast(
          context: context, msg: "Password should be greater by 4 characters");
      return false;
    }
    return true;
  }

  isComNameOK(BuildContext context, TextEditingController tf) {
    if (tf.text.length < 6) {
      showToast(context: context, msg: "Invalid Company Name");
      return false;
    }
    return true;
  }

  isDOBOK(BuildContext context, str) {
    if (str == '') {
      showToast(context: context, msg: "Invalid Date of Birth");
      return false;
    }
    return true;
  }
}
