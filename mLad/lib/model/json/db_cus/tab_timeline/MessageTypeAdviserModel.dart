class MessageTypeAdviserModel {
  int id;
  String title;
  String description;
  String addressOfPropertyToBeMortgaged;
  String howMuchDoYouWishToBorrow;

  MessageTypeAdviserModel({this.id, this.title, this.description, this.addressOfPropertyToBeMortgaged, this.howMuchDoYouWishToBorrow});

  MessageTypeAdviserModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    title = json['Title'];
    description = json['Description'];
    addressOfPropertyToBeMortgaged = json['AddressOfPropertyToBeMortgaged'];
    howMuchDoYouWishToBorrow = json['HowMuchDoYouWishToBorrow'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Title'] = this.title;
    data['Description'] = this.description;
    data['AddressOfPropertyToBeMortgaged'] = this.addressOfPropertyToBeMortgaged;
    data['HowMuchDoYouWishToBorrow'] = this.howMuchDoYouWishToBorrow;
    return data;
  }
}

