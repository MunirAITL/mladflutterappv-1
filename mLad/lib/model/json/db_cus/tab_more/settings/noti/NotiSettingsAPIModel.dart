import 'NotificationSettingModel.dart';

class NotiSettingsAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  NotiSettingsAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  NotiSettingsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : [];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  NotiSettingModel userNotificationSetting;
  ResponseData({this.userNotificationSetting});
  ResponseData.fromJson(Map<String, dynamic> json) {
    try {
      userNotificationSetting = json['UserNotificationSetting'] != null
          ? new NotiSettingModel.fromJson(json['UserNotificationSetting'])
          : null;
    } catch (e) {
      userNotificationSetting = null;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userNotificationSetting != null) {
      data['UserNotificationSetting'] = this.userNotificationSetting.toJson();
    }
    return data;
  }
}
