class CaseLeadNegotiatorUserModel {
  String firstName;
  String lastName;
  String name;
  String userName;
  String profileImageUrl;
  String coverImageUrl;
  String profileUrl;
  String dateCreatedUtc;
  String dateCreatedLocal;
  bool active;
  String lastLoginDateUtc;
  String lastLoginDateLocal;
  int followerCount;
  int followingCount;
  int friendCount;
  bool canFollow;
  int followStatus;
  int friendStatus;
  int unreadNotificationCount;
  int unreadMessageCount;
  bool isOnline;
  String seName;
  String stanfordWorkplaceURL;
  String headline;
  String briefBio;
  String referenceId;
  String referenceType;
  String remarks;
  String community;
  String locations;
  String cohort;
  String communityId;
  bool isFirstLogin;
  String mobileNumber;
  String dateofBirth;
  bool isMobileNumberVerified;
  bool isProfileImageVerified;
  bool isDateOfBirthVerified;
  bool isElectronicIdVerified;
  bool isEmailVerified;
  String lastLoginDate;
  String userProfileUrl;
  int userAccesType;
  String address;
  String email;
  double latitude;
  double longitude;
  int bankVerifiedStatus;
  int nationalIDVerifiedStatus;
  int billingAccountVerifiedStatus;
  int isCustomerPrivacy;
  int profileImageId;
  String workHistory;
  int userTaskCategoryId;
  String deviceName;
  String skillName;
  String userTaskCategoryName;
  String status;
  int groupId;
  String namePrefix;
  String middleName;
  String areYouASmoker;
  String addressLine1;
  String addressLine2;
  String addressLine3;
  String town;
  String county;
  String postcode;
  String telNumber;
  String nationalInsuranceNumber;
  String nationality;
  String countryofBirth;
  String countryofResidency;
  String passportNumber;
  String maritalStatus;
  int userCompanyId;
  String companyName;
  String occupantType;
  String livingDate;
  String visaExpiryDate;
  String passportExpiryDate;
  String visaName;
  String otherVisaName;
  String reportLogoUrl;
  String userCompanyInfo;
  String companyType;
  String isActiveCustomerPrivacy;
  String isActiveCustomerAgreement;
  String companyWebsite;
  String isCallCenterActive;
  int companyCount;
  int id;

  CaseLeadNegotiatorUserModel(
      {this.firstName,
      this.lastName,
      this.name,
      this.userName,
      this.profileImageUrl,
      this.coverImageUrl,
      this.profileUrl,
      this.dateCreatedUtc,
      this.dateCreatedLocal,
      this.active,
      this.lastLoginDateUtc,
      this.lastLoginDateLocal,
      this.followerCount,
      this.followingCount,
      this.friendCount,
      this.canFollow,
      this.followStatus,
      this.friendStatus,
      this.unreadNotificationCount,
      this.unreadMessageCount,
      this.isOnline,
      this.seName,
      this.stanfordWorkplaceURL,
      this.headline,
      this.briefBio,
      this.referenceId,
      this.referenceType,
      this.remarks,
      this.community,
      this.locations,
      this.cohort,
      this.communityId,
      this.isFirstLogin,
      this.mobileNumber,
      this.dateofBirth,
      this.isMobileNumberVerified,
      this.isProfileImageVerified,
      this.isDateOfBirthVerified,
      this.isElectronicIdVerified,
      this.isEmailVerified,
      this.lastLoginDate,
      this.userProfileUrl,
      this.userAccesType,
      this.address,
      this.email,
      this.latitude,
      this.longitude,
      this.bankVerifiedStatus,
      this.nationalIDVerifiedStatus,
      this.billingAccountVerifiedStatus,
      this.isCustomerPrivacy,
      this.profileImageId,
      this.workHistory,
      this.userTaskCategoryId,
      this.deviceName,
      this.skillName,
      this.userTaskCategoryName,
      this.status,
      this.groupId,
      this.namePrefix,
      this.middleName,
      this.areYouASmoker,
      this.addressLine1,
      this.addressLine2,
      this.addressLine3,
      this.town,
      this.county,
      this.postcode,
      this.telNumber,
      this.nationalInsuranceNumber,
      this.nationality,
      this.countryofBirth,
      this.countryofResidency,
      this.passportNumber,
      this.maritalStatus,
      this.userCompanyId,
      this.companyName,
      this.occupantType,
      this.livingDate,
      this.visaExpiryDate,
      this.passportExpiryDate,
      this.visaName,
      this.otherVisaName,
      this.reportLogoUrl,
      this.userCompanyInfo,
      this.companyType,
      this.isActiveCustomerPrivacy,
      this.isActiveCustomerAgreement,
      this.companyWebsite,
      this.isCallCenterActive,
      this.companyCount,
      this.id});

  CaseLeadNegotiatorUserModel.fromJson(Map<String, dynamic> json) {
    firstName = json['FirstName'] ?? '';
    lastName = json['LastName'] ?? '';
    name = json['Name'] ?? '';
    userName = json['UserName'] ?? '';
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    coverImageUrl = json['CoverImageUrl'] ?? '';
    profileUrl = json['ProfileUrl'] ?? '';
    dateCreatedUtc = json['DateCreatedUtc'] ?? '';
    dateCreatedLocal = json['DateCreatedLocal'] ?? '';
    active = json['Active'] ?? false;
    lastLoginDateUtc = json['LastLoginDateUtc'] ?? '';
    lastLoginDateLocal = json['LastLoginDateLocal'] ?? '';
    followerCount = json['FollowerCount'] ?? 0;
    followingCount = json['FollowingCount'] ?? 0;
    friendCount = json['FriendCount'] ?? 0;
    canFollow = json['CanFollow'] ?? false;
    followStatus = json['FollowStatus'] ?? 0;
    friendStatus = json['FriendStatus'] ?? 0;
    unreadNotificationCount = json['UnreadNotificationCount'] ?? 0;
    unreadMessageCount = json['UnreadMessageCount'] ?? 0;
    isOnline = json['IsOnline'] ?? false;
    seName = json['SeName'] ?? '';
    stanfordWorkplaceURL = json['StanfordWorkplaceURL'] ?? '';
    headline = json['Headline'] ?? '';
    briefBio = json['BriefBio'] ?? '';
    referenceId = json['ReferenceId'] ?? '';
    referenceType = json['ReferenceType'] ?? '';
    remarks = json['Remarks'] ?? '';
    community = json['Community'] ?? '';
    locations = json['Locations'] ?? '';
    cohort = json['Cohort'] ?? '';
    communityId = json['CommunityId'] ?? '';
    isFirstLogin = json['IsFirstLogin'] ?? false;
    mobileNumber = json['MobileNumber'] ?? '';
    dateofBirth = json['DateofBirth'] ?? '';
    isMobileNumberVerified = json['IsMobileNumberVerified'] ?? false;
    isProfileImageVerified = json['IsProfileImageVerified'] ?? false;
    isDateOfBirthVerified = json['IsDateOfBirthVerified'] ?? false;
    isElectronicIdVerified = json['IsElectronicIdVerified'] ?? false;
    isEmailVerified = json['IsEmailVerified'] ?? false;
    lastLoginDate = json['LastLoginDate'] ?? '';
    userProfileUrl = json['UserProfileUrl'] ?? '';
    userAccesType = json['UserAccesType'] ?? 0;
    address = json['Address'] ?? '';
    email = json['Email'] ?? '';
    latitude = json['Latitude'] ?? 0.0;
    longitude = json['Longitude'] ?? 0.0;
    bankVerifiedStatus = json['BankVerifiedStatus'] ?? 0;
    nationalIDVerifiedStatus = json['NationalIDVerifiedStatus'] ?? 0;
    billingAccountVerifiedStatus = json['BillingAccountVerifiedStatus'] ?? 0;
    isCustomerPrivacy = json['IsCustomerPrivacy'] ?? 0;
    profileImageId = json['ProfileImageId'] ?? 0;
    workHistory = json['WorkHistory'] ?? '';
    userTaskCategoryId = json['UserTaskCategoryId'] ?? 0;
    deviceName = json['DeviceName'] ?? '';
    skillName = json['SkillName'] ?? '';
    userTaskCategoryName = json['UserTaskCategoryName'] ?? '';
    status = json['Status'] ?? '';
    groupId = json['GroupId'] ?? 0;
    namePrefix = json['NamePrefix'] ?? '';
    middleName = json['MiddleName'] ?? '';
    areYouASmoker = json['AreYouASmoker'] ?? 'No';
    addressLine1 = json['AddressLine1'] ?? '';
    addressLine2 = json['AddressLine2'] ?? '';
    addressLine3 = json['AddressLine3'] ?? '';
    town = json['Town'] ?? '';
    county = json['County'] ?? '';
    postcode = json['Postcode'] ?? '';
    telNumber = json['TelNumber'] ?? '';
    nationalInsuranceNumber = json['NationalInsuranceNumber'] ?? '';
    nationality = json['Nationality'] ?? '';
    countryofBirth = json['CountryofBirth'] ?? '';
    countryofResidency = json['CountryofResidency'] ?? '';
    passportNumber = json['PassportNumber'] ?? '';
    maritalStatus = json['MaritalStatus'] ?? '';
    userCompanyId = json['UserCompanyId'] ?? 0;
    companyName = json['CompanyName'] ?? '';
    occupantType = json['OccupantType'] ?? '';
    livingDate = json['LivingDate'] ?? '';
    visaExpiryDate = json['VisaExpiryDate'] ?? '';
    passportExpiryDate = json['PassportExpiryDate'] ?? '';
    visaName = json['VisaName'] ?? '';
    otherVisaName = json['OtherVisaName'] ?? '';
    reportLogoUrl = json['ReportLogoUrl'] ?? '';
    userCompanyInfo = json['UserCompanyInfo'] ?? '';
    companyType = json['CompanyType'] ?? '';
    isActiveCustomerPrivacy = json['IsActiveCustomerPrivacy'] ?? '';
    isActiveCustomerAgreement = json['IsActiveCustomerAgreement'] ?? '';
    companyWebsite = json['CompanyWebsite'] ?? '';
    isCallCenterActive = json['IsCallCenterActive'] ?? '';
    companyCount = json['CompanyCount'] ?? 0;
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['FirstName'] = this.firstName;
    data['LastName'] = this.lastName;
    data['Name'] = this.name;
    data['UserName'] = this.userName;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['CoverImageUrl'] = this.coverImageUrl;
    data['ProfileUrl'] = this.profileUrl;
    data['DateCreatedUtc'] = this.dateCreatedUtc;
    data['DateCreatedLocal'] = this.dateCreatedLocal;
    data['Active'] = this.active;
    data['LastLoginDateUtc'] = this.lastLoginDateUtc;
    data['LastLoginDateLocal'] = this.lastLoginDateLocal;
    data['FollowerCount'] = this.followerCount;
    data['FollowingCount'] = this.followingCount;
    data['FriendCount'] = this.friendCount;
    data['CanFollow'] = this.canFollow;
    data['FollowStatus'] = this.followStatus;
    data['FriendStatus'] = this.friendStatus;
    data['UnreadNotificationCount'] = this.unreadNotificationCount;
    data['UnreadMessageCount'] = this.unreadMessageCount;
    data['IsOnline'] = this.isOnline;
    data['SeName'] = this.seName;
    data['StanfordWorkplaceURL'] = this.stanfordWorkplaceURL;
    data['Headline'] = this.headline;
    data['BriefBio'] = this.briefBio;
    data['ReferenceId'] = this.referenceId;
    data['ReferenceType'] = this.referenceType;
    data['Remarks'] = this.remarks;
    data['Community'] = this.community;
    data['Locations'] = this.locations;
    data['Cohort'] = this.cohort;
    data['CommunityId'] = this.communityId;
    data['IsFirstLogin'] = this.isFirstLogin;
    data['MobileNumber'] = this.mobileNumber;
    data['DateofBirth'] = this.dateofBirth;
    data['IsMobileNumberVerified'] = this.isMobileNumberVerified;
    data['IsProfileImageVerified'] = this.isProfileImageVerified;
    data['IsDateOfBirthVerified'] = this.isDateOfBirthVerified;
    data['IsElectronicIdVerified'] = this.isElectronicIdVerified;
    data['IsEmailVerified'] = this.isEmailVerified;
    data['LastLoginDate'] = this.lastLoginDate;
    data['UserProfileUrl'] = this.userProfileUrl;
    data['UserAccesType'] = this.userAccesType;
    data['Address'] = this.address;
    data['Email'] = this.email;
    data['Latitude'] = this.latitude;
    data['Longitude'] = this.longitude;
    data['BankVerifiedStatus'] = this.bankVerifiedStatus;
    data['NationalIDVerifiedStatus'] = this.nationalIDVerifiedStatus;
    data['BillingAccountVerifiedStatus'] = this.billingAccountVerifiedStatus;
    data['IsCustomerPrivacy'] = this.isCustomerPrivacy;
    data['ProfileImageId'] = this.profileImageId;
    data['WorkHistory'] = this.workHistory;
    data['UserTaskCategoryId'] = this.userTaskCategoryId;
    data['DeviceName'] = this.deviceName;
    data['SkillName'] = this.skillName;
    data['UserTaskCategoryName'] = this.userTaskCategoryName;
    data['Status'] = this.status;
    data['GroupId'] = this.groupId;
    data['NamePrefix'] = this.namePrefix;
    data['MiddleName'] = this.middleName;
    data['AreYouASmoker'] = this.areYouASmoker;
    data['AddressLine1'] = this.addressLine1;
    data['AddressLine2'] = this.addressLine2;
    data['AddressLine3'] = this.addressLine3;
    data['Town'] = this.town;
    data['County'] = this.county;
    data['Postcode'] = this.postcode;
    data['TelNumber'] = this.telNumber;
    data['NationalInsuranceNumber'] = this.nationalInsuranceNumber;
    data['Nationality'] = this.nationality;
    data['CountryofBirth'] = this.countryofBirth;
    data['CountryofResidency'] = this.countryofResidency;
    data['PassportNumber'] = this.passportNumber;
    data['MaritalStatus'] = this.maritalStatus;
    data['UserCompanyId'] = this.userCompanyId;
    data['CompanyName'] = this.companyName;
    data['OccupantType'] = this.occupantType;
    data['LivingDate'] = this.livingDate;
    data['VisaExpiryDate'] = this.visaExpiryDate;
    data['PassportExpiryDate'] = this.passportExpiryDate;
    data['VisaName'] = this.visaName;
    data['OtherVisaName'] = this.otherVisaName;
    data['ReportLogoUrl'] = this.reportLogoUrl;
    data['UserCompanyInfo'] = this.userCompanyInfo;
    data['CompanyType'] = this.companyType;
    data['IsActiveCustomerPrivacy'] = this.isActiveCustomerPrivacy;
    data['IsActiveCustomerAgreement'] = this.isActiveCustomerAgreement;
    data['CompanyWebsite'] = this.companyWebsite;
    data['IsCallCenterActive'] = this.isCallCenterActive;
    data['CompanyCount'] = this.companyCount;
    data['Id'] = this.id;
    return data;
  }
}
