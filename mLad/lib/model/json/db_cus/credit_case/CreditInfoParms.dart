class CreditInfoParms {
  AddressInfo _address;
  Device _device;
  IndividualDetails _individualDetails;
  String _riskLevel;

  AddressInfo get address => _address;
  Device get device => _device;
  IndividualDetails get individualDetails => _individualDetails;
  String get riskLevel => _riskLevel;

  CreditInfoParms({
    AddressInfo address,
      Device device, 
      IndividualDetails individualDetails, 
      String riskLevel}){
    _address = address;
    _device = device;
    _individualDetails = individualDetails;
    _riskLevel = riskLevel;
}

  CreditInfoParms.fromJson(dynamic json) {
    _address = json['Address'] != null ? AddressInfo.fromJson(json['Address']) : null;
    _device = json['Device'] != null ? Device.fromJson(json['Device']) : null;
    _individualDetails = json['IndividualDetails'] != null ? IndividualDetails.fromJson(json['IndividualDetails']) : null;
    _riskLevel = json['RiskLevel'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_address != null) {
      map['Address'] = _address.toJson();
    }
    if (_device != null) {
      map['Device'] = _device.toJson();
    }
    if (_individualDetails != null) {
      map['IndividualDetails'] = _individualDetails.toJson();
    }
    map['RiskLevel'] = _riskLevel;
    return map;
  }

}

class IndividualDetails {
  String _dateOfBirth;
  String _email;
  String _forename;
  String _middleNames;
  String _phoneNumber;
  String _surname;
  String _title;
  int _userCompanyId;
  int _userId;

  String get dateOfBirth => _dateOfBirth;
  String get email => _email;
  String get forename => _forename;
  String get middleNames => _middleNames;
  String get phoneNumber => _phoneNumber;
  String get surname => _surname;
  String get title => _title;
  int get userCompanyId => _userCompanyId;
  int get userId => _userId;

  IndividualDetails({
      String dateOfBirth, 
      String email, 
      String forename, 
      String middleNames, 
      String phoneNumber, 
      String surname, 
      String title, 
      int userCompanyId, 
      int userId}){
    _dateOfBirth = dateOfBirth;
    _email = email;
    _forename = forename;
    _middleNames = middleNames;
    _phoneNumber = phoneNumber;
    _surname = surname;
    _title = title;
    _userCompanyId = userCompanyId;
    _userId = userId;
}

  IndividualDetails.fromJson(dynamic json) {
    _dateOfBirth = json['DateOfBirth'];
    _email = json['Email'];
    _forename = json['Forename'];
    _middleNames = json['MiddleNames'];
    _phoneNumber = json['PhoneNumber'];
    _surname = json['Surname'];
    _title = json['Title'];
    _userCompanyId = json['UserCompanyId'];
    _userId = json['UserId'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['DateOfBirth'] = _dateOfBirth;
    map['Email'] = _email;
    map['Forename'] = _forename;
    map['MiddleNames'] = _middleNames;
    map['PhoneNumber'] = _phoneNumber;
    map['Surname'] = _surname;
    map['Title'] = _title;
    map['UserCompanyId'] = _userCompanyId;
    map['UserId'] = _userId;
    return map;
  }

}

class Device {
  String _blackBox;
  String _ip;

  String get blackBox => _blackBox;
  String get ip => _ip;

  Device({
      String blackBox, 
      String ip}){
    _blackBox = blackBox;
    _ip = ip;
}

  Device.fromJson(dynamic json) {
    _blackBox = json['BlackBox'];
    _ip = json['Ip'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['BlackBox'] = _blackBox;
    map['Ip'] = _ip;
    return map;
  }

}

class AddressInfo {
  String _abodeNumber;
  String _address1;
  dynamic _address2;
  dynamic _address3;
  String _postcode;
  String _town;

  String get abodeNumber => _abodeNumber;
  String get address1 => _address1;
  dynamic get address2 => _address2;
  dynamic get address3 => _address3;
  String get postcode => _postcode;
  String get town => _town;

  AddressInfo({
      String abodeNumber, 
      String address1, 
      dynamic address2, 
      dynamic address3, 
      String postcode, 
      String town}){
    _abodeNumber = abodeNumber;
    _address1 = address1;
    _address2 = address2;
    _address3 = address3;
    _postcode = postcode;
    _town = town;
}

  AddressInfo.fromJson(dynamic json) {
    _abodeNumber = json['AbodeNumber'];
    _address1 = json['Address1'];
    _address2 = json['Address2'];
    _address3 = json['Address3'];
    _postcode = json['Postcode'];
    _town = json['Town'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['AbodeNumber'] = _abodeNumber;
    map['Address1'] = _address1;
    map['Address2'] = _address2;
    map['Address3'] = _address3;
    map['Postcode'] = _postcode;
    map['Town'] = _town;
    return map;
  }

}