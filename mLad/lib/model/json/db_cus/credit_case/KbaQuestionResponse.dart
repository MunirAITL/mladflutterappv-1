class KbaQuestionResponse {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  KbaQuestionResponseData responseData;

  KbaQuestionResponse({this.success, this.errorMessages, this.messages, this.responseData});

  KbaQuestionResponse.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null ? new ErrorMessages.fromJson(json['ErrorMessages']) : null;
    messages = json['Messages'] != null ? new ErrorMessages.fromJson(json['Messages']) : null;
    responseData = json['ResponseData'] != null ? new KbaQuestionResponseData.fromJson(json['ResponseData']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {


ErrorMessages.fromJson(Map<String, dynamic> json) {
}

Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  return data;
}
}

class KbaQuestionResponseData {
  KbaQuestions kbaQuestions;

  KbaQuestionResponseData({this.kbaQuestions});

  KbaQuestionResponseData.fromJson(Map<String, dynamic> json) {
    kbaQuestions = json['KbaQuestions'] != null ? new KbaQuestions.fromJson(json['KbaQuestions']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.kbaQuestions != null) {
      data['KbaQuestions'] = this.kbaQuestions.toJson();
    }
    return data;
  }
}

class KbaQuestions {
  GetKbaQuestionsResult getKbaQuestionsResult;

  KbaQuestions({this.getKbaQuestionsResult});

  KbaQuestions.fromJson(Map<String, dynamic> json) {
    getKbaQuestionsResult = json['GetKbaQuestionsResult'] != null ? new GetKbaQuestionsResult.fromJson(json['GetKbaQuestionsResult']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.getKbaQuestionsResult != null) {
      data['GetKbaQuestionsResult'] = this.getKbaQuestionsResult.toJson();
    }
    return data;
  }
}

class GetKbaQuestionsResult {
  String status;
  Questions questions;

  GetKbaQuestionsResult({this.status, this.questions});

  GetKbaQuestionsResult.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    questions = json['Questions'] != null ? new Questions.fromJson(json['Questions']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    if (this.questions != null) {
      data['Questions'] = this.questions.toJson();
    }
    return data;
  }
}

class Questions {
  List<KbaQuestionItem> kbaQuestionItem;

  Questions({this.kbaQuestionItem});

  Questions.fromJson(Map<String, dynamic> json) {
    if (json['KbaQuestionItem'] != null) {
      kbaQuestionItem = new List<KbaQuestionItem>();
      json['KbaQuestionItem'].forEach((v) { kbaQuestionItem.add(new KbaQuestionItem.fromJson(v)); });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.kbaQuestionItem != null) {
      data['KbaQuestionItem'] = this.kbaQuestionItem.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class KbaQuestionItem {
  Answers answers;
  String id;
  String text;
  var selectedAnswer;

  KbaQuestionItem({this.answers, this.id, this.text, this.selectedAnswer});

  KbaQuestionItem.fromJson(Map<String, dynamic> json) {
    answers = json['Answers'] != null ? new Answers.fromJson(json['Answers']) : null;
    id = json['Id'];
    text = json['Text'];
    selectedAnswer = json['SelectedAnswer'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.answers != null) {
      data['Answers'] = this.answers.toJson();
    }
    data['Id'] = this.id;
    data['Text'] = this.text;
    data['SelectedAnswer'] = this.selectedAnswer;
    return data;
  }
}

class Answers {
  List<KbaAnswerOption> kbaAnswerOption;

  Answers({this.kbaAnswerOption});

  Answers.fromJson(Map<String, dynamic> json) {
    if (json['KbaAnswerOption'] != null) {
      kbaAnswerOption = new List<KbaAnswerOption>();
      json['KbaAnswerOption'].forEach((v) { kbaAnswerOption.add(new KbaAnswerOption.fromJson(v)); });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.kbaAnswerOption != null) {
      data['KbaAnswerOption'] = this.kbaAnswerOption.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class KbaAnswerOption {
  String id;
  String text;

  KbaAnswerOption({this.id, this.text});

  KbaAnswerOption.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    text = json['Text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['Text'] = this.text;
    return data;
  }
}