import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/UserData.dart';

class CaseReviewHelper {
  getUrl({pageStart = 0, pageCount = 50, status}) {
    var url = Server.CASE_REVIEW;
    url = url.replaceAll("#UserId#", userData.userModel.id.toString());
    url = url.replaceAll(
        "#UserCompanyInfoId#", userData.userModel.userCompanyID.toString());
    url = url.replaceAll("#ClientAgreementStatus#", "Sent");
    return url;
  }
}
