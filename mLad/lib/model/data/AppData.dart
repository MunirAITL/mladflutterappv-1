import 'dart:io';

import 'package:aitl_pkg/classes/Common.dart';
import 'package:flutter/material.dart';

class AppData {
  static final AppData _appData = new AppData._internal();

  String udid;
  factory AppData() {
    return _appData;
  }

  AppData._internal();

  getUDID(BuildContext context) async {
    udid = await Common.getUDID(context);
  }
}

final appData = AppData();
